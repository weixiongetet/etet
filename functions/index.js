const functions = require("firebase-functions");
const admin = require("firebase-admin");

// configs
admin.initializeApp();

// listeners
exports.shareNotify = functions.firestore
  .document(`files/{userId}`)
  .onUpdate((change, context) => {
    const newData = change.after.data();
    const prevData = change.before.data();
    const fileName = `${newData.name}.${newData.ext}`;
    const oldShareList = prevData.share;
    const newShareList = newData.share;

    let payload = {
      data: {
        title: "Share file",
        text: `You have new shared files: ${fileName}`,
        module: "vault"
      }
    };

    let tmpKeys = Object.keys(newShareList);
    let arrPromises = tmpKeys.map(async userid => {
      if (
        oldShareList[userid] !== newShareList[userid] &&
        newShareList[userid]
      ) {
        let friendSnapshot = await admin
          .firestore()
          .collection("users")
          .doc(userid)
          .get();

        let friend = friendSnapshot.data();
        if (!friend.fcmToken)
          return Promise.reject(new Error("Target no FCM token"));
        return await admin.messaging().sendToDevice(friend.fcmToken, payload);
      } else {
        return "Skip notification";
      }
    });

    return Promise.all(arrPromises)
      .then(() => {
        return console.log("Share notification sent!");
      })
      .catch(err => {
        return console.error(err);
      });
  });
exports.acceptedNotify = functions.firestore
  .document(`friends/{userId}`)
  .onUpdate(async (change, context) => {
    const { userId } = context.params;
    const newData = change.after.data();
    const prevData = change.before.data();

    let payload = {
      data: {
        title: "New friend!",
        module: "contact"
      }
    };

    let targetSnapshot = await admin
      .firestore()
      .collection("users")
      .doc(userId)
      .get();
    let target = targetSnapshot.data();
    const friendName = target.firstName;

    let tmpKeys = Object.keys(newData);
    let arrPromises = tmpKeys.map(async userid => {
      if (
        newData[userid].status === "accepted" &&
        prevData[userid].status === "pending"
      ) {
        payload.data.text = `Your friend request to ${friendName} has been accepted.`;

        try {
          let friendSnapshot = await admin
            .firestore()
            .collection("users")
            .doc(userid)
            .get();

          let friend = friendSnapshot.data();
          if (!friend.fcmToken)
            return Promise.reject(new Error("Target no FCM token"));
          console.log("target FCM:", friend.fcmToken);
          return await admin.messaging().sendToDevice(friend.fcmToken, payload);
        } catch (err) {
          return "error while retrieving";
        }
      } else {
        return "Skip notification";
      }
    });

    return Promise.all(arrPromises)
      .then(() => {
        return console.log("Friend accept notification sent!");
      })
      .catch(err => {
        return console.error(err);
      });
  });
exports.notifySample = functions.firestore
  .document("channels/{channelID}/threads/{threadID}")
  .onCreate((snap, context) => {
    const newValue = snap.data();

    let payload = {
      notification: {
        title: "Test",
        body: newValue.content,
        sound: null,
        badge: null
      }
    };

    const options = {
      priority: "high",
      timeToLive: 60 * 60 * 24, //24 hours
      content_available: true
    };

    const token =
      "ehNTnRpDjGo:APA91bG-afm_jY0s_HWjFtyokIYP2VUG8znWxxI5vCykzywQ7A-rLKnMFWQZe06MJ2ztJD3j4ZgjGObr9sFrwwZj2adteOS27E78x0fxmjUiCSo2kC800HbUAS8LxzgpHqGjz-m7ql5p";
    return admin.messaging().sendToDevice(token, payload);
  });
exports.deleteFile = functions.firestore
  .document("files/{fileId}")
  .onDelete((snap, context) => {
    const { fileId } = context.params;
    const owner = snap.data().owner;
    const bucket = admin.storage().bucket();

    return bucket.deleteFiles({
      prefix: `${owner}/${fileId}`
    });
  });

// callable functions
exports.uploadToken = functions.https.onCall(async ({ token, path }) => {
  try {
    // const uid = await Authentication.getUidFromToken(
    //   "eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyZjBiNDZjYjc1OTBjNzRmNTNhYzdhOWUwY2IxYzAzMjRlY2RkNzUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZXRldHNnLWU3ZTUyIiwiYXVkIjoiZXRldHNnLWU3ZTUyIiwiYXV0aF90aW1lIjoxNTU4Njc1NTQwLCJ1c2VyX2lkIjoiR2FLU1B1b0VPV2Mya3lNVG1IaVdWWTJtV0ZGMyIsInN1YiI6IkdhS1NQdW9FT1djMmt5TVRtSGlXVlkybVdGRjMiLCJpYXQiOjE1NTg2ODAxMDIsImV4cCI6MTU1ODY4MzcwMiwiZW1haWwiOiJsb2k5OTAxQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJsb2k5OTAxQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.a2y84z3YiDUJDlu4SIIsbSEd3NXXvvi6dTH78XHTQlTWUhN6GiJL97qBRK75rMGyXAUQBwVxYmpmnWUnyEaKsRjibaRbJ-eTuBf2u8aRQFr_3Sm7-o_YdyUFuYlm3zfdCBZULhdR2C22c6F1YU54z_jJWzNVOS_20Dv3os9ckzt9A3mhROxi2LjUkuY3izmCfmsRcWYWQk8RQi4b7kcKFas-iKwjwplkt1UWPkkn_CA9OajwTiSxb4yOhXXZMfZCccHtmsucbBDlhWHBvELsUdgb5pXBpVlaLldg-sq5CsHWsqWqIo1H06uQRJeUeq30qIwzas440c8RofB6mQYR3A"
    // );
    // const path = "test";

    const uid = await Authentication.getUidFromToken(token);
    const remainingStorage = await Vault.remainingStorage(uid);
    const metadata = {
      remainingStorage: Number(remainingStorage),
      path
    };

    const newToken = await admin.auth().createCustomToken(uid, metadata);

    return { newToken, remainingStorage, path };
  } catch (err) {
    throw new functions.https.HttpsError(
      "invalid-argument", // code
      "request upload token", // message
      { err } // details - optional and can be anything JSON serializable
    );
  }
});
exports.remainingStorage = functions.https.onCall(async ({ token }) => {
  try {
    // const uid = await Authentication.getUidFromToken(
    //   "eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyZjBiNDZjYjc1OTBjNzRmNTNhYzdhOWUwY2IxYzAzMjRlY2RkNzUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZXRldHNnLWU3ZTUyIiwiYXVkIjoiZXRldHNnLWU3ZTUyIiwiYXV0aF90aW1lIjoxNTU4NTA4MjYxLCJ1c2VyX2lkIjoiR2FLU1B1b0VPV2Mya3lNVG1IaVdWWTJtV0ZGMyIsInN1YiI6IkdhS1NQdW9FT1djMmt5TVRtSGlXVlkybVdGRjMiLCJpYXQiOjE1NTg1OTc1MzQsImV4cCI6MTU1ODYwMTEzNCwiZW1haWwiOiJsb2k5OTAxQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJsb2k5OTAxQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.OHMfgQ1cC8e9E5EjqYxAMdK12uR_xV0t26YnixLVJFWSYCTMXdxeBNd8RQIDV9OjMmreiIQOD7227U1zu7w9OWnFfsRr7H8yXoxpwZ9EhbTeqMnj8W3BhQgW7yr2-sjB3jm7jXj8Seb1LpGx0dhhW8bUTfw6TWbof2WlidUDHatUQ4-bPal_19PBFTTFcPbRzvLeshZiNYcMTDfaJtRKyqZ3Qw3iCOivQIFTeQwTr5kcsEr7xtZMsJTZdeVMog7Y9DsC1VUdCsNd19Tc9ACpiv9gYPgwQM6soRioZvnJS3Qfqco6_bg6gbIR3twSg6DHe7CV_S8Bs2Yo1FALCzTyrQ"
    // );

    const uid = await Authentication.getUidFromToken(token);

    const plan = await Profile.of(uid);
    const planLimit = Vault.planLimit(plan);
    const currentUsage = await Vault.currentUsage(uid);
    const remainingStorage = await Vault.remainingStorage(uid);
    console.log("return val", { uid, token, remainingStorage, planLimit, currentUsage });

    return { remainingStorage, planLimit, currentUsage };
  } catch (err) {
    throw new functions.https.HttpsError(
      "invalid-argument", // code
      "Check remaining storage", // message
      { err } // details - optional and can be anything JSON serializable
    );
  }
});
exports.getFriendFCM = functions.https.onCall(async ({ friendId }) => {
  try {
    let friendSnapshot = await admin
      .firestore()
      .collection("users")
      .doc(friendId)
      .get();
    let friend = friendSnapshot.data();
    return { fcmToken: friend.fcmToken };
  } catch (err) {
    throw new functions.https.HttpsError(
      "invalid-argument", // code
      "Check friend FCM token", // message
      { err } // details - optional and can be anything JSON serializable
    );
  }
});

// classes
class Authentication {
  static getUidFromToken(token) {
    return admin
      .auth()
      .verifyIdToken(token)
      .then(decodedToken => {
        return Promise.resolve(decodedToken.uid);
      });
  }
}
class Profile {
  static get none() {
    return "none";
  }

  static get free() {
    return "free";
  }

  static get premium() {
    return "premium";
  }

  static of(uid) {
    return admin
      .firestore()
      .collection("users")
      .doc(uid)
      .get()
      .then(snapshot => {
        if (!snapshot.exists) {
          return Promise.resolve(this.none);
        } else {
          const tmp = snapshot.data();
          return Promise.resolve(tmp.plan);
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
class Vault {
  static remainingStorage(uid) {
    let userPlan;
    return Profile.of(uid)
      .then(plan => {
        userPlan = plan;
        return Vault.currentUsage(uid);
      })
      .then(currentUsage => {
        const limit = Vault.planLimit(userPlan);
        return Promise.resolve(limit - currentUsage);
      })
      .catch(err => {
        throw err;
      });
  }

  static currentUsage(uid) {
    return new Promise((resolve, reject) => {
      admin
        .storage()
        .bucket()
        .getFiles({ directory: uid }, (err, files) => {
          if (err) {
            reject(err);
            return;
          }

          if (!files || !files.length) {
            resolve(0);
            return;
          }

          console.log('uid', uid, 'files', files.map(file => file.metadata.name))

          resolve(
            files
              .map(file => Number(file.metadata.size))
              .reduce((a, b) => a + b, 0)
          );
        });
    });
  }

  static planLimit(plan) {
    switch (plan) {
      case Profile.free: {
        return 1073741824;
      }
      case Profile.premium: {
        return 5368709120;
      }
      default: {
        return 0;
      }
    }
  }
}
