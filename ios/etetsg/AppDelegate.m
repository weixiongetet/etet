/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#if __has_include(<React/RCTPushNotificationManager.h>)
#import <React/RCTPushNotificationManager.h>
#elif __has_include("React/RCTPushNotificationManager.h")
#import "React/RCTPushNotificationManager.h"
#else
#import "RCTPushNotificationManager.h"
#endif

#if __has_include(<React/RCTBundleURLProvider.h>)
#import <React/RCTBundleURLProvider.h>
#elif __has_include("React/RCTBundleURLProvider.h")
#import "React/RCTBundleURLProvider.h"
#else
#import "RCTBundleURLProvider.h"
#endif

#if __has_include(<React/RCTRootView.h>)
#import <React/RCTRootView.h>
#elif __has_include("React/RCTRootView.h")
#import "React/RCTRootView.h"
#else
#import "RCTRootView.h"
#endif

#import <Firebase.h>
#import "RNFirebaseNotifications.h"
#import "RNFirebaseMessaging.h"
#import <PushKit/PushKit.h>
#import <UIKit/UIKit.h>
#import "RNNotifications.h"


@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
int a=0;
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
@implementation AppDelegate
// Required to register for notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
  [[RNFirebaseMessaging instance] didRegisterUserNotificationSettings:notificationSettings];
  [RCTPushNotificationManager didRegisterUserNotificationSettings:notificationSettings];
}
// Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  
  if(a==0){
    a=1;
    [FIRMessaging messaging].APNSToken = deviceToken;
    [RCTPushNotificationManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
  }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  NSDictionary *userInfo = notification.request.content.userInfo;
  
  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // Print full message.
  NSLog(@"%@", userInfo);
  
  // Change this to your preferred presentation option
  completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
  NSDictionary *userInfo = response.notification.request.content.userInfo;
  
  
  // Print full message.
  NSLog(@"%@", userInfo);
  
  completionHandler();
}

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
  NSLog(@"Received data message: %@", remoteMessage.appData);
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  
  // If you are receiving a notification message while your app is in the background,
  // this callback will not be fired till the user taps on the notification launching the application.
  // TODO: Handle data of notification
  
  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
  
  // Print message ID.
  //  if (userInfo[kGCMMessageIDKey]) {
  //    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  //  }
  
  // Print full message.
  NSLog(@"%@", userInfo);
}


// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [[RNFirebaseNotifications instance] didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
  [RCTPushNotificationManager didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
  completionHandler(UIBackgroundFetchResultNewData);
}

// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
  [RCTPushNotificationManager didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for the localNotification event.
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
  [[RNFirebaseNotifications instance] didReceiveLocalNotification:notification];
  [RCTPushNotificationManager didReceiveLocalNotification:notification];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
  UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
  [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
  
  
  
  [FIRApp configure];
  [FIRMessaging messaging].delegate = self;
  //[FIRMessaging mesa].delegate = self;
  //FIRMessaging.messaging().remoteMessageDelegate = self
  [UNUserNotificationCenter currentNotificationCenter].delegate = self;
  [FIRMessaging messaging].shouldEstablishDirectChannel=true;
  [RNFirebaseNotifications configure];
  [application registerForRemoteNotifications];
  NSURL *jsCodeLocation;
  
  
  [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];\
  
  
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  
  // jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"etet"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  
  
  rootView.backgroundColor = [UIColor blackColor];
  // [FIRMessaging messaging].delegate = self;
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  //  PKPushRegistry *voipRegistry = [[PKPushRegistry alloc]
  //                                  initWithQueue:dispatch_get_main_queue()];
  //  voipRegistry.delegate = self;
  //  voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
  
  
  if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) ) {
    if (@available(iOS 12.0, *)) {
      [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |  UNAuthorizationOptionProvidesAppNotificationSettings) categories:nil]];
    } else {
      // Fallback on earlier versions
    }
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
  } else {
    UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
      if( !error ) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        NSLog( @"Push registration success." );
      } else {
        NSLog( @"Push registration FAILED" );
        NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
        NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
      }
    }];
  }
  
  [application registerForRemoteNotifications];
  
  
  
  
  return YES;
}

@end
