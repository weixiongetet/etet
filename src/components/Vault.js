import React from 'react';
import { TouchableOpacity, TouchableHighlight, View, Text } from 'react-native';
import { Icon } from 'native-base';

export default class Vault extends React.PureComponent {

    deleteFile() {
        this.props.doc.ref.delete();
    }

    editFileName() {
        this.props.doc.ref.update({
            fileName: "NewFileName"
        });
    }

    render() {
        return (
          <TouchableHighlight>
              <View style={{ flex: 1, height: 55, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View>
                    <Icon active name="ios-folder" style={{ color: "#6392ff" }}/>
                </View>

                <View style={{ flex: 1, alignItems: 'flex-start', marginLeft: 10}}>
                    <Text style={{ fontSize: 15, color: "black"}}>{this.props.fileName}</Text>
                    <Text style={{ fontSize: 13, color: "gray"}}>{this.props.lastUpdated}</Text>
                </View>

                <View>
                    <TouchableOpacity onPress={() => this.deleteFile()}>
                        <Icon active name="ios-trash" style={{ color: "gray" }}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.editFileName()}>
                        <Icon active name="ios-cloud" style={{ color: "gray" }}/>
                    </TouchableOpacity>
                </View>
              </View>
          </TouchableHighlight>
        );
    }
}