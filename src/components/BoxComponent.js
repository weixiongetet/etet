
import React from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
});

export const Display = props => {
    if (props.isVisible === false) return null;
    return props.children
}