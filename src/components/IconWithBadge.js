import React from "react";
import { Platform, StyleSheet, View } from "react-native";
import { Badge } from "react-native-elements";
import AntDesign from "react-native-vector-icons/AntDesign";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Display } from "./BoxComponent";


export default class IconWithBadge extends React.Component {

  render() {
    const { badgeCount, name, size, color, onpressFunc } = this.props;
    const right = -10;

    return (

      <View style={styles.iconContainer}>
        {onpressFunc !== undefined ? (
          <MaterialIcons
            style={{ alignSelf: "center", right: 15, bottom: 4 }}
            name={name}
            size={size}
            color={color}
            onPress={() => onpressFunc()}
          />

        ) : (
            <AntDesign
              style={{ alignSelf: "center" }}
              name={name}
              size={size}
              color={color}
            />
          )}

        <Display isVisible={badgeCount > 0}>
          {onpressFunc !== undefined ? (
            <Badge
              badgeStyle={{ marginRight: 20 }}
              textStyle={styles.badgeText}
              value={badgeCount}
              status="success"
              containerStyle={[styles.badgeContainer, { right }]}
              onPress={() => onpressFunc()}
            />

          ) : (
              <Badge
                badgeStyle={styles.badge}
                textStyle={styles.badgeText}
                value={badgeCount}
                status="success"
                containerStyle={[styles.badgeContainer, { right }]}
              />
            )}

        </Display>

        <Display isVisible={badgeCount == -99}>
          <Badge
            badgeStyle={styles.badge}
            textStyle={styles.badgeText}
            value={null}
            status="success"
            containerStyle={[styles.badgeContainer, { right }]}
          />
        </Display>

      </View>
    );


    return (
      <View style={{ width: 30, height: 30 }}>
        <Ionicons
          style={{ alignSelf: "center" }}
          name={name}
          size={size}
          color={color}
          onPress={() => onpressFunc()}
        />
        <Display isVisible={badgeCount > 0}>
          <Badge
            badgeStyle={styles.badge}
            textStyle={styles.badgeText}
            value={badgeCount}
            status="success"
            containerStyle={[styles.badgeContainer, { right }]}
          />
        </Display>

        <Display isVisible={badgeCount == -99}>
          <Badge
            badgeStyle={styles.badge}
            textStyle={styles.badgeText}
            value={null}
            status="success"
            containerStyle={[styles.badgeContainer, { right }]}
          />
        </Display>

      </View>
    );
  }

}


const styles = StyleSheet.create({
  badge: {
    borderRadius: 9,
    height: 18,
    minWidth: 0,
    width: 18,
  },
  badgeContainer: {
    position: "absolute",
  },
  badgeText: {
    fontSize: 10,
    paddingHorizontal: 0,
  },
  iconContainer: {
    width: 30,
    height: (Platform.OS === "ios") ? 30 : 20
  }

});
