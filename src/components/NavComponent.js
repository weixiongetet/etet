import { Button, Icon, View } from "native-base";
import React from "react";
import { Platform, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";


const styles = StyleSheet.create({
  navigationButton: { alignSelf: "center" },
  libIcon:
    Platform.OS === "ios"
      ? {
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 5,
        fontSize: 20
      }
      : { padding: 15, fontSize: 20 },
  libIcon2:
    Platform.OS === "ios"
      ? { paddingRight: 12, paddingTop: 5 }
      : { padding: 15 },

  libIcon3: { fontSize: 30 },
  libIcon4:
    Platform.OS === "ios"
      ? { paddingRight: 5, paddingTop: 5, paddingLeft: 5 }
      : { padding: 10 },
  libIcon5:
    Platform.OS === "ios"
      ? { paddingRight: 12, paddingTop: 5 }
      : { padding: 0 },
  libIcon6:
    Platform.OS === "ios"
      ? { paddingRight: 12, paddingTop: 5, fontSize: 25 }
      : { fontSize: 25 },
  libIcon7: { fontSize: 28 },
  iconSize:
    Platform.OS === "ios"
      ? { fontSize: 23, paddingBottom: 3 }
      : { fontSize: 25 },
  navigationIcon: { color: "#616161" },
  navIconDanger: { color: "#ff7043" },
  navIconDisable: { color: "#bdbdbd" },
  navigationBar: { flex: 1, flexDirection: "row" }
});

export const NavButton = props => {
  if (props.isVisible === false) return null;

  let IconType = Icon;
  let arrStyle = [styles.navigationIcon];

  switch (props.type) {
    case "fontawesome":
      IconType = FontAwesome;
      arrStyle.push(styles.libIcon);
      break;
    case "materialIcons":
      IconType = MaterialIcons;
      arrStyle.push(styles.libIcon2);
      arrStyle.push(styles.libIcon3);
      break;
    case "feather":
      IconType = Feather;
      arrStyle.push(styles.libIcon6);
      break;
    case "materialCommunityIcons":
      IconType = MaterialCommunityIcons;
      arrStyle.push(styles.libIcon2);
      arrStyle.push(styles.libIcon7);
      break;
    case "materialIconsBottomSide":
      IconType = MaterialIcons;
      arrStyle.push(styles.libIcon4);
      arrStyle.push(styles.libIcon3);
      break;
    case "AntDesign":
      IconType = AntDesign;
      arrStyle.push(styles.libIcon2);
      arrStyle.push(styles.libIcon7);
      break;


    default:
      break;
  }

  // if (props.iconName == "camera") {
  //   arrStyle.push(styles.libIcon5);
  //   arrStyle.push(styles.libIcon3);
  // }

  if (props.btnType == "file") {
    arrStyle.push(styles.iconSize);
  }

  if (props.danger) arrStyle.push(styles.navIconDanger);
  if (props.disable) arrStyle.push(styles.navIconDisable);

  return (
    <TouchableOpacity onPress={props.onPress}>
      <Button style={styles.navigationButton} transparent>
        <IconType style={arrStyle} name={props.name} />
      </Button>
    </TouchableOpacity>
  );
};

export const NavSearch = props => {
  return (
    <NavButton {...props} name={props.isSearching ? "md-close" : "md-search"} />
  );
};

export const NavBar = props => {
  return <View style={styles.navigationBar}>{props.children}</View>;
};
