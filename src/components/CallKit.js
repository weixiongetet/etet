import { Button, Text } from "native-base";
import React, { Component } from "react";
import { View } from "react-native";
import { Stopwatch } from "react-native-stopwatch-timer";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import { updIncomingFlag, updShowTimerFlag } from "../redux/modules/contacts";

const stopWatchoptions = {
  text: {
    fontSize: 25,
    color: "#000",
    marginLeft: 7
  }
};

class CallKit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTimerStart: false,
      isStopwatchStart: true,
      timerDuration: 90000,
      resetTimer: false,
      resetStopwatch: false
    };

    this.startStopTimer = this.startStopTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.startStopStopWatch = this.startStopStopWatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  startStopTimer() {
    this.setState({
      isTimerStart: !this.state.isTimerStart,
      resetTimer: false
    });
  }
  resetTimer() {
    this.setState({ isTimerStart: false, resetTimer: true });
  }
  startStopStopWatch() {
    this.setState({
      isStopwatchStart: !this.state.isStopwatchStart,
      resetStopwatch: false
    });
  }
  resetStopwatch() {
    this.setState({ isStopwatchStart: false, resetStopwatch: true });
  }
  getFormattedTime(time) {
    this.currentTime = time;
  }

  render() {
    const callFrom = this.props.callFrom;
    const type = this.props.type;

    return (
      <View>
        {type == "incoming" ? ( //is friends, call app to app
          <View>
            <Text>Call from {callFrom}</Text>
            <View style={{ flexDirection: "row", alignSelf: "center" }}>
              <Button
                info
                style={{
                  marginRight: 10,
                  marginTop: 10,
                  backgroundColor: "#32CD32",
                  padding: 13,
                  alignSelf: "center",
                  height: 50,
                  width: 50,
                  borderRadius: 25
                }}
                onPress={() => {
                  this.acceptCall();
                  this.props.updShowTimerFlag(true);
                  this.props.updIncomingFlag(false);
                }}
              >
                <IconAntDesign
                  name="phone"
                  size={20}
                  style={{ color: "#FFF" }}
                />
                <Text>Answer</Text>
              </Button>

              <Button
                info
                style={{
                  marginLeft: 10,
                  marginTop: 10,
                  backgroundColor: "#FF0000",
                  padding: 13,
                  alignSelf: "center",
                  height: 50,
                  width: 50,
                  borderRadius: 25
                }}
                onPress={() => {
                  this.rejectCall();
                  this.props.updIncomingFlag(false);
                }}
              >
                <MaterialIcons
                  name="call-end"
                  size={25}
                  style={{ color: "#FFF" }}
                />
                <Text>Reject</Text>
              </Button>
            </View>
          </View>
        ) : (
          <View>
            <Text>Calling from {callFrom}</Text>
            <View style={{ alignSelf: "center" }}>
              <Stopwatch
                laps
                secs
                start={this.state.isStopwatchStart}
                //To start
                reset={this.state.resetStopwatch}
                //To reset
                options={stopWatchoptions}
                //options for the styling
                getTime={this.getFormattedTime}
              />

              <Button
                info
                style={{
                  marginLeft: 10,
                  marginTop: 10,
                  backgroundColor: "#FF0000",
                  padding: 13,
                  alignSelf: "center",
                  height: 50,
                  width: 50,
                  borderRadius: 25
                }}
                onPress={() => {
                  this.disconnect();
                  this.props.updShowTimerFlag(false);
                }}
              >
                <MaterialIcons
                  name="call-end"
                  size={25}
                  style={{ color: "#FFF" }}
                />
                <Text>Reject</Text>
              </Button>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    callFrom: state.contacts.callFrom
  }),
  { updIncomingFlag, updShowTimerFlag }
)(CallKit);
