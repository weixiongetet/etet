import React from "react";
import { StyleSheet } from "react-native";
import { KeyboardAvoidingView, View } from "react-native";

const Container = ({ children }) => {
  return (
    <KeyboardAvoidingView enabled>
      <View style={styles.main}>{children}</View>
    </KeyboardAvoidingView>
  );
};

const Section = ({ children }) => {
  return <View style={styles.main}>{children}</View>;
};

const styles = StyleSheet.create({
  main: {
    flexGrow: 1,
    padding: 20
  },
  section: {
    marginTop: 15,
    marginBottom: 18
  }
});

export { Container, Section };
