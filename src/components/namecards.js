import React, { Component } from "react";
import { Animated, Dimensions, Image, PixelRatio, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CardFlip from "react-native-card-flip";
import LinearGradient from "react-native-linear-gradient";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import IconEvilIcons from "react-native-vector-icons/EvilIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import { setCardsShareFlag } from "../redux/modules/setting";

const SCREEN_WIDTH = Dimensions.get("window").width;
const assetPath = "../assets";
const xOffset = new Animated.Value(0);
const platform = Platform.OS;

const Screen = props => {
  if (!props.user) return null;

  const userImg = {
    uri: props.user.profilePictureURL,
    width: PixelRatio.getPixelSizeForLayoutSize(300),
    height: PixelRatio.getPixelSizeForLayoutSize(200)
  };
  const image2 = {
    uri: props.img2,
    width: PixelRatio.getPixelSizeForLayoutSize(300),
    height: PixelRatio.getPixelSizeForLayoutSize(200)
  };

  return (
    <Animated.View style={[styles.screen, transitionAnimation(props.index)]}>
      {/* <CardFlip
          style={styles.cardContainer}
          ref={card => (this["card" + props.index] = card)}
        > */}
      <TouchableOpacity
        onLongPress={() => alert(JSON.parse(JSON.stringify(props.text)))}
        activeOpacity={1}
        style={[styles.card, styles.card1]}
      // onPress={() => this["card" + props.index].flip()}
      >
        <LinearGradient
          colors={["#39A3FF", "#6552FF"]}
          style={{
            flex: 1,
            borderRadius: 10
          }}
        >
          <View
            style={
              props.isFriends
                ? {
                  top: 60,
                  justifyContent: "center",
                  alignItems: "center"
                }
                : {
                  position: "absolute",
                  top: 14,
                  left: 13
                }
            }
          >
            {props.isFriends == true ? null : (
              <Image
                source={userImg}
                style={{
                  borderRadius: 80 / 2,
                  borderWidth: 2,
                  borderColor: "#FFF",
                  backgroundColor: "#FFF",
                  width: 80,
                  height: 80
                }}
              />
            )}

            <View
              style={
                props.isFriends
                  ? {
                    width: 250,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                  : {
                    width: 250
                  }
              }
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                style={{
                  fontSize: 25,
                  fontWeight: "bold",
                  color: "#fff",
                  textTransform: "capitalize",
                  fontFamily:
                    platform === "ios" ? "OpenSans-Regular" : "opensans"

                  // top: 8
                }}
              >
                {props.user.firstName} {props.user.lastName}
              </Text>
            </View>

            {props.user.position ? (
              <MaterialIcons
                name="next-week"
                style={{
                  fontSize: 14,
                  top: 3,
                  color: "#FFF",
                  fontWeight: "bold"
                }}
              >
                <Text style={styles.cardText}> : {props.user.position} </Text>
              </MaterialIcons>
            ) : null}

            <IconAntDesign
              name="phone"
              style={{
                fontSize: 14,
                top: 5,
                color: "#FFF",
                fontWeight: "bold"
              }}
            >
              {props.user.phone ? (
                <Text style={styles.cardText}> : {props.user.phone} </Text>
              ) : (
                  <Text style={styles.cardText}> : No Contact </Text>
                )}
            </IconAntDesign>

            {props.user.email ? (
              <IconAntDesign
                name="mail"
                style={{
                  fontSize: 14,
                  top: 5,
                  color: "#FFF",
                  fontWeight: "bold"
                }}
              >
                <Text style={styles.cardText}> : {props.user.email} </Text>
              </IconAntDesign>
            ) : null}
          </View>
        </LinearGradient>
      </TouchableOpacity>

      {/* <TouchableOpacity
            onLongPress={() =>
              alert(JSON.parse(JSON.stringify(props.user.phone)))
            }
            activeOpacity={1}
            style={[styles.card, styles.card2]}
            onPress={() => this["card" + props.index].flip()}
          >

            <LinearGradient
              colors={['#39A3FF', '#6552FF']}
              style={{ flex: 1, borderRadius: 10 }}
            >

              {props.user.address ? (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                  <FontAwesome name="map-marked-alt" style={{ fontSize: 20, color: "#FFF" }} />
                  <Text style={{
                    color: "#FFF",
                    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans",
                  }}>
                    {props.user.address}
                  </Text>
                </View>
              ) :
                null
              }

            </LinearGradient>
          </TouchableOpacity> */}
      {/* </CardFlip> */}

      {props.isFriends == true ? null : ( //is friends
        <IconEvilIcons
          name="share-google"
          size={30}
          style={{ paddingTop: 10 }}
          onPress={props.onPress}
        />
      )}
    </Animated.View>
  );
};

const transitionAnimation = index => {
  return {
    transform: [
      { perspective: 800 },
      {
        scale: xOffset.interpolate({
          inputRange: [
            (index - 2) * SCREEN_WIDTH,
            index * SCREEN_WIDTH,
            (index + 2) * SCREEN_WIDTH
          ],
          outputRange: [0.1, 1, 0.1]
        })
      },
      {
        rotateX: xOffset.interpolate({
          inputRange: [
            (index - 1) * SCREEN_WIDTH,
            index * SCREEN_WIDTH,
            (index + 1) * SCREEN_WIDTH
          ],
          outputRange: ["0deg", "0deg", "0deg"]
        })
      },
      {
        rotateY: xOffset.interpolate({
          inputRange: [
            (index - 1) * SCREEN_WIDTH,
            index * SCREEN_WIDTH,
            (index + 1) * SCREEN_WIDTH
          ],
          outputRange: ["0deg", "0deg", "0deg"]
        })
      }
    ]
  };
};

class Namecards extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { navigation } = this.props;
    return (
      <View style={{ padding: 2, paddingTop: 20 }}>
        {this.props.nameCards.map((item, index) => (
          <Screen
            user={item.user}
            key={index}
            text={item.text}
            index={index}
            img2={item.url2}
            subText={item.subText}
            onPress={() => {
              this.props.setCardsShareFlag(true);
            }}
            isFriends={item.isFriends}
          />
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flexDirection: "row",
    backgroundColor: "#FFF"
  },
  scrollPage: {
    width: SCREEN_WIDTH,
    padding: 10
  },
  iconBottom: {
    alignItems: "center",
    padding: 0
  },
  screen: {
    justifyContent: "center",
    alignItems: "center"
  },
  cardContainer: {
    width: 300,
    height: 200,
    justifyContent: "center",
    alignItems: "center"
  },
  card: {
    width: 300,
    height: 200,
    backgroundColor: "#FE474C",
    shadowColor: "rgba(0,0,0,0.5)",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.5,
    elevation: 10,
    borderRadius: 10
  },
  card1: {
    backgroundColor: "#E6E6FA"
    // backgroundImage:"url('https://mdbootstrap.com/img/Photos/Others/photo7.jpg')"
  },
  card2: {
    backgroundColor: "#E6E6FA"
  },
  cardText: {
    color: "#FFF",
    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
  }
});

CardFlip.defaultProps = {
  style: {},
  duration: 500,
  flipZoom: -0.4,
  flipDirection: "y",
  perspective: 800,
  onFlip: () => { },
  onFlipStart: () => { },
  onFlipEnd: () => { }
};

export default connect(
  undefined,
  { setCardsShareFlag }
)(Namecards);
