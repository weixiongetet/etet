import React from "react";
import { StyleSheet, View } from "react-native";
import { Item } from "native-base";
import PhoneInput from "react-native-phone-input";
import CountryPicker from "react-native-country-picker-modal";
import MarkPass from "./MarkPass";

class PhoneField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cca2: "US"
    };
  }

  openCountryModal = () => {
    this.countryPicker.openModal();
  };

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase());
    this.setState({
      cca2: country.cca2
    });
  }

  render() {
    const { onChange, initialCountry, countryList, isValid } = this.props;

    return (
      <View style={styles.phoneView}>
        <View style={{ width: "90%", height: 30 }}>
          <PhoneInput
            ref={ref => {
              this.phone = ref;
            }}
            initialCountry={initialCountry}
            onPressFlag={this.openCountryModal}
            onChangePhoneNumber={number => {
              onChange({
                number,
                valid: this.phone.isValidNumber()
              });
            }}
            offset={25}
          />
        </View>

        <View style={{ width: "10%", alignItems: "flex-end" }}>
          <Item style={{ borderColor: "#ffffff" }}>
            {isValid ? <MarkPass /> : null}
          </Item>
        </View>

        <CountryPicker
          ref={ref => {
            this.countryPicker = ref;
          }}
          onChange={value => this.selectCountry(value)}
          translation="eng"
          hideAlphabetFilter={true}
          cca2={this.state.cca2}
          // firebase support
          // countryList={["AD","AE","AF","AG","AL","AM","AO","AR","AS","AT","AU","AW","BA","BB","BD","BE","BF","BG","BJ","BM","BN","BO","BR","BS","BT","BW","BY","BZ","CA","CD","CF","CG","CH","CI","CK","CL","CM","CO","CR","CV","CW","CY","CZ","DE","DJ","DK","DM","DO","DZ","EC","EG","ES","ET","FI","FJ","FK","FM","FO","FR","GA","GB","GD","GE","GF","GG","GH","GI","GL","GM","GP","GQ","GR","GT","GY","HK","HN","HR","HT","HU","ID","IE","IL","IM","IN","IQ","IT","JE","JM","JO","JP","KE","KG","KH","KM","KN","KR","KW","KY","KZ","LA","LB","LC","LI","LK","LS","LT","LU","LV","LY","MA","MD","ME","MF","MG","MK","MM","MN","MO","MS","MT","MU","MW","MX","MY","MZ","NA","NC","NE","NF","NG","NI","NL","NO","NP","NZ","OM","PA","PE","PG","PH","PK","PL","PM","PR","PS","PT","PY","QA","RE","RO","RS","RU","RW","SA","SC","SD","SE","SG","SH","SI","SK","SL","SN","SR","ST","SV","SY","SZ","TC","TG","TH","TL","TM","TO","TR","TT","TW","TZ","UA","UG","US","UY","UZ","VC","VE","VG","VI","VN","WS","YE","YT","ZA","ZM","ZW"]}
          // countryList={['SG']}
          // firebase and component both supported
          countryList={countryList}
        >
          <View />
        </CountryPicker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  phoneView: {
    flex: 1,
    flexDirection: "row",
    padding: 0,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 5,
    marginBottom: 20
  }
});

export default PhoneField;
