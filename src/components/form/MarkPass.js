import React from "react";
import { StyleSheet } from "react-native";
import { Icon } from "native-base";

const MarkPass = () => {
  return <Icon active name="md-checkmark" style={styles.icon} />;
};

const styles = StyleSheet.create({
  icon: {
    borderColor: "#a5d6a7",
    color: "#43a047",
    textAlign: "center"
  }
});

export default MarkPass;
