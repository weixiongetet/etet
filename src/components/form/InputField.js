import React from "react";
import { StyleSheet } from "react-native";
import { Item, Icon } from "native-base";
import MarkPass from "./MarkPass";

const InputField = ({ icon, hasError, children, skipValidate }) => {
  return (
    <Item style={styles.border}>
      <Icon active name={icon} style={styles.icon} />
      {children}
      {!hasError && !skipValidate ? <MarkPass /> : null}
    </Item>
  );
};

const styles = StyleSheet.create({
  border: {
    borderColor: "#d9d9d9",
    color: "#757575"
  },
  icon: {
    borderColor: "#d9d9d9",
    color: "#757575",
    width: 30,
    textAlign: "center"
  }
});

export default InputField;
