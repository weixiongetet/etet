import React, { Component } from 'react';
import {View, Dimensions, StyleSheet, Text} from 'react-native';
import { withNavigation } from 'react-navigation';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux';



class VaultUsage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            total: 0,
          };
        
        this.ref = firebase.firestore().collection('vault').where('userID', '==', this.props.user.userID)
        
        this.unsubscribe = null;
      }
    
      componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
      }
    
      componentWillUnmount() {
        this.unsubscribe();
      }

      onCollectionUpdate = (querySnapshot) => {
        this.setState({ total: 0 })
        querySnapshot.forEach((doc) => {
          const { fileSize, fileType } = doc.data();
          if(fileType != "?#$%FOLDER?#$%"){
          this.setState({ total: this.state.total + fileSize })
        }
        });
      }
  
      render() {
        const barWidth = Dimensions.get('screen').width - 40;

        const limitFormatted = this.props.user.vaultLimit/1000000000;
        const usedLimitPercentage = (100/this.props.user.vaultLimit)*this.state.total;
        return (
            
            <View>
              
              <ProgressBarAnimated
                width={barWidth}
                value={usedLimitPercentage}
                backgroundColorOnComplete="#6CC644"
              />
              <Text style={styles.label}>{usedLimitPercentage.toFixed(2)}% of {limitFormatted} GB used</Text>
              <View style={styles.separator} />
            </View>
             
        
          
        );
      }
    }
     
    const styles = StyleSheet.create({
 
      label: {
        color: '#999',
        fontSize: 14,
        fontWeight: '500',
        marginTop: 10,
      },
      separator: {
        marginVertical: 10,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
      },
    });

    const mapStateToProps = state => ({
        user: state.auth.user,
        nav: state.nav
      });
      
      export default connect(mapStateToProps, undefined)(VaultUsage)


// export default withNavigation(VaultUsage);