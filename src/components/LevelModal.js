import React from "react";
import { Modal } from "react-native";
import { View } from "native-base";

const LevelModal = ({ children, ...rest }) => {
  return (
    <Modal animationType="fade" transparent {...rest}>
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#00000080"
        }}
      >
        <View
          style={{
            width: 300,
            height: "auto",
            backgroundColor: "#fff",
            padding: 20,
            borderRadius: 3
          }}
        >
          {children}
        </View>
      </View>
    </Modal>
  );
};

export default LevelModal;
