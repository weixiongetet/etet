/*
 * Copyright (c) 2011-2018, Zingaya, Inc. All rights reserved.
 */

"use strict";

import firebase from "react-native-firebase";
import LoginManager from "./LoginManager";

class PushManager {
  pushToken = null;

  constructor() {}

  init() {
    try {
      firebase.messaging().onTokenRefresh(token => {
        console.log("Refresh token: " + token);
      });
      firebase.messaging().onMessage(async message => {
        LoginManager.getInstance().pushNotificationReceived(message.data);
      });

      firebase
        .messaging()
        .getToken()
        .then(token => {
          this.pushToken = token;
        })
        .catch(() => {
          console.warn("PushManager android: failed to get FCM token");
        });

      const channel = new firebase.notifications.Android.Channel(
        "voximplant_channel_id",
        "Incoming call channel",
        firebase.notifications.Android.Importance.Max
      ).setDescription("Incoming call received");
      firebase.notifications().android.createChannel(channel);
    } catch (e) {
      console.warn(
        "React Native Firebase is not set up. Enable google-services plugin at the bottom of the build.gradle file"
      );
    }
  }

  getPushToken() {
    return this.pushToken;
  }

  showLocalNotification(from) {
    console.log("getshowpushnotification", from);

    try {
      //     let voxMessage = JSON.parse(message.data.voximplant);
      // console.log("secondd call voxMessage", voxMessage);
      //LoginManager.getInstance().loginWithToken();

      const localNotification = new firebase.notifications.Notification({
        // show_in_foreground: true
      }).android
        .setChannelId("voximplant_channel_id")
        .android.setPriority(firebase.notifications.Android.Priority.High)
        .setNotificationId(message.messageId)
        .setTitle("Incoming call from " + voxMessage.display_name)
        .setBody("Bodyy")
        .setData(message.data);

      console.log("pushoooo", localNotification);

      firebase.notifications().displayNotification(localNotification);

      // const notification = new firebase.notifications.Notification()
      //   .setNotificationId("notificationId")
      //   .setTitle("Incoming call");
      // notification.android.setSmallIcon("ic_vox_notification");
      // notification.android.setChannelId("voximplant_channel_id");

      // console.log("notification8888888", from);

      // firebase.notifications().displayNotification(notification);
    } catch (e) {
      console.warn(
        "React Native Firebase is not set up. Enable google-services plugin at the bottom of the build.gradle file"
      );
    }
  }

  removeDeliveredNotification() {
    try {
      firebase.notifications().removeAllDeliveredNotifications();
    } catch (e) {
      console.warn(
        "React Native Firebase is not set up. Enable google-services plugin at the bottom of the build.gradle file"
      );
    }
  }
}

const pushManager = new PushManager();
export default pushManager;
