/*
 * Copyright (c) 2011-2018, Zingaya, Inc. All rights reserved.
 */

"use strict";

import AsyncStorage from "@react-native-community/async-storage";
import { Voximplant } from "react-native-voximplant";
import { connect } from "react-redux";
import CallManager from "./CallManager";
import PushManager from "./PushManager";


const handlersGlobal = {};

class LoginManager {
  static myInstance = null;
  client = null;
  displayName = "";
  fullUserName = "";
  myuser = "";
  username = "";
  password = "";

  static getInstance() {
    if (this.myInstance === null) {
      this.myInstance = new LoginManager();
    }
    return this.myInstance;
  }

  constructor() {

    this.client = Voximplant.getInstance();

    // Connection to the Voximplant Cloud is stayed alive on reloading of the app's
    // JavaScript code. Calling "disconnect" API here makes the SDK and app states
    // synchronized.
    PushManager.init();
    // (async () => {
    //   try {
    //     this.client.disconnect();
    //   } catch (e) {}
    // })();
    // this.client.on(
    //   Voximplant.ClientEvents.ConnectionClosed,
    //   this._connectionClosed
    // );
  }

  async loginWithPassword(user, password) {
    //  PushManager.init();

    this.username = user;
    this.password = password;
    try {
      let state = await this.client.getClientState();

      if (state === Voximplant.ClientState.DISCONNECTED) {
        await this.client.connect();
      }
      let authResult = await this.client.login(user, password);

      await this._processLoginSuccess(authResult);
      await AsyncStorage.setItem("usernameValue", user);
    } catch (e) {
      console.log("LoginManager: loginWithPassword " + e.name + e.message);
      switch (e.name) {
        case Voximplant.ClientEvents.ConnectionFailed:
          this._emit("onConnectionFailed", e.message);
          break;
        case Voximplant.ClientEvents.AuthResult:
          this._emit("onLoginFailed", e.code);
          break;
      }
    }
  }

  async loginWithToken() {

    try {
      let state = await this.client.getClientState();
      if (state === Voximplant.ClientState.DISCONNECTED) {
        await this.client.connect();
      }

      if (state !== Voximplant.ClientState.LOGGED_IN) {

        const username = await AsyncStorage.getItem("usernameValue");
        const accessToken = await AsyncStorage.getItem("accessToken");

        console.log(
          "LoginManager: loginWithToken: user: " +
          username +
          ", token: " +
          accessToken
        );

        if (username !== null && accessToken !== null) {
          const authResult = await this.client.loginWithToken(
            username,
            accessToken
          );
          await this._processLoginSuccess(authResult);
        }
      }
    } catch (e) {
      console.log("LoginManager: loginWithToken: " + e.name);
      if (e.name === Voximplant.ClientEvents.AuthResult) {
        console.log("LoginManager: loginWithToken: error code: " + e.code);
      }
    }
  }

  async logout() {
    this.unregisterPushToken();
    await this.client.disconnect();
    this._emit("onConnectionClosed");
  }

  registerPushToken() {
    console.log("gettttoken", PushManager.getPushToken());
    this.client.registerPushNotificationsToken(PushManager.getPushToken());
  }

  unregisterPushToken() {
    console.log("unregistertoken", PushManager.getPushToken())
    this.client.unregisterPushNotificationsToken(PushManager.getPushToken());
  }

  pushNotificationReceived(notification) {
    (async () => {
      console.log("pushreceivedd", notification.voximplant.userid);

      const username = AsyncStorage.getItem("usernameValue");
      const accessToken = AsyncStorage.getItem("accessToken");

      if (username !== null && accessToken !== null) {
        //login voximplant with token
        this.loginWithToken();
      } else {
        //login voximplant with password
        if (notification.voximplant.userid) {
          this.loginWithPassword(
            "12345" + notification.voximplant.userid + "@etetconf.etetsg.voximplant.com",
            notification.voximplant.userid
          );
        }
      }



      // this.loginWithToken();
      this.client.handlePushNotification(notification);
    })();
  }

  on(event, handler) {
    if (!handlersGlobal[event]) {
      handlersGlobal[event] = [];
    }
    handlersGlobal[event].push(handler);
  }

  off(event, handler) {
    if (handlersGlobal[event]) {
      handlersGlobal[event] = handlersGlobal[event].filter(v => v !== handler);
    }
  }

  _emit(event, ...args) {
    const handlers = handlersGlobal[event];
    if (handlers) {
      for (const handler of handlers) {
        handler(...args);
      }
    }
  }

  _connectionClosed = () => {
    this._emit("onConnectionClosed");
  };

  async _processLoginSuccess(authResult) {
    this.displayName = authResult.displayName;

    // save acceess and refresh token to default preferences to login using
    // access token on push notification, if the connection to Voximplant Cloud
    // is closed
    const loginTokens = authResult.tokens;
    console.log("loginsuccesss", loginTokens)


    if (loginTokens !== null) {
      this.registerPushToken();
      CallManager.getInstance().init();
      this._emit("onLoggedIn", authResult.displayName);


      AsyncStorage.setItem("accessToken", loginTokens.accessToken);
      AsyncStorage.setItem("refreshToken", loginTokens.refreshToken);
      AsyncStorage.setItem(
        "accessExpire",
        loginTokens.accessExpire.toString()
      );
      AsyncStorage.setItem(
        "refreshExpire",
        loginTokens.refreshExpire.toString()
      );
    } else {
      console.error("LoginSuccessful: login tokens are invalid");
    }
    // this.registerPushToken();
    // CallManager.getInstance().init();
    // this._emit("onLoggedIn", authResult.displayName);
  }
}


const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(
  mapStateToProps,
  {
  }
)(LoginManager);


const loginManagerGlobal = LoginManager.getInstance();
