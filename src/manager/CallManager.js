"use strict";
import { Component } from "react";
import { AppState, Platform, PushNotificationIOS } from "react-native";
import { Voximplant } from "react-native-voximplant";
import SendBird from "sendbird";
import NavigationService from "../routes/NavigationService";
import CallKitManager from "./CallKitManager";

// import PushManager from "./PushManager";

// Voximplant SDK supports multiple calls at the same time, however
// this demo app demonstrates only one active call at the moment,
// so it rejects new incoming call if there is already a call.
export default class CallManager extends Component {
  static myInstance = null;
  call = null;
  currentAppState = undefined;
  showIncomingCallScreen = false;
  callKitManager = null;

  constructor(props) {
    super(props);
    this.client = Voximplant.getInstance();
    this.currentAppState = AppState.currentState;
    this.callKitManager = new CallKitManager();
  }

  init() {
    this.client.on(Voximplant.ClientEvents.IncomingCall, this._incomingCall);
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  static getInstance() {
    if (this.myInstance === null) {
      this.myInstance = new CallManager();
    }
    return this.myInstance;
  }

  addCall(call) {
    this.call = call;
  }

  removeCall(call) {
    console.log("CallManager: removeCall: ", call);

    if (this.call && this.call.callId === call.callId) {
      console.log("CallManager: removeCall0000000");

      this.call.off(Voximplant.CallEvents.Connected, this._callConnected);
      this.call.off(Voximplant.CallEvents.Disconnected, this._callDisconnected);
      this.call = null;
      console.log("CallManager: removeCall11111 ");

      if (Platform.OS === "ios" && parseInt(Platform.Version, 10) >= 10) {
        console.log("CallManager: removeCall222222 ");

        this.callKitManager.endCall();

        // AsyncStorage.getItem('useCallKit')
        //     .then((value) => {
        //         const useCallKit = JSON.parse(value);
        //         if (useCallKit) {
        //             this.callKitManager.endCall();
        //         }
        //     });
      }
    } else if (this.call) {
      console.warn("CallManager: removeCall: call id mismatch");
    }

    // NavigationService.navigate("HomePage", {
    // });
  }

  endCall() {
    console.log("encallkit11111");

    if (this.call !== null && this.call !== undefined) {
      console.log("endcallkitcall");
      this.call.hangup();
      // NavigationService.navigate("Chat");
    }

    // NavigationService.goBack();
  }

  getCallById(callId) {
    if (callId === this.call.callId) {
      return this.call;
    }
    return null;
  }

  startOutgoingCallViaCallKit(isVideo, number) {
    this.callKitManager.startOutgoingCall(isVideo, number, this.call.callId);
    this.call.on(Voximplant.CallEvents.Connected, this._callConnected);
    this.call.on(Voximplant.CallEvents.Disconnected, this._callDisconnected);
  }

  //custom callkit
  _showIncomingScreenOrNotification(event) {
    if (this.currentAppState !== "active") {
      this.showIncomingCallScreen = true;
    } else {
      NavigationService.navigate("IncomingCall", {
        callId: event.call.callId,
        isVideo: event.video,
        from: null
      });
    }
  }

  _incomingCall = event => {
    if (this.call !== null) {
      console.log(
        `CallManager: incomingCall: already have a call, rejecting new call, current call id: ${this.call.callId}`
      );
      event.call.decline();
      return;
    }

    this.addCall(event.call);
    this.call.on(Voximplant.CallEvents.Disconnected, this._callDisconnected);

    //ios callkit
    if (Platform.OS === "ios" && parseInt(Platform.Version, 10) >= 10) {
      this.callKitManager.showIncomingCall(
        event.video,
        event.call.getEndpoints()[0].displayName,
        event.call.callId
      );

      // AsyncStorage.getItem('useCallKit')
      //     .then((value) => {
      //         const useCallKit = JSON.parse(value);
      //         if (useCallKit) {
      //             console.log('CallManager: incomingCall: CallKit is selected as incoming call screen');
      //             this.callKitManager.showIncomingCall(event.video, event.call.getEndpoints()[0].displayName, event.call.callId);
      //         } else {
      //             this._showIncomingScreenOrNotification(event);
      //         }
      //     });
    } else {
      //android & ios<10 callkit
      this._showIncomingScreenOrNotification(event);
    }
  };

  _callConnected = event => {
    this.call.off(Voximplant.CallEvents.Connected, this._callConnected);
    this.callKitManager.reportOutgoingCallConnected();
  };

  _callDisconnected = event => {
    console.log("calldisconnect", event);
    this.showIncomingCallScreen = false;
    this.removeCall(event.call);
  };

  _handleAppStateChange = newState => {
    this.currentAppState = newState;

    try {
      const sb = SendBird.getInstance();
      console.log("SBb", sb);
      if (sb) {
        if (this.currentAppState === "active") {
          console.log("acttiveesbbbbb")


          if (Platform.OS === "ios") {
            PushNotificationIOS.setApplicationIconBadgeNumber(0);
          }
          sb.setForegroundState();
        } else if (this.currentAppState === "background") {
          console.log("backgroundsbbbbbbbb")
          sb.setBackgroundState();
        }
      }
    } catch (error) {
      console.log("Errror", error);
    }

    if (
      this.currentAppState === "active" &&
      this.showIncomingCallScreen &&
      this.call !== null
    ) {
      this.showIncomingCallScreen = false;
      // if (Platform.OS === "android") {
      //   // PushManager.removeDeliveredNotification();
      // }
      // //android callkit
      NavigationService.navigate("IncomingCall", {
        callId: this.call.callId,
        isVideo: null,
        from: null
      });
    } else {
      console.log(`CallManager:cuurentstate ${newState}`);
    }
  };

  _handleAppKillInComing = () => {
    console.log("app in foreground 1111111", this.showIncomingCallScreen);

    if (this.showIncomingCallScreen && this.call !== null) {
      this.showIncomingCallScreen = false;
      // //android callkit
      NavigationService.navigate("IncomingCall", {
        callId: this.call.callId,
        isVideo: null,
        from: null
      });
    }
  };
}
