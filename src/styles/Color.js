"use strict";

const COLOR = {
  PRIMARY: "#1c0b43",
  PRIMARY_DARK: "#392b5b",
  ACCENT: "#3370FF",
  BUTTON: "#662eff",
  WHITE: "#fff",
  BLACK: "#000",
  RED: "#f54b5e"
};

export default COLOR;
