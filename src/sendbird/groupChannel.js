import SendBird from "sendbird";

export const sbCreateGroupChannelListQuery = () => {
  const sb = SendBird.getInstance();
  return sb.GroupChannel.createMyGroupChannelListQuery();
};

export const sbGetGroupChannelList = groupChannelListQuery => {
  return new Promise((resolve, reject) => {
    groupChannelListQuery.next((channels, error) => {
      if (error) {
        reject(error);
      } else {
        resolve(channels);
      }
    });
  });
};

export const sbGetGroupChannel = channelUrl => {
  return new Promise((resolve, reject) => {
    const sb = SendBird.getInstance();
    sb.GroupChannel.getChannel(channelUrl, (channel, error) => {
      if (error) {
        reject(error);
      } else {
        resolve(channel);
      }
    });
  });
};

export const sbLeaveGroupChannel = channelUrl => {
  return new Promise((resolve, reject) => {
    const sb = SendBird.getInstance();
    sbGetGroupChannel(channelUrl)
      .then(channel => {
        channel.leave((response, error) => {
          if (error) {
            reject(error);
          } else {
            resolve(response);
          }
        });
      })
      .catch(error => reject(error));
  });
};

export const sbHideGroupChannel = channelUrl => {
  return new Promise((resolve, reject) => {
    const sb = SendBird.getInstance();
    sbGetGroupChannel(channelUrl)
      .then(channel => {
        channel.hide((response, error) => {
          if (error) {
            reject(error);
          } else {
            resolve(response);
          }
        });
      })
      .catch(error => reject(error));
  });
};

export const sbCreateUserListQuery = () => {
  const sb = SendBird.getInstance();
  return sb.createApplicationUserListQuery();
};

export const sbGetUserList = userListQuery => {
  return new Promise((resolve, reject) => {
    userListQuery.next((users, error) => {
      if (error) {
        reject(error);
      } else {
        resolve(users);
      }
    });
  });
};

export const sbCreateGroupChannelOneToOne = (userA, userB, isDistinct) => {
  return new Promise((resolve, reject) => {
    const sb = SendBird.getInstance();
    sb.GroupChannel.createChannelWithUserIds(
      [userA.userId, userB.userId],
      isDistinct,
      "oto",
      null,
      JSON.stringify({
        userA,
        userB
      }),
      (channel, error) => {
        if (error) {
          reject(error);
        } else {
          resolve(channel);
        }
      }
    );
  });
};

export const sbCreateGroupChannel = (
  operatorList,
  userList,
  name,
  isDistinct,
  cover
) => {

  const sb = SendBird.getInstance();
  var params = new sb.GroupChannelParams();
  params.isPublic = false;
  params.isEphemeral = false;
  params.isDistinct = isDistinct;
  params.addUserIds(userList.map(user => user.userId));
  // params.operatorIds = operatorList.map(user => user.userId); // or .operators(Array<User>)
  // params.operators(operatorList);
  params.operatorUserIds = operatorList.map(user => user.userId);
  params.name = name;
  params.coverImage = cover; // or .coverUrl = COVER_URL;

  return new Promise((resolve, reject) => {
    const sb = SendBird.getInstance();
    sb.GroupChannel.createChannel(params, (channel, error) => {
      if (error) {
        reject(error);
      } else {
        resolve(channel);
      }
    });
  });
};

export const sbInviteGroupChannel = ({ userList, channelUrl }) => {
  return new Promise((resolve, reject) => {
    sbGetGroupChannel(channelUrl)
      .then(channel => {
        channel.inviteWithUserIds(
          userList.map(user => user.userID),
          (channel, error) => {
            if (error) {
              reject(error);
            } else {
              resolve(channel);
            }
          }
        );
      })
      .catch(error => {
        reject(error);
      });
  });
};

export const sbUpdateGroupChannel = async ({ channelUrl, cover, name }) => {
  let channel = await sbGetGroupChannel(channelUrl);
  const sb = SendBird.getInstance();

  var params = new sb.GroupChannelParams();
  params.isPublic = false;
  params.isEphemeral = false;
  params.isDistinct = true;
  params.name = name;
  params.coverUrl = cover;
  // params.addUserIds(['John', 'Harry']);
  // params.operatorIds = ['Jay'];
  // params.data = DATA;
  // params.customType = CUSTOM_TYPE;

  return new Promise((resolve, reject) => {
    channel.updateChannel(params, function (chan, error) {
      if (error) {
        reject(error);
      } else {
        resolve(chan);
      }
    });
  });
};

export const sbBanUser = async ({ user, channel }) => {
  let groupChannel = await sbGetGroupChannel(channel.url);

  return new Promise((resolve, reject) => {
    // Ban a user
    if (groupChannel.myRole === "operator") {
      groupChannel.banUser(
        user,
        50000000,
        "Kick out of chat",
        (response, error) => {
          if (error) {
            reject(error);
          } else {
            resolve(response);
          }
        }
      );
    }
  });
};

export const sbUnbanUser = async ({ user, channel }) => {
  let groupChannel = await sbGetGroupChannel(channel.url);

  return new Promise((resolve, reject) => {
    // UnBan a user
    if (groupChannel.myRole === "operator") {
      groupChannel.unbanUserWithUserId(user.userID, (response, error) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      });
    }
  });
};

export const sbUpdateOperatorGroupChannel = async ({ channelUrl, operatorList, userList }) => {
  let channel = await sbGetGroupChannel(channelUrl);
  const sb = SendBird.getInstance();

  var params = new sb.GroupChannelParams();
  params.isPublic = false;
  params.isEphemeral = false;
  params.isDistinct = true;
  params.operatorUserIds = operatorList.map(user => user.userId)[0]
  // params.operators(operatorList);
  // params.operatorUserIds = operatorList.map(user => user.userId);
  // params.addUserIds(['John', 'Harry']);
  // params.operatorIds = operatorList.map(user => user.userId); // or .operators(Array<User>)
  // params.data = DATA;
  // params.customType = CUSTOM_TYPE;

  return new Promise((resolve, reject) => {
    channel.updateChannel(params, function (chan, error) {
      if (error) {
        console.log("errrrr")
        reject(error);
      } else {
        console.log("succeessss")
        resolve(chan);
      }
    });
  });
};
