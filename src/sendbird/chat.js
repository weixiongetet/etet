import SendBird from "sendbird";
import { sbGetGroupChannel } from "./groupChannel";
import { sbGetOpenChannel } from "./openChannel";

export const sbCreatePreviousMessageListQuery = (channelUrl, isOpenChannel) => {
  return new Promise((resolve, reject) => {
    if (isOpenChannel) {
      sbGetOpenChannel(channelUrl)
        .then(channel => resolve(channel.createPreviousMessageListQuery()))
        .catch(error => reject(error));
    } else {
      sbGetGroupChannel(channelUrl)
        .then(channel => resolve(channel.createPreviousMessageListQuery()))
        .catch(error => reject(error));
    }
  });
};

export const sbGetMessageList = previousMessageListQuery => {
  const limit = 30;
  const reverse = true;
  return new Promise((resolve, reject) => {
    previousMessageListQuery.load(limit, reverse, (messages, error) => {
      if (error) {
        reject(error);
      } else {
        resolve(messages);
      }
    });
  });
};

export const sbSendTextMessage = (channel, textMessage, callback) => {
  if (channel.isGroupChannel()) {
    channel.endTyping();
  }
  return new Promise((resolve, reject) => {
    channel.sendUserMessage(textMessage, (message, error) => {
      if (error) {
        reject(error);
      }
      resolve(message);
    });
  });
};

export const sbSendAdminMessage = (channel, textMessage, callback) => {
  if (channel.isGroupChannel()) {
    channel.endTyping();
  }
  const sb = SendBird.getInstance();
  const params = new sb.UserMessageParams();
  params.message = textMessage;
  params.customType = "GroupMessage";

  return new Promise((resolve, reject) => {
    channel.sendUserMessage(params, (message, error) => {
      if (error) {
        reject(error);
      }
      resolve(message);
    });
  });
};


export const sbSendFileMessage = (channel, file, fileUrl) => {

  const sb = SendBird.getInstance();
  const params = new sb.FileMessageParams();
  params.file = file;             // or .fileUrl (You can send a file message with a file URL.)
  params.customType = "FileMessage";
  params.fileName = file.fileName;
  params.mimeType = file.type;
  params.fileSize = file.fileSize;
  params.thumbnailSizes = [{ 'maxWidth': 160, 'maxHeight': 160 }];// Add the maximum sizes of thumbnail images (allowed number of thumbnail images: 3).
  params.fileUrl = fileUrl;

  return new Promise((resolve, reject) => {
    channel.sendFileMessage(params, (fileMessage, error) => {
      if (error) {
        reject(error);
      }
      resolve(fileMessage);
    });
  });

}



export const sbTypingStart = channelUrl => {
  return new Promise((resolve, reject) => {
    sbGetGroupChannel(channelUrl)
      .then(channel => {
        channel.startTyping();
        resolve(channel);
      })
      .catch(reject(error));
  });
};

export const sbTypingEnd = channelUrl => {
  return new Promise((resolve, reject) => {
    sbGetGroupChannel(channelUrl)
      .then(channel => {
        channel.endTyping();
        resolve(channel);
      })
      .catch(reject(error));
  });
};

export const sbIsTyping = channel => {
  if (channel.isTyping()) {
    const typingMembers = channel.getTypingMembers();
    if (typingMembers.length == 1) {
      return `${typingMembers[0].nickname} is typing...`;
    } else {
      return "several member are typing...";
    }
  } else {
    return "";
  }
};

export const sbMarkAsRead = ({ channelUrl, channel }) => {
  if (channel) {
    channel.markAsRead();
  } else {
    sbGetGroupChannel(channelUrl).then(channel => channel.markAsRead());
  }
};
