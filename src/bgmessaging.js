// @flow
import firebase from "react-native-firebase";
import LoginManager from "./manager/LoginManager";
// Optional flow type
import type { RemoteMessage } from "react-native-firebase";

export default async (message: RemoteMessage) => {
  const { title, text } = message.data;

  console.log("mesaggeeerecievee", title)


  if (!message.data.sendbird && !message.data.voximplant) {
    const localNotification = new firebase.notifications.Notification({
      show_in_foreground: true
    }).android
      .setChannelId("notify-channel")
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .setNotificationId(message.messageId)
      .setTitle(title)
      .setBody(text)
      .setData(message.data);

    firebase.notifications().displayNotification(localNotification);
  } else if (message.data.voximplant) {
    LoginManager.getInstance().pushNotificationReceived(message.data);

    let voxMessage = JSON.parse(message.data.voximplant);

    const localNotification = new firebase.notifications.Notification({
      // show_in_foreground: true
    }).android
      .setChannelId("voximplant_channel_id")
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .setNotificationId(message.messageId)
      .setTitle("Incoming call from " + voxMessage.display_name)
      .setBody("")
      .setData(message.data);
    firebase.notifications().displayNotification(localNotification);
  } else {

    console.log('senbriunotifcation', JSON.parse(message.data.sendbird));

    try {
      const payload = JSON.parse(message.data.sendbird);

      const localNotification = new firebase.notifications.Notification({
        show_in_foreground: true
      }).android
        .setChannelId("notify-channel")
        .android.setPriority(firebase.notifications.Android.Priority.High)
        .setNotificationId(message.messageId)
        .setTitle(
          `${payload.sender.name} ${
          payload.channel.name === "oto" ? "" : `@ ${payload.channel.name}`
          }`
        )
        .setBody(payload.message)
        .setData(payload);

      firebase.notifications().displayNotification(localNotification);


      // firebase.notifications().onNotificationOpened(notificationOpen => {
      //   console.log('notificationOpenedListener888888888888', notificationOpen);
      //   // console.log(notificationOpen);
      //   // const { action, notification } = notificationOpen;
      //   // firebase.notifications().removeDeliveredNotification(notification.notificationId);
      //   // console.log('OPEN:', notification);
      // });

      // firebase.notifications().onNotificationOpened(notificationOpen => {
      //   const data = Platform.OS === 'android' ? notificationOpen.notification.data.customDataKeyName : notificationOpen.customDataKeyName;
      //   this.navigate(data);
      // });


    } catch (error) {
      console.log("error from sending push", error);
    }
  }

  return Promise.resolve();
};
