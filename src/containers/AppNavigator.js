import AsyncStorage from "@react-native-community/async-storage";
import React, { Component } from "react";
import { BackHandler, Platform } from "react-native";
import firebase from "react-native-firebase";
import {
  createBottomTabNavigator,
  createStackNavigator,
  NavigationActions,
  StackActions
} from "react-navigation";
//For Redux
import {
  createNavigationReducer,
  createReactNavigationReduxMiddleware,
  createReduxContainer
} from "react-navigation-redux-helpers";
import { fromRight, fromTop } from "react-navigation-transitions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import IconWithBadge from "../components/IconWithBadge";
import { updateUser } from "../redux/modules/auth";
import { updCallFrom, updFriendsRequestArr } from "../redux/modules/contacts";
import { clearNotification } from "../redux/modules/notify";
import Login from "../screens/auth/Login";
import Reset from "../screens/auth/Reset";
import Signup from "../screens/auth/Signup";
import Chat from "../screens/chat";
import FriendList from "../screens/chat/FriendList";
import GroupList from "../screens/chat/GroupList";
import Message from "../screens/chat/Message";
import ParticipantList from "../screens/chat/ParticipantList";
import ContactsDetails from "../screens/contacts/ContactsDetails";
import ContactsSub from "../screens/contacts/ContactsSub";
import FriendsRequestList from "../screens/contacts/FriendsRequestList";
import SearchUser from "../screens/contacts/SearchUser";
import HomePage from "../screens/HomePage";
import Loading from "../screens/Loading";
import CameraScreen from "../screens/orc/CameraScreen";
import ContactScreen from "../screens/orc/ContactScreen";
import QRScan from "../screens/qr/QRScan";
import ShowQRCode from "../screens/qr/ShowQRCode";
import EditProfile from "../screens/setting/EditProfile";
import SettingMenu from "../screens/setting/SettingMenu";
import Vault from "../screens/vault";
import FileSharing from "../screens/vault/FileSharing";
//For Screens
import Walkthrough from "../screens/Walkthrough";
import AiChatBotScreen from "../screens/chat/AiChatBotScreen";
import NavigationService from "../routes/NavigationService";
import CallScreen from "../screens/call/CallScreen";
import IncomingCallScreen from "../screens/call/IncomingCallScreen";

const platform = Platform.OS;

const AuthStack = createStackNavigator(
  {
    Walkthrough: {
      screen: Walkthrough,
      path: "etet/:walkthrough",
      navigationOptions: () => ({
        header: null
      })
    },
    Loading: {
      screen: Loading,
      path: "etet/:login",
      navigationOptions: () => ({
        header: null
      })
    },
    Login: {
      screen: Login,
      path: "etet/:login",
      navigationOptions: () => ({
        header: null
      })
    },
    Signup: {
      screen: Signup,
      path: "etet/:signup",
      navigationOptions: () => ({
        header: null
      })
    },
    Reset: {
      screen: Reset,
      path: "etet/:reset",
      navigationOptions: () => ({
        title: `Reset Password`,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "center",
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "Walkthrough",
    transitionConfig: () => fromTop()
  }
);

const HomeStack = createStackNavigator(
  {
    HomePage: {
      screen: HomePage,
      path: "etet/:homepage",
      navigationOptions: () => ({
        // title: `Home`,
        headerBackTitle: null
      })
    },
    QRScan: {
      screen: QRScan,
      path: "etet/:qrscan",
      navigationOptions: () => ({
        title: `QR Scanner`,
        headerBackTitle: null
      })
    },
    ShowQRCode: {
      screen: ShowQRCode,
      path: "etet/:showqrcode",
      navigationOptions: () => ({
        title: `My QR Code`,
        headerBackTitle: null
      })
    },
    HomeQRUserDetails: {
      screen: ContactsDetails,
      path: "etet/:contactsdetails",
      navigationOptions: () => ({
        title: `Contacts`,
        headerBackTitle: null
      })
    },
    CameraScreen: {
      screen: CameraScreen,
      path: "etet/:contactsdetails",
      navigationOptions: () => ({
        title: `Scanner`,
        headerBackTitle: null
      })
    },
    AiChatBotScreen: {
      screen: AiChatBotScreen,
      navigationOptions: () => ({
        title: `e-Bot`,
        headerBackTitle: null
      })
    },
    ContactScreen: {
      screen: ContactScreen,
      path: "etet/:contactsdetails",
      navigationOptions: () => ({
        title: `Save Contact`,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "left",
    navigationOptions: {
      tabBarLabel: "Home"
    },
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: "#fff", elevation: 0 },
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "HomePage",
    transitionConfig: () => fromRight()
  }
);

const ContactsStack = createStackNavigator(
  {
    ContactsSub: {
      screen: ContactsSub,
      path: "etet/:contactssub",
      navigationOptions: () => ({
        // title: `Home`,
        headerBackTitle: null
      })

      // navigationOptions: ({ navigation, screenProps }) => {
      //   // if (
      //   //   navigation.state &&
      //   //   navigation.state.params &&
      //   //   navigation.state.params.countFriendsRequest
      //   // ) {
      //   //   countRequest =
      //   //     navigation.state.params.countFriendsRequest.countFriendsRequest;
      //   // }

      //   let countRequest = + screenProps.friendsRequestArr.length + screenProps.companyRequestArr.length

      //   // const BadgedIcon = withBadge(countRequest)(Icon);
      //   let IconComponent = IconWithBadge;

      //   return {
      //     title: ``,
      //     headerLeft: (
      //       <View>
      //         <Image
      //           source={require(`${assetPath}/logo.png`)}
      //           style={{
      //             flex: 1,
      //             width: 90,
      //             height: 30,
      //             marginLeft: 18,
      //             resizeMode: "contain"
      //           }}
      //         />
      //       </View>
      //     ),
      //     headerRight: (
      //       <IconComponent
      //         name={`${platform === "ios" ? "ios" : "md"}-notifications`}
      //         size={30}
      //         badgeCount={countRequest}
      //         onpressFunc={() => navigation.navigate("FriendsRequestList")}
      //       />

      //       // <BadgedIcon
      //       //   name={`${platform === "ios" ? "ios" : "md"}-notifications`}
      //       //   type="ionicon"
      //       //   color="white"
      //       //   style={{
      //       //     paddingRight: 24
      //       //   }}
      //       //   onPress={() => navigation.navigate("FriendsRequestList")}
      //       // />
      //     )
      //   };
      // }
    },
    ContactsDetails: {
      screen: ContactsDetails,
      path: "etet/:contactsdetails",
      navigationOptions: () => ({
        title: `Contact Details`,
        headerBackTitle: null
      })
    },
    FriendsRequestList: {
      screen: FriendsRequestList,
      path: "etet/:friendsrequestlist",
      navigationOptions: () => ({
        title: `Friends Request`,
        headerBackTitle: null
      })
    },
    SearchUser: {
      screen: SearchUser,
      path: "etet/:searchuser",
      navigationOptions: () => ({
        title: `Search Contact`,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "left",
    navigationOptions: {
      tabBarLabel: "Contacts"
    },
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: "#fff", elevation: 0 },
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "ContactsSub",
    transitionConfig: () => fromRight()
  }
);

const VaultStack = createStackNavigator(
  {
    Vault: {
      screen: Vault,
      path: "etet/:vault",
      navigationOptions: () => ({
        title: `Vault`,
        // headerLeft: (
        //   <View>
        //     <Image
        //       source={require(`${assetPath}/logo.png`)}
        //       style={{
        //         flex: 1,
        //         width: 90,
        //         height: 30,
        //         marginLeft: 16,
        //         resizeMode: "contain"
        //       }}
        //     />
        //   </View>
        // ),
        headerBackTitle: null
      })
    },
    FileSharing: {
      screen: FileSharing,
      path: "etet/:filesharing",
      navigationOptions: () => ({
        title: `File Sharing`,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "left",
    navigationOptions: {
      tabBarLabel: "Vault"
    },
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: "#fff", elevation: 0 },
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "Vault",
    transitionConfig: () => fromTop()
  }
);

const ChatsStack = createStackNavigator(
  {
    Chat: {
      screen: Chat,
      path: "etet/:chat",
      navigationOptions: () => ({
        title: `Chats`,
        headerBackTitle: null
        // headerLeft: (
        //   <View>
        //     <Image
        //       source={require(`${assetPath}/logo.png`)}
        //       style={{
        //         flex: 1,
        //         width: 90,
        //         height: 30,
        //         marginLeft: 17,
        //         resizeMode: "contain"
        //       }}
        //     />
        //   </View>
        // )
      })
    },
    FriendList: {
      screen: FriendList,
      path: "etet/:friendlist",
      navigationOptions: () => ({
        title: `Select friends`,
        headerBackTitle: null
      })
    },
    GroupList: {
      screen: GroupList,
      path: "etet/:grouplist",
      navigationOptions: () => ({
        title: `Select participants`,
        headerBackTitle: null
      })
    },
    Message: {
      screen: Message,
      path: "etet/:message",
      navigationOptions: () => ({
        title: `Message`,
        headerBackTitle: null
      })
    },
    ParticipantList: {
      screen: ParticipantList,
      path: "etet/:participantlist",
      navigationOptions: () => ({
        title: `Manage participants`,
        headerBackTitle: null
      })
    },
    Call: {
      screen: CallScreen,
      path: "etet/:call",
      navigationOptions: () => ({
        title: `Call`,
        headerLeft: null,
        headerBackTitle: null
      })
    },
    IncomingCall: {
      screen: IncomingCallScreen,
      path: "etet/:incomingcall",
      navigationOptions: () => ({
        title: `Incoming Call`,
        headerLeft: null,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "left",
    // defaultNavigationOptions: ({ navigation }) => ({
    // header: null,
    // }),
    navigationOptions: {
      tabBarLabel: "Chats"
    },
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: "#fff", elevation: 0 },
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "Chat",
    transitionConfig: () => fromRight()
  }
);

const SettingsStack = createStackNavigator(
  {
    SettingMenu: {
      screen: SettingMenu,
      path: "etet/:SettingMenu",
      navigationOptions: () => ({
        title: `Settings`,
        headerBackTitle: null
        // headerLeft: (
        //   <View>
        //     <Image
        //       source={require(`${assetPath}/logo.png`)}
        //       style={{
        //         flex: 1,
        //         width: 90,
        //         height: 30,
        //         marginLeft: 16,
        //         resizeMode: "contain"
        //       }}
        //     />
        //   </View>
        // )
      })
    },
    EditProfile: {
      screen: EditProfile,
      path: "etet/:EditProfile",
      navigationOptions: () => ({
        title: `Profile`,
        headerBackTitle: null
      })
    }
  },
  {
    headerLayoutPreset: "left",
    navigationOptions: {
      tabBarLabel: "Settings"
    },
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: "#fff", elevation: 0 },
      headerTitleStyle: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
      }
    }),
    initialRouteName: "SettingMenu",
    transitionConfig: () => fromRight()
  }
);

let DashboardStack = createBottomTabNavigator(
  {
    HomeStack,
    ContactsStack,
    VaultStack,
    ChatsStack,
    SettingsStack
  },
  {
    // swipeEnabled: true,
    // animationEnabled: true,
    // initialRouteName: 'ContactsStack',
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = IconWithBadge;
        let iconName,
          badgeCount = 0,
          notifyCount = screenProps.notifyCount;
        let size = 25;

        if (routeName === "HomeStack") {
          iconName = `home`;
          badgeCount = notifyCount.home;
        } else if (routeName === "ContactsStack") {
          iconName = `contacts`;
          if (
            screenProps.companyRequestArr !== undefined &&
            screenProps.companyRequestArr.length > 0
          ) {
            badgeCount =
              +screenProps.friendsRequestArr.length +
              screenProps.companyRequestArr.length;
          } else {
            badgeCount = screenProps.friendsRequestArr.length;
          }
          // badgeCount = notifyCount.contact;
        } else if (routeName === "VaultStack") {
          iconName = `upload`;
          badgeCount = notifyCount.vault;
          // size = 32
        } else if (routeName === "ChatsStack") {
          iconName = `message1`;
          badgeCount = notifyCount.chat;
        } else if (routeName === "SettingsStack") {
          iconName = `setting`;
          badgeCount = notifyCount.setting;
        }

        return (
          <IconComponent
            name={iconName}
            size={size}
            color={tintColor}
            badgeCount={badgeCount}
          />
        );
      },

      tabBarOnPress: ({ navigation, defaultHandler }) => {
        const { routeName } = navigation.state;
        let module;

        if (routeName === "HomeStack") {
          module = `home`;
        } else if (routeName === "ContactsStack") {
          module = `contact`;
        } else if (routeName === "VaultStack") {
          module = `vault`;
        } else if (routeName === "ChatsStack") {
          module = `chat`;
        } else if (routeName === "SettingsStack") {
          module = `setting`;
        }
        screenProps.clearNotification(module);
        defaultHandler();
      },

      tabBarVisible: navigation.state.index > 0 ? false : true //hide bottom tabbar
    }),
    tabBarOptions: {
      activeTintColor: "#3370FF",
      inactiveTintColor: "#616161",
      // activeBackgroundColor: "#eeeeee",
      inactiveBackgroundColor: "#fff",
      labelStyle: {
        fontSize: 12,
        fontFamily: "Roboto"
      },
      style: {
        backgroundColor: "#fff",
        borderTopWidth: 0,
        height: 55,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: "black",
        shadowOpacity: 0.5
        // marginBottom: 5,
        // elevation: 5
      }
    }
  }
);

export const RootNavigator = createStackNavigator(
  {
    AuthStack: {
      screen: AuthStack
    },
    DashboardStack: {
      screen: DashboardStack
    }
  },
  {
    headerMode: "none",
    initialRouteName: "AuthStack",
    transitionConfig: () => fromTop()
  }
);

// export const ReduxNavigator = createAppContainer(RootNavigator);
const ReduxNavigator = createReduxContainer(RootNavigator);

class BackHardware extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    this.updateBound = bindActionCreators(
      updateUser,
      dispatch,
      updCallFrom,
      updFriendsRequestArr
    );
  }

  componentDidMount() {
    const { state, dispatch } = this.props;

    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    // listen to firebase auth
    this.authUnlink = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.props.dispatch(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: "DashboardStack"
              })
            ]
          })
        );
        this.userUnlink = firebase
          .firestore()
          .collection("users")
          .doc(user.uid)
          .onSnapshot(docSnapshot => {
            this.updateBound(docSnapshot.data());
          });
      } else {
        if (this.userUnlink) this.userUnlink();
        this.updateBound(null);
        AsyncStorage.getItem("walkthrough").then(res => {
          this.props.dispatch(
            StackActions.reset({
              index: 0,
              key: null,
              actions: [
                NavigationActions.navigate({
                  routeName: "AuthStack",
                  params: {},
                  action: NavigationActions.navigate({
                    routeName: res !== "no" ? "Walkthrough" : "Login"
                  })
                })
              ]
            })
          );
        });
      }
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    this.authUnlink();
  }

  onBackPress = () => {
    const { nav, dispatch } = this.props;

    // already first page, ask quit
    // if (nav.routes[0].routes[0].index === 0) {
    //   Alert.alert(
    //     "Exiting App ..",
    //     "Are you sure?",
    //     [
    //       { text: "Confirm", onPress: () => BackHandler.exitApp() },
    //       {
    //         text: "Cancel",
    //         onPress: () => console.log("Cancel Pressed"),
    //         style: "cancel"
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }

    // perform back
    dispatch(NavigationActions.back());

    return true;
  };

  render() {
    const {
      nav,
      dispatch,
      notifyCount,
      clearNotification,
      friendsRequestArr,
      companyRequestArr
    } = this.props;

    return (
      <ReduxNavigator
        screenProps={{
          notifyCount,
          clearNotification,
          friendsRequestArr,
          companyRequestArr
        }}
        state={nav}
        dispatch={dispatch}
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
BackHardware = connect(
  state => ({
    user: state.auth.user,
    notifyCount: state.notify,
    friendsRequestArr: state.contacts.friendsRequestArr,
    companyRequestArr: state.contacts.companyRequestArr
  }),
  {
    clearNotification
  }
)(BackHardware);

export const AppWithNavigationState = connect(state => ({
  nav: state.nav
}))(BackHardware);
export const navReducer = createNavigationReducer(RootNavigator);
export const middleware = createReactNavigationReduxMiddleware(state => {
  state.nav;
});
