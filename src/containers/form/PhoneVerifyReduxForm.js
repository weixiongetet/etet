import AsyncStorage from "@react-native-community/async-storage";
import { Button, Input, Spinner, Text } from "native-base";
import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import firebase from "react-native-firebase";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import { connect } from "react-redux";
import { change, Field, formValueSelector, getFormSyncErrors, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";
import PhoneField from "../../components/form/PhoneField";
import { manualUpdate } from "../../redux/modules/util";

const validate = values => {
  const error = {};
  error.verifyCode = "";
  var code = values.verifyCode;

  if (values.verifyCode === undefined) code = "";

  // code
  if (!/^[0-9]+$/.test(code)) error.verifyCode = "not digit";
  if (code.length != 6) error.verifyCode = "code not in format";
  if (code == "") error.verifyCode = "empty";

  return error;
};

const numberBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="code" hasError={hasError}>
      <Input
        keyboardType="numeric"
        autoCapitalize="none"
        placeholder="code"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class PhoneVerifyReduxForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      verifying: false,
      verificationObj: {},
      smsSent: false,
      timeout: false,
      countdown: 0
    };
  }

  checkAutoVerified = () => {
    const { verificationObj } = this.state;

    if (verificationObj.state == firebase.auth.PhoneAuthState.AUTO_VERIFIED) {
      this.linkCredential();
    }
  };

  linkCredential = async () => {
    await this.setState({ verifying: true });
    const { code, phone, redirect } = this.props;
    const { verificationObj } = this.state;

    let credential = firebase.auth.PhoneAuthProvider.credential(
      verificationObj.verificationId,
      verificationObj.code ? verificationObj.code : code
    );

    try {
      await firebase.auth().currentUser.linkWithCredential(credential);
      const { _user } = firebase.auth().currentUser;
      await firebase
        .firestore()
        .collection("users")
        .doc(_user.uid)
        .update({
          phone
        });
      if (redirect) redirect();
      AsyncStorage.setItem("tiePhone", "no")
    } catch (err) {
      alert(err);
    } finally {
      await this.setState({ verifying: false });
    }
  };

  render() {
    const { phone, phoneValid, synchronousError, manualUpdate } = this.props;
    const { timeout } = this.state;
    const codeValid = (this.state.smsSent) ? true : "";
    const { redirect } = this.props;

    return (
      <View>
        {/* Phone input */}
        <TouchableOpacity onPress={() => redirect()} style={{ paddingLeft: 18, paddingTop: 20 }}>
          <View>
            <IconAntDesign color="#000" name="arrowleft" size={22} />
          </View>
        </TouchableOpacity>


        <View style={styles.verifyForm}>

          <View style={{ flexDirection: "row" }}>


            <PhoneField
              onChange={({ number, valid }) => {
                manualUpdate(change("phoneVerifyForm", "phone", number));
                manualUpdate(change("phoneVerifyForm", "phoneValid", valid));
              }}
              initialCountry="sg"
              countryList={[
                "AF",
                "AL",
                "DZ",
                "AS",
                "AD",
                "AO",
                "AG",
                "AR",
                "AM",
                "AW",
                "AU",
                "AT",
                "BS",
                "BD",
                "BB",
                "BY",
                "BE",
                "BZ",
                "BJ",
                "BM",
                "BT",
                "BO",
                "BA",
                "BW",
                "BR",
                "VG",
                "BN",
                "BG",
                "BF",
                "KH",
                "CM",
                "CA",
                "CV",
                "KY",
                "CF",
                "CL",
                "CO",
                "KM",
                "CK",
                "CR",
                "HR",
                "CW",
                "CY",
                "CZ",
                "CD",
                "DK",
                "DJ",
                "DM",
                "DO",
                "EC",
                "EG",
                "SV",
                "GQ",
                "ET",
                "FK",
                "FO",
                "FJ",
                "FI",
                "FR",
                "GF",
                "GA",
                "GM",
                "GE",
                "DE",
                "GH",
                "GI",
                "GR",
                "GL",
                "GD",
                "GP",
                "GT",
                "GG",
                "GY",
                "HT",
                "HN",
                "HK",
                "HU",
                "IN",
                "ID",
                "IQ",
                "IE",
                "IM",
                "IL",
                "IT",
                "CI",
                "JM",
                "JP",
                "JE",
                "JO",
                "KZ",
                "KE",
                "KW",
                "KG",
                "LA",
                "LV",
                "LB",
                "LS",
                "LY",
                "LI",
                "LT",
                "LU",
                "MO",
                "MK",
                "MG",
                "MW",
                "MY",
                "MT",
                "MU",
                "YT",
                "MX",
                "FM",
                "MD",
                "MN",
                "ME",
                "MS",
                "MA",
                "MZ",
                "MM",
                "NA",
                "NP",
                "NL",
                "NC",
                "NZ",
                "NI",
                "NE",
                "NG",
                "NF",
                "NO",
                "OM",
                "PK",
                "PS",
                "PA",
                "PG",
                "PY",
                "PE",
                "PH",
                "PL",
                "PT",
                "PR",
                "QA",
                "CG",
                "RO",
                "RU",
                "RW",
                "RE",
                "KN",
                "LC",
                "MF",
                "PM",
                "VC",
                "WS",
                "SA",
                "SN",
                "RS",
                "SC",
                "SL",
                "SG",
                "SK",
                "SI",
                "ZA",
                "KR",
                "ES",
                "LK",
                "SD",
                "SR",
                "SZ",
                "SE",
                "CH",
                "SY",
                "ST",
                "TW",
                "TZ",
                "TH",
                "TL",
                "TG",
                "TO",
                "TT",
                "TR",
                "TM",
                "TC",
                "UG",
                "UA",
                "AE",
                "GB",
                "US",
                "VI",
                "UY",
                "UZ",
                "VE",
                "VN",
                "YE",
                "ZM",
                "ZW"
              ]}
              isValid={phoneValid}
            />
          </View>

          {this.state.loading ? (
            <Spinner color="#B39DDB" />
          ) : (
              <Button
                disabled={!phoneValid || timeout}
                primary={phoneValid && !timeout}
                light={!phoneValid || timeout}
                onPress={async () => {
                  console.log("presss", phone);

                  // await requestSMS(phone);
                  await this.setState({ loading: true });
                  try {
                    let verificationObj = await firebase
                      .auth()
                      .verifyPhoneNumber(phone)
                      .on("state_changed", phoneAuthSnapshot => {
                        console.log("phoneAuthSnapshot", phoneAuthSnapshot);
                        console.log("PhoneAuthState", firebase.auth.PhoneAuthState);

                        switch (phoneAuthSnapshot.state) {
                          case firebase.auth.PhoneAuthState.CODE_SENT:
                          case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
                          case firebase.auth.PhoneAuthState.AUTO_VERIFIED:
                            return Promise.resolve(phoneAuthSnapshot);
                            break;
                          case firebase.auth.PhoneAuthState.ERROR:
                            return Promise.reject("Firebase auth error");
                            break;
                          default:
                            break;
                        }
                      });
                    await this.setState({
                      smsSent: true,
                      timeout: true,
                      countdown: 60,
                      verificationObj
                    });
                    setTimeout(() => {
                      this.setState({ timeout: false });
                    }, 60000);
                    setInterval(
                      () =>
                        this.setState({
                          countdown: this.state.countdown - 1
                        }),
                      1000
                    );
                    this.checkAutoVerified();
                  } catch (err) {
                    alert(err);
                  } finally {
                    this.setState({ loading: false });
                  }
                }}
                style={styles.verifyButton}
              >
                {this.state.smsSent ? (
                  <Text>Resend SMS</Text>
                ) : (
                    <Text>Send SMS</Text>
                  )}
              </Button>
            )}

          {timeout ? (
            <Text>Wait for {this.state.countdown} seconds to resend</Text>
          ) : null}
        </View>

        {/* Verification Code */}
        <View style={styles.verifyForm}>
          <Text>Please enter the code that you received on your phone</Text>
          <Field name="verifyCode" component={numberBox} />

          {this.state.verifying ? (
            <Spinner color="#B39DDB" />
          ) : (
              <Button
                disabled={!codeValid}
                primary={codeValid}
                light={!codeValid}
                onPress={this.linkCredential}
                style={styles.verifyButton}
              >
                <Text>Verify</Text>
              </Button>
            )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  verifyForm: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "95%",
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30,
    paddingTop: 10,
    marginLeft: "auto",
    marginRight: "auto"
  },
  verifyButton: {
    marginTop: 15,
    marginLeft: "auto",
    marginRight: "auto"
  },
  phoneView: {
    flex: 1,
    flexDirection: "row",
    padding: 0,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 5,
    marginBottom: 20
  },
  smsNotice: {
    fontSize: 10,
    marginLeft: "auto",
    marginRight: "auto"
  }
});

PhoneVerifyReduxForm = reduxForm({
  // a unique name for the form
  form: "phoneVerifyForm",
  validate
})(PhoneVerifyReduxForm);

const selector = formValueSelector("phoneVerifyForm");
PhoneVerifyReduxForm = connect(
  state => {
    return {
      // invalid: isInvalid("phoneVerifyForm")(state),
      synchronousError: getFormSyncErrors("phoneVerifyForm")(state),
      phone: selector(state, "phone"),
      code: selector(state, "verifyCode"),
      phoneValid: selector(state, "phoneValid")
    };
  },
  { manualUpdate }
)(PhoneVerifyReduxForm);

export default PhoneVerifyReduxForm;
