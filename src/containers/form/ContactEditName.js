import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Field, reduxForm, isInvalid, formValueSelector } from "redux-form";
import { Button, Input, View, Text } from "native-base";
import { connect } from "react-redux";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.firstName = "";
  // error.lastName = "";
  var firstName = values.firstName;
  // var lastName = values.lastName;

  if (values.firstName === undefined) firstName = "";
  // if (values.lastName === undefined) lastName = "";

  // firstName
  if (firstName == "") error.firstName = "ignore";

  // lastName
  //if (lastName == "") error.lastName = "ignore";

  return error;
};

const firstNameBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (
    <InputField skipValidate icon="md-person" hasError={hasError}>
      <Input
        autoCapitalize="none"
        placeholder="Name"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

// const lastNameBox = ({
//   input: { onChange, ...restInput },
//   meta: { error }
// }) => {
//   let hasError = error !== undefined;

//   return (
//     <InputField skipValidate icon="md-home" hasError={hasError}>
//       <Input
//         autoCapitalize="none"
//         placeholder="Last Name"
//         onChangeText={onChange}
//         {...restInput}
//       />
//     </InputField>
//   );
// };

class EditNameReduxForm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit, invalid, email } = this.props;

    return (
      <View>
        <Field name="firstName" component={firstNameBox} />
        {/* <Field name="lastName" component={lastNameBox} /> */}

        <Button
          disabled={invalid}
          primary={!invalid}
          light={invalid}
          style={styles.button}
          block
          enable={!invalid}
          onPress={async val => {
            await handleSubmit(val);
          }}
        >
          <Text>Update</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 15
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

EditNameReduxForm = reduxForm({
  form: "contactNameForm",
  validate
})(EditNameReduxForm);

const selector = formValueSelector("contactNameForm");
EditNameReduxForm = connect(state => {
  return {
    initialValues: {
      firstName: state.contacts.selectedContacts.name
    },
    firstName: selector(state, "firstName"),
    // lastName: selector(state, "lastName"),
    invalid: isInvalid("contactNameForm")(state)
  };
})(EditNameReduxForm);

export default EditNameReduxForm;
