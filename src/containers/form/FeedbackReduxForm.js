import { Button, Input, Text, View } from "native-base";
import React, { Component } from "react";
import {
  ActivityIndicator,
  Linking,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { Field, formValueSelector, isInvalid, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.feedback = "";
  var feedback = values.feedback;

  if (values.feedback === undefined) feedback = "";

  // feedback
  if (feedback.length < 12) error.feedback = "too short";
  if (feedback == "") error.feedback = "ignore";

  return error;
};

const feedbackBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="md-book" hasError={hasError}>
      <Input
        multiline={true}
        autoCapitalize="none"
        placeholder="Share Your Thought"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class FeedbackReduxForm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit, invalid } = this.props;

    return (
      <View>
        <Text style={{ fontSize: 15 }}>Write Feedback</Text>

        {this.props.sent ? (
          <View>
            <Text style={{ marginTop: 5, fontSize: 12 }}>
              We heard you! Thanks for your feedback.
            </Text>
            <TouchableOpacity onPress={this.props.feedOn}>
              <Text
                style={{
                  marginTop: 5,
                  fontSize: 12,
                  textDecorationLine: "underline",
                  color: "blue"
                }}
              >
                Wait! You have more to said?
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View>
            <Field name="feedback" component={feedbackBox} />
            <Button
              disabled={invalid}
              primary={!invalid}
              light={invalid}
              style={styles.button}
              block
              enable={!invalid}
              onPress={async val => {
                await handleSubmit(val);
                this.props.reset();
              }}
            >
              {this.props.loading ? <ActivityIndicator /> : <Text>Send</Text>}
            </Button>
          </View>
        )}

        <TouchableOpacity
          onPress={() => Linking.openURL("mailto:etetdevteam@gmail.com")}
        >
          <Text style={{ marginTop: 15, fontSize: 12 }}>
            We will reply via email. Any enquiry please send to
            etetdevteam@gmail.com
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 15
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

FeedbackReduxForm = reduxForm({
  form: "feedbackForm",
  validate
})(FeedbackReduxForm);

const selector = formValueSelector("feedbackForm");
FeedbackReduxForm = connect(state => {
  return {
    // firstName: selector(state, "firstName"),
    // lastName: selector(state, "lastName"),
    invalid: isInvalid("feedbackForm")(state)
  };
})(FeedbackReduxForm);

export default FeedbackReduxForm;
