import { Button, Icon, Input, Text, View } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, ImageBackground, StyleSheet,TouchableOpacity } from "react-native";
// import { TouchableOpacity } from "react-native-gesture-handler";
import ImagePicker from "react-native-image-picker";
import { connect } from "react-redux";
import { Field, formValueSelector, isInvalid, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";
import { changeGroupImage } from "../../redux/modules/chat";

const validate = values => {
  const error = {};
  error.name = "";
  var name = values.name;

  if (values.name === undefined) name = "";

  // name
  if (name.length > 64) error.name = "max 64 characters";
  if (name.length < 4) error.name = "min 4 characters";
  if (name == "") error.name = "ignore";

  return error;
};

const nameBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField skipValidate icon="md-contacts" hasError={hasError}>
      <Input
        autoCapitalize="none"
        placeholder="Group Name"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class CreateGroupChannel extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // this.props.changeGroupImage({ uri: "" });
  }

  selectImage = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.props.changeGroupImage(response);
      }
    });
  };

  render() {
    const { handleSubmit, handleClose, invalid } = this.props;

    return (
      <View>
        <View style={{ alignSelf: "center" }}>
          {this.props.image.uri === "" ? (
            <Button
              style={{ borderRadius: 3 }}
              light
              onPress={this.selectImage}
            >
              <Text style={{ fontSize: 12 }}>+ Select image</Text>
            </Button>
          ) : (
            <View style={{ alignSelf: "center" }}>
              <TouchableOpacity onPress={this.selectImage}>
                <View>
                  <ImageBackground
                    source={{
                      uri: this.props.image.uri
                    }}
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      width: 120,
                      height: 120,
                      borderRadius: 60
                    }}
                    imageStyle={{
                      borderRadius: 5
                    }}
                  >
                    <Icon
                      active
                      style={{
                        color: "white",
                        position: "absolute",
                        right: 5,
                        bottom: 5
                      }}
                      fontSize={8}
                      name="md-create"
                    />
                  </ImageBackground>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>

        <Field name="name" component={nameBox} />

        <View style={styles.buttonList}>
          {this.props.loading ? <ActivityIndicator /> : null}

          {!this.props.loading ? (
            <Button
              disabled={invalid}
              small
              transparent
              style={{ alignSelf: "flex-start" }}
              enable={!invalid}
              onPress={handleSubmit}
            >
              <Text>Start</Text>
            </Button>
          ) : null}

          {!this.props.loading ? (
            <Button
              style={{ alignSelf: "flex-end" }}
              small
              transparent
              onPress={handleClose}
            >
              <Text>Close</Text>
            </Button>
          ) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonList: {
    flexDirection: "row",
    marginTop: 10
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

CreateGroupChannel = reduxForm({
  form: "newnameForm",
  validate
})(CreateGroupChannel);

const selector = formValueSelector("createGroupChannelForm");
CreateGroupChannel = connect(
  state => {
    return {
      // firstName: selector(state, "firstName"),
      // lastName: selector(state, "lastName"),
      invalid: isInvalid("createGroupChannelForm")(state),
      image: state.chat.image
    };
  },
  { changeGroupImage }
)(CreateGroupChannel);

export default CreateGroupChannel;
