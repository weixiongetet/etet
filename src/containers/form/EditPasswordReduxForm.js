import { Button, Input, Text, View } from "native-base";
import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { connect } from "react-redux";
import { Field, formValueSelector, isInvalid, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";

const validate = values => {

  const error = {};
  error.curPassword = "";
  error.newPassword = "";
  var curPassword = values.curPassword;
  var newPassword = values.newPassword;

  if (values.curPassword === undefined) curPassword = "";
  if (values.newPassword === undefined) newPassword = "";

  // curPassword
  if (
    [
      "1234abcd",
      "abcd1234",
      "a1234567",
      "qwer1234",
      "1234qwer",
      "zxcv1234",
      "asdf1234",
      "1234zxcv",
      "1234asdf"
    ].includes(curPassword)
  )
    error.curPassword = "too common";

  if (!/[^a-zA-Z ]/i.test(curPassword) || !/[^0-9 ]/i.test(curPassword))
    error.curPassword = "not alphanumeric";
  if (curPassword.length > 64) error.curPassword = "max 64 characters";
  if (curPassword.length < 6) error.curPassword = "min 6 characters";
  if (curPassword == "") error.curPassword = "ignore";


  // newPassword
  if (
    [
      "1234abcd",
      "abcd1234",
      "a1234567",
      "qwer1234",
      "1234qwer",
      "zxcv1234",
      "asdf1234",
      "1234zxcv",
      "1234asdf"
    ].includes(newPassword)
  )
    error.newPassword = "too common";

  if (!/[^a-zA-Z ]/i.test(newPassword) || !/[^0-9 ]/i.test(newPassword))
    error.newPassword = "not alphanumeric";
  if (newPassword.length > 64) error.newPassword = "max 64 characters";
  if (newPassword.length < 6) error.newPassword = "min 6 characters";
  if (newPassword == "") error.newPassword = "ignore";

  console.log("error", error);

  return error;
};



const curPasswordBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (
    <InputField skipValidate icon="" hasError={hasError}>
      <Text>Old:  </Text>
      <Input
        secureTextEntry
        autoCapitalize="none"
        placeholder=" Password"
        // style={styles.passwordInput}
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};



const passwordBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (

    <InputField skipValidate icon="" hasError={hasError}>
      <Text>New : </Text>
      <Input
        secureTextEntry
        autoCapitalize="none"
        placeholder=" Password"
        // style={styles.passwordInput}
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class EditPasswordReduxForm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit, invalid } = this.props;

    return (
      <View>
        <Field name="curPassword" component={curPasswordBox} />
        <Field name="newPassword" component={passwordBox} />

        <Button
          disabled={invalid}
          primary={!invalid}
          light={invalid}
          style={styles.button}
          block
          enable={!invalid}
          onPress={async val => {
            await handleSubmit(val);
          }}
        >
          <Text>Replace</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 15
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

EditPasswordReduxForm = reduxForm({
  form: "passwordForm",
  validate
})(EditPasswordReduxForm);

const selector = formValueSelector("passwordForm");
EditPasswordReduxForm = connect(state => {
  return {
    // firstName: selector(state, "firstName"),
    // lastName: selector(state, "lastName"),
    invalid: isInvalid("passwordForm")(state)
  };
})(EditPasswordReduxForm);

export default EditPasswordReduxForm;
