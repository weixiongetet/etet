import { Button, Input, Spinner, Text } from "native-base";
import React from "react";
import { Dimensions, KeyboardAvoidingView, StyleSheet, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { BarPasswordStrengthDisplay as PasswordMeter } from "react-native-password-strength-meter";
import { connect } from "react-redux";
import { Field, formValueSelector, isInvalid, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.profile = "";
  error.email = "";
  error.password = "";
  var pro = values.profile;
  var ema = values.email;
  var pw = values.password;

  if (values.profile === undefined) pro = "";
  if (values.email === undefined) ema = "";
  if (values.password === undefined) pw = "";

  // profile
  if (pro.length < 6 && pro !== "") error.profile = "too short";
  if (pro == "") error.profile = "ignore";

  // email
  if (ema.length < 6 && ema !== "") error.email = "too short";
  if (!ema.includes("@") && ema !== "") error.email = "@ not included";
  if (ema == "") error.email = "ignore";

  // password
  if (
    [
      "1234abcd",
      "abcd1234",
      "a1234567",
      "qwer1234",
      "1234qwer",
      "zxcv1234",
      "asdf1234",
      "1234zxcv",
      "1234asdf",
      "p@ssword"
    ].includes(pw)
  )
    error.password = "too common";
  if (!/[^a-zA-Z ]/i.test(pw) || !/[^0-9 ]/i.test(pw))
    error.password = "not alphanumeric";
  if (pw.length > 64) error.password = "max 64 characters";
  if (pw.length < 6) error.password = "min 6 characters";
  if (pw == "") error.password = "ignore";

  return error;
};

const profileBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="md-contact" hasError={hasError}>
      <Input
        placeholder="Profile name, Eg. 'John Wick'"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

const emailBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="mail" hasError={hasError}>
      <Input
        keyboardType="email-address"
        placeholder="Email"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

const passwordBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="md-lock" hasError={hasError}>
      <Input
        secureTextEntry
        autoCapitalize="none"
        placeholder="Password"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

let signupLevels = [
  {
    label: "Very weak",
    labelColor: "#ff5400",
    activeBarColor: "#ff5400"
  },
  {
    label: "Weak",
    labelColor: "#ff6900",
    activeBarColor: "#ff6900"
  },
  {
    label: "Average",
    labelColor: "#f3d331",
    activeBarColor: "#f3d331"
  },
  {
    label: "Strong",
    labelColor: "#14eb6e",
    activeBarColor: "#14eb6e"
  },
  {
    label: "Very strong",
    labelColor: "#0af56d",
    activeBarColor: "#0af56d"
  }
];

let SignUpReduxForm = props => {
  const submit = values => {
    console.log("submitting form", values);
  };

  return (
    <KeyboardAvoidingView enabled>
      <ScrollView>
        <Field name="profile" component={profileBox} />
        <Field name="email" component={emailBox} />
        <Field name="password" component={passwordBox} />

        <View>
          <PasswordMeter
            password={props.password}
            wrapperStyle={{
              marginTop: 10,
              marginLeft: "auto",
              marginRight: "auto",
              marginBottom: 50
            }}
            width={Dimensions.get("window").width - 100}
            levels={signupLevels}
          />
        </View>
        <Button
          disabled={!props.validity}
          light={!props.validity}
          primary={props.validity}
          block
          onPress={props.handleSubmit}
        >
          {props.loading ? <Spinner color="#B39DDB" /> : <Text>Sign Up</Text>}
        </Button>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  phoneView: {
    flex: 1,
    flexDirection: "row",
    padding: 0,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 5,
    marginBottom: 20
  },
  passwordInput: {
    fontSize: 17
  },
  signupButton: {
    marginTop: 10
  },
  signupButtonDisabled: {
    color: "#616161",
    backgroundColor: "#eeeeee",
    borderColor: "#616161"
  }
});

SignUpReduxForm = reduxForm({
  // a unique name for the form
  form: "signupForm",
  validate
})(SignUpReduxForm);

const selector = formValueSelector("signupForm"); // <-- same as form name
SignUpReduxForm = connect(state => {
  return {
    profile: selector(state, "profile"),
    email: selector(state, "email"),
    password: selector(state, "password"),
    phone: selector(state, "phone"),
    validity: !isInvalid("signupForm")(state),
    isFetching: state.auth.isFetching
  };
})(SignUpReduxForm);

export default SignUpReduxForm;
