import { Button, Input, Spinner, Text } from "native-base";
import React from "react";
import { KeyboardAvoidingView, StyleSheet } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { Field, isInvalid, reduxForm } from "redux-form";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.email = "";
  error.password = "";
  var ema = values.email;
  var pw = values.password;

  if (values.email === undefined) ema = "";
  if (values.password === undefined) pw = "";

  // email
  if (!ema.includes("@") && ema !== "") error.email = "@ not included";
  if (ema.length < 6) error.email = "too short";
  if (ema == "") error.email = "ignore";

  // password
  if (pw.length < 6) error.password = "too short";
  if (pw == "") error.password = "ignore";
  return error;
};

const emailBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="mail" hasError={hasError}>
      <Input
        keyboardType="email-address"
        autoCapitalize="none"
        placeholder="Email"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

const passwordBox = ({
  input: { onChange, ...restInput },
  meta: { error }
}) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="md-lock" hasError={hasError}>
      <Input
        secureTextEntry
        autoCapitalize="none"
        placeholder="Password"
        // style={styles.passwordInput}
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

let LoginReduxForm = ({ handleSubmit, invalid, isFetching }) => {
  return (
    <KeyboardAvoidingView>
      <ScrollView>
        <Field name="email" component={emailBox} />
        <Field name="password" component={passwordBox} />

        <Button
          disabled={invalid}
          primary={!invalid}
          light={invalid}
          style={styles.button}
          block
          text="Login"
          loading={isFetching}
          enable={!invalid}
          onPress={handleSubmit}
        // onPress={handleSubmit(submit)} //for debug
        >
          {isFetching ? <Spinner color="#B39DDB" /> : <Text>Login</Text>}
        </Button>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  passwordInput: {
    fontSize: 17
  },
  button: {
    marginTop: 15
  }
});

LoginReduxForm = reduxForm({
  // a unique name for the form
  form: "loginForm",
  validate
})(LoginReduxForm);

LoginReduxForm = connect(state => {
  return {
    invalid: isInvalid("loginForm")(state)
  };
})(LoginReduxForm);

export default LoginReduxForm;
