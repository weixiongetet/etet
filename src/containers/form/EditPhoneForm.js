import React, { Component } from "react";
import {
  Field,
  reduxForm,
  formValueSelector,
} from "redux-form";
import { StyleSheet, View } from "react-native";
import { Button, Input, Text } from "native-base";
import { connect } from "react-redux";
import firebase from "react-native-firebase";
import InputField from "../../components/form/InputField";

const numberBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
    let hasError = error !== undefined;
  
    return (
      <InputField icon="md-call" hasError={hasError} skipValidate>
        <Input
          keyboardType="numeric"
          autoCapitalize="none"
          placeholder="phone no"
          onChangeText={onChange}
          {...restInput}
        />
      </InputField>
    );
};

class EditPhoneForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const {
      handleSubmit,
      invalid
    } = this.props;

    return (
      <View>
        {/* Phone input */}
        <Field name="phone" component={numberBox} /> 
                
        <Button
          disabled={invalid}
          primary={!invalid}
          light={invalid}
          style={styles.button}
          block
          enable={!invalid}
          onPress={async val => {
            await handleSubmit(val);
          }}
        >
          <Text>Update</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  verifyForm: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "95%",
    padding: 30,
    marginLeft: "auto",
    marginRight: "auto"
  },
  verifyButton: {
    marginTop: 15,
    marginLeft: "auto",
    marginRight: "auto"
  },
  phoneView: {
    flex: 1,
    flexDirection: "row",
    padding: 0,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 5,
    marginBottom: 20
  },
  smsNotice: {
    fontSize: 10,
    marginLeft: "auto",
    marginRight: "auto"
  }
});

EditPhoneForm = reduxForm({
  // a unique name for the form
  form: "editPhoneForm",
  //validate
})(EditPhoneForm);

const selector = formValueSelector("editPhoneForm");
EditPhoneForm = connect(
  state => {
    return {
        initialValues: {
          phone: state.contacts.selectedContacts.phone
        },
        phone: selector(state, "phone"),
    };
  },
)(EditPhoneForm);

export default EditPhoneForm;
