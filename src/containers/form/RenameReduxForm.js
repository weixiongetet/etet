import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Field, reduxForm, isInvalid, formValueSelector } from "redux-form";
import { Button, Input, View, Text } from "native-base";
import { connect } from "react-redux";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.name = "";
  var name = values.name;

  if (values.name === undefined) name = "";

  // name
  if (name.length > 64) error.name = "max 64 characters";
  if (name.length < 4) error.name = "min 4 characters";
  if (name == "") error.name = "ignore";

  return error;
};

const nameBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField skipValidate icon="md-create" hasError={hasError}>
      <Input
        autoCapitalize="none"
        placeholder="New Name"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class RenameReduxForm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit, handleClose, invalid } = this.props;

    return (
      <View>
        <Field name="name" component={nameBox} />

        <View style={styles.buttonList}>
          <Button
            disabled={invalid}
            small
            transparent
            style={{ alignSelf: "flex-end" }}
            enable={!invalid}
            onPress={handleSubmit}
          >
            <Text>Rename</Text>
          </Button>

          <Button
            style={{ alignSelf: "flex-end" }}
            small
            transparent
            onPress={handleClose}
          >
            <Text>Close</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonList: {
    marginTop: 10
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

RenameReduxForm = reduxForm({
  form: "renameForm",
  validate
})(RenameReduxForm);

const selector = formValueSelector("renameForm");
RenameReduxForm = connect(state => {
  return {
    // firstName: selector(state, "firstName"),
    // lastName: selector(state, "lastName"),
    invalid: isInvalid("renameForm")(state)
  };
})(RenameReduxForm);

export default RenameReduxForm;
