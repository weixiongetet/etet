import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Field, reduxForm, isInvalid, formValueSelector } from "redux-form";
import { Button, Input, View, Text } from "native-base";
import { connect } from "react-redux";
import InputField from "../../components/form/InputField";

const validate = values => {
  const error = {};
  error.email = "";
  error.password = "";
  var ema = values.email;
  var pw = values.password;

  if (values.email === undefined) ema = "";
  if (values.password === undefined) pw = "";

  // email
  if (!ema.includes("@") && ema !== "") error.email = "@ not included";
  if (ema.length < 6) error.email = "too short";
  if (ema == "") error.email = "ignore";

  // password
  if (pw.length < 8) error.password = "too short";
  if (pw == "") error.password = "ignore";
  return error;
};

const emailBox = ({ input: { onChange, ...restInput }, meta: { error } }) => {
  let hasError = error !== undefined;

  return (
    <InputField icon="mail" hasError={hasError}>
      <Input
        keyboardType="email-address"
        autoCapitalize="none"
        placeholder="Email"
        onChangeText={onChange}
        {...restInput}
      />
    </InputField>
  );
};

class ResetReduxForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resetSent: false,
      email: ""
    };
  }

  render() {
    const { handleSubmit, invalid, email } = this.props;

    return (
      <View>
        <Field name="email" component={emailBox} />

        <Button
          disabled={invalid}
          primary={!invalid}
          light={invalid}
          style={styles.button}
          block
          text="Reset Password"
          enable={!invalid}
          onPress={async val => {
            await this.setState({ resetSent: false });
            let sent = await handleSubmit(val);
            this.setState({ resetSent: true, email: email }); // to protect email tried attack
          }}
          // onPress={handleSubmit(submit)} //for debug
        >
          <Text>Reset Password</Text>
        </Button>

        {this.state.resetSent ? (
          <Text style={styles.message}>{`A reset email had sent to ${
            this.state.email
          }.`}</Text>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 15
  },
  message: {
    textAlign: "center",
    marginTop: 10,
    color: "#757575",
    fontStyle: "italic"
  }
});

ResetReduxForm = reduxForm({
  form: "resetForm",
  validate
})(ResetReduxForm);

const selector = formValueSelector("resetForm");
ResetReduxForm = connect(state => {
  return {
    email: selector(state, "email"),
    invalid: isInvalid("resetForm")(state)
  };
})(ResetReduxForm);

export default ResetReduxForm;
