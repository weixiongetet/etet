import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { navReducer } from "../containers/AppNavigator";
import auth from "./modules/auth";
import chat from "./modules/chat";
import contacts from "./modules/contacts";
import notify from "./modules/notify";
import setting from "./modules/setting";
import todo from "./modules/todo";
import vault from "./modules/vault";

// const loggerMiddleware = createLogger(); // initialize logger
// const createStoreWithMiddleware = applyMiddleware(loggerMiddleware, promise)(createStore); // apply logger to redux

const reducers = combineReducers({
  todo,
  auth,
  setting,
  vault,
  contacts,
  chat,
  notify,
  form: formReducer,
  nav: navReducer
});

export default reducers;

// const configureStore = (initialState) => createStoreWithMiddleware(reducer, initialState);
// export default configureStore;
