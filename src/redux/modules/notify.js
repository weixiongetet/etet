
// Actions
const NEW_NOTIFY = "NOTIFY/NEW_NOTIFY";
const CLEAR_NOTIFY = "NOTIFY/CLEAR_NOTIFY";
const SET_NOTIFY = "NOTIFY/SET_NOTIFY";
const ADD_NOTIFY = "NOTIFY/ADD_NOTIFY";

// Reducer
const INITIAL_STATE = {
  home: 0,
  contact: 0,
  vault: 0,
  chat: 0,
  setting: 0
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `NOTIFY/NEW_NOTIFY`:
      let newNotify = { ...state };
      newNotify[action.module] += 1;
      return newNotify;
    case `NOTIFY/CLEAR_NOTIFY`:
      let clearNotify = { ...state };
      clearNotify[action.module] = 0;
      return clearNotify;
    case `NOTIFY/SET_NOTIFY`:
      let setNotify = { ...state };
      setNotify[action.module] = action.payload;
      return setNotify;
    case `NOTIFY/ADD_NOTIFY`:
      return {
        chat: action.payload
      };
    default:
      return state;
  }
}

// Action Creators
export const newNotification = module => async dispatch => {
  const action = {
    type: NEW_NOTIFY,
    module
  };

  return dispatch(action);
};
export const clearNotification = module => async dispatch => {
  const action = {
    type: CLEAR_NOTIFY,
    module
  };

  return dispatch(action);
};
export const setNotification = (module, count) => async dispatch => {
  const action = {
    type: SET_NOTIFY,
    module,
    payload: count
  };

  return dispatch(action);
};

export const addNotification = (item) => async dispatch => {
  const action = {
    type: ADD_NOTIFY,
    payload: item
  };

  return dispatch(action);
};