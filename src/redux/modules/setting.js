import { Platform } from "react-native";
import RNFetchBlob from "react-native-fetch-blob";
import firebase from "react-native-firebase";

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

// Actions
const EDIT_NAME = "SETTING/EDIT_NAME";
const UPLOAD_PROFILE_IMG = "SETTING/UPLOAD_PROFILE_IMG";
const GET_REMAINING = "SETTING/GET_REMAINING";
const CARD_SHARE = "SETTING/CARD_SHARE";
const SCAN_WEB = "SETTING/SCAN_WEB";
const UPD_CONNECT_INFO = "SETTING/UPD_CONNECT_INFO";


// Reducer
const INITIAL_STATE = {
  isFetching: false,
  hasError: false,
  errorMessage: "",
  planStatus: null,
  cardShareFlag: false,
  scanWebFlag: false,
  connectInfo: "default"
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `SETTING/EDIT_NAME_PENDING`:
    case `SETTING/UPLOAD_PROFILE_IMG_PENDING`:
      return { ...state, isFetching: true };
    case `SETTING/EDIT_NAME_FULFILLED`:
    case `SETTING/UPLOAD_PROFILE_IMG_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `SETTING/EDIT_NAME_REJECTED`:
    case `SETTING/UPLOAD_PROFILE_IMG_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        errorMessage: action.payload
      };
    case `SETTING/GET_REMAINING`:
      return {
        ...state,
        planStatus: action.payload
      };
    case `SETTING/CARD_SHARE`:
      return {
        ...state,
        cardShareFlag: action.payload
      };
    case `SETTING/SCAN_WEB`:
      return {
        ...state,
        scanWebFlag: action.payload
      };

    case `SETTING/UPD_CONNECT_INFO`:
      return {
        ...state,
        connectInfo: action.payload
      };
    default:
      return state;
  }
}

// Action Creators
export const checkRemainingStorage = () => async dispatch => {
  const userToken = await firebase.auth().currentUser.getIdToken(true);
  const callable = await firebase.functions().httpsCallable("remainingStorage");
  const { data } = await callable({ token: userToken });

  return dispatch({
    type: GET_REMAINING,
    payload: data
  });
};
export const editName = val => async dispatch => {
  const { _user } = firebase.auth().currentUser;
  const refA = firebase
    .firestore()
    .collection("users")
    .doc(_user.uid);

  const { firstName, lastName } = val;

  const act = async transaction => {
    return transaction.update(refA, {
      firstName,
      lastName
    });
  };
  const action = {
    type: EDIT_NAME,
    payload: firebase
      .firestore()
      .runTransaction(act)
      .catch(err => {
        throw err;
      })
  };
};
export const uploadProfileImg = ({ uri }) => async dispatch => {
  const mime = "image/jpeg";

  const uploadUri = Platform.OS === "ios" ? uri.replace("file://", "") : uri;
  const { currentUser } = firebase.auth();

  const imageRef = firebase
    .storage()
    .ref(`/${currentUser.uid}/profile`)
    .child("profile_pic");

  const imgPromise = imageRef
    .put(uploadUri, { contentType: mime })
    .then(() => {
      return imageRef.getDownloadURL();
    })
    .then(uri => {
      const { currentUser } = firebase.auth();
      const userRef = firebase
        .firestore()
        .collection("users")
        .doc(currentUser.uid);

      const act = async transaction => {
        return transaction.update(userRef, {
          profilePictureURL: uri
        });
      };

      return firebase
        .firestore()
        .runTransaction(act)
        .catch(err => {
          throw err;
        });
    })
    .catch(err => {
      throw err;
    });

  const action = {
    type: UPLOAD_PROFILE_IMG,
    payload: imgPromise
  };
};

export const setCardsShareFlag = val => async dispatch => {
  return dispatch({
    type: CARD_SHARE,
    payload: val
  });
};

export const updScanWebFlag = val => async dispatch => {
  return dispatch({
    type: SCAN_WEB,
    payload: val
  });
};

export const updConnnectInfo = val => async dispatch => {
  return dispatch({
    type: UPD_CONNECT_INFO,
    payload: val
  });
};