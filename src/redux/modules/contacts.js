import { parsePhoneNumberFromString } from "libphonenumber-js";
import { Platform } from "react-native";
import RNFetchBlob from "react-native-fetch-blob";
import firebase from "react-native-firebase";

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

// Actions
const EDIT_NAME = "CONTACTS/EDIT_NAME";
const UPLOAD_PROFILE_IMG = "CONTACTS/UPLOAD_PROFILE_IMG";
const CONTACTS_LIST = "CONTACTS/CONTACTS_LIST";
const SELECTED_CONTACTS = "CONTACTS/SELECTED_CONTACTS";
const FIND_CONTACTS_DETAILS = "CONTACTS/FIND_CONTACTS_DETAILS";
const FRIEND_REQUEST = "CONTACTS/FRIEND_REQUEST";
const GET_FRIENDS_REQUEST = "CONTACTS/GET_FRIENDS_REQUEST";
const GET_FRIENDS_LIST = "CONTACTS/GET_FRIENDS_LIST";
const ALL_CONTACTS = "CONTACTS/ALL_CONTACTS";
const UPDATE_FRIENDS_REQUEST_ARR = "CONTACTS/UPDATE_FRIENDS_REQUEST_ARR";
const FIND_USER_DETAILS = "CONTACTS/FIND_USER_DETAILS";
const UPDATE_SELECTED_USER_FLAG = "CONTACTS/UPDATE_SELECTED_USER_FLAG";
const UPDATE_DB_FRIENDS_ARR = "CONTACTS/UPDATE_DB_FRIENDS_ARR";
// const UPDATE_TWILIO_INITED = "CONTACTS/UPDATE_TWILIO_INITED";
const UPDATE_CALLING_FLAG = "CONTACTS/UPDATE_CALLING_FLAG";
const UPDATE_INCOMING_FLAG = "CONTACTS/UPDATE_INCOMING_FLAG";
const UPDATE_TIMER_FLAG = "CONTACTS/UPDATE_TIMER_FLAG";
const UPDATE_CALLFROM = "CONTACTS/UPDATE_CALLFROM";
const GET_COMPANY_REQUEST = "CONTACTS/GET_COMPANY_REQUEST";
const COMPANY_REQUEST = "CONTACTS/COMPANY_REQUEST";
const UPDATE_DB_COMPANY_ARR = "CONTACTS/UPDATE_DB_COMPANY_ARR";
const UPDATE_COMPANY_REQUEST_ARR = "CONTACTS/UPDATE_COMPANY_REQUEST_ARR";
const UPDATE_CARD_CONTACTS = "CONTACTS/UPDATE_CARD_CONTACTS";
const GET_CARD_CONTACT = "CONTACTS/GET_CARD_CONTACT";

// Reducer
const INITIAL_STATE = {
  contactsList: {},
  selectedContacts: {},
  selectedUserEtetId: "",
  friendsRequestArr: [],
  dbFriendsList: {},
  mergedContactsList: [],
  dbFriendsFlag: false,
  dbAcceptedFriendsList: [],
  selectedUserDetails: {},
  selectedUserStatusFlag: "",
  friendRequest: "",
  acceptedDeletedFlag: "",
  allContactList: {},
  contactFlag: false,
  showEditForm: false,
  twilioInited: false,
  callingFlag: false,
  incomingFlag: false,
  showTimerFlag: false,
  callFrom: null,
  companyRequestArr: [],
  dbCompanyList: {},
  cardContactsFlag: false,
  cardContactsList: {}
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `SETTING/EDIT_NAME_PENDING`:
    case `SETTING/UPLOAD_PROFILE_IMG_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/FIND_CONTACTS_DETAILS_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/FRIEND_REQUEST_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/GET_FRIENDS_REQUEST_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/GET_FRIENDS_LIST_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/FIND_USER_DETAILS_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/GET_COMPANY_REQUEST_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/COMPANY_REQUEST_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/UPDATE_CARD_CONTACTS_PENDING`:
      return { ...state, isFetching: true };
    case `CONTACTS/GET_CARD_CONTACT_PENDING`:
      return { ...state, isFetching: true };
    case `SETTING/EDIT_NAME_FULFILLED`:
    case `SETTING/UPLOAD_PROFILE_IMG_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `CONTACTS/FIND_CONTACTS_DETAILS_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        selectedUserStatusFlag: action.payload[0],
        selectedUserDetails: action.payload[1],
        showEditForm: true
      };
    case `CONTACTS/FRIEND_REQUEST_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        selectedUserStatusFlag: "pending"
      };
    case `CONTACTS/GET_FRIENDS_REQUEST_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        friendsRequestArr: action.payload
      };
    case `CONTACTS/GET_FRIENDS_LIST_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        dbFriendsFlag: true,
        dbFriendsList: action.payload !== undefined ? action.payload : {}
      };
    case `CONTACTS/FIND_USER_DETAILS_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        selectedUserStatusFlag: action.payload[0],
        selectedUserDetails: action.payload[1],
        selectedContacts: action.payload[2]
      };
    case `CONTACTS/GET_COMPANY_REQUEST_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        companyRequestArr: action.payload
      };
    case `CONTACTS/COMPANY_REQUEST_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false
      };
    case `CONTACTS/UPDATE_CARD_CONTACTS_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        cardContactsFlag: true
      };
    case `CONTACTS/GET_CARD_CONTACT_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        cardContactsList: action.payload
      };
    case `SETTING/EDIT_NAME_REJECTED`:
    case `SETTING/UPLOAD_PROFILE_IMG_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        errorMessage: action.payload
      };
    case `CONTACTS/FIND_CONTACTS_DETAILS_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/FRIEND_REQUEST_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/GET_FRIENDS_REQUEST_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/GET_FRIENDS_LIST_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/FIND_USER_DETAILS_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/GET_COMPANY_REQUEST_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/COMPANY_REQUEST_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/UPDATE_CARD_CONTACTS_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/GET_CARD_CONTACT_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true
      };
    case `CONTACTS/CONTACTS_LIST`:
      return {
        ...state,
        contactFlag: true,
        contactsList: action.payload
      };
    case `CONTACTS/SELECTED_CONTACTS`:
      return {
        ...state,
        selectedContacts: action.payload
      };
    case `CONTACTS/ALL_CONTACTS`:
      return {
        ...state,
        allContactList: action.payload
      };
    case `CONTACTS/UPDATE_FRIENDS_REQUEST_ARR`:
      return {
        ...state,
        friendsRequestArr: action.payload
      };
    case `CONTACTS/UPDATE_SELECTED_USER_FLAG`:
      return {
        ...state,
        selectedUserStatusFlag: action.payload
      };
    case `CONTACTS/UPDATE_DB_FRIENDS_ARR`:
      return {
        ...state,
        dbFriendsList: action.payload
      };
    // case `CONTACTS/UPDATE_TWILIO_INITED`:
    //   return {
    //     ...state,
    //     twilioInited: action.payload
    //   };
    case `CONTACTS/UPDATE_CALLING_FLAG`:
      return {
        ...state,
        callingFlag: action.payload
      };
    case `CONTACTS/UPDATE_INCOMING_FLAG`:
      return {
        ...state,
        incomingFlag: action.payload
      };
    case `CONTACTS/UPDATE_TIMER_FLAG`:
      return {
        ...state,
        showTimerFlag: action.payload
      };
    case `CONTACTS/UPDATE_CALLFROM`:
      return {
        ...state,
        callFrom: action.payload
      };
    case `CONTACTS/UPDATE_DB_COMPANY_ARR`:
      return {
        ...state,
        dbCompanyList: action.payload
      };
    case `CONTACTS/UPDATE_COMPANY_REQUEST_ARR`:
      return {
        ...state,
        companyRequestArr: action.payload
      };

    default:
      return state;
  }
}

// Action Creators
const updateContactName = val => {
  const { _user } = firebase.auth().currentUser;
  const refA = firebase
    .firestore()
    .collection("users")
    .doc(_user.uid);

  const { firstName, lastName } = val;

  const act = async transaction => {
    return transaction.update(refA, {
      firstName,
      lastName
    });
  };
  return {
    type: EDIT_NAME,
    payload: firebase
      .firestore()
      .runTransaction(act)
      .catch(err => {
        throw err;
      })
  };
};

const storeContactPicture = uri => {
  const mime = "image/jpeg";

  const uploadUri = Platform.OS === "ios" ? uri.replace("file://", "") : uri;
  const { currentUser } = firebase.auth();

  const imageRef = firebase
    .storage()
    .ref(`/images/${currentUser.uid}`)
    .child("profile_pic");

  const imgPromise = imageRef
    .put(uploadUri, { contentType: mime })
    .then(() => {
      return imageRef.getDownloadURL();
    })
    .then(uri => {
      const { currentUser } = firebase.auth();
      const userRef = firebase
        .firestore()
        .collection("users")
        .doc(currentUser.uid);

      const act = async transaction => {
        return transaction.update(userRef, {
          profilePictureURL: uri
        });
      };

      return firebase
        .firestore()
        .runTransaction(act)
        .catch(err => {
          throw err;
        });
    })
    .catch(err => {
      throw err;
    });

  return {
    type: UPLOAD_PROFILE_IMG,
    payload: imgPromise
  };
};

const updateContacts = val => {
  res = val.map(item => {
    let tempPhoneNumber = item.phoneNumbers;
    let phone =
      tempPhoneNumber[0] !== undefined
        ? tempPhoneNumber[0].number.replace(/[^a-zA-Z0-9]/g, "").toString()
        : "";

    let countryCode = "";
    let phoneNumber = "";
    let formatPhone = "";

    if (phone.length == "10") {
      var getStr = phone.substring(0, 2);
      if (getStr == "65") {
        formatPhone = "+" + phone;
      } else {
        phoneNumber = parsePhoneNumberFromString(phone, "MY");
        formatPhone = phoneNumber ? phoneNumber.number : "";
      }
    } else if (phone.length == "8") {
      phoneNumber = parsePhoneNumberFromString(phone, "SG");
      formatPhone = phoneNumber ? phoneNumber.number : "";
    } else {
      formatPhone = "+" + phone;
    }

    let firstName = item.givenName !== null ? item.givenName : "";
    let middleName = item.middleName !== null ? item.middleName : "";
    let lastName = item.familyName !== null ? item.familyName : "";
    let emailAddresses =
      item.emailAddresses !== null && item.emailAddresses[0]
        ? item.emailAddresses[0].email
        : "";

    return {
      name: firstName + " " + middleName + " " + lastName,
      phone: formatPhone,
      email: emailAddresses
    };
  });

  return {
    type: CONTACTS_LIST,
    payload: res
  };
};

const updateSelectedContacts = ({ items }) => {
  return {
    type: SELECTED_CONTACTS,
    payload: items
  };
};

const isFriend = ({ items }) => {
  const { _user } = firebase.auth().currentUser;
  const selectedCont = items.phone ? items.phone.replace(/\s/g, "") : null;
  const selectedEmail = items.email ? items.email : null;

  const act = () => {
    let friendData = {};
    let checkisFriendsFlag = "";

    if (selectedCont == null && selectedEmail !== null) {
      let returnData = [];
      returnData = firebase
        .firestore()
        .collection("friends")
        .doc(_user.uid)
        .get()
        .then(snapshot => {
          if (snapshot.data() === undefined) {
            return Promise.all([false, null]); //etet user and not friends, add btn
          } else {
            const friendVal = Object.values(snapshot.data());

            for (const val of friendVal) {
              if (val.email == selectedEmail) {
                if (val.status == "accepted") {
                  return Promise.all([true, val]); //is friends, send msg btn
                } else if (
                  val.status == "pending" ||
                  val.status == "requested"
                ) {
                  return Promise.all(["pending", val]); //request sended, added msg btn
                } else {
                  //deleted
                  return Promise.all([false, val]); //etet user and not friends, add btn
                }
              }
            }

            // return Promise.all([false, {}]) //etet user and not friends, add btn
          }
        })
        .catch(err => {
          throw err;
        });

      return Promise.resolve(returnData);
    } else {
      return firebase
        .firestore()
        .collection("users")
        .where("phone", "==", selectedCont)
        .get()
        .then(res => {
          if (res.empty) {
            // not etet user
            checkisFriendsFlag = Promise.resolve("not"); //invite btn
          } else {
            const friendID = res.docs[0].data().userID;
            friendData = res.docs[0].data();

            checkisFriendsFlag = firebase
              .firestore()
              .collection("friends")
              .doc(_user.uid)
              .get()
              .then(snapshot => {
                if (snapshot.data() === undefined) {
                  return Promise.resolve(false); //etet user and not friends, add btn
                } else {
                  const keys = Object.keys(snapshot.data());
                  for (const key of keys) {
                    if (key == friendID) {
                      if (snapshot.data()[key]) {
                        if (snapshot.data()[key].status == "accepted") {
                          return Promise.resolve(true); //is friends, send msg btn
                        } else if (
                          snapshot.data()[key].status == "pending" ||
                          snapshot.data()[key].status == "requested"
                        ) {
                          return Promise.resolve("pending"); //request sended, added msg btn
                        } else {
                          //deleted
                          return Promise.resolve(false); //etet user and not friends, add btn
                        }
                      }
                    }
                  }
                }
              })
              .catch(err => {
                throw err;
              });
          }
          return Promise.all([checkisFriendsFlag, friendData]);
        })
        .catch(err => {
          throw err;
        });
    }
  };

  return {
    type: FIND_CONTACTS_DETAILS,
    payload: act()
  };
};

const friendsRequestAct = () => {
  const { _user } = firebase.auth().currentUser;

  let pendingFriendsArr = [];

  const docRef = firebase
    .firestore()
    .collection("friends")
    .doc(_user.uid);

  docRef.onSnapshot(
    docSnapshot => {
      const keys = Object.values(docSnapshot._data);
      for (const key of keys) {
        if (key.status == "pending") {
          pendingFriendsArr.push(docSnapshot._data[key.userID]);
        }
      }
    },
    err => {
      throw err;
    }
  );

  return {
    type: GET_FRIENDS_REQUEST,
    payload: pendingFriendsArr
  };
};

const viewFriendRequest = () => {
  const { _user } = firebase.auth().currentUser;

  const act = () => {
    let pendingFriendsArr = [];

    return firebase
      .firestore()
      .collection("friends")
      .doc(_user.uid)
      .get()
      .then(snapshot => {
        if (snapshot.data() === undefined) {
          return Promise.resolve(false); //no db friends
        } else {
          const keys = Object.values(snapshot.data());
          for (const key of keys) {
            if (key.status == "pending") {
              pendingFriendsArr.push(snapshot.data()[key.userID]);
            }
          }

          return Promise.resolve(pendingFriendsArr);
        }
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: GET_FRIENDS_REQUEST,
    payload: act()
  };
};

const viewCompanyRequest = () => {
  const { _user } = firebase.auth().currentUser;

  const act = () => {
    let pendingComArr = [];

    return firebase
      .firestore()
      .collection("users")
      .doc(_user.uid)
      .collection("companies")
      .where("status", "==", false)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          const tmp = doc.data();
          pendingComArr.push({
            ...tmp
          });
        });
        return Promise.resolve(pendingComArr);
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: GET_COMPANY_REQUEST,
    payload: act()
  };
};

const submitBtnAction = ({ items, user, status }) => {
  let friendID = items.userID;
  const { _user } = firebase.auth().currentUser;
  const statusB = status == "requested" ? "pending" : status;

  const updateObj = {};
  updateObj[friendID] = {
    status: status, //requested
    firstName: items.firstName ? items.firstName : null,
    lastName: items.lastName ? items.lastName : null,
    phone: items.phone ? items.phone : null,
    email: items.email ? items.email : null,
    userID: friendID ? friendID : null,
    etetID: items.etetID ? items.etetID : null,
    profilePictureURL: items.profilePictureURL ? items.profilePictureURL : null,
    fcmToken: items.fcmToken ? items.fcmToken : null,
    address: items.postalAddresses ? items.postalAddresses : null,
    operationHour: items.opHour ? items.opHour : null,
    jobTitle: items.jobTitle ? items.jobTitle : null,
    company: items.company ? items.company : null
  };

  const updateObjB = {};
  updateObjB[_user.uid] = {
    status: statusB,
    firstName: user.firstName,
    lastName: user.lastName,
    phone: user.phone,
    email: user.email,
    userID: user.userID,
    etetID: user.etetID,
    profilePictureURL: user.profilePictureURL,
    fcmToken: user.fcmToken
  };

  const act = async () => {
    const refA = firebase
      .firestore()
      .collection("friends")
      .doc(_user.uid);

    const refB = firebase
      .firestore()
      .collection("friends")
      .doc(friendID);

    if (status == "requested") {
      //add friends

      refA
        .get()
        .then(docA => {
          if (!docA.exists) {
            refA
              .set(updateObj)
              .then(function() {
                refB
                  .get()
                  .then(docB => {
                    if (!docB.exists) {
                      refB
                        .set(updateObjB)
                        .then(function() {
                          return Promise.resolve(statusB);
                        })
                        .catch(function(error) {
                          throw error;
                        });
                    } else {
                      refB
                        .update(updateObjB)
                        .then(function() {
                          return Promise.resolve(statusB);
                        })
                        .catch(function(error) {
                          throw error;
                        });
                    }
                  })
                  .catch(err => {
                    console.log("Error getting document", err);
                  });
              })
              .catch(function(error) {
                throw error;
              });
          } else {
            refA
              .update(updateObj)
              .then(function() {
                refB
                  .get()
                  .then(docB => {
                    if (!docB.exists) {
                      refB
                        .set(updateObjB)
                        .then(function() {
                          return Promise.resolve(statusB);
                        })
                        .catch(function(error) {
                          throw error;
                        });
                    } else {
                      refB
                        .update(updateObjB)
                        .then(function() {
                          return Promise.resolve(statusB);
                        })
                        .catch(function(error) {
                          throw error;
                        });
                    }
                  })
                  .catch(err => {
                    console.log("Error getting document", err);
                  });
              })
              .catch(function(error) {
                throw error;
              });
          }
        })
        .catch(err => {
          console.log("Error getting document", err);
        });
    } else {
      //accept or delete friends
      return refA
        .update(updateObj)
        .then(function() {
          return refB
            .update(updateObjB)
            .then(function() {
              return Promise.resolve(status);
            })
            .catch(function(error) {
              throw error;
            });
        })
        .catch(function(error) {
          throw error;
        });
    }
  };

  return {
    type: FRIEND_REQUEST,
    payload: act()
  };
};

const getFriends = () => {
  const { _user } = firebase.auth().currentUser;
  var dbFriendsArr = [];
  var updFriendDetails = [];
  let acceptedDBfriend = [];
  let updateObj = {};
  const refA = firebase
    .firestore()
    .collection("friends")
    .doc(_user.uid);

  const act = () => {
    return firebase
      .firestore()
      .collection("friends")
      .doc(_user.uid)
      .get()
      .then(snapshot => {
        if (snapshot.data() !== undefined) {
          const keys = Object.values(snapshot.data());

          for (const key of keys) {
            if (key.status == "accepted") {
              firebase
                .firestore()
                .collection("users")
                .doc(key.userID)
                .get()
                .then(res => {
                  if (res.data() !== undefined) {
                    snapshot.data()[key.userID];

                    userDetails = res.data();

                    updateObj[userDetails.userID] = {
                      status: "accepted",
                      firstName: userDetails.firstName,
                      lastName: userDetails.lastName,
                      phone: userDetails.phone,
                      email: userDetails.email,
                      etetID: userDetails.etetID,
                      userID: userDetails.userID,
                      profilePictureURL: userDetails.profilePictureURL,
                      fcmToken: userDetails.fcmToken
                    };

                    refA
                      .update(updateObj)
                      .then(function() {})
                      .catch(function(error) {
                        throw error;
                      });
                  }
                });

              dbFriendsArr.push(key);
            }
          }

          return Promise.resolve(dbFriendsArr);
        }
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: GET_FRIENDS_LIST,
    payload: act()
  };
};

const allContactsAct = ({ items }) => {
  return {
    type: ALL_CONTACTS,
    payload: items
  };
};

const updFriendsRequestArrAct = ({ items }) => {
  return {
    type: UPDATE_FRIENDS_REQUEST_ARR,
    payload: items
  };
};

const getUserDetailsAct = ({ items }) => {
  const { _user } = firebase.auth().currentUser;
  const etetUserID = items;

  const act = () => {
    let userDetails = {};
    let checkisFriendsFlag = "";
    let etetNamePhone = {};

    return firebase
      .firestore()
      .collection("users")
      .doc(etetUserID)
      .get()
      .then(res => {
        if (res.data() === undefined) {
          // not etet user
          console.log("No Etet user exist!");
          // checkisFriendsFlag = Promise.resolve('not'); //invite btn
        } else {
          userDetails = res.data();
          let tempName = userDetails.firstName + " " + userDetails.lastName;
          etetNamePhone = { name: tempName, phone: userDetails.phone };

          checkisFriendsFlag = firebase
            .firestore()
            .collection("friends")
            .doc(_user.uid)
            .get()
            .then(snapshot => {
              if (snapshot.data() === undefined) {
                return Promise.resolve(false); //etet user and not friends, add btn
              } else {
                const keys = Object.keys(snapshot.data());
                for (const key of keys) {
                  if (key == etetUserID) {
                    if (snapshot.data()[key]) {
                      if (snapshot.data()[key].status == "accepted") {
                        return Promise.resolve(true); //is friends, no btn
                      } else if (
                        snapshot.data()[key].status == "pending" ||
                        snapshot.data()[key].status == "requested"
                      ) {
                        return Promise.resolve("pending"); //request sended
                      } else {
                        //deleted
                        return Promise.resolve(false); //etet user and not friends, add btn
                      }
                    }
                  }
                }
              }
            })
            .catch(err => {
              throw err;
            });
        }

        return Promise.all([checkisFriendsFlag, userDetails, etetNamePhone]);
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: FIND_USER_DETAILS,
    payload: act()
  };
};

const updSelectedUserFlagAct = val => {
  return {
    type: UPDATE_SELECTED_USER_FLAG,
    payload: val
  };
};

const updDBfriendsListArrAct = ({ items }) => {
  return {
    type: UPDATE_DB_FRIENDS_ARR,
    payload: items
  };
};

const updCallingFlagAct = item => {
  return {
    type: UPDATE_CALLING_FLAG,
    payload: item
  };
};

const updIncomingFlagAct = item => {
  return {
    type: UPDATE_INCOMING_FLAG,
    payload: item
  };
};

const updShowTimerFlagAct = item => {
  return {
    type: UPDATE_TIMER_FLAG,
    payload: item
  };
};

const updCallFromAct = ({ items }) => {
  return {
    type: UPDATE_CALLFROM,
    payload: items
  };
};

const searchUserAct = ({ items, searchType }) => {
  const { _user } = firebase.auth().currentUser;

  const act = () => {
    let userDetails = {};
    let checkisFriendsFlag = "";
    let etetNamePhone = {};
    let inputItem = searchType == "email" ? items.toLowerCase() : items;

    return firebase
      .firestore()
      .collection("users")
      .where(searchType, "==", inputItem)
      .get()
      .then(res => {
        if (res.empty) {
          // not etet user
          console.log("No Etet user exist!");
        } else {
          userDetails = res.docs[0].data();

          if (userDetails.private) {
            return Promise.all(["", {}, {}]);
          }

          let tempName = userDetails.firstName + " " + userDetails.lastName;
          etetNamePhone = { name: tempName, phone: userDetails.phone };
          const etetUserID = userDetails.userID;

          checkisFriendsFlag = firebase
            .firestore()
            .collection("friends")
            .doc(_user.uid)
            .get()
            .then(snapshot => {
              if (snapshot.data() === undefined) {
                return Promise.resolve(false); //etet user and not friends, add btn
              } else {
                const keys = Object.keys(snapshot.data());
                for (const key of keys) {
                  if (key == etetUserID) {
                    if (snapshot.data()[key]) {
                      if (snapshot.data()[key].status == "accepted") {
                        return Promise.resolve(true); //is friends, no btn
                      } else if (
                        snapshot.data()[key].status == "pending" ||
                        snapshot.data()[key].status == "requested"
                      ) {
                        return Promise.resolve("pending"); //request sended
                      } else {
                        //deleted
                        return Promise.resolve(false); //etet user and not friends, add btn
                      }
                    }
                  }
                }
              }
            })
            .catch(err => {
              throw err;
            });
        }

        return Promise.all([checkisFriendsFlag, userDetails, etetNamePhone]);
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: FIND_USER_DETAILS,
    payload: act()
  };
};

const comRequestBtnAction = ({ items, status }) => {
  const companyID = items.comid;
  const { _user } = firebase.auth().currentUser;

  const act = async () => {
    if (status == "delete") {
      firebase
        .firestore()
        .collection("users")
        .doc(_user.uid)
        .collection("companies")
        .doc(companyID)
        .delete()
        .then(function() {
          firebase
            .firestore()
            .collection("companies")
            .doc(companyID)
            .collection("staffs")
            .doc(_user.uid)
            .delete()
            .then(function() {
              console.log("Deleted companies success");
            })
            .catch(function(error) {
              console.log("error delete companies docs");
            });

          console.log("Deleted users success");
        })
        .catch(function(error) {
          console.log("error delete users docs");
        });
    } else {
      firebase
        .firestore()
        .collection("users")
        .doc(_user.uid)
        .collection("companies")
        .doc(companyID)
        .update({
          status: status
        })
        .then(function() {
          firebase
            .firestore()
            .collection("companies")
            .doc(companyID)
            .collection("staffs")
            .doc(_user.uid)
            .update({
              status: status
            })
            .then(function() {
              console.log("update companies docs success");
            })
            .catch(function(error) {
              console.log("error update companies docs");
            });

          console.log("update users docs succes");
        })
        .catch(function(error) {
          console.log("error update users docs");
        });
    }
  };

  return {
    type: COMPANY_REQUEST,
    payload: act()
  };
};

const updDBacceptedCompanyAct = ({ items }) => {
  return {
    type: UPDATE_DB_COMPANY_ARR,
    payload: items
  };
};

const updCompanyRequestArrAct = ({ items }) => {
  return {
    type: UPDATE_COMPANY_REQUEST_ARR,
    payload: items
  };
};

const addCardContactsAct = ({ items }) => {
  let friendPhone = items.phone;
  const { _user } = firebase.auth().currentUser;

  const updateObj = {};
  updateObj[friendPhone] = {
    firstName: items.name ? items.name : null,
    phone: items.phone ? items.phone : null,
    email: items.email ? items.email : null,
    address: items.postalAddresses ? items.postalAddresses : null,
    operationHour: items.opHour ? items.opHour : null,
    jobTitle: items.jobTitle ? items.jobTitle : null,
    company: items.company ? items.company : null,
    others: items.others ? items.others : null,
    cardContacts: true
  };

  const act = async () => {
    const refA = firebase
      .firestore()
      .collection("contacts")
      .doc(_user.uid);

    refA
      .get()
      .then(docA => {
        if (!docA.exists) {
          //set data
          refA
            .set(updateObj)
            .then(function() {
              return Promise.resolve(true);
            })
            .catch(function(error) {
              throw error;
            });
        } else {
          //upd data
          refA
            .update(updateObj)
            .then(function() {
              return Promise.resolve(true);
            })
            .catch(function(error) {
              throw error;
            });
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
  };

  return {
    type: UPDATE_CARD_CONTACTS,
    payload: act()
  };
};

const getCardContactsAct = () => {
  const { _user } = firebase.auth().currentUser;

  const act = () => {
    let contactData = {};

    return firebase
      .firestore()
      .collection("contacts")
      .doc(_user.uid)
      .get()
      .then(res => {
        if (res.data() === undefined) {
          console.log("No contacts!");
        } else {
          contactData = res.data();
          console.log("datat", contactData);
        }

        return Promise.resolve(contactData);
      })
      .catch(err => {
        throw err;
      });
  };

  return {
    type: GET_CARD_CONTACT,
    payload: act()
  };
};

// Operations
export const setContacts = val => dispatch => {
  return dispatch(updateContacts(val));
};

export const setSelectedContacts = val => dispatch => {
  dispatch(updSelectedUserFlagAct(""));
  dispatch(updateSelectedContacts(val));
  return dispatch(isFriend(val));
};

export const editContactName = val => async dispatch => {
  return dispatch(updateContactName(val))
    .then(() => {
      //return dispatch(getUser());
    })
    .catch(err => {
      throw err;
    });
};

export const uploadContactPrfImg = ({ uri }) => async dispatch => {
  return dispatch(storeContactPicture(uri))
    .then(() => {
      //return dispatch(getUser());
    })
    .catch(err => {
      throw err;
    });
};

export const editContactPhone = val => async dispatch => {
  return dispatch(storeContactPhone(val))
    .then(() => {
      // return dispatch(getUser());
    })
    .catch(err => {
      throw err;
    });
};

export const getFriendsRequest = () => dispatch => {
  return dispatch(viewFriendRequest());
};

export const getCompanyRequest = () => async dispatch => {
  return dispatch(viewCompanyRequest());
};

export const addFriends = val => dispatch => {
  return dispatch(submitBtnAction(val));
};

export const submitBtn = val => dispatch => {
  return dispatch(submitBtnAction(val))
    .then(() => {
      return dispatch(viewFriendRequest())
        .then(() => {
          return dispatch(viewCompanyRequest());
        })
        .catch(err => {
          throw err;
        });
    })
    .catch(err => {
      throw err;
    });
};

export const getDB_Friends = () => dispatch => {
  return dispatch(getFriends());
};

export const updateAllContacts = val => dispatch => {
  return dispatch(allContactsAct(val));
};

export const friendsRequest = () => dispatch => {
  return dispatch(friendsRequestAct());
};

export const updFriendsRequestArr = val => dispatch => {
  return dispatch(updFriendsRequestArrAct(val));
};

export const getUserDetails = val => dispatch => {
  return dispatch(getUserDetailsAct(val));
};

export const updSelectedUserFlag = val => dispatch => {
  return dispatch(updSelectedUserFlagAct(val));
};

export const updDBfriendsListArr = val => dispatch => {
  return dispatch(updDBfriendsListArrAct(val));
};

export const searchUser = val => dispatch => {
  return dispatch(searchUserAct(val));
};

// export const twilioCallInited = (val) => dispatch => {
//   return dispatch(updtwilioInited(val))
// };

export const updCallingFlag = val => dispatch => {
  return dispatch(updCallingFlagAct(val));
};

export const updIncomingFlag = val => dispatch => {
  return dispatch(updIncomingFlagAct(val));
};

export const updShowTimerFlag = val => dispatch => {
  return dispatch(updShowTimerFlagAct(val));
};

export const updCallFrom = val => dispatch => {
  return dispatch(updCallFromAct(val));
};

export const comRequestBtn = val => dispatch => {
  return dispatch(comRequestBtnAction(val))
    .then(() => {
      return dispatch(viewFriendRequest())
        .then(() => {
          return dispatch(viewCompanyRequest());
        })
        .catch(err => {
          throw err;
        });
    })
    .catch(err => {
      throw err;
    });
};

export const updDBacceptedCompany = val => dispatch => {
  return dispatch(updDBacceptedCompanyAct(val));
};

export const updCompanyRequestArr = val => dispatch => {
  return dispatch(updCompanyRequestArrAct(val));
};

export const addCardContacts = val => dispatch => {
  return dispatch(addCardContactsAct(val));
};

export const getCardContacts = () => dispatch => {
  return dispatch(getCardContactsAct());
};
