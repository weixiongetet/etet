import { Platform } from "react-native";
import RNFetchBlob from "react-native-fetch-blob";
import FileViewer from 'react-native-file-viewer';
import firebase from "react-native-firebase";
import RNFS from 'react-native-fs';

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

// Actions
const TRANS = "VAULT/TRANS";
const GET_FILES = "VAULT/GET_FILES";
const DOWNLOAD_PROGRESS = "VAULT/DOWNLOAD_PROGRESS";
const GET_GROUPS = "VAULT/GET_GROUPS";
const GET_SHARED_FILES = "VAULT/GET_SHARED_FILES";
const SELECT_FILE = "VAULT/SELECT_FILE";
const SELECT_GROUP = "VAULT/SELECT_GROUP";
const SET_FILES = `VAULT/SET_FILES`;
const SET_FOLDERS = `VAULT/SET_FOLDERS`;
const SET_SHARED_FILES = `VAULT/SET_SHARED_FILES`;
const SET_UPLOAD_PROGRESS = `VAULT/SET_UPLOAD_PROGRESS`;

// Reducer
const INITIAL_STATE = {
  files: [],
  groups: [],
  sharedFiles: [],
  selectedFile: null,
  selectedGroup: null,
  downloadPercent: 0,
  isFetching: false,
  hasError: false,
  errorMessage: "",
  uploadProgress: 0
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `VAULT/TRANS_PENDING`:
    case `VAULT/GET_FILES_PENDING`:
    case `VAULT/GET_GROUPS_PENDING`:
    case `VAULT/GET_SHARED_FILES_PENDING`:
      return { ...state, isFetching: true };
    case `VAULT/TRANS_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `VAULT/GET_FILES_FULFILLED`:
      return {
        ...state,
        files: action.payload,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `VAULT/GET_GROUPS_FULFILLED`:
      return {
        ...state,
        groups: action.payload,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `VAULT/GET_SHARED_FILES_FULFILLED`:
      return {
        ...state,
        sharedFiles: action.payload,
        isFetching: false,
        hasError: false,
        errorMessage: ""
      };
    case `VAULT/TRANS_REJECTED`:
    case `VAULT/GET_FILES_REJECTED`:
    case `VAULT/GET_GROUPS_REJECTED`:
    case `VAULT/GET_SHARED_FILES_REJECTED`:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        errorMessage: action.payload
      };
    case `VAULT/DOWNLOAD_PROGRESS`:
      return {
        ...state,
        downloadPercent: action.payload
      };

    case `VAULT/SELECT_FILE`:
      return {
        ...state,
        selectedFile: action.payload
      };

    case `VAULT/SELECT_GROUP`:
      return {
        ...state,
        selectedGroup: action.payload
      };

    case `VAULT/SET_FILES`:
      return {
        ...state,
        files: action.payload
      };

    case `VAULT/SET_FOLDERS`:
      return {
        ...state,
        groups: action.payload
      };

    case `VAULT/SET_SHARED_FILES`:
      return {
        ...state,
        sharedFiles: action.payload
      };

    case `VAULT/SET_UPLOAD_PROGRESS`:
      return {
        ...state,
        uploadProgress: action.payload
      };

    default:
      return state;
  }
}

// helper
const currentDate = () => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  var today = new Date();
  return `${
    monthNames[today.getMonth()]
    } ${today.getDate()}, ${today.getFullYear()}`;
};


function getLocalPath(fileName) {
  console.log("fileName", fileName);
  return `${RNFS.DocumentDirectoryPath}/${fileName}`;
}

// File Action Creators
export const getFiles = () => async dispatch => {
  const act = async () => {
    return await firebase
      .firestore()
      .collection("files")
      .where("owner", "==", firebase.auth().currentUser.uid)
      .onSnapshot(async snapshot => {
        let res = snapshot.docs.map(item => {
          const file = item.data();
          return {
            id: item.id,
            ...file
          };
        });
        return Promise.resolve(res);
      })
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: GET_FILES,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const addFile = ({ file, path }) => async dispatch => {
  try {

    const uploadUri =
      Platform.OS === "ios" ? file.uri.replace("file://", "") : file.uri;

    const docRef = firebase
      .firestore()
      .collection(`files`)
      .doc();

    const fileRef = firebase
      .storage()
      .ref(`/${firebase.auth().currentUser.uid}/${docRef.id}`)
      .child(file.fileName);

    const userToken = await firebase.auth().currentUser.getIdToken(true);
    const callable = await firebase.functions().httpsCallable("uploadToken");
    const { data } = await callable({
      token: userToken,
      path: `${firebase.auth().currentUser.uid}/${docRef.id}/${file.fileName}`
    });

    if (data.remainingStorage < file.fileSize) {
      throw new Error("You have reached the storage limit.");
    }

    await firebase.auth().signInWithCustomToken(data.newToken);

    let uploadTask = fileRef.put(uploadUri, { contentType: file.type });

    await new Promise((resolve, reject) => {
      let listenUpload = uploadTask.on(
        'state_changed',
        taskSnapshot => {
          var progress =
            taskSnapshot.bytesTransferred / taskSnapshot.totalBytes;

          dispatch({
            type: SET_UPLOAD_PROGRESS,
            payload: progress
          });

          if (taskSnapshot.state === firebase.storage.TaskState.SUCCESS) {
            listenUpload();
            resolve("Total bytes uploaded: ", taskSnapshot.totalBytes);
          }
        },
        error => {
          listenUpload();
          reject(error);
        }
      );
    });

    await dispatch({
      type: SET_UPLOAD_PROGRESS,
      payload: 0
    });

    const uri = await fileRef.getDownloadURL();

    const saveFile = docRef.set({
      name: file.fileName.split(".")[0],
      ext: file.fileName.split(".")[1],
      uri,
      size: file.fileSize,
      mime: file.type,
      owner: firebase.auth().currentUser.uid,
      share: {},
      modified: currentDate(),
      path
    });

    const action = {
      type: TRANS,
      payload: saveFile
    };

    return dispatch(action);
  } catch (err) {
    throw err;
  }
};
export const renameFile = ({ fileId, newName }) => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("files")
      .doc(fileId)
      .update({
        name: newName,
        modified: currentDate()
      })
      .catch(err => {
        throw err;
      });
  };
  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const deleteFile = ({ fileId }) => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("files")
      .doc(fileId)
      .delete()
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const openFile = ({ name, ext, uri, mime }) => async dispatch => {
  const fileName = `${name}.${ext}`;
  const localFile = getLocalPath(fileName);
  const options = {
    fromUrl: uri,
    toFile: localFile
  };

  await dispatch({
    type: DOWNLOAD_PROGRESS,
    payload: 50
  });

  return await RNFetchBlob.fs
    .exists(localFile)
    .then(exist => {
      if (exist) {
        //open 
        return FileViewer.open(localFile)
          .then(() => {
            // success
            const progress = 1 * 100;
            dispatch({
              type: DOWNLOAD_PROGRESS,
              payload: progress
            });
          })
          .catch(error => {
            console.log("error", error)
          });
      }

      //download and open
      return RNFS.downloadFile(options).promise
        .then(() => FileViewer.open(localFile))
        .then(() => {
          consol.log();
          console.log("download successs");

          // success
          const progress = 1 * 100;
          dispatch({
            type: DOWNLOAD_PROGRESS,
            payload: progress
          });
        })
        .catch(error => {
          console.log("error", error)
        });
    })
};

// export const openFile = ({ name, ext, uri, mime }) => async dispatch => {
//   const dirs = RNFetchBlob.fs.dirs;
//   const android = RNFetchBlob.android;

//   const filePath = `${dirs.DownloadDir}/${name}.${ext}`;
//   console.log("filePath", filePath);

//   await dispatch({
//     type: DOWNLOAD_PROGRESS,
//     payload: 0
//   });

//   return await RNFetchBlob.fs
//     .exists(filePath)
//     .then(exist => {
//       if (exist) {
//         return android.actionViewIntent(filePath, mime);
//       }

//       return RNFetchBlob.config({
//         path: filePath
//       })
//         .fetch("GET", uri)
//         .progress({ count: 10 }, (received, total) => {
//           const progress = (received / total) * 100;
//           dispatch({
//             type: DOWNLOAD_PROGRESS,
//             payload: progress
//           });
//         })
//         .then(res => {
//           console.log("The file saved to ", res.path());
//           return android.actionViewIntent(res.path(), mime);

//           // return FileProvider.getUriForFile("com.etetsg.fileprovider", res.path()).then(
//           //   contentUri => {
//           //     return Linking.openURL(contentUri).catch(err => {
//           //       throw err
//           //     });
//           //     // Linking.openURL("content:///storage/emulated/0/Download/CatHat1.jpg")
//           //   }
//           // );
//         })
//         .catch(err => {
//           throw err;
//         });
//     })
//     .catch(err => {
//       throw err;
//     });
// };

export const shareFile = ({
  fileId,
  friendId,
  friendFCM,
  allow
}) => async dispatch => {
  const update = {};
  update[`share.${friendId}`] = allow;
  update[`shareFCM.${friendId}`] = friendFCM;

  const act = () => {
    return firebase
      .firestore()
      .collection("files")
      .doc(fileId)
      .update(update)
      .catch(err => {
        throw err;
      });
  };
  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};

// Group Action Creators
export const getGroups = () => async dispatch => {
  const act = async () => {
    return await firebase
      .firestore()
      .collection("folders")
      .where("owner", "==", firebase.auth().currentUser.uid)
      .get()
      .then(async snapshot => {
        let res = snapshot.docs.map(item => {
          const group = item.data();
          return {
            id: item.id,
            ...group
          };
        });
        return Promise.resolve(res);
      })
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: GET_GROUPS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const addGroup = ({ name, path }) => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("folders")
      .add({
        name,
        path,
        owner: firebase.auth().currentUser.uid
      })
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const renameGroup = ({ groupId, newName }) => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("folders")
      .doc(groupId)
      .update({
        name: newName,
        modified: currentDate()
      })
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const deleteGroup = ({ path, id }) => async dispatch => {
  var strSearch = `${path},${id}`;
  var strlength = strSearch.length;
  var strFrontCode = strSearch.slice(0, strlength - 1);
  var strEndCode = strSearch.slice(strlength - 1, strSearch.length);

  var startcode = strSearch;
  var endcode =
    strFrontCode + String.fromCharCode(strEndCode.charCodeAt(0) + 1);

  const act = () => {
    var batch = firebase.firestore().batch();

    const folderPromise = firebase
      .firestore()
      .collection("folders")
      .where("path", ">", startcode)
      .where("path", "<", endcode)
      .get()
      .then(function (querySnapshot) {
        // delete self
        batch.delete(
          firebase
            .firestore()
            .collection("folders")
            .doc(id)
        );

        // delete all subFolders
        querySnapshot.forEach(function (doc) {
          batch.delete(doc.ref);
        });

        return Promise.resolve();
      });

    const filePromise = firebase
      .firestore()
      .collection("files")
      .where("path", ">", startcode)
      .where("path", "<", endcode)
      .get()
      .then(function (querySnapshot) {
        // delete all files
        querySnapshot.forEach(function (doc) {
          console.log("file", doc.ref);
          batch.delete(doc.ref);
        });

        return Promise.resolve();
      });

    return Promise.all([folderPromise, filePromise]).then(() => {
      return batch.commit();
    });
  };

  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};
export const joinGroup = ({ fileId, groupId }) => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("files")
      .doc(fileId)
      .update({
        group: groupId
      })
      .catch(err => {
        throw err;
      });
  };
  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};

// Shared File Action Creators
export const getSharedFiles = () => async dispatch => {
  const act = async () => {
    return await firebase
      .firestore()
      .collection("files")
      .where(`share.${firebase.auth().currentUser.uid}`, "==", true)
      .get()
      .then(async snapshot => {
        let res = snapshot.docs.map(item => {
          const file = item.data();
          return {
            id: item.id,
            ...file
          };
        });
        return Promise.resolve(res);
      })
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: GET_SHARED_FILES,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });
};

export const selectFile = file => dispatch => {
  return dispatch({
    type: SELECT_FILE,
    payload: file
  });
};
export const selectGroup = group => dispatch => {
  return dispatch({
    type: SELECT_GROUP,
    payload: group
  });
};

export const setFiles = files => dispatch => {
  return dispatch({
    type: SET_FILES,
    payload: files
  });
};
export const setFolders = folders => dispatch => {
  return dispatch({
    type: SET_FOLDERS,
    payload: folders
  });
};
export const setSharedFiles = sharedFiles => dispatch => {
  return dispatch({
    type: SET_SHARED_FILES,
    payload: sharedFiles
  });
};


export const discardSharedFile = ({ fileId }) => async dispatch => {
  const myUserId = firebase.auth().currentUser.uid;
  const update = {};
  update[`share.${myUserId}`] = false;
  // update[`shareFCM.${myUserId}`] = friendFCM;

  const act = () => {
    return firebase
      .firestore()
      .collection("files")
      .doc(fileId)
      .update(update)
      .catch(err => {
        throw err;
      });
  };

  const action = {
    type: TRANS,
    payload: act()
  };

  return dispatch(action).catch(err => {
    throw err;
  });


};
