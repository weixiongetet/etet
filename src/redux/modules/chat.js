import { Platform } from "react-native";
import RNFetchBlob from "react-native-fetch-blob";
import FileViewer from "react-native-file-viewer";
import firebase from "react-native-firebase";
import RNFS from "react-native-fs";
import { sbBanUser, sbConnect, sbCreateGroupChannel, sbCreateGroupChannelListQuery, sbCreateGroupChannelOneToOne, sbCreatePreviousMessageListQuery, sbDisconnect, sbGetGroupChannel, sbGetGroupChannelList, sbGetMessageList, sbInviteGroupChannel, sbLeaveGroupChannel, sbSendAdminMessage, sbSendTextMessage, sbUnbanUser, sbUpdateGroupChannel } from "../../sendbird";

// Actions
const LOGIN = "CHAT/LOGIN";
const LOGOUT = "CHAT/LOGOUT";
const LOAD_CHANNELS = "CHAT/LOAD_CHANNELS";
const CREATE_CHANNEL = "CHAT/CREATE_CHANNEL";
const CREATE_INDIE_CHANNEL = "CHAT/CREATE_INDIE_CHANNEL";
const SELECT_CHANNEL = "CHAT/SELECT_CHANNEL";
const UPDATE_CHANNEL = "CHAT/UPDATE_CHANNEL";
const TRIGGER_SEARCH = "CHAT/TRIGGER_SEARCH";
const UPDATE_SEARCH = "CHAT/UPDATE_SEARCH";
const LOAD_MESSAGES = "CHAT/LOAD_MESSAGES";
const ADD_MESSAGE = "CHAT/ADD_MESSAGE";
const ADD_ADMIN_MESSAGE = "CHAT/ADD_ADMIN_MESSAGE";
const PICK_GROUP_IMG = "CHAT/PICK_GROUP_IMG";
const UPLOAD_GROUP_IMG = "CHAT/UPLOAD_GROUP_IMG";
const LEAVE_CHANNEL = "CHAT/LEAVE_CHANNEL";
const INVITE_TO_CHANNEL = "CHAT/INVITE_TO_CHANNEL";
const KICK_USER = "CHAT/KICK_USER";
const REJOIN_USER = "CHAT/REJOIN_USER";
const FROM_CONTACTS = "CHAT/FROM_CONTACTS";
const GET_FRIEND_CONTACT = "CHAT/GET_FRIEND_CONTACT";
const SET_SEND_PROGRESS = "CHAT/SET_SEND_PROGRESS";
const FILEURL = "CHAT/FILEURL";
const DOWNLOAD_PROGRESS = "CHAT/DOWNLOAD_PROGRESS";
const UPDATE_CHATMSG = "CHAT/UPDATE_CHATMSG";

const DEFAULT_IMG =
  "https://www.materialui.co/materialIcons/file/cloud_circle_grey_192x192.png";
const UPDATE_CONFERENCE = "CHAT/UPDATE_CONFERENCE";
const UPDATE_CALLING = "CHAT/UPDATE_CALLING";
const UPD_SELECTED_FRIENDS = "CHAT/UPD_SELECTED_FRIENDS";
const UPD_ISVIDEO = "CHAT/UPD_ISVIDEO";

// Reducer
const INITIAL_STATE = {
  // channel list
  user: null,
  channels: [],

  // friend list
  channel: null,

  // message list
  messages: [],

  // tmp group image
  image: {
    uri:
      "https://static.vecteezy.com/system/resources/previews/000/544/231/non_2x/group-of-people-icon-flat-design-vector-with-shadow-on-isolated-white-background-black-color-and-monocrome-theme.jpg"
  },
  imgDownloadLink:
    "https://static.vecteezy.com/system/resources/previews/000/544/231/non_2x/group-of-people-icon-flat-design-vector-with-shadow-on-isolated-white-background-black-color-and-monocrome-theme.jpg",

  // search feature
  isSearching: false,
  searchString: "",

  // data retrieving
  isFetching: false,
  error: "",

  //from contacts page
  fromContactsPage: false,

  friendContact: "",

  sendProgress: 0,

  //get chat file url
  chatFileUrl: null,

  downloadPercent: 0,
  chatmsgFlag: false,

  conferenceMsg: null,
  endcalling: null,
  seletedFriends: [],
  isVideo: false
};

function getLocalPath(fileName) {
  return `${RNFS.DocumentDirectoryPath}/${fileName}`;
}

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `CHAT/LOGIN_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/LOGIN_FULFILLED`:
      return { ...state, isFetching: false, user: action.payload };
    case `CHAT/LOGIN_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/LOGOUT_PENDING`:
      return state;
    case `CHAT/LOGOUT_FULFILLED`:
      return { ...state, ...INITIAL_STATE };
    case `CHAT/LOGOUT_REJECTED`:
      return state;
    case `CHAT/LOAD_CHANNELS_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/LOAD_CHANNELS_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        channels: action.payload
      };
    case `CHAT/LOAD_CHANNELS_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/CREATE_CHANNEL_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/CREATE_CHANNEL_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        channel: action.payload
      };
    case `CHAT/CREATE_CHANNEL_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/CREATE_INDIE_CHANNEL_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/CREATE_INDIE_CHANNEL_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        channel: action.payload
      };
    case `CHAT/CREATE_INDIE_CHANNEL_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };
    case `CHAT/SELECT_CHANNEL`:
      return { ...state, channel: action.payload };
    case `CHAT/UPDATE_CHANNEL_PENDING`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/UPDATE_CHANNEL_FULFILLED`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/UPDATE_CHANNEL_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/TRIGGER_SEARCH`:
      return { ...state, isSearching: !state.isSearching };

    case `CHAT/UPDATE_SEARCH`:
      return { ...state, searchString: action.payload };

    case `CHAT/LOAD_MESSAGES_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/LOAD_MESSAGES_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        messages: action.payload
      };
    case `CHAT/LOAD_MESSAGES_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/ADD_MESSAGE_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/ADD_MESSAGE_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/ADD_MESSAGE_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/ADD_ADMIN_MESSAGE_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/ADD_ADMIN_MESSAGE_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/ADD_ADMIN_MESSAGE_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/PICK_GROUP_IMG`:
      return { ...state, image: action.payload };

    case `CHAT/UPLOAD_GROUP_IMG_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/UPLOAD_GROUP_IMG_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        imgDownloadLink: action.payload
      };
    case `CHAT/UPLOAD_GROUP_IMG_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/LEAVE_CHANNEL_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/LEAVE_CHANNEL_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/LEAVE_CHANNEL_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/INVITE_TO_CHANNEL_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/INVITE_TO_CHANNEL_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/INVITE_TO_CHANNEL_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/KICK_USER_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/KICK_USER_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/KICK_USER_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/REJOIN_USER_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/REJOIN_USER_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: ""
      };
    case `CHAT/REJOIN_USER_REJECTED`:
      return { ...state, isFetching: false, error: action.payload };

    case `CHAT/UPDATE_CONFERENCE_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/UPDATE_CONFERENCE_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        conferenceMsg: action.payload
      };
    case `CHAT/UPDATE_CONFERENCE_REJECTED`:

    case `CHAT/UPDATE_CALLING_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/UPDATE_CALLING_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        endcalling: action.payload
      };
    case `CHAT/UPDATE_CALLING_REJECTED`:

    case `CHAT/FROM_CONTACTS`:
      return {
        ...state,
        isFetching: false,
        error: "",
        fromContactsPage: action.payload
      };
    case `CHAT/GET_FRIEND_CONTACT_REJECTED`:
      return state;
    case `CHAT/GET_FRIEND_CONTACT_PENDING`:
      return { ...state, isFetching: true, error: "" };
    case `CHAT/GET_FRIEND_CONTACT_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        error: "",
        friendContact: action.payload
      };
    case `CHAT/SET_SEND_PROGRESS`:
      return {
        ...state,
        sendProgress: action.payload
      };
    case `CHAT/FILEURL`:
      return {
        ...state,
        chatFileUrl: action.payload
      };
    case `CHAT/DOWNLOAD_PROGRESS`:
      return {
        ...state,
        downloadPercent: action.payload
      };
    case `CHAT/UPDATE_CHATMSG`:
      return {
        ...state,
        chatmsgFlag: action.payload
      };
    case `CHAT/UPD_SELECTED_FRIENDS`:
      return {
        ...state,
        seletedFriends: action.payload
      };
    case `CHAT/UPD_ISVIDEO`:
      return {
        ...state,
        isVideo: action.payload
      };

    default:
      return state;
  }
}

// Action Creators
export const sendbirdLogin = ({
  userID,
  etetID,
  firstName,
  profilePictureURL
}) => async dispatch => {
  let tmp = firstName ? firstName : etetID;
  return dispatch({
    type: LOGIN,
    payload: sbConnect(userID, tmp, profilePictureURL)
  });
};

export const sendbirdLogout = () => async dispatch => {
  return dispatch({
    type: LOGOUT,
    payload: sbDisconnect()
  });
};

export const loadChannels = () => async dispatch => {
  const act = async () => {
    let channelQuery = await sbCreateGroupChannelListQuery();
    let channels = await sbGetGroupChannelList(channelQuery);
    return channels;
  };

  return dispatch({
    type: LOAD_CHANNELS,
    payload: act()
  });
};

export const createChannel = ({
  operators,
  selectedFriends,
  name,
  cover
}) => async dispatch => {
  return dispatch({
    type: CREATE_CHANNEL,
    payload: sbCreateGroupChannel(operators, selectedFriends, name, true, cover)
  });
};

export const createIndieChannel = ({ userA, userB }) => async dispatch => {
  return dispatch({
    type: CREATE_INDIE_CHANNEL,
    payload: sbCreateGroupChannelOneToOne(userA, userB, true)
  });
};

export const updateChannel = params => async dispatch => {
  return dispatch({
    type: UPDATE_CHANNEL,
    payload: sbUpdateGroupChannel(params)
  });
};

export const selectChannel = channel => dispatch => {
  return dispatch({
    type: SELECT_CHANNEL,
    payload: channel
  });
};

export const triggerSearch = () => dispatch => {
  return dispatch({
    type: TRIGGER_SEARCH
  });
};

export const updateSearch = text => dispatch => {
  return dispatch({
    type: UPDATE_SEARCH,
    payload: text
  });
};

export const loadMessages = channel => async dispatch => {
  console.log("reduxLoadmessage", channel);
  const act = async () => {
    let msgQuery = await sbCreatePreviousMessageListQuery(channel.url, false);
    return await sbGetMessageList(msgQuery);
  };

  return dispatch({
    type: LOAD_MESSAGES,
    payload: act()
  });
};

export const addMessage = (channel, text) => async dispatch => {
  const act = async () => {
    let tmp = await sbGetGroupChannel(channel.url);
    return await sbSendTextMessage(tmp, text);
    // return await sbSendTextMessage(channel, text);
  };

  return dispatch({
    type: ADD_MESSAGE,
    payload: act()
  });
};

export const addAdminMessage = (channel, text) => async dispatch => {
  const act = async () => {
    let tmp = await sbGetGroupChannel(channel.url);
    return await sbSendAdminMessage(tmp, text);
    // return await sbSendTextMessage(channel, text);
  };

  return dispatch({
    type: ADD_ADMIN_MESSAGE,
    payload: act()
  });
};

export const changeGroupImage = image => async dispatch => {
  return dispatch({
    type: PICK_GROUP_IMG,
    payload: image
  });
};

export const uploadGroupImg = (channelUrl, image) => async dispatch => {
  const { uri } = image;
  const mime = "image/jpeg";

  const uploadUri = Platform.OS === "ios" ? uri.replace("file://", "") : uri;

  const imageRef = firebase
    .storage()
    .ref(`/group/${channelUrl}`)
    .child("profile_pic");

  const imgPromise = imageRef.put(uploadUri, { contentType: mime }).then(() => {
    return imageRef.getDownloadURL();
  });

  const action = {
    type: UPLOAD_GROUP_IMG,
    payload: imgPromise
  };

  return dispatch(action);
};

export const leaveChannel = channelUrl => async dispatch => {
  return dispatch({
    type: LEAVE_CHANNEL,
    payload: sbLeaveGroupChannel(channelUrl)
  });
};

export const inviteToChannel = params => async dispatch => {
  return dispatch({
    type: INVITE_TO_CHANNEL,
    payload: sbInviteGroupChannel(params)
  });
};

export const kickUser = params => async dispatch => {
  return dispatch({
    type: KICK_USER,
    payload: sbBanUser(params)
  });
};

export const rejoinUser = params => async dispatch => {
  return dispatch({
    type: REJOIN_USER,
    payload: sbUnbanUser(params)
  });
};

export const updFromContactFlag = params => async dispatch => {
  return dispatch({
    type: FROM_CONTACTS,
    payload: params
  });
};

export const getFriendContact = friendID => async dispatch => {
  const act = () => {
    return firebase
      .firestore()
      .collection("users")
      .doc(friendID)
      .get()
      .then(res => {
        if (res.data() !== undefined) {
          let userDetails = res.data();
          if (userDetails.phone) {
            return userDetails.phone;
          } else {
            return "";
          }
        }
      });
  };

  const action = {
    type: GET_FRIEND_CONTACT,
    payload: act()
  };

  return dispatch(action);
};

export const addChatFile = ({ file, path }) => async dispatch => {
  try {
    // const uploadUri =
    //   Platform.OS === "ios" ? file.uri.replace("file://", "") : file.uri;
    // const uploadUri =
    //   Platform.OS === "ios"
    //     ? file.uri.replace("file://", "")
    //     : "file://" + file.path;
    const uploadUri = file.uri;
    var str = file.uri;
    var n = str.lastIndexOf('/');
    var result = str.substring(n + 1);
    var fileExt = result.split(".")[1];
    fileExt = "." + fileExt

    const docRef = firebase
      .firestore()
      .collection(`files`)
      .doc();

    const fileRef = firebase
      .storage()
      .ref(`/${firebase.auth().currentUser.uid}/chat/${docRef.id}`)
      .child(file.fileName);

    const userToken = await firebase.auth().currentUser.getIdToken(true);
    const callable = await firebase.functions().httpsCallable("uploadToken");

    const { data } = await callable({
      token: userToken,
      path: `${firebase.auth().currentUser.uid}/chat/${docRef.id}/${
        file.fileName
        }`
    });

    if (data.remainingStorage < file.fileSize) {
      throw new Error("You have reached the storage limit.");
    }

    await firebase.auth().signInWithCustomToken(data.newToken);

    let uploadTask = fileRef.put(uploadUri, { contentType: fileExt });
    try {
      await new Promise((resolve, reject) => {
        try {

          let listenUpload = uploadTask.on(
            "state_changed",
            taskSnapshot => {
              var progress =
                taskSnapshot.bytesTransferred / taskSnapshot.totalBytes;

              dispatch({
                type: SET_SEND_PROGRESS,
                payload: progress
              });

              if (taskSnapshot.state === firebase.storage.TaskState.SUCCESS) {
                listenUpload();
                resolve("Total bytes uploaded: ", taskSnapshot.totalBytes);
              }
            },
            error => {
              listenUpload();
              reject(error);
            }
          );
        } catch (e) {
          return Promise.resolve();
        }
      });
    } catch (e) {
      return Promise.resolve();
    }

    await dispatch({
      type: SET_SEND_PROGRESS,
      payload: 0
    });

    const uri = await fileRef.getDownloadURL();

    const action = {
      type: FILEURL,
      payload: uri
    };

    return dispatch(action);
  } catch (err) {
    throw err;
  }
};

export const openFile = ({ uri, fileName }) => async dispatch => {
  const localFile = getLocalPath(fileName);
  const options = {
    fromUrl: uri,
    toFile: localFile
  };

  await dispatch({
    type: DOWNLOAD_PROGRESS,
    payload: 50
  });

  return await RNFetchBlob.fs.exists(localFile).then(exist => {
    if (exist) {
      //open
      return FileViewer.open(localFile)
        .then(() => {
          // success
          const progress = 1 * 100;
          dispatch({
            type: DOWNLOAD_PROGRESS,
            payload: progress
          });
        })
        .catch(error => {
          console.log("error", error);
        });
    }

    //download and open
    return RNFS.downloadFile(options)
      .promise.then(() => FileViewer.open(localFile))
      .then(() => {
        consol.log();
        console.log("download successs");

        // success
        const progress = 1 * 100;
        dispatch({
          type: DOWNLOAD_PROGRESS,
          payload: progress
        });
      })
      .catch(error => {
        console.log("error", error);
      });
  });
};

export const addConference = ({ userID, items }) => async dispatch => {
  const act = async () => {
    firebase
      .firestore()
      .collection("conference")
      .doc(userID)
      .set(items)
      .then(function () {
        return Promise.resolve(true);
      })
      .catch(function (error) {
        throw error;
      });
  };

  return {
    type: UPDATE_CONFERENCE,
    payload: act()
  };
};

export const endRejectCall = ({ userID }) => async dispatch => {
  const act = async () => {
    firebase
      .firestore()
      .collection("conference")
      .doc(userID)
      .delete()
      .then(function () {
        console.log("Deleted calling success");
      })
      .catch(function (error) {
        console.log("error delete calling docs", error);
      });
  };

  return {
    type: UPDATE_CALLING,
    payload: act()
  };
};

export const updSelectedFriends = ({ items }) => async dispatch => {
  return dispatch({
    type: UPD_SELECTED_FRIENDS,
    payload: items
  });
};

export const updIsVideo = ({ items }) => async dispatch => {
  return dispatch({
    type: UPD_ISVIDEO,
    payload: items
  });
};

export const updateChatMsg = params => async dispatch => {
  return dispatch({
    type: UPDATE_CHATMSG,
    payload: params
  });
};
