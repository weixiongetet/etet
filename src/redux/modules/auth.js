// Actions
const UPDATE_USER = "AUTH/UPDATE_USER";

// Reducer
const INITIAL_STATE = {
  isFetching: false,
  hasError: false,
  errorMessage: "",
  user: null,
  loggedIn: false
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case `AUTH/UPDATE_USER`: {
      return {
        ...state,
        user: action.payload
      };
    }

    default:
      return state;
  }
}

// Action Creators
export const updateUser = user => dispatch => {
  dispatch({
    type: UPDATE_USER,
    payload: user
  });
};
