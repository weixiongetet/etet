import React, { Component } from "react";
import { connect } from "react-redux";
import HomePage from "./HomePage";
import Walkthrough from "./Walkthrough";

class Loading extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   user: null,
    // };
  }

  componentDidMount() {
    // console.log(this.props.user);
  }

  componentWillUnmount() {}

  render() {
    if (!this.props) {
      return <HomePage />;
    } else {
      return <Walkthrough />;
    }
  }
} //end class

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps)(Loading);
