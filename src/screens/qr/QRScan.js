import { Container, Text } from 'native-base';
import React, { Component } from 'react';
import { Alert, Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import * as Animatable from "react-native-animatable";
// import { QRscanner } from 'react-native-qr-scanner';
import QRCodeScanner from "react-native-qrcode-scanner";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
import { getUserDetails, updSelectedUserFlag } from "../../redux/modules/contacts";

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "red";
const scanBarWidth = SCREEN_WIDTH * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "#22ff00";
const iconScanColor = "blue";


class QRScan extends Component {
  constructor(props) {
    super(props);
    const data = props.navigation.getParam('data');
    this.state = {
      data: data,
    }
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }


  render() {
    return (

      <Container>

        <View style={styles.container}>

          {/* <QRscanner onRead={this.onRead} renderBottomView={this.bottomView} flashMode={this.state.flashMode} zoom={this.state.zoom} finderY={50} hintText="Scan E-tet QR code" /> */}

          <QRCodeScanner
            showMarker
            onRead={this.onRead}
            cameraStyle={{ height: SCREEN_HEIGHT }}
            customMarker={
              <View style={styles.rectangleContainer}>
                <View style={styles.topOverlay}>
                  <Text style={{ fontSize: 20, color: "white" }}>
                    Scan E-tet QR code
              </Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <View style={styles.leftAndRightOverlay} />

                  <View style={styles.rectangle}>
                    <Icon
                      name="ios-qr-scanner"
                      size={SCREEN_WIDTH * 0.73}
                      color={iconScanColor}
                    />
                    <Animatable.View
                      style={styles.scanBar}
                      direction="alternate-reverse"
                      iterationCount="infinite"
                      duration={1700}
                      easing="linear"
                      animation={this.makeSlideOutTranslation(
                        "translateY",
                        SCREEN_WIDTH * -0.54
                      )}
                    />
                  </View>

                  <View style={styles.leftAndRightOverlay} />
                </View>

                <View style={styles.bottomOverlay} />
              </View>
            }

          />

        </View>
      </Container>

    );



  }
  bottomView = () => {
    return (
      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#0000004D' }}>
        <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.setState({ flashMode: !this.state.flashMode })}>
          {/* <Text style={{color:'#fff'}}>Activate Flashlight</Text> */}
          <Text style={{ color: '#fff' }}>{this.state.data}</Text>
        </TouchableOpacity>
      </View>
    );
  }
  onRead = (res) => {
    const scanWebFlag = this.props.scanWebFlag;

    if (scanWebFlag) {
      const userName = this.props.user.firstName + '-' + this.props.user.lastName;
      const { email } = this.props.user;
      const { phone } = this.props.user;
      // var formPhone = (phone !== undefined) ? phone.replace('+', '') : '';
      var formPhone = (phone !== undefined) ? phone : '';
      const combineData = `${userName},${formPhone},${email}`;

      // fetch("https://i.etet.app/get.php", {
      //   method: "POST",
      //   body: 'testinggggg'
      // });
      fetch(`https://etet.app/scanner/get.php?u=${JSON.parse(JSON.stringify(res.data, null, 4))}&e=${combineData}`);
      this.props.navigation.navigate('HomePage')
    } else {

      if (res.data) {
        var regex = /^[A-Za-z0-9 ]+$/
        var isValid = regex.test(res.data);

        if (!isValid) {
          Alert.alert('', 'Invalid QR Code.', [
            { text: 'Ok', onPress: () => this.props.navigation.navigate('HomePage') },
            { cancelable: false },
          ])
        } else {
          this.props.updSelectedUserFlag('')
          this.props.getUserDetails({ items: res.data })
          this.props.navigation.navigate('HomeQRUserDetails');
        }
      }

    }
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    scanWebFlag: state.setting.scanWebFlag
  }),
  { updSelectedUserFlag, getUserDetails }
)(QRScan);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },

  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    // borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
});
