import React, { Component } from 'react';
import { connect } from "react-redux";
import {View, StyleSheet, TouchableOpacity, StatusBar} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import QRCode from 'react-native-qrcode-svg';
const assetPath = "../../assets";

class ShowQRCode extends Component {

    constructor(props) {
        super(props);
    }     

    render() {
        // const user = this.props.user;

        const { userID } = this.props.user
// console.log(user);

        return (
            <View style={styles.MainContainer}>
        
                <QRCode
                value={userID}
                size={250}
                bgColor='#000'
                // logo={require(`${assetPath}/logo.png`)}
                // logoSize={30}
                // ecl='L'
                fgColor='#fff'/>

                <Text style={styles.TextStyle}> Scan the QR Code to add me on Etet </Text>

        
          </View>


        );
    };
    
}


export default connect(
    state => ({
      user: state.auth.user,
    }),
  )(ShowQRCode);

const styles = StyleSheet.create({

  MainContainer: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
},
TextStyle:{
    color:'#000',
    textAlign:'center',
    fontSize: 15,
    paddingTop:20
}
 
});
