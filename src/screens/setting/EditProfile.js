import { Body, Content, Icon, Left, List, ListItem, Right, Text } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Image, Modal, Platform, StyleSheet, TouchableOpacity, View } from "react-native";
import firebase from "react-native-firebase";
import ImagePicker from "react-native-image-picker";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import { connect } from "react-redux";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import EditNameReduxForm from "../../containers/form/EditNameReduxForm";
import EditPasswordReduxForm from "../../containers/form/EditPasswordReduxForm";
import PhoneVerifyReduxForm from "../../containers/form/PhoneVerifyReduxForm";
import { sendbirdLogin, sendbirdLogout } from "../../redux/modules/chat";
import { editName, uploadProfileImg } from "../../redux/modules/setting";

const ListItemIcon = ({ icon, label, children, onPress }) => {
  return (
    <ListItem icon onPress={onPress}>
      <Left>
        <Icon active name={icon} style={{ color: "gray" }} />
      </Left>
      <Body>
        <Text style={{ fontSize: 15 }}>{label}</Text>
      </Body>
      <Right>
        <Icon name="md-arrow-dropright" />
      </Right>
    </ListItem>
  );
};

const DetailText = ({ label, value, isTitle }) => {
  return (
    <View
      style={{ flexDirection: "row", alignSelf: "center", marginBottom: 5 }}
    >
      {/* <Text style={{ width: 100 }}>{`${label}`}</Text> */}
      <Text
        style={{
          fontFamily: "Roboto",
          fontWeight: "bold",
          fontSize: isTitle ? 20 : 14
        }}
      >{`${value}`}</Text>
    </View>
  );
};

const CustomModal = ({ children, ...rest }) => {
  return (
    <Modal animationType="fade" {...rest}>
      <Content>{children}</Content>
    </Modal>
  );
};

class EditProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      firstName: "",
      lastName: "",
      // firstName: ""
      // firstName: '',
      // lastName: '',
      // email: '',
      // phone: '',
      // password: '',
      // profilePictureURL: ''
      nameModal: false,
      passwordModal: false,
      phoneModal: false
    };

    // const docRef = firebase.firestore().collection("users").doc(this.props.user.profile.userID);
    // this.unsubscribe = null;
  }

  async componentDidMount() {
    // this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    const firstName = "";

    const docRef = firebase
      .firestore()
      .collection("users")
      .doc(this.props.user.userID);

    docRef.onSnapshot(doc => {
      console.log("Document data:", doc.data());
      const { firstName, lastName } = doc.data();
      this.setState({ firstName, lastName });
    });
  }

  componentWillUnmount() {
    // this.unsubscribe();
  }

  selectPhotoTapped = () => {
    const options = {
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.fileSize > 5242880) {
        alert("File capped at 5mb.");
        return false;
      }

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.props
          .uploadProfileImg(response)
          .then(() => this.props.sendbirdLogout)
          .then(() => {
            // refresh profile image in sendbird server
            this.props.sendbirdLogin(this.props.user);
          })
          .catch(err => {
            alert(err);
          });
      }
    });
  };

  closeNameModal = () => {
    this.setState({ nameModal: false });
  };

  closePasswordModal = () => {
    this.setState({ passwordModal: false });
  };

  closePhoneModal = () => {
    this.setState({ phoneModal: false });
  };

  editNameSubmit = async val => {
    await this.props
      .editName(val)
      .then(this.closeNameModal())
      .catch(err => {
        alert(err);
      });
  };

  reauthenticate = (curPassword) => {
    var user = firebase.auth().currentUser;
    var cred = firebase.auth.EmailAuthProvider.credential(
      user.email, curPassword);
    return user.reauthenticateWithCredential(cred);
  }

  editPasswordSubmit = async ({ curPassword, newPassword }) => {
    this.reauthenticate(curPassword).then(() => {
      var user = firebase.auth().currentUser;
      user.updatePassword(newPassword).then(() => {
        Alert.alert('', 'Password updated!', [
          { text: 'Confirm', onPress: () => this.closePasswordModal() },
          { cancelable: false },
        ])
      }).catch((error) => { alert("Failed!") });
    }).catch((error) => { alert('Old password incorrect!'); });
  }


  render() {
    // if (this.state.loading) {
    //   return null; // or render a loading icon
    // }
    const {
      profilePictureURL,
      lastName,
      firstName,
      phone,
      email
    } = this.props.user;

    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;


    return (
      <View>
        <View style={{ alignSelf: "center" }}>
          {this.props.isFetching ? (
            <ActivityIndicator
              style={styles.profileImage}
              size="large"
              color="#3370FF"
            />
          ) : (
              <TouchableOpacity onPress={this.selectPhotoTapped}>
                <Image
                  style={styles.profileImage}
                  source={{
                    uri: profilePictureURL
                      ? profilePictureURL
                      : "https://www.shareicon.net/download/2017/01/08/869376_superman_512x512.png"
                  }}
                />
              </TouchableOpacity>
            )}
        </View>

        {firstName || lastName ? (
          <DetailText isTitle value={`${firstName} ${lastName}`} />
        ) : (
            <DetailText isTitle value={"No Name"} />
          )}
        <DetailText value={email} />
        {phone ? (
          <DetailText value={phone} />
        ) : (
            <DetailText value={"No Phone"} />
          )}

        <List>
          <ListItemIcon
            icon="md-person"
            label="Edit Name"
            onPress={() => {
              this.setState({ nameModal: true });
            }}
          />
          <ListItemIcon
            icon="md-lock"
            label="Replace Password"
            onPress={() => {
              this.setState({ passwordModal: true });
            }}
          />
          <ListItemIcon
            icon="md-call"
            label="Change Phone Number"
            onPress={() => {
              Alert.alert(
                "Remove phone",
                "To link a new phone, required to unlink current one. Are you sure?",
                [
                  {
                    text: "Confirm",
                    onPress: async () => {
                      try {
                        await firebase
                          .auth()
                          .currentUser.unlink(
                            firebase.auth.PhoneAuthProvider.PROVIDER_ID
                          )
                          .catch(err => {
                            console.log(err);
                          });
                        const { _user } = firebase.auth().currentUser;
                        await firebase
                          .firestore()
                          .collection("users")
                          .doc(_user.uid)
                          .update({
                            phone: firebase.firestore.FieldValue.delete()
                          });
                      } catch (err) {
                        alert(err);
                      }

                      await this.setState({ phoneModal: true });
                    }
                  },
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  }
                ],
                { cancelable: false }
              );
            }}
          />
        </List>

        <LevelModal
          visible={this.state.nameModal}
          onRequestClose={() => {
            this.closeNameModal();
          }}
        >

          <IconAntDesign
            style={{
              position: "absolute",
              right: 10,
              top: 10,
              zIndex: 99999
            }}
            name="closecircle"
            onPress={() => {
              this.closeNameModal();
            }}
            size={20}
          />

          <EditNameReduxForm onSubmit={this.editNameSubmit} />
        </LevelModal>

        <LevelModal
          visible={this.state.passwordModal}
          onRequestClose={() => {
            this.closePasswordModal();
          }}
        >

          <IconAntDesign
            style={{
              position: "absolute",
              right: 10,
              top: 10,
              zIndex: 99999
            }}
            name="closecircle"
            onPress={() => {
              this.closePasswordModal();
            }}
            size={20}
          />

          <EditPasswordReduxForm onSubmit={this.editPasswordSubmit} />
        </LevelModal>

        <CustomModal
          visible={this.state.phoneModal}
          onRequestClose={() => {
            this.closePhoneModal();
          }}
        >
          <PhoneVerifyReduxForm redirect={this.closePhoneModal} />
        </CustomModal>

        <Display isVisible={incomingFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={incomingFlag} >
            <CallKit type={'incoming'} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={showTimerFlag} >
            <CallKit type={'timer'} />
          </LevelModal>
        </Display>


      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    isFetching: state.setting.isFetching,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag,
  }),
  {
    editName,
    uploadProfileImg,
    sendbirdLogin,
    sendbirdLogout
  }
)(EditProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  item: {
    flex: 1,
    flexDirection: "row"
  },
  profileImage: {
    marginBottom: 20,
    width: 120,
    height: 120,
    borderRadius: 60,
    marginTop: Platform.OS === "ios" ? 10 : 0,
  }
});
