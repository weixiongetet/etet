import AsyncStorage from "@react-native-community/async-storage";
import moment from "moment";
import { Body, Container, Content, Icon, Left, List, ListItem, Right, Separator, Text, View } from "native-base";
import React, { Component } from "react";
import { Alert, Image, Linking, PermissionsAndroid, Platform, ProgressBarAndroid, ProgressViewIOS, StyleSheet, Switch } from "react-native";
import Contacts from "react-native-contacts";
import firebase from "react-native-firebase";
import { NavigationActions, StackActions } from "react-navigation";
import { connect } from "react-redux";
import SendBird from "sendbird";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import FeedbackReduxForm from "../../containers/form/FeedbackReduxForm";
import LoginManager from "../../manager/LoginManager";
import { sendbirdLogin, sendbirdLogout } from "../../redux/modules/chat";
import { checkRemainingStorage } from "../../redux/modules/setting";

class ListItemIcon extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    const { icon, label, note, children } = this.props;
    return (
      label !== nextProps.label ||
      icon !== nextProps.icon ||
      note !== nextProps.note ||
      children !== nextProps.children
    );
  }

  render() {
    const { icon, label, note, children, onPress } = this.props;
    return (
      <ListItem icon onPress={onPress}>
        <Left>
          <Icon active name={icon} style={styles.icon} />
        </Left>
        <Body>
          <Text style={{ fontSize: 14 }}>{label}</Text>
          {note ? (
            <Text note style={{ fontSize: 10 }}>
              {note}
            </Text>
          ) : null}
        </Body>
        {children ? <Right>{children}</Right> : null}
      </ListItem>
    );
  }
}

class SettingMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profilePictureURL: null,
      feedbackModal: false,
      feedbackSent: false,
      loading: false,
      feedbacks: null,
      showFeedbacks: false,
      notificationOpen:
        this.props.user && this.props.user.fcmToken ? true : false,
      privateOpen:
        this.props.user && this.props.user.private
          ? this.props.user.private
          : false
    };
  }

  componentWillMount() {
    const feedbackRef = firebase
      .firestore()
      .collection("feedback")
      .doc(this.props.user.userID);

    this.listenFeedback = feedbackRef.onSnapshot(doc => {
      if (doc.exists) {
        console.log(doc.data());
        this.setState({
          feedbacks: doc.data()
        });
      }
    });
  }

  async componentDidMount() {
    // await this.props.checkRemainingStorage();
    // console.log("planStatus", this.props.planStatus);
    const { user } = this.props;
    await this.setState({
      profilePictureURL: user ? user.profilePictureURL : ""
    });
  }

  componentWillUnmount() {
    // this.usersUnsubscribe();
    this.listenFeedback();
  }

  handleLogout = () => {
    Alert.alert(
      "Logging Out",
      "Are you sure?",
      [
        {
          text: "Yes",
          onPress: async () => {
            await AsyncStorage.setItem("walkthrough", "no");

            await new Promise((resolve, reject) => {
              let sb = SendBird.getInstance();
              if (!sb || (sb && sb.connecting === false)) {
                this.props.sendbirdLogin(this.props.user).then(() => {
                  sb = SendBird.getInstance();
                  sb.unregisterGCMPushTokenForCurrentUser(
                    this.props.user.fcmToken,
                    function (response, error) {
                      if (error) reject(error);
                      console.log("success unregister sendbird FCM");
                      resolve(response);
                    }
                  );
                });
              }
            });

            //voximplant logout
            LoginManager.getInstance().logout();

            await firebase
              .firestore()
              .collection("users")
              .doc(this.props.user.userID)
              .update({
                fcmToken: null
              });

            await this.props.sendbirdLogout();
            firebase.auth().signOut();


            // this.props.logout();
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  };

  clearAsyncStorage = async () => {
    console.log("clearrassyn");
    AsyncStorage.clear();
    await AsyncStorage.setItem("walkthrough", "no");
  };

  closeFeedbackModal = () => {
    this.setState({ feedbackModal: false });
  };

  feedbackSubmit = async ({ feedback }) => {
    await this.setState({
      loading: true
    });
    let updateItem = {};
    updateItem[Date.now()] = { message: feedback, status: "sent" };

    const feedbackRef = firebase
      .firestore()
      .collection("feedback")
      .doc(this.props.user.userID);

    var docExist = await feedbackRef.get().then(doc => {
      if (!doc.exists) {
        return Promise.resolve(false);
      }
      return Promise.resolve(true);
    });
    docExist ? feedbackRef.update(updateItem) : feedbackRef.set(updateItem);
    await this.setState({
      loading: false,
      feedbackSent: true
    });

    setTimeout(() => {
      this.closeFeedbackModal();
    }, 3000);
  };

  turnFeedbackOn = () => {
    this.setState({
      feedbackSent: false
    });
  };

  feedColor = status => {
    switch (status) {
      case "sent":
        return "#bdbdbd";
      case "reviewing":
        return "#ffca28";
      case "developing":
        return "#ffa726";
      case "merged":
        return "#9ccc65";
      case "rejected":
        return "#ff7043";
    }
  };

  _renderFeedbacks = () => {
    let feedArray = [];
    for (var property in this.state.feedbacks) {
      feedArray.push({
        ...this.state.feedbacks[property],
        time: property
      });
    }

    let sortedArr = _.sortBy(feedArray, "time");

    return sortedArr.map(feed => {
      return (
        <View
          style={{
            flexDirection: "row",
            padding: 5
          }}
        >
          <View style={{ width: "20%", padding: 5 }}>
            <Text
              style={{
                fontSize: 10,
                padding: 3,
                color: "white",
                alignSelf: "center",
                alignContent: "stretch",
                backgroundColor: this.feedColor(feed.status),
                borderRadius: 2
              }}
            >
              {feed.status}
            </Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              width: "60%",
              // alignItems: "center",
              padding: 5
            }}
          >
            <Icon
              name="md-book"
              style={[styles.icon, { fontSize: 18, paddingRight: 5 }]}
            />

            <Text style={{ fontSize: 12 }}>{feed.message}</Text>
          </View>

          <View style={{ width: "20%", padding: 5 }}>
            <Text style={{ fontSize: 10, marginRight: 5 }}>
              {moment(new Date(Number(feed.time))).format("DD/MM/YY")}
            </Text>
          </View>
        </View>
      );
    });
  };

  toggleNotification = async value => {
    await this.setState({
      notificationOpen: value
    });
    const deviceToken = await AsyncStorage.getItem("fcmToken");

    let updateToken = {};
    updateToken.fcmToken = value
      ? deviceToken
      : firebase.firestore.FieldValue.delete();

    try {
      let sb = SendBird.getInstance();
      if (!sb || (sb && sb.connecting === false)) {
        await this.props.sendbirdLogin(this.props.user);
        sb = SendBird.getInstance();
      }
      await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .update(updateToken);

      if (!value) {
        await new Promise((resolve, reject) => {
          sb.unregisterGCMPushTokenForCurrentUser(deviceToken, function (
            response,
            error
          ) {
            if (error) reject(error);
            resolve("success unregister sendbird FCM");
          });
        });
      } else {
        await new Promise((resolve, reject) => {
          // if (Platform == "ios") {
          //   firebase
          //     .messaging()
          //     .ios.getAPNSToken()
          //     .then(token => {
          //       if (token) {
          //         sb.registerAPNSPushTokenForCurrentUser(
          //           token,
          //           (response, error) => {
          //             if (error) reject(error);
          //             resolve("success register sendbird FCM");
          //           }
          //         );
          //       }
          //     });
          // } else {
          sb.registerGCMPushTokenForCurrentUser(
            deviceToken,
            (response, error) => {
              if (error) {
                console.log("Error:: ", error);
                reject(error);
              }
              console.log("response:: ", response);
              console.log("deviceToken:: ", deviceToken);
              resolve("success register sendbird FCM");
            }
          );
          //  }
        });
      }
      this.clearAsyncStorage();

    } catch (err) {
      await this.setState({
        notificationOpen: !value
      });
      alert(err);
    }
  };

  togglePrivateMode = async value => {
    await this.setState({
      privateOpen: value
    });

    let updateToken = {};
    updateToken.private = value;

    try {
      await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .update(updateToken);
    } catch (err) {
      await this.setState({
        privateOpen: !value
      });
      alert(err);
    }
  };

  syncContact() {
    if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "Sync contacts to begin.",
        buttonPositive: "OK"
      })
        .then(granted => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            const replaceAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "DashboardStack",
                  action: NavigationActions.navigate({
                    routeName: "ContactsStack",
                    action: NavigationActions.navigate({
                      routeName: "ContactsSub"
                    })
                  })
                })
              ]
            });

            this.props.navigation.dispatch(replaceAction);
          } else {
            console.log("Denied");
          }
        })
        .catch(err => {
          console.log("PermissionsAndroid", err);
        });
    } else {
      Contacts.getAll((err, contacts) => {
        if (err === "denied") {
          console.warn("Permission to access contacts was denied");
        } else {
          const replaceAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: "DashboardStack",
                action: NavigationActions.navigate({
                  routeName: "ContactsStack",
                  action: NavigationActions.navigate({
                    routeName: "ContactsSub"
                  })
                })
              })
            ]
          });

          this.props.navigation.dispatch(replaceAction);
        }
      });
    }
  }

  render() {
    const { user } = this.props;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    if (!user) return null;

    return (
      <Container>
        <Content>
          {/* <Text>{this.props.planStatus.remainingStorage}</Text>
          <Text>{this.props.planStatus.planLimit}</Text>
          <Text>{this.props.planStatus.currentUsage}</Text> */}

          <List style={styles.listContainers}>
            <ListItem
              thumbnail
              onPress={() => this.props.navigation.navigate("EditProfile")}
            >
              <Left>
                <Image
                  style={styles.profileThumbnail}
                  source={{
                    uri: user.profilePictureURL
                      ? user.profilePictureURL
                      : "https://www.shareicon.net/download/2017/01/08/869376_superman_512x512.png"
                  }}
                />
              </Left>
              <Body>
                <Text>{`${
                  this.props.user.lastName !== ""
                    ? `${this.props.user.firstName} ${this.props.user.lastName}'s`
                    : this.props.user.firstName + "'s"
                  } Profile`}</Text>
                <Text note>Privacy settings</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>

            <Separator bordered>
              <Text style={styles.titleSeperator}>Vault usage</Text>
            </Separator>

            {Platform.OS == "android" && this.props.planStatus ? (
              <ListItem icon>
                <Left>
                  <Icon active name="md-list-box" style={{ color: "gray" }} />
                </Left>
                <Body>
                  <View style={styles.remainingStorageCont}>
                    <ProgressBarAndroid
                      styleAttr="Horizontal"
                      color="gray"
                      indeterminate={false}
                      progress={
                        this.props.planStatus.currentUsage /
                        this.props.planStatus.planLimit
                      }
                    />
                  </View>
                </Body>
                <Right>
                  <Text style={{ fontSize: 11 }}>
                    {`${(
                      (this.props.planStatus.currentUsage /
                        this.props.planStatus.planLimit) *
                      100
                    ).toFixed(2)}%`}
                  </Text>
                </Right>
              </ListItem>
            ) : null}

            {Platform.OS == "ios" && this.props.planStatus ? (
              <ListItem icon>
                <Left>
                  <Icon active name="md-list-box" style={{ color: "gray" }} />
                </Left>
                <Body>
                  <View style={styles.remainingStorageCont}>
                    <ProgressViewIOS
                      progressTintColor="gray"
                      progress={
                        this.props.planStatus.currentUsage /
                        this.props.planStatus.planLimit
                      }
                    />
                  </View>
                </Body>
                <Right>
                  <Text style={{ fontSize: 11 }}>
                    {`${(
                      (this.props.planStatus.currentUsage /
                        this.props.planStatus.planLimit) *
                      100
                    ).toFixed(2)}%`}
                  </Text>
                </Right>
              </ListItem>
            ) : null}

            <Separator bordered>
              <Text style={styles.titleSeperator}>Link</Text>
            </Separator>
            <ListItemIcon
              icon="md-contacts"
              label="Sync contact"
              // note="Cannot be unsynced"
              onPress={async () => {
                // await AsyncStorage.setItem("checkSync", "yes");
                await this.syncContact();

                // const replaceAction = StackActions.reset({
                //   index: 0,
                //   actions: [
                //     NavigationActions.navigate({
                //       routeName: "DashboardStack",
                //       action: NavigationActions.navigate({
                //         routeName: "ContactsStack",
                //         action: NavigationActions.navigate({
                //           routeName: "ContactsSub"
                //         })
                //       })
                //     })
                //   ]
                // });

                // this.props.navigation.dispatch(replaceAction);
              }}
            />
            <ListItemIcon
              icon="md-search"
              label="Private mode"
              note="Hide from other users' searching"
            >
              {this.state.privateOpen ? (
                <Switch
                  trackColor={{ true: "#bbdefb", false: "#e0e0e0" }}
                  thumbColor={"#3370FF"}
                  value={this.state.privateOpen}
                  onValueChange={this.togglePrivateMode}
                />
              ) : null}

              {!this.state.privateOpen ? (
                <Switch
                  trackColor={{ true: "#bbdefb", false: "#e0e0e0" }}
                  value={this.state.privateOpen}
                  onValueChange={this.togglePrivateMode}
                />
              ) : null}
            </ListItemIcon>

            <Separator bordered>
              <Text style={styles.titleSeperator}>General</Text>
            </Separator>

            <ListItemIcon
              icon="md-notifications"
              label="Receive notifications"
              note="Subscribe to chat, vault & news"
            >
              {this.state.notificationOpen ? (
                <Switch
                  trackColor={{ true: "#bbdefb", false: "#e0e0e0" }}
                  thumbColor="#3370FF"
                  value={this.state.notificationOpen}
                  onValueChange={this.toggleNotification}
                />
              ) : null}

              {!this.state.notificationOpen ? (
                <Switch
                  trackColor={{ true: "#bbdefb", false: "#e0e0e0" }}
                  value={this.state.notificationOpen}
                  onValueChange={this.toggleNotification}
                />
              ) : null}
            </ListItemIcon>
            <ListItemIcon
              icon="md-send"
              label="Send feedback"
              onPress={() => {
                this.setState({ feedbackModal: true });
              }}
            />

            {this.state.feedbacks ? (
              <ListItemIcon
                icon={
                  this.state.showFeedbacks ? "ios-arrow-up" : "ios-arrow-down"
                }
                label="Old feedbacks"
                onPress={() => {
                  this.setState({ showFeedbacks: !this.state.showFeedbacks });
                }}
              />
            ) : null}

            {this.state.showFeedbacks ? this._renderFeedbacks() : null}

            <Separator bordered>
              <Text style={styles.titleSeperator}>More</Text>
            </Separator>
            <ListItemIcon
              onPress={() =>
                Linking.openURL("https://etet.app/terms-conditions/")
              }
              icon="md-document"
              label="Terms of use"
            />
            <ListItemIcon
              onPress={() => Linking.openURL("https://etet.app/faq/")}
              icon="ios-chatbubbles"
              label="FAQs"
            />
            <ListItemIcon
              onPress={() => Linking.openURL("https://etet.app/credits/")}
              icon="md-phone-landscape"
              label="Credits"
            />
            <ListItemIcon
              onPress={() =>
                Linking.openURL("https://etet.app/privacy-policy/")
              }
              icon="md-lock"
              label="Privacy policy"
            />
            <ListItemIcon
              onPress={this.handleLogout}
              icon="md-log-out"
              label="Logout"
            />
          </List>

          <LevelModal
            visible={this.state.feedbackModal}
            onRequestClose={() => {
              this.closeFeedbackModal();
            }}
          >
            <FeedbackReduxForm
              onSubmit={this.feedbackSubmit}
              loading={this.state.loading}
              sent={this.state.feedbackSent}
              feedOn={this.turnFeedbackOn}
            />
          </LevelModal>
        </Content>

        <Display isVisible={incomingFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={incomingFlag}
          >
            <CallKit type={"incoming"} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={showTimerFlag}
          >
            <CallKit type={"timer"} />
          </LevelModal>
        </Display>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    planStatus: state.setting.planStatus,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag
  }),
  { checkRemainingStorage, sendbirdLogin, sendbirdLogout }
)(SettingMenu);

// export default SettingsSub1;

const styles = StyleSheet.create({
  listContainers: { marginBottom: 50 },
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: "#fff"
  },
  divider: {
    paddingLeft: 15,
    marginTop: 10,
    fontSize: 17,
    marginTop: 20
  },
  divider2: {
    paddingLeft: 15,
    marginTop: 20,
    marginBottom: 10,
    fontSize: 17
  },
  title: {
    paddingLeft: 20,
    marginTop: 10,
    fontSize: 17
  },
  remainingStorageCont: {
    flex: 1,
    justifyContent: "space-evenly",
    padding: 10
  },
  profileThumbnail: { width: 56, height: 56, borderRadius: 28 },
  icon: { color: "gray" },
  titleSeperator: {
    paddingBottom: Platform.OS === "ios" ? 17 : 0
  }
});
