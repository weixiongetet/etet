// App.js

import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
// import { Dialogflow_V2 } from "react-native-dialogflow";

// import { dialogflowConfig } from "../../../env";

const BOT_USER = {
  _id: 2,
  name: "E-Bot",
  avatar: "https://drive.google.com/open?id=1LOAsiG6QrG7JNrrgyk900zAu9t_7ReHC"
};

class AiChatBotScreen extends Component {
  state = {
    messages: [
      {
        _id: 1,
        text: `Hi! I am the E-Bot 🤖 from E-Tet.\n\nHow may I help you with today?`,
        createdAt: new Date(),
        user: BOT_USER
      }
    ]
  };

  componentDidMount() {
    // Dialogflow_V2.setConfiguration(
    //   dialogflowConfig.client_email,
    //   dialogflowConfig.private_key,
    //   Dialogflow_V2.LANG_ENGLISH_US,
    //   dialogflowConfig.project_id
    // );
    // this.ai();
  }
  //https://dialogflow.googleapis.com/v2/projects/bots-haejcd/agent/sessions/12345:detectIntent

  async ai(message) {
    try {
      // let body = JSON.stringify({
      //   lang: "en",
      //   query: message,
      //   sessionId: "12345",
      //   timezone: "America/New_York"
      // });

      let body = JSON.stringify({
        query_input: {
          text: {
            text: message,
            language_code: "en-US"
          }
        }
      });
      console.log(body);

      // let body = JSON.stringify({{
      //   lang: "en",
      //   query: "Date today",
      //   sessionId: "12345",
      //   timezone: "America/New_York"
      // };

      let response = await fetch(
        "https://dialogflow.googleapis.com/v2/projects/bots-haejcd/agent/sessions/123456:detectIntent",
        {
          headers: {
            Authorization:
              "Bearer ya29.ImWUB6AMIOglwBSsxJ1Ff_CEd5EmTR2ZvMzhMBeBEFT5mOzF_5M3slwzA8USED5v1QYI_YlVbw2enem1duaZoywc1qZDnSS5HpB3OPMiIVeA8xflveywvsAj-AX-kcNdk07umF3L",
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          method: "POST",
          body: body
        }
      );

      let jsonRes = await response.json();

      let text = "";
      console.log("dialogflow response:: ", jsonRes);
      text = jsonRes.queryResult.fulfillmentMessages[0].text.text[0];
      this.sendBotResponse(text);
      console.log("Response from Dialogflow " + text);
      //text = jsonRes.responses.fulfillment.speech;
      //console.log("Response from Dialogflow " + text);
    } catch (err) {
      console.log("Dialogflow error" + err);
      return Promise.reject("Dialog flow error.");
    }
  }

  // handleGoogleResponse(result) {
  //   console.log("result ::: " + JSON.stringify(result));
  //   let text = JSON.stringify(
  //     result.queryResult.fulfillmentMessages[0].text.text[0]
  //   );
  //   this.sendBotResponse(text);
  // }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));

    let message = messages[0].text;
    // Dialogflow_V2.requestQuery(
    //   message,
    //   result => this.ai(result),
    //   error => console.log(error)
    // );

    this.ai(message);
  }

  sendBotResponse(text) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      createdAt: new Date(),
      user: BOT_USER
    };

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg])
    }));
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1
          }}
        />
      </View>
    );
  }
}

export default AiChatBotScreen;
