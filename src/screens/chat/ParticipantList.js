import { Body, Button, Icon, Left, List, ListItem, Right, Separator, Text, View } from "native-base";
import React, { Component } from "react";
import { Alert, FlatList, RefreshControl, ScrollView, StyleSheet, TextInput } from "react-native";
import { CachedImage } from "react-native-cached-image";
import AntDesign from "react-native-vector-icons/AntDesign";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import { NavBar, NavButton, NavSearch } from "../../components/NavComponent";
import { addAdminMessage, createIndieChannel, inviteToChannel, kickUser, leaveChannel, rejoinUser, selectChannel, updateOperatorChannel, updateSearch } from "../../redux/modules/chat";
import { getDB_Friends } from "../../redux/modules/contacts";
import { sbUpdateGroupChannel } from "../../sendbird";

class ParticipantList extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Text>Group participants</Text>,
      headerLeft: (
        <NavButton
          onPress={() => {
            navigation.navigate("Message", {
              name: navigation.getParam("name"),
              profileUrl: navigation.getParam("profileUrl")
            });
          }}
          name="md-arrow-back"
        />
      ),
      headerRight: (
        <NavBar>
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
          />
          <NavButton
            onPress={() => {
              navigation.getParam("leaveGroup")();
            }}
            danger
            name="md-log-out"
          />
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      search: false,
      searchString: "",
      editName: false,
      newGroupName: ""
    };
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };

  async componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      search: this.state.search,
      toggleSearch: this.toggleSearch,
      leaveGroup: this.leaveGroup
    });
    this.props.getDB_Friends();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }
  }

  createChannelOneToOne = async user => {
    await this.props.createIndieChannel({
      userA: this.props.me,
      userB: {
        userId: user.userID,
        nickname: user.firstName,
        profileUrl: user.profilePictureURL
      }
    });
  };

  leaveGroup = () => {
    var allMemberList = this.props.channel.members;

    var allUserList = _.filter(allMemberList, user => {
      return user.userId !== this.props.user.userID;
    })

    var operatorName = '';
    var operatorList = [];

    if (allUserList.length > 0) {
      operatorName = allUserList[0].nickname;
      operatorList = allUserList.splice(0, 1);
    }

    Alert.alert(
      "Leaving group",
      "Are you sure?",
      [
        {
          text: "Yes",
          onPress: async () => {

            // const res = await sbUpdateOperatorGroupChannel({
            //   channelUrl: this.props.channel.url,
            //   operatorList: operatorList,
            //   userList: allUserList,
            // });

            // await this.props.addAdminMessage(
            //   this.props.channel,
            //   `${operatorName} is now an admin.`
            // );

            await this.props.addAdminMessage(
              this.props.channel,
              `${this.props.user.firstName} leaves the chat.`
            );
            await this.props.leaveChannel(this.props.channel.url);
            this.props.navigation.navigate("Chat");

          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  };

  render() {

    const inGroupList = this.props.channel
      ? this.props.channel.members.map(member => {
        return member.userId;
      })
      : [];

    const availableList = _.filter(this.props.friends, parti => {
      let passSearch = new RegExp(this.state.searchString, "i").test(
        parti.nickname
      );
      let notInGroup = !_.contains(inGroupList, parti.userID);
      return passSearch && notInGroup;
    });


    return (

      <ScrollView style={{ flex: 1 }}>
        <Display isVisible={this.props.channel.myRole === "operator"}>
          <ListItem
            thumbnail
            noBorder
            onPress={() => {
              this.setState({
                editName: !this.state.editName
              });
            }}
          >
            <Left
              style={{
                justifyContent: "center",
                alignItems: "flex-start"
              }}
            >
              <AntDesign style={{ fontSize: 25 }} name="edit" />
            </Left>
            <Body>
              <Text>Change group name</Text>
            </Body>
            {this.state.editName ? (
              <Right>
                <Icon name="md-close" />
              </Right>
            ) : null}
          </ListItem>
        </Display>

        <Display isVisible={this.state.editName}>
          <View
            style={{
              flexDirection: "row",
              paddingLeft: 20,
              paddingRight: 20,
              justifyContent: "space-between",
              alignContent: "center"
            }}
          >
            <TextInput
              style={{ flex: 1 }}
              underlineColorAndroid="transparent"
              placeholder="  New name"
              value={this.state.newGroupName}
              autoFocus={true}
              onChangeText={text => {
                this.setState({ newGroupName: text });
              }}
            />
            <Button
              small
              disabled={!this.state.newGroupName}
              style={{ alignSelf: "center" }}
              onPress={async () => {
                try {
                  const res = await sbUpdateGroupChannel({
                    channelUrl: this.props.channel.url,
                    cover: this.props.channel.imgDownloadLink,
                    name: this.state.newGroupName
                  });
                  await this.props.selectChannel(res);

                  this.props.navigation.setParams({
                    name: this.props.channel.name
                  });
                } catch (err) {
                  alert(err);
                } finally {
                  this.setState({ editName: false });
                }
              }}
            >
              <Text>Update</Text>
            </Button>
          </View>
        </Display>

        <Display isVisible={this.state.search}>
          <TextInput
            style={{ paddingLeft: 15 }}
            underlineColorAndroid="transparent"
            placeholder="  Search .."
            value={this.state.searchString}
            autoFocus={true}
            onChangeText={text => {
              this.setState({ searchString: text });
            }}
          />
        </Display>

        <List style={{ flexDirection: "column" }}>
          <Separator bordered>
            <Text>In Group</Text>
          </Separator>

          <FlatList
            data={_.filter(
              this.props.channel ? this.props.channel.members : [],
              friend => {
                return new RegExp(this.state.searchString, "i").test(
                  friend.nickname
                );
              }
            )}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              if (item.userId === this.props.user.userID) {
                return null;
              }
              return (
                <ListItem
                  thumbnail
                  noBorder
                  key={item.userId}
                  onPress={async () => {
                    if (this.props.channel.myRole !== "operator") return;

                    Alert.alert(
                      "Removing participant",
                      "Are you sure?",
                      [
                        {
                          text: "Yes",
                          onPress: async () => {
                            try {
                              await this.props.kickUser({
                                user: item,
                                channel: this.props.channel
                              });
                              await this.props.addAdminMessage(
                                this.props.channel,
                                `${
                                item.nickname
                                } has been removed from the chat.`
                              );
                            } catch (err) {
                              alert(err);
                            }
                          }
                        },
                        {
                          text: "No",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }}
                >
                  <Left>
                    <CachedImage
                      style={styles.smallThumbnail}
                      source={{
                        uri: item.profileUrl
                      }}
                    />
                  </Left>
                  <Body>
                    <Text>{item.nickname}</Text>
                  </Body>
                  {this.props.channel.myRole === "operator" ? (
                    <Right>
                      <Icon name="md-close" />
                    </Right>
                  ) : null}
                </ListItem>
              );
            }}
          />

          <Display isVisible={this.props.channel.myRole === "operator"}>
            <Separator bordered>
              <Text>Available</Text>
            </Separator>
          </Display>

          <Display isVisible={this.props.channel.myRole === "operator"}>
            <FlatList
              data={availableList}
              keyExtractor={(item, index) => index}
              renderItem={({ item }) => {
                if (item.userID === this.props.user.userID) {
                  return null;
                }

                return (
                  <ListItem
                    thumbnail
                    noBorder
                    onPress={async () => {
                      var bannedUserListQuery = this.props.channel.createBannedUserListQuery();
                      bannedUserListQuery.next(async (users, error) => {
                        if (error) {
                          return;
                        }
                        if (
                          _.contains(
                            users.map(user => user.userId),
                            item.userID
                          )
                        ) {
                          await this.props.rejoinUser({
                            user: item,
                            channel: this.props.channel
                          });
                          await this.props.inviteToChannel({
                            userList: [item],
                            channelUrl: this.props.channel.url
                          });
                          await this.props.addAdminMessage(
                            this.props.channel,
                            `${item.firstName} has rejoined the chat.`
                          );
                        } else {
                          await this.props.inviteToChannel({
                            userList: [item],
                            channelUrl: this.props.channel.url
                          });
                          await this.props.addAdminMessage(
                            this.props.channel,
                            `${item.firstName} has joined the chat.`
                          );
                        }
                      });
                      // this.props.navigation.navigate("Chat");
                    }}
                    key={item.userID}
                  >
                    <Left>
                      <CachedImage
                        style={styles.smallThumbnail}
                        source={{ uri: item.profilePictureURL }}
                      />
                    </Left>
                    <Body>
                      <Text>{item.firstName}</Text>
                      {/* <Text note>{item.lastMessage.message}</Text> */}
                    </Body>
                    <Right>
                      <Icon name="md-add" />
                    </Right>
                  </ListItem>
                );
              }}
              progressViewOffset={3}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.isFetching}
                  onRefresh={this.props.getDB_Friends}
                />
              }
            />
          </Display>
        </List>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  me: state.chat.user,
  // friends: state.chat.friends,
  friends: state.contacts.dbFriendsList,
  isFetching: state.chat.isFetching,
  searchString: state.chat.searchString,
  channel: state.chat.channel
});

export default connect(
  mapStateToProps,
  {
    updateSearch,
    getDB_Friends,
    createIndieChannel,
    leaveChannel,
    inviteToChannel,
    addAdminMessage,
    kickUser,
    rejoinUser,
    selectChannel,
    updateOperatorChannel
  }
)(ParticipantList);

const styles = StyleSheet.create({
  vaultView: { flex: 1, padding: 8 },
  icon: { color: "#616161" },
  iconBack: { backgroundColor: "#eeeeee" },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  headerTitle: { flex: 1, flexDirection: "row" },
  headerRight: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingRight: 15
  },
  smallThumbnail: { width: 36, height: 36, borderRadius: 18 },
  actionThumbnail: { width: 36, height: 36 }
});
