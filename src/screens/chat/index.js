import moment from "moment";
import { Badge, Body, Button, Left, List, ListItem, Right, Text, View } from "native-base";
import React, { Component } from "react";
import { Alert, FlatList, Image, RefreshControl, StyleSheet, TextInput } from "react-native";
import { CachedImage } from "react-native-cached-image";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { connect } from "react-redux";
import SendBird from "sendbird";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import { NavBar, NavButton, NavSearch } from "../../components/NavComponent";
import { addAdminMessage, createIndieChannel, loadChannels, selectChannel, sendbirdLogin, sendbirdLogout } from "../../redux/modules/chat";
import { getDB_Friends } from "../../redux/modules/contacts";
import { addNotification, clearNotification } from "../../redux/modules/notify";
import { sbGetGroupChannel } from "../../sendbird";




class Chat extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <NavBar>
          <NavButton
            onPress={() => {
              navigation.getParam("leavePrompt")();
            }}
            danger
            name="md-trash"
            isVisible={navigation.getParam("selectMode")}
          />
          <NavButton
            onPress={() => {
              navigation.getParam("clearSelection")();
            }}
            name="md-close"
            isVisible={navigation.getParam("selectMode")}
          />
          {/* <NavButton
            onPress={() => {
              navigation.getParam("toggleArchive")();
            }}
            name="md-archive"
            disable={!navigation.getParam("showArchive")}
          /> */}
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
            isVisible={!navigation.getParam("selectMode")}
          />
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      showArchive: false,
      channelUrlMap: {},
      selectMode: false,
      selectedChannel: {},
      search: false,
      searchString: ""
    };
  }

  async componentWillMount() {
    const { navigation } = this.props;
    console.log("navigationpass", navigation)


    navigation.setParams({
      selectMode: this.state.selectMode,
      showArchive: this.state.showArchive,
      leavePrompt: this.leavePrompt,
      clearSelection: this.clearSelection,
      toggleArchive: this.toggleArchive,
      search: this.state.search,
      toggleSearch: this.toggleSearch
    });

    this.props.getDB_Friends();

    let sb = SendBird.getInstance();
    console.log("Sendbird Instance", sb);
    if (!sb || (sb && sb.connecting === false)) {
      console.log("sign in sendbird now", this.props.user);
      await this.props.sendbirdLogin(this.props.user);
      console.log("end sign in sendbird now", this.props.user);

      sb = SendBird.getInstance();
    }

    await this.props.loadChannels();
    let tmp = {};
    for (var i = 0; i < this.props.channels.length; ++i) {
      tmp[this.props.channels[i].url] = this.props.channels[i].name;
    }

    this.setState({
      channelUrlMap: tmp
    });

    var ChannelHandler = new sb.ChannelHandler();

    // const params = navigation.state.params;

    // if (params && params.userID && params.name && params.profileUrl) {
    //   let userID = params.userID;
    //   let name = params.name;
    //   let profileUrl = params.profileUrl;
    //   console.log("yesparams", params)
    //   await this.createChannelOneToOne(params);

    //   this.props.navigation.navigate("Message", {
    //     userID,
    //     name,
    //     profileUrl
    //   });
    // }

    // for sendbird notification in foreground and exactly in chat tab
    // ChannelHandler.onMessageReceived = (channel, message) => {
    //   this.props.loadChannels();
    //   this.props.addNotification(-99);

    //   if (this.props.channel && this.props.channel.url === channel.url) {
    //     return;
    //   }

    //   let subtitle;
    //   // if (this.state.channelUrlMap[message.channelUrl] !== "oto")
    //   //   subtitle = this.state.channelUrlMap[message.channelUrl];
    //   // // Alert.alert("hih", "background push656")
    //   // const localNotification = new firebase.notifications.Notification({
    //   //   show_in_foreground: true
    //   // }).android
    //   //   .setChannelId("notify-channel")
    //   //   .android.setPriority(firebase.notifications.Android.Priority.High)
    //   //   .setTitle(`${message.sender.nickname}`)
    //   //   .setBody(message.message)
    //   //   .setData(message);


    //   // if (this.props.user && !!this.props.user.fcmToken)
    //   //   firebase.notifications().displayNotification(localNotification);
    // };
    // sb.addChannelHandler(`CHANNEL_LISTENER`, ChannelHandler);
  }

  createChannelOneToOne = async user => {
    console.log("usercreatechannelmeeee", this.props.me)
    await this.props.createIndieChannel({
      userA: this.props.me,
      userB: {
        userId: user.userID,
        nickname: user.name,
        profileUrl: user.profileUrl
      }
    });
  };


  componentWillUnmount() {
    const sb = SendBird.getInstance();
    sb.removeChannelHandler(`CHANNEL_LISTENER`);
    this.props.sendbirdLogout();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectMode !== prevState.selectMode) {
      this.props.navigation.setParams({
        selectMode: this.state.selectMode
      });
    }
    if (this.state.showArchive !== prevState.showArchive) {
      this.props.navigation.setParams({
        showArchive: this.state.showArchive
      });
    }
    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };

  toggleArchive = () => {
    this.setState({
      showArchive: !this.state.showArchive
    });
  };

  leavePrompt = () => {
    Alert.alert(
      "Leave channels",
      "Are you sure?",
      [
        {
          text: "Yes",
          onPress: async () => {
            this.leaveSelectedGroup();
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  };

  leaveSelectedGroup = () => {
    let tmpPromises = [];
    let tmpKeys = Object.keys(this.state.selectedChannel);
    for (var i = 0; i < tmpKeys.length; i++) {
      if (this.state.selectedChannel[tmpKeys[i]]) {
        tmpPromises.push(
          sbGetGroupChannel(tmpKeys[i]).then(channel => {
            return this.props
              .addAdminMessage(
                channel,
                `${this.props.user.firstName} has left the chat.`
              )
              .then(() => {
                return channel.leave((response, error) => {
                  if (error) {
                    return Promise.reject(error);
                  }
                  return Promise.resolve(response);
                });
              });
          })
        );
      }
    }

    Promise.all(tmpPromises)
      .then(() => {
        this.clearSelection();
        setTimeout(() => {
          this.props.loadChannels();
        }, 1000)
      })
      .catch(err => {
        alert(err);
      });
  };

  clearSelection = () => {
    this.setState({
      selectMode: false,
      selectedChannel: {}
    });
  };

  displayOppName = data => {
    const jsonData = JSON.parse(data);
    if (jsonData.userA.userId === this.props.user.userID) {
      return jsonData.userB.nickname;
    } else {
      return jsonData.userA.nickname;
    }
  };

  displayOppImage = data => {
    const jsonData = JSON.parse(data);
    let friendPfr = jsonData.userA.profileUrl;

    _.forEach(this.props.friends, (data, key) => {
      if (data.userID == jsonData.userA.userId) {
        friendPfr = data.profilePictureURL;
      } else if (data.userID == jsonData.userB.userId) {
        friendPfr = data.profilePictureURL;
      } else {
        if (jsonData.userA.userId === this.props.user.userID) {
          friendPfr = jsonData.userB.profileUrl;
        }
      }
    })

    return friendPfr;
  };


  render() {
    if (!this.props.user) return null;

    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;
    let displayingChannel = _.filter(this.props.channels, channel => {
      let name = channel.name;
      if (channel.name === "oto") name = this.displayOppName(channel.data);
      return (
        new RegExp(this.state.searchString, "i").test(name) &&
        !(!this.state.showArchive && channel.members.length === 1)
      );
    });


    return (
      <View style={{ flex: 1 }}>
        <Display isVisible={this.state.search}>
          <TextInput
            style={{ paddingLeft: 15, paddingTop: 10 }}
            underlineColorAndroid="transparent"
            placeholder="  Search name.."
            value={this.state.searchString}
            autoFocus={true}
            onChangeText={text => {
              this.setState({ searchString: text });
            }}
          />
        </Display>

        <Display isVisible={displayingChannel.length === 0 && this.state.searchString == ''}>
          <Image
            source={require(`../../assets/chat.png`)}
            style={{
              marginTop: 50,
              marginBottom: 20,
              width: 80,
              height: 80,
              alignSelf: "center",
              justifyContent: "center",
              resizeMode: "contain"
            }}
          />
        </Display>

        <Display isVisible={displayingChannel.length === 0 && this.state.searchString == ''}>
          <Text
            style={{
              alignSelf: "center",
              justifyContent: "center"
            }}
          >
            No chats yet!
          </Text>
        </Display>


        <Display isVisible={displayingChannel.length === 0 && this.state.searchString !== ''}>
          <Text
            style={{
              alignSelf: "center",
              justifyContent: "center",
              color: 'red'
            }}
          >
            No results found!
          </Text>
        </Display>


        <List style={{ flex: 1, flexDirection: "column" }}>
          <FlatList
            data={displayingChannel}
            keyExtractor={(item, index) => item.url}
            extraData={{
              selectedChannel: this.state.selectedChannel,
              showArchive: this.state.showArchive
            }}
            renderItem={({ item }) => {

              if (!this.state.showArchive && item.members.length === 1)
                return null;

              let profileUrl = item.coverUrl;
              let name = item.name;
              if (item.name === "oto") {
                profileUrl = this.displayOppImage(item.data);
                name = this.displayOppName(item.data);
              }

              let fileName = "File"
              let tmpIco;
              switch (item.lastMessage.type) {
                case "image/png":
                case "image/jpg":
                case "image/jpeg":
                case "image/bmp":
                case "image/gif":
                case "image/vnd.microsoft.icon":
                case "image/svg+xml":
                case "image/tiff":
                case "image/webp":
                  tmpIco = "image";
                  fileName = "Photo"
                  break;
                case "application/pdf":
                  tmpIco = "file-pdf-o";
                  break;
                case "application/vnd.ms-excel":
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                  tmpIco = "file-excel-o";
                  break;
                case "application/vnd.ms-powerpoint":
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                  tmpIco = "file-powerpoint-o";
                  break;
                case "application/msword":
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                  tmpIco = "file-word-o";
                  break;
                case "text/plain":
                  tmpIco = "file-text-o";
                  break;
                case "application/x-freearc":
                case "application/x-bzip":
                case "application/x-bzip2":
                case "application/java-archive":
                case "application/x-rar-compressed":
                case "application/x-tar":
                case "application/zip":
                case "application/x-7z-compressed":
                  tmpIco = "file-zip-o";
                  break;
                case "audio/aac":
                case "audio/midi audio/x-midi":
                case "audio/mpeg":
                case "audio/ogg":
                case "audio/wav":
                case "audio/webm":
                case "audio/3gpp":
                case "audio/3gpp2":
                  tmpIco = "file-audio-o";
                  break;
                case "video/mp4":
                case "video/x-msvideo":
                case "video/mpeg":
                case "video/ogg":
                case "video/mp2t":
                case "video/webm":
                case "video/3gpp":
                case "video/3gpp2":
                  tmpIco = "file-video-o";
                  break;
                default:
                  tmpIco = "file-o";
                  break;
              }

              return (
                <ListItem
                  thumbnail
                  // noBorder
                  key={item.url}
                  onPress={() => {
                    if (this.state.selectMode) {
                      let tmp = { ...this.state.selectedChannel };
                      tmp[item.url] = !this.state.selectedChannel[item.url];
                      this.setState({
                        selectedChannel: tmp
                      });
                    } else {
                      this.props.selectChannel(item);
                      this.props.clearNotification('chat');
                      this.props.navigation.navigate("Message", {
                        name,
                        profileUrl
                      });
                    }
                  }}
                  onLongPress={async () => {
                    if (this.state.selectMode) {
                      this.clearSelection();
                      return;
                    }
                    if (!this.state.selectMode) {
                      await this.setState({
                        selectMode: true
                      });
                      let tmp = { ...this.state.selectedChannel };
                      tmp[item.url] = true;
                      await this.setState({
                        selectedChannel: tmp
                      });
                      console.log('tnp', tmp);
                    }
                  }}
                  onPress={() => {
                    if (this.state.selectMode) {
                      let tmp = { ...this.state.selectedChannel };
                      tmp[item.url] = !this.state.selectedChannel[item.url];
                      this.setState({
                        selectedChannel: tmp
                      });
                    } else {
                      this.props.selectChannel(item);
                      this.props.navigation.navigate("Message", {
                        name,
                        profileUrl
                      });
                    }
                  }}
                >
                  <Left>
                    <CachedImage
                      style={styles.thumbnail}
                      imageStyle={styles.thumbnailBackground}
                      source={{
                        uri: this.state.selectedChannel[item.url]
                          ? "https://static.thenounproject.com/png/446229-200.png"
                          : profileUrl
                      }}
                    />
                  </Left>
                  <Body>
                    <Text
                      style={{
                        fontFamily: "Roboto",
                        fontWeight: "500",
                        fontSize: 16,
                        color: "#3b4a5f"
                      }}
                    >
                      {name}
                    </Text>


                    {item.lastMessage.message ? (
                      <Text ellipsizeMode="tail" numberOfLines={1} note>
                        {item.lastMessage.message}
                      </Text>
                    ) :
                      <View style={{ flexDirection: "row" }}>
                        <FontAwesome style={styles.fileIcon} name={tmpIco} />
                        <Text ellipsizeMode="tail" numberOfLines={1} note style={{ paddingLeft: 10 }}>
                          {fileName}
                        </Text>
                      </View>
                    }


                  </Body>
                  {this.state.selectedChannel[item.url] ? null : (
                    <Right>
                      {item.unreadMessageCount ? (
                        <Badge small success>
                          <Text
                            style={{ fontSize: 10, fontWeight: "500" }}
                          >{` ${item.unreadMessageCount} `}</Text>
                        </Badge>
                      ) : null}
                      <Text note>
                        {moment(item.lastMessage.createdAt).isBefore(
                          moment().subtract(1, "days")
                        )
                          ? moment(item.lastMessage.createdAt).isBefore(
                            moment().subtract(1, "years")
                          )
                            ? moment(item.lastMessage.createdAt).format(
                              "D MMM Y"
                            )
                            : moment(item.lastMessage.createdAt).format("D MMM")
                          : moment(item.lastMessage.createdAt).format("h:mm A")}
                      </Text>
                    </Right>
                  )}
                </ListItem>
              );
            }}
            progressViewOffset={3}
            refreshControl={
              <RefreshControl
                refreshing={this.props.isFetching}
                onRefresh={this.props.loadChannels}
              />
            }
          />
        </List>

        <View
          style={{
            position: "absolute",
            right: 20,
            bottom: 25
          }}
        >
          <Button
            transparent
            style={{ backgroundColor: "white", borderRadius: 4 }}
            onPress={() => {
              if (_.isEmpty(this.props.friends)) {
                alert("No friend to chat yet.");
              } else {
                this.props.navigation.navigate({
                  routeName: "FriendList"
                });
              }
            }}
          >
            <FontAwesome
              style={{
                color: "#3370FF",
                fontSize: 35,
              }}
              name="pencil-square-o"
            />
          </Button>
          {/* <Icon
            style={{
              position: "absolute",
              right: 5,
              color: "#3370FF",
              fontSize: 15
            }}
            name="md-add"
          /> */}
        </View>

        <Display isVisible={incomingFlag}> {/*is friends, call app to app*/}
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={incomingFlag} >
            <CallKit type={'incoming'} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}> {/*is friends, call app to app*/}
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={showTimerFlag} >
            <CallKit type={'timer'} />
          </LevelModal>
        </Display>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  isFetching: state.chat.isFetching,
  channels: state.chat.channels,
  channel: state.chat.channel,
  incomingFlag: state.contacts.incomingFlag,
  showTimerFlag: state.contacts.showTimerFlag,
  friends: state.contacts.dbFriendsList,
  me: state.chat.user
});

export default connect(
  mapStateToProps,
  {
    sendbirdLogin,
    sendbirdLogout,
    loadChannels,
    selectChannel,
    addAdminMessage,
    getDB_Friends,
    addNotification,
    clearNotification,
    createIndieChannel
  }
)(Chat);

const styles = StyleSheet.create({
  vaultView: { flex: 1, padding: 8 },
  icon: { color: "#616161" },
  iconBack: { backgroundColor: "transparent" },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  thumbnail: { width: 56, height: 56 },
  thumbnailBackground: {
    borderRadius: 24,
    borderWidth: 2,
    borderColor: "#eeeeee"
  },
  fileIcon: {
    color: "#a7aebb",
    fontSize: 20
  },
  item: {
    fontFamily: "Roboto",
    fontSize: 16,
    color: "#3b4a5f"
  },
});
