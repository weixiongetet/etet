import { Body, Content, Left, List, ListItem, Separator, Text, View } from "native-base";
import React, { Component } from "react";
import { FlatList, Image, Platform, RefreshControl, StyleSheet, TextInput } from "react-native";
import { CachedImage } from "react-native-cached-image";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import { NavBar, NavSearch } from "../../components/NavComponent";
import { createIndieChannel, updateSearch } from "../../redux/modules/chat";
import { getDB_Friends } from "../../redux/modules/contacts";

class FriendList extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Text style={styles.screenTitle}>Select participants</Text>,
      headerRight: (
        <NavBar>
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
          />
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      search: false,
      searchString: ""
    };
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };

  async componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      search: this.state.search,
      toggleSearch: this.toggleSearch
    });
    this.props.getDB_Friends();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }
  }

  createChannelOneToOne = async user => {
    await this.props.createIndieChannel({
      userA: this.props.me,
      userB: {
        userId: user.userID,
        nickname: user.firstName,
        profileUrl: user.profilePictureURL
      }
    });
  };

  render() {

    let displayingFriends = _.filter(this.props.friends, friend => {
      return new RegExp(this.state.searchString, "i").test(
        friend.firstName
      );
    });


    return (
      <View style={styles.container}>
        <Content>
          <ListItem
            thumbnail
            noBorder
            onPress={() => {
              this.props.navigation.navigate({
                routeName: "GroupList"
              });
            }}
          >
            <Left>
              <CachedImage
                style={styles.smallThumbnail}
                source={{
                  uri:
                    "https://static.vecteezy.com/system/resources/previews/000/544/231/non_2x/group-of-people-icon-flat-design-vector-with-shadow-on-isolated-white-background-black-color-and-monocrome-theme.jpg"
                }}
              />
            </Left>
            <Body>
              <Text>New group</Text>
            </Body>
          </ListItem>

          <Separator bordered>
            <Text style={styles.titleSeperator}>Friends</Text>
          </Separator>

          <Display isVisible={this.state.search}>
            <TextInput
              style={{ paddingLeft: 15, paddingTop: 10 }}
              underlineColorAndroid="transparent"
              placeholder="  Search .."
              value={this.state.searchString}
              autoFocus={true}
              onChangeText={text => {
                this.setState({ searchString: text });
              }}
            />
          </Display>

          <Display isVisible={displayingFriends.length === 0 && this.state.searchString == ''}>
            <Image
              source={require(`../../assets/chat.png`)}
              style={{
                marginTop: 30,
                marginBottom: 20,
                width: 80,
                height: 80,
                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain"
              }}
            />
          </Display>

          <Display isVisible={displayingFriends.length === 0 && this.state.searchString == ''}>
            <Text
              style={{
                alignSelf: "center",
                justifyContent: "center",
              }}
            >
              No friends found, add friends to start chat!
          </Text>
          </Display>

          <Display isVisible={displayingFriends.length === 0 && this.state.searchString !== ''}>
            <Text
              style={{
                alignSelf: "center",
                justifyContent: "center",
                color: 'red'
              }}
            >
              No results found!
          </Text>
          </Display>

          <List style={styles.list}>
            <FlatList
              data={displayingFriends}
              keyExtractor={(item, index) => index}
              renderItem={({ item }) => {
                if (item.userID === this.props.user.userID) {
                  return null;
                }

                return (
                  <ListItem
                    thumbnail
                    noBorder
                    onPress={async () => {
                      await this.createChannelOneToOne(item);

                      await this.props.navigation.dispatch(
                        NavigationActions.back({ key: null })
                      );

                      this.props.navigation.navigate("Message", {
                        userID: item.userID,
                        name: item.firstName,
                        profileUrl: item.profilePictureURL
                      });
                    }}
                    key={item.userID}
                  >
                    <Left>
                      <CachedImage
                        style={styles.smallThumbnail}
                        source={{ uri: item.profilePictureURL }}
                      />
                    </Left>
                    <Body>
                      <Text>{item.firstName}</Text>
                      {/* <Text note>{item.lastMessage.message}</Text> */}
                    </Body>
                  </ListItem>
                );
              }}
              progressViewOffset={3}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.isFetching}
                  onRefresh={this.props.getDB_Friends}
                />
              }
            />
          </List>
        </Content>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  me: state.chat.user,
  // friends: state.chat.friends,
  friends: state.contacts.dbFriendsList,
  isFetching: state.chat.isFetching,
  searchString: state.chat.searchString
});

export default connect(
  mapStateToProps,
  { updateSearch, getDB_Friends, createIndieChannel }
)(FriendList);

const styles = StyleSheet.create({
  vaultView: { flex: 1, padding: 8 },
  icon: { color: "#616161" },
  iconBack: { backgroundColor: "#eeeeee" },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  smallThumbnail: { width: 36, height: 36, borderRadius: 18 },
  container: { flex: 1 },
  list: { flex: 1, flexDirection: "column" },
  screenTitle: {
    paddingLeft: 10
  },
  titleSeperator: {
    paddingBottom: Platform.OS === "ios" ? 17 : 0,
  }
});
