import AsyncStorage from "@react-native-community/async-storage";
import { Body, Button, Icon, Left, List, ListItem, Right, Text, Thumbnail, View } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, Alert, FlatList, PermissionsAndroid, Platform, ScrollView, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import { CachedImage } from "react-native-cached-image";
import { DocumentPicker, DocumentPickerUtil } from "react-native-document-picker";
import FilePickerManager from "react-native-file-picker";
import { GiftedChat } from "react-native-gifted-chat";
import ImagePicker from "react-native-image-picker";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Voximplant } from "react-native-voximplant";
import { connect } from "react-redux";
import SendBird from "sendbird";
import request from "superagent";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import { NavBar, NavButton, NavSearch } from "../../components/NavComponent";
import CallManager from "../../manager/CallManager";
import LoginManager from "../../manager/LoginManager";
import { addChatFile, addMessage, getFriendContact, loadChannels, loadMessages, openFile, updateChatMsg } from "../../redux/modules/chat";
import { clearNotification } from "../../redux/modules/notify";
import { checkRemainingStorage } from "../../redux/modules/setting";
import { sbGetGroupChannel, sbSendFileMessage, sbSendTextMessage } from "../../sendbird";

const platform = Platform.OS;

class Message extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View style={styles.thumb}>
          <CachedImage
            style={styles.thumbnail}
            imageStyle={styles.thumbnailBackground}
            source={{ uri: navigation.getParam("profileUrl") }}
          />
          <Text style={{}}>{navigation.getParam("name")}</Text>
        </View>
      ),
      headerRight: (
        <NavBar>
          <NavButton
            onPress={() => {
              // do voice call
              navigation.getParam("pressCall")();
            }}
            name="md-call"
            isVisible={!navigation.getParam("isGroup")}
          />
          <NavButton
            onPress={() => {
              // do grp video call
              navigation.getParam("pressVideoCall")();
            }}
            name="md-videocam"
            isVisible={!navigation.getParam("isGroup")}
          />

          <NavButton
            onPress={() => {
              // do voice conference call
              navigation.getParam("pressGrpVideoCall")(false);
            }}
            name="md-call"
            isVisible={navigation.getParam("isGroup")}
          />
          <NavButton
            onPress={() => {
              // do grp video call
              navigation.getParam("pressGrpVideoCall")(true);
            }}
            name="md-videocam"
            isVisible={navigation.getParam("isGroup")}
          />
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
          />
          <NavButton
            onPress={() => {
              navigation.navigate({
                routeName: "ParticipantList",
                params: {
                  name: navigation.getParam("name"),
                  profileUrl: navigation.getParam("profileUrl")
                }
              });
            }}
            name="md-settings"
            isVisible={navigation.getParam("isGroup")}
          />
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      reachedTop: false,
      messages: [],
      processedMessages: [],
      readMembers: [],
      typingMembers: [],
      detailVisible: false,
      search: false,
      searchString: "",
      friendId: null,
      friendName: null,
      uploadingFile: false,
      path: "",
      msgFlag: false,
      flagLoad: false,
      fileBtnClick: false
    };
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };

  async componentDidMount() {

    const params = this.props.navigation.state.params;


    console.log("propoppsparams", params)
    console.log("loadcchannel", this.props.channel)
    if (this.props.channel) {
      var friendID = this.props.channel.members[0].userId;
      var friendName = this.props.channel.members[0].nickname;
      console.log("channeelll")

      if (this.props.channel.members[1].userId !== this.props.user.userID) {
        friendID = this.props.channel.members[1].userId;
        friendName = this.props.channel.members[1].nickname;
      }
    } else {
      console.log("elseeeeenullll", this.props.channel)
      if (params.userID) {
        console.log("yesyyyiiiidddd", params)
        friendID = params.userID;
        friendName = params.name;
      }
    }

    console.log("endddd")



    await this.props.getFriendContact(friendID);

    if (this.props.friendContact) {
      this.props.navigation.setParams({
        friendContactFlag: true
      });
    }

    this.setState({
      friendId: friendID,
      friendName: friendName
    });
  }

  async componentWillMount() {
    console.log("going_app_will_mount1111");
    const { navigation } = this.props;
    navigation.setParams({
      search: this.state.search,
      toggleSearch: this.toggleSearch,
      pressCall: this.pressCall,
      pressGrpVideoCall: this.pressGrpVideoCall,
      isGroup: this.props.channel.name !== "oto",
      loadChannel: this.props.loadChannels,
      friendContactFlag: false,
      addImageMessage: this.addImageMessage,
      addFileMessage: this.addFileMessage,
      pressVideoCall: this.pressVideoCall
    });

    this.props.clearNotification("chat");

    if (!this.props.channel) {
      this.props.navigation.navigate("Chat");
      return;
    }

    this.channel = await sbGetGroupChannel(this.props.channel.url);
    this.channel.markAsRead();
    this.debounce = _.debounce(() => {
      this.channel.endTyping();
    }, 2000);

    let prevMessageListQuery = this.channel.createPreviousMessageListQuery();
    prevMessageListQuery.limit = 30;
    prevMessageListQuery.reverse = true;

    this.messageQuery = prevMessageListQuery;

    let initMessages = await new Promise((resolve, reject) => {
      this.messageQuery.load(function (messages, error) {
        if (error) {
          reject(error);
        }
        resolve(messages);
      });
    });

    await this.setState({
      messages: initMessages
    });

    if (initMessages.length < 30) {
      this.setState({ reachedTop: true });
    }

    this.processMessages();
    this.loadReadReceipt(this.channel);

    const sb = SendBird.getInstance();
    var ChannelHandler = new sb.ChannelHandler();

    ChannelHandler.onMessageReceived = (channel, message) => {
      if (channel.url === this.props.channel.url) {
        console.log("messagereceived", message);
        this.addMessageToFlow(message);
        this.loadReadReceipt(channel);
      }
    };

    ChannelHandler.onReadReceiptUpdated = channel => {
      if (channel.url === this.props.channel.url) {
        this.loadReadReceipt(channel);
      }
    };

    ChannelHandler.onTypingStatusUpdated = channel => {
      if (channel.url === this.props.channel.url) {
        this.loadTypingStatus(channel);
      }
    };

    sb.addChannelHandler(`MESSAGE_LISTENER`, ChannelHandler);
  }

  componentWillUnmount() {
    console.log("going_app_will_unmount1111");
    const { params } = this.props.navigation.state;

    if (params && params.loadChannel) {
      params.loadChannel();
    }
    const sb = SendBird.getInstance();
    sb.removeChannelHandler(`MESSAGE_LISTENER`);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    console.log("diduodatemessage");

    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }

    // if (this.state.messages !== prevState.messages) {
    console.log("Before chat update");
    if (this.props.chatmsgFlag) {
      console.log("Inside chat update");
      await this.props.updateChatMsg(false);
      // let cloneMessages = await this.state.messages;
      console.log("Join_message11111", _.isArray(this.state.messages));
      console.log("Join_messa222222", _.isArray(this.props.messages));
      let oldMsg = this.state.messages;
      let newMsg = this.props.messages;
      // let joinMessage = oldMsg.concat(newMsg);
      // let joinMessage = [...oldMsg, ...newMsg];
      console.log("typeoldMsg", typeof oldMsg); // "function"
      console.log("typenewmsg", typeof newMsg); // "function"

      console.log("mergeearr", _.union(oldMsg, newMsg));

      // console.log("mergeearr", joinMessage)

      // console.log("Join_message22222", joinMessage)
      // await this.setState({
      //   messages: joinMessage
      // });
      // let mergeMsgGifted = this.state.messages.map(this.mapMessage);
      // console.log("mergeMsgGifted12345", mergeMsgGifted)
    }
    //}
  };

  processMessages = async () => {
    if (this.channel) this.channel.markAsRead();
    let messagesGifted = this.state.messages.map(this.mapMessage);

    await this.setState({
      processedMessages: messagesGifted
    });

    // await this.addPrevMessages();
    await this.props.updateChatMsg(false);
  };

  mapMessage = (msg, index) => {
    console.log("mapmessgae1111");

    var fileType = "";

    if (msg.messageType == "file") {
      var getFileType = msg.type.split("/");
      var splitFileType = getFileType[0];
      var splitFileType2 = getFileType[1];

      if (splitFileType == "image") {
        fileType = splitFileType;
      } else {
        fileType = msg.type;
      }
    }

    return {
      _id: index,
      text: msg.message,
      createdAt: msg.createdAt,
      user: {
        _id: msg._sender.userId,
        name: msg._sender.nickname,
        avatar: msg._sender.profileUrl
      },
      system: msg.customType === "GroupMessage",
      image: fileType === "image" ? msg.url : "",
      filetype: msg.messageType == "file" ? fileType : "",
      uri: fileType !== "" ? msg.url : "",
      messageType: msg.messageType,
      fileName: msg.messageType == "file" ? msg.name : ""
    };
  };

  loadReadReceipt = channel => {
    let readMembers = channel.getReadMembers(channel.lastMessage);
    this.setState({
      readMembers
    });
  };

  loadTypingStatus = channel => {
    let typingMembers = channel.getTypingMembers();
    this.setState({
      typingMembers
    });
  };

  pushMessage = async text => {
    this.setState({
      readMembers: []
    });
    let newMessage = await sbSendTextMessage(this.channel, text);
    this.addMessageToFlow(newMessage);
  };

  addMessageToFlow = async msg => {
    let cloneMessages = await this.state.messages.slice();
    cloneMessages.unshift(msg);
    await this.setState({
      messages: cloneMessages
    });

    this.processMessages();
  };

  addPrevMessages = () => {
    console.log("addprev1111");

    this.messageQuery.load(async (messages, error) => {
      if (error) {
        alert(error);
        return;
      }

      if (messages.length < 30) {
        this.setState({ reachedTop: true });
      }

      if (messages.length === 0) {
        return;
      }

      let cloneMessages = await this.state.messages.slice();
      await this.setState({
        messages: cloneMessages.concat(messages)
      });

      this.processMessages();
    });
  };

  addImageMessage = async () => {
    let channel = await sbGetGroupChannel(this.props.channel.url);

    return new Promise(function (resolve, reject) {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, response => {
        if (response.fileSize > 5242880) {
          alert("File capped at 5mb.");
          this.setState({
            uploadingFile: false
          });
          reject("Blocked due to size limit.");
        }

        if (response.didCancel) {
          console.log("User cancelled photo picker");
        } else if (response.error) {
          console.log("ImagePicker Error: ", response.error);
        } else if (response.customButton) {
          console.log("User tapped custom button: ", response.customButton);
        } else {
          resolve(response);
        }
      });
    }).then(async file => {
      this.setState({ fileBtnClick: false });
      await this.setState({
        uploadingFile: true
      });

      if (file.fileSize > 5242880) {
        alert("File capped at 5mb.");
        this.setState({
          uploadingFile: false
        });
        return Promise.resolve("Blocked due to size limit.");
      }

      return await this.props
        .addChatFile({ file, path: this.state.path })
        .catch(err => {
          alert(err);
        })
        .finally(() => {
          this.props.checkRemainingStorage();
          this.setState({
            uploadingFile: false
          });
          this.sendFileSendbird(channel, file);
        });
    });
  };

  addFileMessage = async () => {
    let channel = await sbGetGroupChannel(this.props.channel.url);
    return new Promise(function (resolve, reject) {
      if (Platform.OS === "ios") {
        DocumentPicker.show(
          {
            filetype: [DocumentPickerUtil.allFiles()]
          },
          (error, res) => {
            if (error) {
              reject(error);
            } else {
              resolve(res);
            }
          }
        );
      } else {
        FilePickerManager.showFilePicker(null, response => {
          if (response.didCancel) {
            console.log("User cancelled file picker");
          } else if (response.error) {
            console.log("FilePickerManager Error: ", response.error);
          } else {
            resolve(response);
          }
        });
      }
    }).then(async file => {
      this.filesImgModal("close");
      await this.setState({
        uploadingFile: true
      });

      if (file.fileSize > 5242880) {
        alert("File capped at 5mb.");
        this.setState({
          uploadingFile: false
        });
        return Promise.resolve("Blocked due to size limit.");
      }

      return await this.props
        .addChatFile({ file, path: this.state.path })
        .catch(err => {
          alert(err);
        })
        .finally(() => {
          this.props.checkRemainingStorage();
          this.setState({
            uploadingFile: false
          });
          this.sendFileSendbird(channel, file);
        });
    });
  };

  sendFileSendbird = async (channel, file) => {
    if (this.props.chatFileUrl) {
      let newFileMsg = await sbSendFileMessage(
        channel,
        file,
        this.props.chatFileUrl
      );
      this.addMessageToFlow(newFileMsg);
    }
  };

  closeDetailModal = () => {
    this.setState({
      detailVisible: false
    });
  };

  _renderActions = () => {
    return (
      <View style={{ flexDirection: "row" }}>
        <NavButton
          style={{ marginLeft: 0 }}
          onPress={() => {
            if (Platform.OS === "ios") {
              this.addImageMessage();
            } else {
              this.clickSendFIle();
            }
          }}
          type="materialIcons"
          name="camera-enhance"
          btnType="file"
        // isVisible={!navigation.getParam("search")}
        />
      </View>
    );
  };

  _renderFooter = () => {
    if (
      this.props.channel.lastMessage._sender.userId !==
      this.props.user.userID ||
      this.state.readMembers.length === 0
    ) {
      return null;
    }

    return (
      <View style={{ height: 32, alignItems: "flex-end" }}>
        {/* <Text>Test</Text> */}
        <ScrollView horizontal={true} style={{}}>
          {this.state.readMembers.map(member => {
            return (
              <Thumbnail
                style={styles.readReceipt}
                source={{ uri: member.profileUrl }}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  };

  _renderChatFooter = () => {
    if (!this.state.typingMembers.length) {
      return null;
    }

    if (this.state.typingMembers.length === 1) {
      return (
        <Text
          style={styles.typing}
        >{`${this.state.typingMembers[0].nickname} is typing...`}</Text>
      );
    }
    if (this.state.typingMembers.length > 1) {
      return (
        <Text style={styles.typing}>{`${this.state.typingMembers
          .map(m => m.nickname)
          .join(", ")} are typing...`}</Text>
      );
    }
  };

  getMicrophonePermission = () => {
    const audioPermission = PermissionsAndroid.PERMISSIONS.RECORD_AUDIO;

    return PermissionsAndroid.check(audioPermission).then(async result => {
      if (!result) {
        const granted = await PermissionsAndroid.request(audioPermission, {
          title: "Microphone Permission",
          message:
            "App needs access to you microphone " +
            "so you can talk with other users."
        });
      }
    });
  };

  getAuthToken = () => {
    if (platform === "ios") {
      return request
        .get(
          "http://etet.app/tv/accessToken.php?identity=" +
          this.props.user.userID +
          "&device=ios"
        )
        .then(res => {
          return res.text;
        })
        .catch(error => console.error(error));
    } else if (platform === "android") {
      return request
        .get(
          "http://etet.app/tv/accessToken.php?identity=" +
          this.props.user.userID +
          "&device=android"
        )
        .then(res => {
          return res.text;
        })
        .catch(error => console.error(error));
    } else {
      alert("platform not found");
    }
  };

  pressCall = () => {
    Alert.alert("Call", this.state.friendName, [
      { text: "Confirm", onPress: () => this.makeVoximCall(false) },
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { cancelable: false }
    ]);
  };

  _renderCustomView = props => {
    if (
      props.currentMessage &&
      props.currentMessage.filetype !== "image" &&
      props.currentMessage.filetype !== "FILE" &&
      props.currentMessage.filetype !== ""
    ) {
      return this.renderPdf(props.currentMessage);
    }
    // } else if (this.props.currentMessage.template && this.props.currentMessage.template != 'none') {
    //   // return this.renderHtml();
    // }
    return null;
  };

  renderPdf(curMsg) {
    let tmpIco;
    switch (curMsg.filetype) {
      case "application/pdf":
        tmpIco = "file-pdf-o";
        break;
      case "application/vnd.ms-excel":
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        tmpIco = "file-excel-o";
        break;
      case "application/vnd.ms-powerpoint":
      case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
        tmpIco = "file-powerpoint-o";
        break;
      case "application/msword":
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
        tmpIco = "file-word-o";
        break;
      case "text/plain":
        tmpIco = "file-text-o";
        break;
      case "application/x-freearc":
      case "application/x-bzip":
      case "application/x-bzip2":
      case "application/java-archive":
      case "application/x-rar-compressed":
      case "application/x-tar":
      case "application/zip":
      case "application/x-7z-compressed":
        tmpIco = "file-zip-o";
        break;
      case "audio/aac":
      case "audio/midi audio/x-midi":
      case "audio/mpeg":
      case "audio/ogg":
      case "audio/wav":
      case "audio/webm":
      case "audio/3gpp":
      case "audio/3gpp2":
        tmpIco = "file-audio-o";
        break;
      case "video/mp4":
      case "video/x-msvideo":
      case "video/mpeg":
      case "video/ogg":
      case "video/mp2t":
      case "video/webm":
      case "video/3gpp":
      case "video/3gpp2":
        tmpIco = "file-video-o";
        break;
      default:
        tmpIco = "file-o";
        break;
    }

    return (
      <TouchableOpacity
        // onPress={() => props.openFile(props.item)}
        onPress={() => {
          this.openFile(curMsg);
          // props.showFileActionSheet();
        }}
      >
        <View style={styles.rowFront}>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: "#e9edf0",
              borderRadius: 8,
              height: 70,
              alignItems: "center"
            }}
          >
            <View style={{ width: "20%", justifyContent: "center" }}>
              <FontAwesome style={styles.fileIcon} name={tmpIco} />
            </View>
            <View
              style={{
                // width: props.downloading ? "60%" : "80%",
                width: "60%",
                paddingRight: 15
              }}
            >
              <Text ellipsizeMode="tail" numberOfLines={1} style={styles.item}>
                {`${curMsg.fileName}`}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  // renderHtml() {
  //   return (
  //     <TouchableOpacity style={[styles.container, this.props.containerStyle]} onPress={() => {
  //       Actions.chat_html({ properties: this.props.currentMessage });
  //     }}>
  //       <Image
  //         {...this.props.imageProps}
  //         style={[styles.image, this.props.imageStyle]}
  //         source={{ uri: this.props.currentMessage.template_image }}
  //       />
  //     </TouchableOpacity>
  //   );
  // }

  openFile = async file => {
    // await this.setState({
    //   downloadingFile: file.id
    // });
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "Storage Permission",
        message: "Allow to download file?",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      this.props
        .openFile(file)
        .catch(err => {
          console.log(err);
          alert("Downloaded but no app to open.");
        })
        .finally(() => {
          // this.setState({
          //   downloadingFile: null
          // });
        });
    } else {
      alert("No access to storage.");
      // this.setState({
      //   downloadingFile: null
      // });
    }
  };

  makeVoximCall = async isVideoCall => {
    // let testNumber = 'rama';

    console.log(
      "MainScreen: make call: " +
      this.state.friendId +
      ", isVideo:" +
      isVideoCall
    );
    try {
      if (Platform.OS === "android") {
        let permissions = [PermissionsAndroid.PERMISSIONS.RECORD_AUDIO];
        if (isVideoCall) {
          permissions.push(PermissionsAndroid.PERMISSIONS.CAMERA);
        }
        const granted = await PermissionsAndroid.requestMultiple(permissions);
        const recordAudioGranted =
          granted["android.permission.RECORD_AUDIO"] === "granted";
        const cameraGranted =
          granted["android.permission.CAMERA"] === "granted";
        if (recordAudioGranted) {
          if (isVideoCall && !cameraGranted) {
            console.warn(
              "MainScreen: makeCall: camera permission is not granted"
            );
            return;
          }
        } else {
          console.warn(
            "MainScreen: makeCall: record audio permission is not granted"
          );
          return;
        }
      }

      const callSettings = {
        video: {
          sendVideo: isVideoCall,
          receiveVideo: isVideoCall
        }
      };
      console.log("callaction111111");

      if (Platform.OS === "ios" && parseInt(Platform.Version, 10) >= 10) {
        callSettings.setupCallKit = true;
        // const useCallKitString = await AsyncStorage.getItem('useCallKit');
        // callSettings.setupCallKit = JSON.parse(useCallKitString);
      }
      console.log("callaction22222", callSettings);
      console.log("callaction33333");
      console.log("friendId", this.state.friendId);

      // let vox = Voximplant.getInstance();
      // let call = "";
      // console.log("voxxxx", vox);
      // if (!vox) {
      if (this.props.user.userID) {
        //login voximplant
        console.log("handleappstateVoximCall");

        const username = await AsyncStorage.getItem("usernameValue");
        const accessToken = await AsyncStorage.getItem("accessToken");

        if (username !== null && accessToken !== null) {
          //login voximplant with token
          await LoginManager.getInstance().loginWithToken();
        } else {
          //login voximplant with password
          await LoginManager.getInstance().loginWithPassword(
            "12345" +
            this.props.user.userID +
            "@etetconf.etetsg.voximplant.com",
            this.props.user.userID
          );
        }
      }

      // await CallManager.getInstance().init();
      // await CallManager.getInstance()._handleAppKillInComing();
      let call = await Voximplant.getInstance().call(
        "12345" + this.state.friendId,
        callSettings
      );
      // } else {
      //   call = await Voximplant.getInstance().call(
      //     "12345" + this.state.friendId,
      //     callSettings
      //   );
      // }
      // let call = await Voximplant.getInstance().call(testNumber, callSettings);

      console.log("callactionicnominhg", call);

      let callManager = CallManager.getInstance();
      callManager.addCall(call); // set call number

      if (callSettings.setupCallKit) {
        //ios callkit
        console.log("starOutgoingcall");
        // callManager.startOutgoingCallViaCallKit(isVideoCall, testNumber);
        callManager.startOutgoingCallViaCallKit(
          isVideoCall,
          this.state.friendId
        );
      }

      //android callkit
      console.log("callnavigate", this.props.navigation);
      this.props.navigation.navigate("Call", {
        callId: call.callId,
        isVideo: isVideoCall,
        isIncoming: false
      });
    } catch (e) {
      console.log("MainScreen: makeCall failed: " + e);

      // await LoginManager.getInstance().loginWithToken();
      // await CallManager.getInstance().init();
    }
  };

  pressVideoCall = () => {
    Alert.alert("Call", this.state.friendName, [
      { text: "Confirm", onPress: () => this.makeVoximCall(true) },
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { cancelable: false }
    ]);
  };

  pressGrpVideoCall = item => {
    this.props.navigation.navigate("GroupList", {
      conferenceCall: true,
      isVideo: item,
      grpName: this.props.navigation.getParam("name")
    });
  };

  clickSendFIle = () => {
    this.setState({ fileBtnClick: true });
  };

  filesImgModal = val => {
    if (val == "camera") {
      this.addImageMessage();
    } else if (val == "files") {
      this.sendIosFile();
    } else if (val == "close") {
      this.setState({ fileBtnClick: false });
    }
    //this.setState({ fileBtnClick: false });
  };

  sendIosFile = async () => {
    this.addFileMessage();
  };

  render() {
    if (!this.props.channel || !this.channel) return null;

    const callingFlag = this.props.callingFlag;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    console.log("this.state.fileBtnClick", this.state.fileBtnClick);

    return (
      <View style={styles.main}>
        {/* <NavigationEvents
          onWillFocus={() => {
            console.log("load_chanannert");
            this.loadRecentChats()
          }} /> */}

        <Display isVisible={this.state.search}>
          <View style={styles.SectionStyle}>
            <TextInput
              ref={input => {
                this.textInput = input;
              }}
              style={styles.TextInputSearch}
              underlineColorAndroid="transparent"
              placeholder="  Search .."
              value={this.state.searchString}
              autoFocus={true}
              onChangeText={text => {
                this.setState({ searchString: text });
              }}
            />
            {this.state.searchString ? (
              <MaterialIcons
                onPress={async () => {
                  this.textInput.clear();
                  this.setState({ searchString: "" });
                }}
                name="cancel"
                size={23}
                style={{ color: "red", marginRight: 10 }}
              />
            ) : (
                <MaterialIcons
                  name="search"
                  size={23}
                  style={{ color: "#C0C0C0", paddingRight: 10 }}
                />
              )}
          </View>
        </Display>

        <LevelModal
          visible={this.state.fileBtnClick}
          onRequestClose={() => {
            this.filesImgModal("close");
          }}
        >
          <IconAntDesign
            style={{
              position: "absolute",
              right: 10,
              top: 10,
              zIndex: 99999
            }}
            name="closecircle"
            onPress={() => {
              this.filesImgModal("close");
            }}
            size={20}
          />

          <List>
            <ListItem
              style={{ borderBottomColor: "#747576" }}
              onPress={() => {
                this.filesImgModal("camera");
              }}
            >
              <Left>
                <View>
                  <IconAntDesign
                    color="#000"
                    name="camera"
                    style={styles.iconStyle}
                  />
                </View>
                <Text style={{ paddingLeft: 20 }}>Camera</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem
              IconFontAwesome
              style={{ borderBottomColor: "#747576" }}
              onPress={() => {
                // this.filesImgModal("files");
                this.filesImgModal("files");
                //await this.addFileMessage()
              }}
            >
              <Left>
                <View style={{ paddingLeft: 5, width: 25, height: 30 }}>
                  <Ionicons style={styles.iconStyle} name="ios-document" />
                </View>
                <Text style={{ paddingLeft: 20 }}>Documents</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </LevelModal>

        <Display isVisible={this.state.uploadingFile}>
          <ActivityIndicator size="large" color="#808080" />
        </Display>

        <Display isVisible={this.props.channel}>
          <LevelModal
            visible={this.state.detailVisible}
            onRequestClose={() => {
              this.closeDetailModal();
            }}
          >
            <Text style={styles.modalTitle}>Members</Text>
            <List>
              <FlatList
                data={this.props.channel.members}
                keyExtractor={(item, index) => index}
                renderItem={({ item }) => {
                  return (
                    <ListItem thumbnail noBorder key={item.userId}>
                      <Left>
                        <CachedImage
                          style={styles.smallThumbnail}
                          source={{
                            uri: item.profileUrl
                          }}
                        />
                      </Left>
                      <Body>
                        <Text>{item.nickname}</Text>
                      </Body>
                    </ListItem>
                  );
                }}
              />
            </List>
          </LevelModal>
        </Display>

        <GiftedChat
          extraData={this.state}
          renderUsernameOnMessage
          scrollToBottom
          scrollToBottomComponent={props => {
            return (
              <View
                style={{
                  width: 30,
                  height: 30,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Icon style={{ color: "#616161" }} name="ios-arrow-down" />
              </View>
            );
          }}
          messages={_.filter(this.state.processedMessages, message => {
            return new RegExp(this.state.searchString, "i").test(message.text);
          })}
          onSend={messages => {
            this.pushMessage(messages[0].text);
          }}
          user={{
            _id: this.props.user.userID,
            name: this.props.user.firstName,
            avatar: this.props.user.profilePictureURL
          }}
          // renderInputToolbar={this._renderInputToolbar}
          renderCustomView={this._renderCustomView}
          renderChatFooter={this._renderChatFooter}
          renderFooter={this._renderFooter}
          renderActions={this._renderActions}
          onInputTextChanged={text => {
            this.channel.startTyping();
            this.debounce();
          }}
          loadEarlier={!this.state.reachedTop}
          onLoadEarlier={this.addPrevMessages}
        />

        <LevelModal
          onBackdropPress={() => console.log("Pressed")}
          visible={callingFlag}
        >
          <Text>Call to {this.state.friendName} </Text>
          <View style={{ flexDirection: "row", alignSelf: "center" }}>
            <Button
              info
              style={{
                backgroundColor: "#FF0000",
                padding: 13,
                alignSelf: "center",
                height: 50,
                width: 50,
                borderRadius: 25
              }}
              onPress={() => {
                this.disconnect();
              }}
            >
              <MaterialIcons
                name="call-end"
                size={25}
                style={{ color: "#FFF" }}
              />
            </Button>
          </View>
        </LevelModal>

        <Display isVisible={incomingFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={incomingFlag}
          >
            <CallKit type={"incoming"} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={showTimerFlag}
          >
            <CallKit type={"timer"} />
          </LevelModal>
        </Display>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  channel: state.chat.channel,
  messages: state.chat.messages,
  friendContact: state.chat.friendContact,
  callingFlag: state.contacts.callingFlag,
  incomingFlag: state.contacts.incomingFlag,
  showTimerFlag: state.contacts.showTimerFlag,
  chatFileUrl: state.chat.chatFileUrl,
  channels: state.chat.channels,
  chatmsgFlag: state.chat.chatmsgFlag
});

export default connect(mapStateToProps, {
  loadMessages,
  addMessage,
  loadChannels,
  getFriendContact,
  checkRemainingStorage,
  addChatFile,
  openFile,
  clearNotification,
  updateChatMsg
})(Message);

const styles = StyleSheet.create({
  main: { flex: 1, padding: 8 },
  topBar: { flexDirection: "row", padding: 10 },
  backButton: { borderStyle: "solid", borderColor: "black" },
  backIcon: { flexDirection: "row" },
  icon: { color: "#616161" },
  backThumbnail: { marginRight: 5, width: 36, height: 36, borderRadius: 14 },
  smallThumbnail: { width: 36, height: 36, borderRadius: 18 },
  channelName: { flex: 1, justifyContent: "center", marginLeft: 15 },
  channelAction: {
    height: 36,
    justifyContent: "flex-end"
  },
  messageList: { flex: 1, flexDirection: "column" },
  messageForm: {
    margin: 8,
    justifyContent: "flex-end",
    paddingBottom: Platform.OS === "android" ? 10 : 0
  },
  messageInput: { height: 35, paddingLeft: 8, paddingRight: 8 },
  modalTitle: { marginLeft: 10 },
  readReceipt: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginRight: 5
  },
  typing: { fontSize: 10, marginTop: 3, marginBottom: 3 },
  thumb: {
    paddingLeft: Platform.OS === "ios" ? 10 : 0,
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  thumbnail: { width: 36, height: 36, marginRight: 10 },
  thumbnailBackground: {
    borderRadius: 16,
    borderWidth: 1.5,
    borderColor: "#eeeeee"
  },
  TextInputSearch: {
    color: "#000",
    flex: 1,
    textAlign: "center",
    fontFamily: Platform.OS === "ios" ? "OpenSans-Regular" : "opensans"
  },
  SectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#E8EBEC",
    borderColor: "#000",
    height: 40,
    borderRadius: 20,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20
  },
  img: {
    width: 193,
    height: 110
  },
  rowFront: {
    justifyContent: "center",
    padding: 5
  },
  fileIcon: {
    alignSelf: "center",
    color: "#a7aebb",
    padding: 2,
    fontSize: 28
  },
  item: {
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 16,
    color: "#3b4a5f"
  },
  iconStyle: {
    fontSize: Platform.OS === "ios" ? 21 : 25
  }
});
