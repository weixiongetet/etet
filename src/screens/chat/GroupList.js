import { Body, Content, Fab, Icon, Left, List, ListItem, Right, Separator, Text, View } from "native-base";
import React, { Component } from "react";
import { FlatList, Image, ScrollView, StyleSheet, TextInput } from "react-native";
import { CachedImage } from "react-native-cached-image";
import { TouchableOpacity } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import LevelModal from "../../components/LevelModal";
import { NavBar, NavSearch } from "../../components/NavComponent";
import CreateGroupChannel from "../../containers/form/CreateGroupChannel";
import { addAdminMessage, createChannel, loadChannels, updateChannel, updateSearch, uploadGroupImg } from "../../redux/modules/chat";

class GroupList extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Text>Select participants</Text>,
      headerRight: (
        <NavBar>
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
          />
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      createGroupVisible: false,
      search: false,
      searchString: "",
      loading: false
    };
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };

  async componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      search: this.state.search,
      toggleSearch: this.toggleSearch
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }
  }

  createGroupChannel = async ({ name }) => {
    if (this.props.image.uri === "") {
      alert("Group need an image.");
      return;
    }

    await this.setState({
      loading: true
    });
    let filteredFriends = _.filter(this.props.friends, item => {
      return item.selected;
    });

    let selectedFriends = filteredFriends.map(friend => {
      return {
        userId: friend.userID,
        nickname: friend.firstName,
        profileUrl: friend.profilePictureURL
      };
    });

    let operators = [this.props.me];

    try {
      if (
        this.props.image.uri !==
        "https://static.vecteezy.com/system/resources/previews/000/544/231/non_2x/group-of-people-icon-flat-design-vector-with-shadow-on-isolated-white-background-black-color-and-monocrome-theme.jpg"
      ) {
        await this.props.uploadGroupImg(
          this.props.user.userID,
          this.props.image
        );
      }

      await this.props.createChannel({
        operators,
        selectedFriends,
        name,
        cover: this.props.imgDownloadLink
      });

      await this.props.updateChannel({
        channelUrl: this.props.channel.url,
        cover: this.props.imgDownloadLink,
        name
      });

      if (!this.props.channel.lastMessage)
        await this.props.addAdminMessage(this.props.channel, `Chat created.`);

      await this.props.loadChannels();

      await this.props.navigation.dispatch(
        NavigationActions.back({ key: null })
      );

      await this.props.navigation.dispatch(
        NavigationActions.back({ key: null })
      );

      await this.props.navigation.navigate("Message", {
        name,
        profileUrl: this.props.imgDownloadLink
      });
    } catch (err) {
      alert(err);
    } finally {
      this.closeGroupModal();
      await this.setState({
        loading: false
      });
    }
  };

  removeParticipant = async index => {
    let tmpFriends = this.props.friends.slice();
    tmpFriends[index].selected = false;

    await this.setState({
      friends: tmpFriends
    });
  };

  closeGroupModal = () => {
    this.setState({
      createGroupVisible: false
    });
  };

  actionCreateGroup = () => {
    let selectedFriends = _.filter(this.props.friends, item => {
      return item.selected;
    });

    if (selectedFriends.length < 2) {
      alert("Select 2 participants at least.");
      return;
    }

    this.setState({
      createGroupVisible: true
    });
  };

  render() {

    let displayingFriends = _.filter(
      this.props.friends.map((item, index) => {
        return { ...item, index };
      }),
      friend => {
        return new RegExp(this.state.searchString, "i").test(
          friend.firstName
        );
      }
    );


    return (
      <View style={styles.main}>
        <Content>
          <Separator bordered>
            <Text>Friends</Text>
          </Separator>

          <Display isVisible={this.state.search}>
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="  Search .."
              value={this.state.searchString}
              autoFocus={true}
              onChangeText={text => {
                this.setState({ searchString: text });
              }}
            />
          </Display>


          <Display isVisible={displayingFriends.length === 0 && this.state.searchString == ''}>
            <Image
              source={require(`../../assets/chat.png`)}
              style={{
                marginTop: 30,
                marginBottom: 20,
                width: 80,
                height: 80,
                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain"
              }}
            />
          </Display>

          <Display isVisible={displayingFriends.length === 0 && this.state.searchString == ''}>
            <Text
              style={{
                alignSelf: "center",
                justifyContent: "center",
              }}
            >
              No friends found, add friends to start chat!
          </Text>
          </Display>

          <Display isVisible={displayingFriends.length === 0 && this.state.searchString !== ''}>
            <Text
              style={{
                alignSelf: "center",
                justifyContent: "center",
                color: 'red'
              }}
            >
              No results found!
          </Text>
          </Display>


          <List style={styles.friendList}>
            <FlatList
              data={displayingFriends}
              keyExtractor={(item, index) => index}
              renderItem={({ item, index }) => {
                if (item.userID === this.props.user.userID) {
                  return null;
                }

                return (
                  <ListItem
                    thumbnail
                    noBorder
                    onPress={async () => {
                      let tmpFriends = this.props.friends.slice();
                      if (!tmpFriends[item.index].selected) {
                        tmpFriends[item.index].selected = true;
                      } else {
                        tmpFriends[item.index].selected = false;
                      }

                      await this.setState({
                        friends: tmpFriends
                      });
                    }}
                    key={item.userID}
                  >
                    <Left>
                      <CachedImage
                        style={styles.smallThumbnail}
                        source={{ uri: item.profilePictureURL }}
                      />
                    </Left>
                    <Body>
                      {item.selected ? (
                        <Text style={styles.selectedFriend}>
                          {item.firstName}
                        </Text>
                      ) : (
                          <Text>{item.firstName}</Text>
                        )}

                      {/* <Text note>{item.lastMessage.message}</Text> */}
                    </Body>
                    {item.selected ? (
                      <Right>
                        <Icon
                          active
                          style={styles.selectedClose}
                          name="md-close"
                        />
                      </Right>
                    ) : (
                        <Right />
                      )}
                  </ListItem>
                );
              }}
            />
          </List>
        </Content>

        <LevelModal
          visible={this.state.createGroupVisible}
          onRequestClose={this.closeGroupModal}
        >
          <CreateGroupChannel
            onSubmit={this.createGroupChannel}
            handleClose={this.closeGroupModal}
            loading={this.state.loading}
          />
        </LevelModal>

        <View style={styles.memberList}>
          <ScrollView horizontal={true}>
            {this.props.friends.map((item, index) => {
              if (!item.selected) return null;

              return (
                <TouchableOpacity
                  style={styles.participant}
                  onPress={() => {
                    this.removeParticipant(index);
                  }}
                >
                  <CachedImage
                    style={styles.memberThumbnail}
                    source={{ uri: item.profilePictureURL }}
                  />
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>

        <Fab
          position="bottomRight"
          style={styles.fab}
          onPress={this.actionCreateGroup}
        >
          <Ionicons style={{ color: "white" }} name="md-checkmark" />
        </Fab>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  me: state.chat.user,
  // friends: state.chat.friends,
  friends: state.contacts.dbFriendsList,
  channel: state.chat.channel,
  searchString: state.chat.searchString,
  image: state.chat.image,
  imgDownloadLink: state.chat.imgDownloadLink
});

export default connect(
  mapStateToProps,
  {
    createChannel,
    updateSearch,
    uploadGroupImg,
    updateChannel,
    addAdminMessage,
    loadChannels
  }
)(GroupList);

const styles = StyleSheet.create({
  main: { flex: 1 },
  memberList: { flexDirection: "row", padding: 10, paddingBottom: 18, width: "80%" },
  participant: { marginLeft: 8 },
  memberThumbnail: {
    width: 50,
    height: 50,
    borderRadius: 25
    // borderColor: "green",
    // borderStyle: "solid",
    // borderWidth: 3
  },
  smallThumbnail: { width: 36, height: 36, borderRadius: 18 },
  friendList: { flex: 1, flexDirection: "column" },
  selectedFriend: { fontWeight: "bold", color: "#3370FF" },
  selectedClose: { marginRight: 10 },
  fabContainer: { marginLeft: 10 },
  icon: { color: "#616161" },
  iconBack: { backgroundColor: "#eeeeee" },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  modalTitle: { marginLeft: 10 },
  navBar: { flex: 1, flexDirection: "row" },
  navButton: { alignSelf: "center" },
  navIcon: { color: "#616161" },
  fab: { backgroundColor: "#3370FF" }
});
