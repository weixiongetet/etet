// App.js

import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";

const BOT_USER = {
  _id: 2,
  name: "E-Bot",
  avatar:
    "https://lh5.googleusercontent.com/p/AF1QipOWrHKJ1vZ66eIN01rcniwL-C029U468NLZYBTY=w160-h160-k-no"
};

export default class AiChatBotScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [
        {
          _id: 1,
          text: `Hi! I am the e-Bot from E-Tet, your unpaid personal assistant:)`,
          createdAt: new Date(),
          user: BOT_USER
        }
      ]
    };
  }

  componentDidMount() {}

  async onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));

    let message = messages[0].text;
    let returnMessage = await this.handleGoogleResponse(message);
    this.sendBotResponse(returnMessage);
  }

  async handleGoogleResponse(message) {
    try {
      let response = await fetch(
        "https://etetsg-e7e52.appspot.com/dialogflowres",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            msg: message
          })
        }
      );

      // let responseJson = await response.json();
      console.log(response.resp.data);
      return response.resp.data;
      // return responseJson.queryResult.fulfillmentMessages[0].text.text[0];
    } catch (error) {
      console.error(error);
    }
  }

  sendBotResponse(text) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      createdAt: new Date(),
      user: BOT_USER
    };

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg])
    }));
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1
          }}
        />
      </View>
    );
  }
}
