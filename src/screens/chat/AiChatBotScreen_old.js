import React, { Component } from "react";
import { GiftedChat } from "react-native-gifted-chat";
import { Dialogflow_V2 } from "react-native-dialogflow";

export default class AiChatBotScreen extends Component {
  state = {
    messages: []
  };

  componentDidMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: "Hello! How can I help you?",
          createdAt: new Date(),
          user: {
            _id: 2,
            name: "React Native",
            avatar: "https://placeimg.com/140/140/any"
          }
        }
      ]
    });
    Dialogflow_V2.setConfiguration(
      "dialogflow-mnbfgp@etetbot-sjvhlf.iam.gserviceaccount.com",
      "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCtls3yRDEFDDY9\nmfiMQl+OnNwiYhcnVGxxslFB92iVgM7knuPlRhBD5/cY9KWN5gXw4fw2aMl5AK7a\nRrjKj7typCwkiDTYdxHgyJoM6nxzrR1tuWqMA9cmxjKPXPhh+qYYBIiu6oTDQl49\nG+ICPO2GXdTU+VVUCHTFLSzaHr5JsN4ScxnwlpgbUFLHtqJ8DgpmTl9IVaFbAnkA\na/pzVKn9IgQbb9Ha4MSmHsizN1nXLUbHSmJQ7EU5AEGdvMVbW6pXyhkL8FbYODB8\nYq8qWgoc4rlTCP5RN7t2cXvUTvU2oWC4d+sv3i8G30rhGUp3zWxOhf/YZqrVul6A\nNnceX1y3AgMBAAECggEAUf1hL9UynBI9ClKPuVveO4NuXFUdX4e6IueZBsHfZYHt\n9sDvnkq+Avzs0p43FfM4CiMLF/3db2g5rkJqJM7HuCm/6c9lunpe+N93F7OnbZGu\nTTWfFBv6QsepeNpuPQUA72Yw0wFVDM7rqQxi7zCs9eKa1K8htOu7j6g7P5j2wuHc\nxP242VwL0FPJbQdztOMXXQsE+cb26dof0HHsyUlNkGGOcOLV9E1ybviweXP9ezbp\nyAZtv47gx2W2cLLZJd8xIYcWN3If/3J3xDw6zxKzBOnPWJgyUKmCId551AWDkpjQ\nIgrCUR1lynKw8mzgVfubTbcw0Gk++wZuuvN4FKscLQKBgQDxSumTyUjBR6iYJJOx\nFMcwDRLX/JnXm6jbOPqgzFp9CQK9pUXNewasSfK7OA+x3R1dAZNC3N0jWZ20TqAh\ncCAwy6obXEiOd6HcHb8R9xjBxhop7SJcfEDpKi2WIWv+rNLbcKwJiL8FTjBwvMAt\nZv6kM/O+ixK3CFlUhsfqvp0+wwKBgQC4K3U2t7HEP7JAM9qJEFE3sXfiYJQ/JP6j\negcONa/oAe2jkndzyyicSofdYWGQjcw89nqVLPi2kH7dyo8OQmwZdD0jJEIgN/oH\ne+t4J8z7aat77kfENUnlucMGcuSe1PaQI/M6ZbPg6F6A+t7PxgRCMKPwz7xzjuCy\nGarc14zy/QKBgQDRQep35GRYWtD3wSDkveRQ48Wn0sWImtJB7JROx2ribOIXnjF/\nFmafHDKJ8wCJe/1vH2Ka5fhSI/IE4Ps2hIsmRjpsKAg0pcOVpaaC6gxqYGdoXOMj\n2g5hJ9cJEEscPr1O2dQZIKKo8zpHSypPNbc4qGWGpg3A9liiWZEfH9g3CQKBgQCF\n5hKyVZvJMwZF521beaGS/FRNIiqmPh9ke7PlgOMDq2M7KaHvbiV4fDeLnOFyV6XG\nLO2yx5+MHEIgvsSDeYdeekR46EOraQc/qbjpAwblQ06KLBPBc3zmqMhCSAYv9GXP\nJlQQgSDmxArxe3PDXBR8iyCmVGKPIbH7yu2O4qh3gQKBgGiyBKuE166B38PpkCle\n49uDVy/Vk4SAqYyZGa9zETATdMmRoj6DkmibxDB5ZofghOWyoHIjQOrIRWq2mqFp\nook2CgsGl5kLfga7mlTNCDKzr9Si8HlfJhofaYzy3KwcgN7xPABe5zglbtxL+JJl\nf6Gf5Ux/eFqdLdSM6jg8oayg\n-----END PRIVATE KEY-----\n",
      Dialogflow_V2.LANG_ENGLISH,
      "etetbot-sjvhlf"
    );
    Dialogflow_V2.startListening(
      result => {
        console.log(result);
      },
      error => {
        console.log(error);
      }
    );
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));
    messages.forEach(message => {
      console.log(message.text);
      Dialogflow_V2.requestQuery(
        message.text,
        result => {
          console.log(result);
          messages = [
            {
              _id: result.responseId,
              text: result.queryResult.fulfillmentText,
              createdAt: new Date(),
              user: {
                _id: 2,
                name: "Bot",
                avatar: "https://placeimg.com/140/140/any"
              }
            }
          ];
          this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages)
          }));
        },
        error => console.log(error)
      );
    });
  }

  render() {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        placeholder=" Type here ... "
        locale=" en "
        user={{
          _id: 1
        }}
      />
    );
  }
}
