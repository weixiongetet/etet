import { View } from "native-base";
import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { connect } from "react-redux";

class Obj extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <View style={styles.view} />;
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps)(Obj);

const styles = StyleSheet.create({
  vaultView: { flex: 1, padding: 8 },
  view: { flex: 1 }
});
