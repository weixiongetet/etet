import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform
} from "react-native";
import Contacts from "react-native-contacts";
import RNPickerSelect from "react-native-picker-select";

export default class ContactScreen extends Component {
  static navigationOptions = {
    title: "New Contact"
  };

  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const text = navigation.getParam("text");
    if (!text) return;
    const lines = text.match(/[^\r\n]+/g);

    this.state = {
      text: text,
      lines: lines,
      contact: {
        displayName: "",
        givenName: "",
        phoneNumbers: [],
        emailAddresses: [],
        jobTitle: "",
        company: "",
        postalAddresses: []
      },

      lineTypes: [
        Platform.OS === "ios"
          ? { label: "Given Name", value: "givenName" }
          : { label: "Given Name", value: "displayName" },
        { label: "Phone Number", value: "phoneNumbers" },
        { label: "Email Address", value: "emailAddresses" },
        { label: "Job Title", value: "jobTitle" },
        { label: "Company", value: "company" },
        { label: "Postal Address", value: "postalAddresses" }
      ]
    };

    this.selectedType = this.selectedType.bind(this);
  }

  selectedType(type, value) {
    var updatedContact = Object.assign({}, this.state.contact);
    if (type == "phoneNumbers") {
      updatedContact[type].push({
        label: "mobile",
        number: value.replace(/ /g, "")
      });
    } else if (type == "emailAddresses") {
      updatedContact[type].push({
        label: "work",
        email: value.replace(/ /g, "")
      });
    } else if (type == "postalAddresses") {
      updatedContact["postalAddresses"].push({
        label: "home",
        street: value.replace(/ /g, ""),
        city: "",
        state: "",
        region: "",
        postCode: "",
        country: ""
      });
    } else {
      updatedContact[type] = value;
    }
    this.setState({ contact: updatedContact });
  }

  saveContact = async () => {
    let contact = this.state.contact;
    console.log("Contact::: ", contact);
    if (contact.displayName == null || contact.phoneNumbers.length == 0) {
      alert("Select at least an option for Name and Phone");
    } else {
      await Contacts.openContactForm(this.state.contact, err => {
        // if (err) {
        //   alert("Select an option");
        // }
        console.log("Error :: ", err);

        this.props.navigation.navigate("HomePage");

        // this.props.navigation.navigate("ContactScreen", {
        //   text: this.state.lines
        // });
      });
    }
  };

  render() {
    if (!this.state.text) return null;

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={styles.container}>
          {this.state.lines.map((line, index) => {
            return (
              <View style={styles.lineContainer} key={index}>
                <Text style={styles.lineText}>{line}</Text>
                <View style={styles.lineType}>
                  <RNPickerSelect
                    placeholder={{
                      label: "Select type or nothing to discard",
                      value: null
                    }}
                    onValueChange={type => this.selectedType(type, line)}
                    items={this.state.lineTypes}
                    style={styles.picker}
                  />
                </View>
              </View>
            );
          })}
        </ScrollView>
        <TouchableOpacity
          style={styles.saveButton}
          onPress={this.saveContact.bind(this)}
        >
          <Text>Save Contact</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  lineContainer: {
    flex: 1,
    borderColor: "#e7e7e7",
    borderWidth: 1,
    padding: 10
  },
  picker: {
    fontSize: 16,
    borderWidth: 1,
    borderColor: "#e7e7e7",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black"
  },
  saveButton: {
    alignContent: "center",
    alignSelf: "center",
    paddingVertical: 15
  }
});
