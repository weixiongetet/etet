import { Button, Icon, Text, View } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, Dimensions, Image, StyleSheet } from "react-native";
// import Icon from "react-native-vector-icons/Ionicons";
import { RNCamera } from "react-native-camera";
import RNFetchBlob from "react-native-fetch-blob";
import { TouchableOpacity } from "react-native-gesture-handler";

const Fetch = RNFetchBlob.polyfill.Fetch
window.fetch = new Fetch({
  auto: true,
  binaryContentTypes: [
    'image/',
    'video/',
    'audio/',
    'foo/',
  ]
}).build()


const win = Dimensions.get("window");
const imageWidth = ((win.width * 0.9) / 2) * 0.8;
const imageHeight = (imageWidth / 4) * 3;
const superagent = require("superagent");
const flashModeOrder = {
  off: "on",
  on: "auto",
  auto: "off"
  // torch: "off"
};

const wbOrder = {
  auto: "sunny",
  sunny: "cloudy",
  cloudy: "shadow",
  shadow: "fluorescent",
  fluorescent: "incandescent",
  incandescent: "auto"
};

// const vision = require('react-cloud-vision-api')

export default class CameraScreen extends Component {
  // static navigationOptions = {
  //   header: null
  // };

  constructor(props) {
    super(props);
    this.camera = null;

    this.state = {
      width: 300,
      height: 200,
      // frontCard: null,
      // backCard: null,
      loading: false,
      // camera: {
      //   type: RNCamera.Constants.Type.back,
      //   flashMode: RNCamera.Constants.FlashMode.auto,
      //   barcodeFinderVisible: true
      // },
      type: "back",
      flash: "off",
      whiteBalance: "auto",
      capturingFront: true,
      frontImage: null,
      backImage: null,
      cardFrontData: null,
      cardBackData: null
    };
  }

  toggleOrientation() {
    this.setState({
      width: this.state.width === 200 ? 300 : 200,
      height: this.state.height === 300 ? 200 : 300
    });
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash]
    });
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance]
    });
  }

  toggle = value => () =>
    this.setState(prevState => ({ [value]: !prevState[value] }));

  takePicture = async function () {
    if (this.camera) {
      // clear previous image
      if (this.state.capturingFront) {
        await this.setState({
          frontImage: null,
          cardFrontData: null
        });
      } else {
        await this.setState({
          backImage: null,
          cardBackData: null
        });
      }

      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);

      // for on-device (Supports Android and iOS)
      // const deviceTextRecognition = await RNMlKit.deviceTextRecognition(data.uri);
      // console.log('Text Recognition On-Device', deviceTextRecognition);
      // for cloud (At the moment supports only Android)
      //  const cloudTextRecognition = await RNMlKit.cloudTextRecognition(data.uri);
      //  console.log('Text Recognition Cloud', cloudTextRecognition);

      if (this.state.capturingFront) {
        await this.setState({
          frontImage: data.base64
        });
      } else {
        await this.setState({
          backImage: data.base64
        });
      }



      // let imagePath = null;
      // RNFetchBlob.config({
      //   fileCache: true
      // })
      //   .fetch("GET", "http://etet.app/tv/testscan.png")
      //   // the image is now dowloaded to device's storage
      //   .then(resp => {
      //     // the image path you can use it directly with Image component
      //     imagePath = resp.path();
      //     return resp.readFile("base64");
      //   })
      //   .then(base64Data => {
      //     // here's base64 encoded image

      //     // let res = this.detectText(base64Data);
      //     // remove the file from storage
      //     return fs.unlink(imagePath);
      //   });


      let res = await this.detectText(data.base64);
    }
  };


  async detectText(base64) {
    await this.setState({
      loading: true
    });

    try {

      let body = JSON.stringify({
        requests: [
          {
            features: [
              { type: "TEXT_DETECTION", maxResults: 5 },
            ],
            image: { content: base64 },
          }
        ]
      });

      let response = await fetch(
        "https://vision.googleapis.com/v1/images:annotate?key=" +
        "AIzaSyCJXbTZ9TgseuG6oi0pamOKfS3O1uLb0BI",
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          method: "POST",
          body: body
        }
      );
      let jsonRes = await response.json();

      let text = "";
      // text =
      //   jsonRes.body.responses[0].fullTextAnnotation &&
      //   jsonRes.body.responses[0].fullTextAnnotation.text;
      //console.log("hello shajeer", jsonRes.responses[0].fullTextAnnotation.text)
      text = jsonRes.responses[0].fullTextAnnotation.text;

      let updateState = {};
      updateState[
        this.state.capturingFront ? "cardFrontData" : "cardBackData"
      ] = text;

      this.setState(updateState);
      return Promise.resolve("ORC scan successfully.");
    } catch (err) {
      return Promise.reject("ORC server error.");
    } finally {
      await this.setState({
        loading: false
      });
    }
  }

  analyze = () => {
    let allText = (this.state.cardFrontData !== undefined && this.state.cardFrontData !== null) ? this.state.cardFrontData + this.state.cardBackData : this.state.cardBackData;
    this.props.navigation.navigate("ContactScreen", {
      text: allText
    });
  };

  render() {

    return (
      <View style={styles.container}>
        <RNCamera
          ref={cam => {
            this.camera = cam;
          }}
          style={styles.preview}
          type={this.state.type}
          flashMode={this.state.flash}
          whiteBalance={this.state.whiteBalance}
          orientation={this.state.orientation}
          defaultTouchToFocus
          mirrorImage={false}
          onFocusChanged={() => { }}
          onZoomChanged={() => { }}
          playSoundOnCapture
        >
          {/* Top Container */}
          <View style={styles.topContainer}>
            <Button iconLeft transparent onPress={this.toggleFlash.bind(this)}>
              <Icon
                type="FontAwesome"
                name={this.state.flash == "flash" ? "flash-off" : "flash"}
                style={{ fontSize: 24, color: "white" }}
              />
              <Text style={styles.flashText}> {this.state.flash} </Text>
            </Button>
            <Text style={[styles.flashText, { marginTop: 10 }]}>|</Text>
            <Button transparent onPress={this.toggleWB.bind(this)}>
              <Text style={styles.flashText}>
                Mode: {this.state.whiteBalance}
              </Text>
            </Button>
          </View>
        </RNCamera>

        <View
          style={{
            flexDirection: "column",
            position: "absolute",
            bottom: 15,
            width: "90%",
            marginLeft: "5%",
            flex: 0
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: "50%" }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    capturingFront: true
                  });
                }}
              >
                <Image
                  source={{
                    isStatic: true,
                    uri: this.state.frontImage
                      ? "data:image/jpeg;base64," + this.state.frontImage
                      : ""
                  }}
                  style={
                    this.state.capturingFront
                      ? styles.imagePreviewSelected
                      : styles.imagePreview
                  }
                />
                <Text
                  style={
                    this.state.capturingFront
                      ? styles.imagePreviewLabelSelected
                      : styles.imagePreviewLabel
                  }
                >
                  Front
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: "50%" }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    capturingFront: false
                  });
                }}
              >
                <Image
                  source={{
                    isStatic: true,
                    uri: this.state.backImage
                      ? "data:image/jpeg;base64," + this.state.backImage
                      : ""
                  }}
                  style={
                    !this.state.capturingFront
                      ? styles.imagePreviewSelected
                      : styles.imagePreview
                  }
                />
                <Text
                  style={
                    !this.state.capturingFront
                      ? styles.imagePreviewLabelSelected
                      : styles.imagePreviewLabel
                  }
                >
                  Back
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              flex: 0,
              marginTop: 15,
              marginBottom: 15
              // justifyContent: "space-around"
            }}
          >

            <View style={styles.cameraBtnContainer}>
              <View style={{ position: "absolute", left: 10 }}>
                {this.state.loading ? (
                  <Text style={styles.instructions}>Processing ...</Text>
                ) : (
                    <Text style={styles.instructions}>{`Capturing ${
                      this.state.capturingFront ? "front" : "back"
                      }`}</Text>
                  )}

                {this.state.capturingFront ? (
                  <View>
                    {!(this.state.cardFrontData) ? (
                      <Text style={styles.instructions}>No text detected!</Text>
                    ) :
                      null
                    }
                  </View>
                ) :
                  <View>
                    {!(this.state.cardBackData) ? (
                      <Text style={styles.instructions}>No text detected!</Text>
                    ) :
                      null
                    }
                  </View>
                }
              </View>


              {this.state.loading ? (
                <ActivityIndicator
                  style={{
                    height: 50,
                    width: 50,
                  }}
                  color="#fff"
                  animating={this.state.loading}
                />
              ) : (
                  <Button
                    onPress={this.takePicture.bind(this)}
                    disabled={this.state.loading}
                    light
                    small
                    style={styles.cameraButton}
                  >
                    <Icon name="md-camera" />
                  </Button>
                )}
            </View>

            <View
              style={{ position: "absolute", right: 10, flexDirection: "row" }}
            >
              <Button
                small
                success
                disabled={
                  !(this.state.cardFrontData || this.state.cardBackData)
                }
                style={styles.bottomActionButton}
                onPress={this.analyze}
              >
                <Icon name="ios-arrow-forward" />
              </Button>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  topContainer: {
    flexDirection: "row",
    position: "absolute",
    top: 10
    // justifyContent: "space-between"
  },
  mask: {
    justifyContent: "center",
    alignItems: "center"
  },
  instructions: {
    color: "#fff",
    fontSize: 12
  },
  loadingIndicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  flashText: {
    color: "white",
    fontSize: 15
  },
  bottomActionButton: {
    marginLeft: 8
  },
  imagePreview: {
    width: imageWidth,
    height: imageHeight,
    alignSelf: "center",
    resizeMode: "contain",
    marginBottom: 5,
    borderWidth: 2,
    borderColor: "#eeeeee"
  },
  imagePreviewSelected: {
    width: imageWidth,
    height: imageHeight,
    alignSelf: "center",
    resizeMode: "contain",
    marginBottom: 5,
    borderWidth: 2,
    borderColor: "#0086c3"
  },
  imagePreviewLabel: {
    backgroundColor: "#f4f4f4",
    alignSelf: "center",
    color: "#616161",
    padding: 3,
    borderRadius: 2,
    fontSize: 11
  },
  imagePreviewLabelSelected: {
    backgroundColor: "#0086c3",
    alignSelf: "center",
    color: "white",
    padding: 3,
    borderRadius: 2,
    fontSize: 11
  },
  cameraBtnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraButton: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#fff',
    alignSelf: "center",

  }
});
