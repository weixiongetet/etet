import AsyncStorage from "@react-native-community/async-storage";
import React, { Component } from 'react';
import { Dimensions, Image, Platform } from 'react-native';
import Onboarding from 'react-native-onboarding-swiper'; // 0.4.0


const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = "material";
const isIphoneX =
  platform === "ios" && deviceHeight === 812 && deviceWidth === 375;

class Walkthrough extends Component {

  constructor(props) {

    super(props);
  }



  async finishWalkthrough() {
    await AsyncStorage.setItem("walkthrough", "no");
    this.props.navigation.navigate('Login');
  }


  render() {

    return (

      <Onboarding
        onSkip={() => this.finishWalkthrough()}
        onDone={() => this.finishWalkthrough()}
        pages={[
          {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/walkthrough/id-card.png')} style={{ width: 206, height: 206 }} />,
            title: 'Business cards to contacts',
            subtitle: 'Scan physical cards and convert them into valuable contacts within seconds, with our business card scanner.',
            titleStyles: {
              fontSize: 24,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            },
            subTitleStyles: {
              fontSize: 16,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            }
          },
          {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/walkthrough/vault.png')} style={{ width: 206, height: 206 }} />,
            title: 'Manage your data',
            subtitle: 'Personalize, collaborate and share files within your network, real-time. Be in a business proposal or a snapshot form a recent business trip, manage your documents anytime, anywhere.',
            titleStyles: {
              fontSize: 24,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            },
            subTitleStyles: {
              fontSize: 16,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            }
          },
          {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/walkthrough/group.png')} style={{ width: 206, height: 206 }} />,
            title: 'Communication made easier',
            subtitle: "Share ideas and chat with verified parties. Engage with one to one and group chats with colleagues and external parties for faster collaboration.",
            titleStyles: {
              fontSize: 24,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            },
            subTitleStyles: {
              fontSize: 16,
              fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans"
            }
          },
        ]}
      />

    );
  }
}

export default Walkthrough;