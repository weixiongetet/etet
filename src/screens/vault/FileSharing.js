import { Body, Left, List, ListItem, Right, Text, View } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, Alert, FlatList, Platform, RefreshControl, StyleSheet, TextInput } from "react-native";
import { CachedImage } from "react-native-cached-image";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import { NavBar, NavButton, NavSearch } from "../../components/NavComponent";
import { updateSearch } from "../../redux/modules/chat";
import { getDB_Friends } from "../../redux/modules/contacts";
import { selectFile, shareFile } from "../../redux/modules/vault";

class FileSharing extends Component {
  static navigationOptions = ({ navigation }) => {
    let canShare = navigation.getParam("shareSelected") &&
      Object.keys(navigation.getParam("shareSelected")).length > 0 &&
      navigation.getParam("sharing") == false;

    return {
      headerTitle: <Text style={styles.shareTitle}>Share to others</Text>,
      headerRight: (
        <NavBar>
          <NavSearch
            onPress={() => {
              navigation.getParam("toggleSearch")();
            }}
            isSearching={navigation.getParam("search")}
          />
          <NavButton
            onPress={() => {
              navigation.getParam("shareFileConfirm")();
            }}
            name="md-share"
            isVisible={canShare}
          />
          <Display isVisible={navigation.getParam("sharing") === true}>
            <ActivityIndicator
              style={{ padding: 15 }}
              size="small"
              color="#616161"
            />
          </Display>
        </NavBar>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: "", // for loading indicator
      search: false,
      searchString: "",
      shareSelected: {}
    };
  }

  async componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      search: this.state.search,
      toggleSearch: this.toggleSearch,
      shareSelected: this.state.shareSelected,
      shareFileConfirm: this.shareFileConfirm,
      sharing: false
    });
    this.props.getDB_Friends();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.search !== prevState.search) {
      this.props.navigation.setParams({
        search: this.state.search
      });
      if (!this.state.search) this.setState({ searchString: "" });
      if (this.state.search) this.setState({ searchString: "" });
    }
  }

  toggleSearch = () => {
    this.setState({
      search: !this.state.search
    });
  };


  shareFileTo = async user => {

    let status = this.props.selectedFile.share[user.userID];

    await this.setState({
      loading: user.userID
    });

    //latest FCM Token
    const callable = await firebase.functions().httpsCallable("getFriendFCM");
    const { data } = await callable({ friendId: user.userID });

    await this.props
      .shareFile({
        fileId: this.props.selectedFile.id,
        friendId: user.userID,
        friendFCM: data.fcmToken,
        allow: !status
      })
      .then(() => {
        // manually update share status
        let tmpFile = { ...this.props.selectedFile };
        tmpFile.share[user.userID] = !status;
        this.props.selectFile(tmpFile);
      });

  };

  shareFileConfirm = () => {
    Alert.alert(
      "Share file",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => {
            /* do nothing */
          },
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.submitShare();
          }
        }
      ],
      { cancelable: false }
    );

  }


  submitShare = async () => {

    const { navigation } = this.props;
    navigation.setParams({
      sharing: true
    });
    let ps = [];
    let sharetoName = "";
    for (var userID in this.state.shareSelected) {
      sharetoName = this.state.shareSelected[userID].firstName;
      let p = this.shareFileTo(this.state.shareSelected[userID]);
      ps.push(p);
    }

    Promise.all(ps)
      .then(values => {
        this.setState({
          shareSelected: {}
        });
        navigation.setParams({
          shareSelected: {}
        });
      })
      .finally(() => {
        this.sharedSuccess(sharetoName);
        navigation.setParams({
          sharing: false
        });
      });
  };

  sharedSuccess = (name) => {

    Alert.alert('', 'Shared Successful!', [
      { text: 'Ok', onPress: () => this.props.navigation.navigate({ routeName: "Vault" }) },

      // const { navigation } = this.props;

      // Alert.alert('', 'Shared Successful!', [
      //   {
      //     text: "Ok",
      //     onPress: () => {
      //       this.setState({
      //         shareSelected: {}
      //       });
      //       navigation.setParams({
      //         shareSelected: {}
      //       });
      //       this.props.navigation.navigate({ routeName: "Vault" })
      //     },
      //     // style: "cancel"
      //   },
      //   // { text: 'Ok', onPress: () => this.props.navigation.navigate({ routeName: "Vault" }) },
      //   {
      //     text: "Unshare?",
      //     onPress: () => {
      //       this.unshareFiles();
      //     }
      //   },
      { cancelable: false },
    ])
  };

  unshareFiles = () => {
    let ps = [];
    for (var userID in this.state.shareSelected) {
      let p = this.unshareFileTo(this.state.shareSelected[userID]);
      ps.push(p);
    }

  }


  unshareFileTo = async user => {
    let status = this.props.selectedFile.share[user.userID];
    // await this.setState({
    //   loading: user.userID
    // });

    //latest FCM Token
    const callable = await firebase.functions().httpsCallable("getFriendFCM");
    const { data } = await callable({ friendId: user.userID });

    await this.props
      .shareFile({
        fileId: this.props.selectedFile.id,
        friendId: user.userID,
        friendFCM: data.fcmToken,
        allow: status
      })
      .then(() => {
      });

  };


  render() {

    let displayingFriends = _.filter(this.props.friends, friend => {
      return new RegExp(this.state.searchString, "i").test(
        friend.firstName
      );
    });

    return (
      <View style={{ flex: 1 }}>
        {this.state.search ? (
          <TextInput
            style={{ paddingLeft: 15, paddingTop: 10 }}
            underlineColorAndroid="transparent"
            placeholder="  Search .."
            value={this.state.searchString}
            autoFocus={true}
            onChangeText={text => {
              this.setState({ searchString: text });
            }}
          />
        ) : null}

        <Display isVisible={displayingFriends.length === 0 && this.state.searchString !== ''}>
          <Text
            style={{
              alignSelf: "center",
              justifyContent: "center",
              color: 'red'
            }}
          >
            No results found!
          </Text>
        </Display>

        <List style={{ flex: 1, flexDirection: "column" }}>
          <FlatList
            data={displayingFriends}
            // data={_.filter(this.props.friends, friend => {
            //   return new RegExp(this.state.searchString, "i").test(
            //     friend.firstName
            //   );
            // })}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state.shareSelected}
            renderItem={({ item }) => {
              if (item.userID === this.props.user.userID) {
                return null;
              }

              return (
                <ListItem
                  thumbnail
                  noBorder
                  onPress={() => {
                    let updateItem = { ...this.state.shareSelected };
                    updateItem[item.userID] = item;
                    if (this.state.shareSelected[item.userID]) {
                      delete updateItem[item.userID];
                    }
                    this.setState({
                      shareSelected: updateItem
                    });
                    this.props.navigation.setParams({
                      shareSelected: updateItem
                    });
                  }}
                  key={item.userID}
                >
                  <Left>
                    <CachedImage
                      style={styles.smallThumbnail}
                      // source={{ uri: item.profilePictureURL }}
                      source={{
                        uri: this.state.shareSelected[item.userID]
                          ? "https://static.thenounproject.com/png/446229-200.png"
                          : item.profilePictureURL
                      }}
                    />
                  </Left>
                  <Body>
                    <Text>{item.firstName}</Text>
                    {/* <Text note>{item.lastMessage.message}</Text> */}
                  </Body>
                  <Right>
                    {this.props.isSharing ? (
                      this.state.loading == item.userID ? (
                        <ActivityIndicator size="small" color="#616161" />
                      ) : this.props.shareTo[item.userID] ? (
                        <Text style={styles.indicator}>Shared</Text>
                      ) : null
                    ) : this.props.shareTo[item.userID] ? (
                      <Text style={styles.indicator}>Shared</Text>
                    ) : null}
                  </Right>
                </ListItem>
              );
            }}
            progressViewOffset={3}
            refreshControl={
              <RefreshControl
                refreshing={this.props.isFetching}
                onRefresh={this.props.getDB_Friends}
              />
            }
          />
        </List>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  me: state.chat.user,
  // friends: state.chat.friends,
  selectedFile: state.vault.selectedFile,
  shareTo: state.vault.selectedFile.share,
  isSharing: state.vault.isFetching,
  friends: state.contacts.dbFriendsList,
  isFetching: state.contacts.isFetching,
  searchString: state.chat.searchString
});

export default connect(
  mapStateToProps,
  { updateSearch, getDB_Friends, shareFile, selectFile }
)(FileSharing);

const styles = StyleSheet.create({
  vaultView: { flex: 1, padding: 8 },
  icon: { color: "#616161" },
  iconBack: { backgroundColor: "#eeeeee" },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  },
  headerTitle: { flex: 1, flexDirection: "row" },
  headerRight: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingRight: 15
  },
  smallThumbnail: { width: 36, height: 36, borderRadius: 18 },
  indicator: {
    color: "#616161",
    backgroundColor: "#eeeeee",
    padding: 5,
    fontStyle: "italic",
    fontSize: 10
  },
  shareTitle: {
    paddingLeft: Platform.OS === "ios" ? 10 : 0,
  }
});
