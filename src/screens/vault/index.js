import { Button, Fab, Text } from "native-base";
import React, { Component } from "react";
import {
  ActionSheetIOS,
  ActivityIndicator,
  Alert,
  PermissionsAndroid,
  Picker,
  Platform,
  ScrollView,
  StyleSheet,
  View
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import { CachedImage } from "react-native-cached-image";
// import { Icons } from "react-native-vector-icons/Ionicons"
import {
  DocumentPicker,
  DocumentPickerUtil
} from "react-native-document-picker";
import firebase from "react-native-firebase";
import { TouchableOpacity } from "react-native-gesture-handler";
import ImagePicker from "react-native-image-picker";
import ProgressCircle from "react-native-progress-circle";
import { SwipeListView } from "react-native-swipe-list-view";
import AntdIcon from "react-native-vector-icons/AntDesign";
import { default as Icon } from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import { NavBar, NavButton } from "../../components/NavComponent";
import NewnameReduxForm from "../../containers/form/NewnameReduxForm";
import RenameReduxForm from "../../containers/form/RenameReduxForm";
import { checkRemainingStorage } from "../../redux/modules/setting";
import {
  addFile,
  addGroup,
  deleteFile,
  deleteGroup,
  discardSharedFile,
  joinGroup,
  openFile,
  renameFile,
  renameGroup,
  selectFile,
  selectGroup,
  setFiles,
  setFolders,
  setSharedFiles,
  shareFile
} from "../../redux/modules/vault";

const assetPath = "../../assets";

const EmptyPlaceholder = props => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <CachedImage
        style={{ width: 70, height: 80, resizeMode: "contain" }}
        source={require(`${assetPath}/vault.png`)}
      />
      {props.mode === "file" ? (
        <Text style={styles.warningMessage}>No file uploaded yet.</Text>
      ) : null}
      {props.mode === "all" ? (
        <Text style={styles.warningMessage}>
          No file or folder created yet.
        </Text>
      ) : null}
      {props.mode === "shared" ? (
        <Text style={styles.warningMessage}>No file shared to you yet.</Text>
      ) : null}
    </View>
  );
};

const SwipeFileRow = props => {
  const { id, name, ext, modified, mime } = props.item;
  let tmpIco;
  switch (mime) {
    case "image/png":
    case "image/jpg":
    case "image/jpeg":
    case "image/bmp":
    case "image/gif":
    case "image/vnd.microsoft.icon":
    case "image/svg+xml":
    case "image/tiff":
    case "image/webp":
      tmpIco = "image";
      break;
    case "application/pdf":
      tmpIco = "file-pdf-o";
      break;
    case "application/vnd.ms-excel":
    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
      tmpIco = "file-excel-o";
      break;
    case "application/vnd.ms-powerpoint":
    case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
      tmpIco = "file-powerpoint-o";
      break;
    case "application/msword":
    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
      tmpIco = "file-word-o";
      break;
    case "text/plain":
      tmpIco = "file-text-o";
      break;
    case "application/x-freearc":
    case "application/x-bzip":
    case "application/x-bzip2":
    case "application/java-archive":
    case "application/x-rar-compressed":
    case "application/x-tar":
    case "application/zip":
    case "application/x-7z-compressed":
      tmpIco = "file-zip-o";
      break;
    case "audio/aac":
    case "audio/midi audio/x-midi":
    case "audio/mpeg":
    case "audio/ogg":
    case "audio/wav":
    case "audio/webm":
    case "audio/3gpp":
    case "audio/3gpp2":
      tmpIco = "file-audio-o";
      break;
    case "video/mp4":
    case "video/x-msvideo":
    case "video/mpeg":
    case "video/ogg":
    case "video/mp2t":
    case "video/webm":
    case "video/3gpp":
    case "video/3gpp2":
      tmpIco = "file-video-o";
      break;
    default:
      tmpIco = "file-o";
      break;
  }

  let ItemRow = (
    <TouchableOpacity
      // onPress={() => props.openFile(props.item)}
      onLongPress={() => {
        props.openFile(props.item);
        if (!props.setFile) return; // shared item
        props.setFile(props.item);
        props.showFileActionSheet();
      }}
    >
      <View style={styles.rowFront}>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "#e9edf0",
            borderRadius: 8,
            height: 70,
            alignItems: "center"
          }}
        >
          <View style={{ width: "20%", justifyContent: "center" }}>
            <Icon style={styles.fileIcon} name={tmpIco} />
          </View>
          <View
            style={{
              width: props.downloading ? "60%" : "80%",
              paddingRight: 15
            }}
          >
            <Text ellipsizeMode="tail" numberOfLines={1} style={styles.item}>
              {`${name}.`}
              <Text style={styles.itemExt}>{ext}</Text>
            </Text>
            <Text note style={styles.itemNote}>
              {modified}
            </Text>
          </View>
          {props.downloading ? (
            <View
              style={{
                width: "20%",
                justifyContent: "center",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <ProgressCircle
                percent={props.percent}
                radius={15}
                borderWidth={2}
                color="#3399FF"
                shadowColor="#999"
                bgColor="#fff"
              >
                <Text style={{ fontSize: 10 }}>{`${props.percent.toFixed(
                  0
                )}%`}</Text>
              </ProgressCircle>
            </View>
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  );

  return ItemRow;
};

const FolderObj = props => {
  const { id, name, path, modified } = props.item;

  if (path != props.currentPath) {
    return null;
  }

  return (
    <TouchableOpacity
      onPress={() => props.openFolder(props.item)}
      onLongPress={() => {
        props.setFolder(props.item);
        props.showFolderActionSheet();
      }}
    >
      <View
        style={{
          margin: 5,
          marginRight: 15,
          width: 180,
          height: "auto",
          borderRadius: 8,
          backgroundColor: "white",
          padding: 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,

          elevation: 4
        }}
      >
        <AntdIcon
          style={{
            color: "#3d72de",
            fontSize: 30
          }}
          name="folder1"
        />
        {/* <View style={{ width: "20%", justifyContent: "center" }}>
        <TouchableOpacity onPress={() => {}}>
          <Icon style={styles.fileIcon} name="arrow-right" />
        </TouchableOpacity>
      </View> */}
        <Text style={styles.item}>{name}</Text>
        <Text style={styles.itemNote}>
          {modified ? `Modified: ${modified}` : "Folder"}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const pickerStyle = "itemStyle={{ height: 50, width: 100, fontSize: 9 }}";

class Vault extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <NavBar>
          {navigation.getParam("platform") == "ios" ? (
            <Picker
              itemStyle={{ height: 50, width: 100, fontSize: 12 }}
              selectedValue={navigation.getParam("mode")}
              onValueChange={(itemValue, itemIndex) => {
                navigation.getParam("toggleMode")(itemValue);
              }}
            >
              <Picker.Item label="Files" value="file" />
              <Picker.Item label="All" value="all" />
              <Picker.Item label="Shared" value="shared" />
            </Picker>
          ) : (
            <Picker
              style={{ height: 50, width: 100, fontSize: 9 }}
              selectedValue={navigation.getParam("mode")}
              onValueChange={(itemValue, itemIndex) => {
                navigation.getParam("toggleMode")(itemValue);
              }}
            >
              <Picker.Item label="Files" value="file" />
              <Picker.Item label="All" value="all" />
              <Picker.Item label="Shared" value="shared" />
            </Picker>
          )}

          <NavButton
            onPress={() => {
              navigation.getParam("toggleAsc")(!navigation.getParam("sortAsc"));
            }}
            type="fontawesome"
            name={
              navigation.getParam("sortAsc")
                ? "sort-alpha-asc"
                : "sort-alpha-desc"
            }
          />
        </NavBar>
      )
    };
  };

  toggleMode = val => {
    this.setState({
      mode: val
    });
  };

  toggleAsc = val => {
    this.setState({
      sortAsc: val
    });
  };

  constructor(props) {
    super(props);
    this.state = {
      mode: "all",
      sortAsc: true,

      path: "",
      downloadingFile: null,
      uploadingFile: false,

      groupModal: false,
      renameFileModal: false,
      renameGroupModal: false,

      generalFab: true,
      fileFab: false,
      groupFab: false
    };
  }

  componentWillMount() {
    this.props.navigation.setParams({
      mode: this.state.mode,
      sortAsc: this.state.sortAsc,
      toggleMode: this.toggleMode,
      toggleAsc: this.toggleAsc,
      pickerStyle: pickerStyle,
      platform: Platform.OS
    });

    this.fileListener = firebase
      .firestore()
      .collection("files")
      .where("owner", "==", firebase.auth().currentUser.uid)
      .onSnapshot(async snapshot => {
        let res = snapshot.docs.map(item => {
          const file = item.data();
          return {
            id: item.id,
            ...file
          };
        });
        this.props.setFiles(res);
      });

    this.folderListener = firebase
      .firestore()
      .collection("folders")
      .where("owner", "==", firebase.auth().currentUser.uid)
      .onSnapshot(async snapshot => {
        let res = snapshot.docs.map(item => {
          const group = item.data();
          return {
            id: item.id,
            ...group
          };
        });
        this.props.setFolders(res);
      });

    this.shareListener = firebase
      .firestore()
      .collection("files")
      .where(`share.${firebase.auth().currentUser.uid}`, "==", true)
      .onSnapshot(async snapshot => {
        let res = snapshot.docs.map(item => {
          const file = item.data();
          return {
            shared: true,
            id: item.id,
            ...file
          };
        });
        this.props.setSharedFiles(res);
      });
  }

  componentWillUnmount() {
    this.fileListener();
    this.folderListener();
    this.shareListener();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.mode !== prevState.mode) {
      this.props.navigation.setParams({
        mode: this.state.mode
      });
    }
    if (this.state.sortAsc !== prevState.sortAsc) {
      this.props.navigation.setParams({
        sortAsc: this.state.sortAsc
      });
    }
  }

  //images
  uploadImageSubmit = async () => {
    const options = {
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    return new Promise(function(resolve, reject) {
      ImagePicker.showImagePicker(options, response => {
        if (response.fileSize > 5242880) {
          alert("File capped at 5mb.");
          this.setState({
            uploadingFile: false
          });
          reject("Blocked due to size limit.");
        }

        if (response.didCancel) {
          console.log("User cancelled photo picker");
        } else if (response.error) {
          console.log("ImagePicker Error: ", response.error);
        } else if (response.customButton) {
          console.log("User tapped custom button: ", response.customButton);
        } else {
          resolve(response);
        }
      });
    }).then(async file => {
      await this.setState({
        uploadingFile: true
      });
      return await this.props
        .addFile({ file, path: this.state.path })
        .catch(err => {
          alert("errror mon", err);
        })
        .finally(() => {
          this.props.checkRemainingStorage();
          this.setState({
            uploadingFile: false
          });
        });
    });
  };

  // files
  uploadFileSubmit = async () => {
    return new Promise(function(resolve, reject) {
      DocumentPicker.show(
        {
          filetype: [DocumentPickerUtil.allFiles()]
        },
        (error, res) => {
          if (error) {
            reject(error);
          } else {
            resolve(res);
          }
        }
      );
    }).then(async file => {
      await this.setState({
        uploadingFile: true
      });

      if (file.fileSize > 5242880) {
        alert("File capped at 5mb.");
        this.setState({
          uploadingFile: false
        });
        return Promise.resolve("Blocked due to size limit.");
      }

      return await this.props
        .addFile({ file, path: this.state.path })
        .catch(err => {
          alert(err);
        })
        .finally(() => {
          this.props.checkRemainingStorage();
          this.setState({
            uploadingFile: false
          });
        });
    });
  };

  renameFileSubmit = async ({ name }) => {
    return await this.props
      .renameFile({
        fileId: this.props.selectedFile.id,
        newName: name
      })
      .then(this.toggleRenameFileModal())
      .catch(err => {
        alert(err);
      });
  };

  deleteFileSubmit = file => {
    Alert.alert(
      "Delete file",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => {
            /* do nothing */
          },
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            if (file.shared) {
              this.props
                .discardSharedFile({
                  fileId: file.id
                })
                .then(() => {
                  alert("Successful Removed!");
                  this.props.selectFile(null);
                  this.props.checkRemainingStorage();
                })
                .catch(err => {
                  alert(err);
                });
            } else {
              this.props
                .deleteFile({
                  fileId: file.id
                })
                .then(() => {
                  alert("Successful deleted!");
                  this.props.selectFile(null);
                  this.props.checkRemainingStorage();
                })
                .catch(err => {
                  alert(err);
                });
            }
          }
        }
      ],
      { cancelable: false }
    );
  };

  shareFileSubmit = async item => {
    await this.props.selectFile(item);
    await this.props.navigation.navigate({ routeName: "FileSharing" });
  };
  selectFile = item => {
    if (this.props.selectedFile && this.props.selectedFile.id == item.id) {
      this.props.selectFile(null);
    } else {
      this.props.selectFile(item);
      this.props.selectGroup(null);
    }
  };
  openFile = async file => {
    await this.setState({
      downloadingFile: file.id
    });

    if (Platform.OS == "ios") {
      this.props
        .openFile(file)
        .catch(err => {
          console.log(err);
          alert("Downloaded but no app to open.");
        })
        .finally(() => {
          this.setState({
            downloadingFile: null
          });
        });
    } else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "Storage Permission",
          message: "Allow to download file?",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.props
          .openFile(file)
          .catch(err => {
            console.log(err);
            alert("Downloaded but no app to open.");
          })
          .finally(() => {
            this.setState({
              downloadingFile: null
            });
          });
      } else {
        alert("No access to storage.");
        this.setState({
          downloadingFile: null
        });
      }
    }
  };
  editFile = async file => {
    await this.props.selectFile(file);
    this.toggleRenameFileModal();
  };
  toggleRenameFileModal = () => {
    this.setState({
      renameFileModal: !this.state.renameFileModal
    });
  };

  // folder
  toggleAddGroupModal = async () => {
    await this.setState({
      mode: "all"
    });
    await this.setState({
      groupModal: !this.state.groupModal
    });
  };
  addGroupSubmit = ({ name }) => {
    return this.props
      .addGroup({
        name,
        path: this.state.path
      })
      .then(this.toggleAddGroupModal())
      .catch(err => {
        alert(err);
      });
  };
  toggleRenameGroupModal = () => {
    this.setState({
      renameGroupModal: !this.state.renameGroupModal
    });
  };
  renameGroupSubmit = ({ name }) => {
    return this.props
      .renameGroup({
        groupId: this.props.selectedGroup.id,
        newName: name
      })
      .then(this.toggleRenameGroupModal())
      .catch(err => {
        alert(err);
      });
  };
  deleteGroupSubmit = folder => {
    Alert.alert(
      "Delete grouping",
      "All of the files in it will also be deleted, are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => {
            /* do nothing */
          },
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.props
              .deleteGroup(folder)
              .then(() => {
                this.props.selectGroup(null);
              })
              .catch(err => {
                alert(err);
              });
          }
        }
      ],
      { cancelable: false }
    );
  };
  goBackGroup = () => {
    let tmpPath = this.state.path.substr(1);
    tmpPath = tmpPath.slice(0, -1);
    let arr = tmpPath.split(",");
    arr.pop();
    let newPath;
    if (arr.length == 0) {
      newPath = "";
    } else {
      newPath = `,${arr.join(",")},`;
    }
    this.setState({
      path: newPath
    });
  };
  selectGroup = group => {
    if (this.props.selectedGroup && this.props.selectedGroup.id == group.id) {
      this.props.selectGroup(null);
    } else {
      this.props.selectGroup(group);
      this.props.selectFile(null);
    }
  };
  openFolder = folder => {
    const { id, name, path } = folder;
    this.setState({
      path: path == "" ? `,${id},` : `${path}${id},`
    });
  };

  // render
  _fileList = () => {
    let fileList = _.sortBy(this.props.files, item => {
      return item.name.toLowerCase();
    });

    if (!this.state.sortAsc) fileList = fileList.reverse();

    return (
      <SwipeListView
        useSectionList
        recalculateHiddenLayout={true}
        sections={[
          {
            title: "File",
            data: fileList.concat([{ key: "empty" }])
          }
        ]}
        renderItem={({ item, section }, rowMap) => {
          if (item.key === "empty") return <View style={{ height: 80 }} />;
          if (section.title === "File") {
            return (
              <SwipeFileRow
                downloading={this.state.downloadingFile === item.id}
                percent={
                  this.state.downloadingFile === item.id
                    ? this.props.downloadPercent
                    : 0
                }
                item={item}
                openFile={this.openFile}
              />
            );
          }
        }}
        renderHiddenItem={({ item, section }, rowMap) => {
          if (item.key === "empty") return null;

          if (section.title === "File") {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: "flex-end",
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    paddingVertical: 23
                  }}
                >
                  <Button
                    onPress={() => this.deleteFileSubmit(item)}
                    transparent
                  >
                    <Icon
                      style={[styles.filesicon, { color: "#ff7043" }]}
                      name="trash"
                    />
                  </Button>
                  <Button
                    onPress={async () => {
                      await this.props.selectFile(item);
                      this.toggleRenameFileModal();
                    }}
                    transparent
                  >
                    <Icon style={styles.filesicon} name="edit" />
                  </Button>
                  <Button
                    style={{ paddingRight: 21 }}
                    onPress={() => this.shareFileSubmit(item)}
                    transparent
                  >
                    <Icon style={styles.filesicon} name="share" />
                  </Button>
                </View>
              </View>
            );
          }
        }}
        leftOpenValue={75}
        rightOpenValue={-125}
      />
      // <FlatList
      //   data={fileList.concat([{ key: "empty" }])}
      //   extraData={this.state.downloadingFile}
      //   keyExtractor={item => {
      //     item.key ? item.key : item.id;
      //   }}
      //   renderItem={({ item }, rowMap) => {
      //     if (item.key === "empty") return <View style={{ height: 100 }} />;

      //     return (
      //       <SwipeFileRow
      //         item={item}
      //         downloading={this.state.downloadingFile === item.id}
      //         percent={
      //           this.state.downloadingFile === item.id
      //             ? this.props.downloadPercent
      //             : 0
      //         }
      //         openFile={this.openFile}
      //         setFile={this.setFile}
      //         showFileActionSheet={this.showFileActionSheet}
      //       />
      //     );
      //   }}
      // />
    );
  };

  _allList = () => {
    let groupList = _.sortBy(this.props.groups, item => {
      if (!item.name) return false;
      return item.name.toLowerCase();
    });

    let fileList = _.sortBy(
      this.props.files.filter(file => {
        if (file.key === "empty") return true;
        if (file.path != this.state.path) {
          return false;
        }
        return true;
      }),
      item => {
        return item.name.toLowerCase();
      }
    );

    let shareList = _.sortBy(this.props.sharedFiles, item => {
      return item.name.toLowerCase();
    });

    let AllFiles = [];

    if (shareList.length != 0) {
      AllFiles = fileList.concat(shareList);
    } else {
      AllFiles = fileList;
    }

    if (!this.state.sortAsc) groupList = groupList.reverse();
    if (!this.state.sortAsc) fileList = fileList.reverse();
    if (!this.state.sortAsc) shareList = shareList.reverse();

    return (
      <View style={{ flex: 1 }}>
        {this.props.groups.length != 0 ? (
          <Text
            style={{
              paddingBottom: 10,
              paddingLeft: 16,
              fontSize: 18,
              fontWeight: "500",
              color: "#bdc3ca"
            }}
          >
            Folders
          </Text>
        ) : null}

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: 30
          }}
        >
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {this.state.path != "" ? (
              <TouchableOpacity onPress={this.goBackGroup}>
                <View
                  style={{
                    margin: 5,
                    marginRight: 15,
                    width: 60,
                    height: "auto",
                    borderRadius: 8,
                    backgroundColor: "white",
                    padding: 10,
                    justifyContent: "center",
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}
                >
                  <Text>{""}</Text>
                  <AntdIcon
                    style={{
                      color: "#3d72de",
                      fontSize: 30,
                      alignSelf: "center"
                    }}
                    name="left"
                  />
                  <Text>{""}</Text>
                </View>
              </TouchableOpacity>
            ) : null}

            {groupList.map(item => {
              return (
                <FolderObj
                  key={item.id}
                  item={item}
                  currentPath={this.state.path}
                  openFolder={this.openFolder}
                  setFolder={this.setFolder}
                  showFolderActionSheet={this.showFolderActionSheet}
                />
              );
            })}
          </ScrollView>
        </View>

        {AllFiles.length != 0 ? (
          <Text
            style={{
              padding: 10,
              paddingLeft: 16,
              fontSize: 18,
              fontWeight: "500",
              color: "#bdc3ca"
            }}
          >
            All Files
          </Text>
        ) : null}

        <SwipeListView
          useSectionList
          recalculateHiddenLayout={true}
          sections={[
            {
              title: "File",
              data: AllFiles.concat([{ key: "empty" }])
            }
          ]}
          renderItem={({ item, section }, rowMap) => {
            if (item.key === "empty") return <View style={{ height: 80 }} />;
            if (section.title === "File") {
              return (
                <SwipeFileRow
                  downloading={this.state.downloadingFile === item.id}
                  percent={
                    this.state.downloadingFile === item.id
                      ? this.props.downloadPercent
                      : 0
                  }
                  item={item}
                  openFile={this.openFile}
                />
              );
            }
          }}
          renderHiddenItem={({ item, section }, rowMap) => {
            if (item.key === "empty") return null;

            if (section.title === "File") {
              let hideEdit = false;
              if (item.share) {
                if (Object.keys(item.share).length !== 0) {
                  hideEdit = true;
                }
              }

              return (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "flex-end",
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      paddingVertical: 23
                    }}
                  >
                    <Button
                      onPress={() => this.deleteFileSubmit(item)}
                      transparent
                    >
                      <Icon
                        style={[styles.filesicon, { color: "#ff7043" }]}
                        name="trash"
                      />
                    </Button>
                    {/* <Display isVisible={!hideEdit}> */}
                    <Button
                      onPress={async () => {
                        await this.props.selectFile(item);
                        this.toggleRenameFileModal();
                      }}
                      transparent
                    >
                      <Icon style={styles.filesicon} name="edit" />
                    </Button>
                    {/* </Display> */}
                    <Button
                      style={{ paddingRight: 21 }}
                      onPress={() => this.shareFileSubmit(item)}
                      transparent
                    >
                      <Icon style={styles.filesicon} name="share" />
                    </Button>
                  </View>
                </View>
              );
            }
          }}
          leftOpenValue={75}
          rightOpenValue={-125}
        />
      </View>
    );
  };

  _shareList = () => {
    let shareList = _.sortBy(this.props.sharedFiles, item => {
      return item.name.toLowerCase();
    });

    if (!this.state.sortAsc) shareList = shareList.reverse();

    return (
      <SwipeListView
        useSectionList
        recalculateHiddenLayout={true}
        sections={[
          {
            title: "File",
            data: shareList.concat([{ key: "empty" }])
          }
        ]}
        renderItem={({ item, section }, rowMap) => {
          if (item.key === "empty") return <View style={{ height: 80 }} />;
          if (section.title === "File") {
            return (
              <SwipeFileRow
                downloading={this.state.downloadingFile === item.id}
                percent={
                  this.state.downloadingFile === item.id
                    ? this.props.downloadPercent
                    : 0
                }
                item={item}
                openFile={this.openFile}
              />
            );
          }
        }}
        renderHiddenItem={({ item, section }, rowMap) => {
          if (item.key === "empty") return null;

          if (section.title === "File") {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: "flex-end",
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    paddingVertical: 23
                  }}
                >
                  <Button
                    onPress={() => this.deleteFileSubmit(item)}
                    transparent
                  >
                    <Icon
                      style={[styles.filesicon, { color: "#ff7043" }]}
                      name="trash"
                    />
                  </Button>
                  {/* <Display isVisible={false}> */}
                  <Button
                    onPress={async () => {
                      await this.props.selectFile(item);
                      this.toggleRenameFileModal();
                    }}
                    transparent
                  >
                    <Icon style={styles.filesicon} name="edit" />
                  </Button>
                  {/* </Display>  */}
                  <Button
                    style={{ paddingRight: 21 }}
                    onPress={() => this.shareFileSubmit(item)}
                    transparent
                  >
                    <Icon style={styles.filesicon} name="share" />
                  </Button>
                </View>
              </View>
            );
          }
        }}
        leftOpenValue={75}
        rightOpenValue={-125}
      />

      // <FlatList
      //   data={shareList.concat([{ key: "empty" }])}
      //   keyExtractor={item => {
      //     item.key ? item.key : item.id;
      //   }}
      //   renderItem={({ item }, rowMap) => {
      //     if (item.key === "empty") return <View style={{ height: 100 }} />;

      //     return (
      //       <SwipeFileRow
      //         downloading={this.state.downloadingFile === item.id}
      //         percent={
      //           this.state.downloadingFile === item.id
      //             ? this.props.downloadPercent
      //             : 0
      //         }
      //         item={item}
      //         openFile={this.openFile}
      //       />
      //     );
      //   }}
      // />
    );
  };

  // action sheet
  showActionSheet = () => {
    if (Platform.OS == "ios") {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ["Cancel", "Photo Library", "Upload File", "New Folder"],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 0
        },
        buttonIndex => {
          switch (buttonIndex) {
            case 1:
              this.uploadImageSubmit();
              break;
            case 2:
              this.uploadFileSubmit();
              break;
            case 3:
              this.toggleAddGroupModal();
              break;
            default:
              break;
          }
        }
      );
    } else {
      this.ActionSheet.show();
    }
  };
  showFolderActionSheet = () => {
    if (Platform.OS == "ios") {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ["Cancel", "Delete folder", "Change folder name"],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 0
        },
        buttonIndex => {
          switch (buttonIndex) {
            case 1:
              this.deleteGroupSubmit(this.folder);
              break;
            case 2:
              this.props.selectGroup(this.folder);
              this.toggleRenameGroupModal();
              break;
            default:
              break;
          }
        }
      );
    } else {
      this.FolderActionSheet.show();
    }
  };
  setFolder = folder => {
    this.folder = folder;
  };
  showFileActionSheet = () => {
    if (Platform.OS == "ios") {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: [
            "Cancel",
            "Delete file",
            "Change file name",
            "Share to friends"
          ],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 0
        },
        buttonIndex => {
          switch (buttonIndex) {
            case 1:
              this.deleteFileSubmit(this.file);
              break;
            case 2:
              this.editFile(this.file);
              break;
            case 3:
              this.shareFileSubmit(this.file);
              break;
            default:
              break;
          }
        }
      );
    } else {
      this.FileActionSheet.show();
    }
  };
  setFile = file => {
    this.file = file;
  };

  render() {
    if (!this.props.user) return null;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    return (
      <View style={styles.vaultView}>
        <Display isVisible={this.state.uploadingFile}>
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginBottom: 10
            }}
          >
            <ActivityIndicator color="#0000ff" />
            <Text>Uploading ..</Text>
          </View>
        </Display>

        {/* file list */}
        {this.state.mode === "file" && this.props.files.length == 0 ? (
          <EmptyPlaceholder mode="file" />
        ) : null}
        {this.state.mode === "file" ? this._fileList() : null}

        {/* Group list */}
        {this.state.mode === "all" &&
        this.props.files.length == 0 &&
        this.props.groups.length == 0 ? (
          <EmptyPlaceholder mode="all" />
        ) : null}
        {this.state.mode === "all" ? this._allList() : null}

        {/* shared file list */}
        {this.state.mode === "shared" && this.props.sharedFiles.length == 0 ? (
          <EmptyPlaceholder mode="shared" />
        ) : null}
        {this.state.mode === "shared" ? this._shareList() : null}

        {/* Group add modal */}
        <LevelModal
          visible={this.state.groupModal}
          onRequestClose={this.toggleAddGroupModal}
        >
          <NewnameReduxForm
            onSubmit={this.addGroupSubmit}
            handleClose={this.toggleAddGroupModal}
          />
        </LevelModal>

        {/* Group rename modal */}
        <LevelModal
          visible={this.state.renameGroupModal}
          onRequestClose={this.toggleRenameGroupModal}
        >
          <RenameReduxForm
            onSubmit={this.renameGroupSubmit}
            handleClose={this.toggleRenameGroupModal}
          />
        </LevelModal>

        {/* file rename modal */}
        <LevelModal
          visible={this.state.renameFileModal}
          onRequestClose={this.toggleRenameFileModal}
        >
          <RenameReduxForm
            onSubmit={this.renameFileSubmit}
            handleClose={this.toggleRenameFileModal}
          />
        </LevelModal>

        <Fab
          position="bottomRight"
          style={styles.iconBack}
          onPress={this.showActionSheet}
        >
          <MaterialIcons style={{ color: "#3370FF" }} name="add" />
        </Fab>

        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={"What you want to add ?"}
          options={[
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="picture" />
              <Text>Photo Library</Text>
            </View>,
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="addfile" />
              <Text>Upload File</Text>
            </View>,
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="addfolder" />
              <Text>New Folder</Text>
            </View>,
            "Cancel"
          ]}
          cancelButtonIndex={3}
          destructiveButtonIndex={1}
          onPress={index => {
            switch (index) {
              case 0:
                this.uploadImageSubmit();
                break;
              case 1:
                this.uploadFileSubmit();
                break;
              case 2:
                this.toggleAddGroupModal();
                break;
              default:
                break;
            }
          }}
          styles={{ flex: 1 }}
        />

        <ActionSheet
          ref={o => (this.FolderActionSheet = o)}
          title={"Folder actions"}
          options={[
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="delete" />
              <Text>Delete folder</Text>
            </View>,
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="edit" />
              <Text>Change folder name</Text>
            </View>,
            "Cancel"
          ]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={async index => {
            switch (index) {
              case 0:
                this.deleteGroupSubmit(this.folder);
                break;
              case 1:
                await this.props.selectGroup(this.folder);
                this.toggleRenameGroupModal();
                break;
              default:
                break;
            }
          }}
          styles={{ flex: 1 }}
        />

        <ActionSheet
          ref={o => (this.FileActionSheet = o)}
          title={"File actions"}
          options={[
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="delete" />
              <Text>Delete file</Text>
            </View>,
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="edit" />
              <Text>Change file name</Text>
            </View>,
            <View style={{ flexDirection: "row" }}>
              <AntdIcon style={styles.actionSheetIcon} name="sharealt" />
              <Text>Share to friends</Text>
            </View>,
            "Cancel"
          ]}
          cancelButtonIndex={3}
          destructiveButtonIndex={1}
          onPress={async index => {
            switch (index) {
              case 0:
                this.deleteFileSubmit(this.file);
                break;
              case 1:
                this.editFile(this.file);
                break;
              case 2:
                this.shareFileSubmit(this.file);
                break;
              default:
                break;
            }
          }}
          styles={{ flex: 1 }}
        />

        <Display isVisible={incomingFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={incomingFlag}
          >
            <CallKit type={"incoming"} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={showTimerFlag}
          >
            <CallKit type={"timer"} />
          </LevelModal>
        </Display>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  nav: state.nav,
  groups: state.vault.groups,
  files: state.vault.files,
  sharedFiles: state.vault.sharedFiles,
  selectedFile: state.vault.selectedFile,
  selectedGroup: state.vault.selectedGroup,
  isFetching: state.vault.isFetching,
  downloadPercent: state.vault.downloadPercent,
  uploadProgress: state.vault.uploadProgress,
  incomingFlag: state.contacts.incomingFlag,
  showTimerFlag: state.contacts.showTimerFlag
});

export default connect(mapStateToProps, {
  addFile,
  renameFile,
  deleteFile,
  openFile,
  shareFile,
  addGroup,
  renameGroup,
  joinGroup,
  deleteGroup,
  checkRemainingStorage,
  selectFile,
  selectGroup,
  setFiles,
  setFolders,
  setSharedFiles,
  discardSharedFile
})(Vault);

const styles = StyleSheet.create({
  vaultView: { flex: 1 },
  cancel: {
    backgroundColor: "blue"
  },
  item: {
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 16,
    color: "#3b4a5f"
  },
  itemExt: {
    fontFamily: "Roboto",
    fontSize: 16,
    color: "#adb3bc"
  },
  itemNote: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: "#c9ced6"
  },
  fileItem: { marginBottom: 5, marginTop: 5 },
  folder: {
    fontSize: 15,
    fontFamily: "Roboto"
  },
  warningMessage: {
    fontSize: 14,
    // alignSelf: "center",
    marginTop: 20
  },
  sectionHeader: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    marginTop: 3,
    marginBottom: 3,
    color: "white",
    backgroundColor: "#616161",
    padding: 5,
    borderRadius: 3,
    width: "30%"
  },
  bottomScroll: {
    justifyContent: "flex-end",
    height: 50
  },
  icon: { color: "#616161", fontSize: 20, paddingLeft: 15, paddingRight: 15 },
  filesicon: { color: "#616161", fontSize: 20, paddingHorizontal: 10 },
  actionSheetIcon: { color: "#616161", fontSize: 20, paddingRight: 15 },
  fabIcon: { color: "white" },
  iconDanger: { color: "#eeeeee" },
  iconBack: { backgroundColor: "#fff" },
  iconBackDanger: { backgroundColor: "#DD5144" },
  tabHeader: { color: "#3370FF" },
  tabsUnderline: { backgroundColor: "#3370FF" },
  fileIcon: {
    alignSelf: "center",
    color: "#a7aebb",
    padding: 2,
    fontSize: 28
  },
  actionIcon: {
    alignSelf: "center",
    color: "#3370FF",
    padding: 2,
    fontSize: 20
  },
  rowFront: {
    justifyContent: "center",
    paddingTop: 15,
    paddingLeft: 30,
    paddingRight: 30
  }
});
