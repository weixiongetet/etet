"use strict";

import React from "react";
import { PermissionsAndroid, Platform, SafeAreaView, Text, View } from "react-native";
import { Voximplant } from "react-native-voximplant";
import { connect } from "react-redux";
import { Display } from "../../components/BoxComponent";
import CallButton from "../../components/CallButton";
import CallManager from "../../manager/CallManager";
import { endRejectCall } from "../../redux/modules/chat";
import COLOR from "../../styles/Color";
import styles from "../../styles/Styles";

class IncomingCallScreen extends React.Component {
  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    const callId = params ? params.callId : null;
    this.isVideoCall = params ? params.isVideo : false;

    if (callId) {
      this.call = CallManager.getInstance().getCallById(callId);
    }

    this.state = {
      displayName: params ? params.from : null,
      callNumber: params && params.callNumber ? params.callNumber : null
    };
  }

  componentDidMount() {
    if (this.call) {
      Object.keys(Voximplant.CallEvents).forEach(eventName => {
        const callbackName = `_onCall${eventName}`;
        if (typeof this[callbackName] !== "undefined") {
          this.call.on(eventName, this[callbackName]);
        }
      });
    }
  }

  componentWillUnmount() {
    if (this.call) {
      Object.keys(Voximplant.CallEvents).forEach(eventName => {
        const callbackName = `_onCall${eventName}`;
        if (typeof this[callbackName] !== "undefined") {
          this.call.off(eventName, this[callbackName]);
        }
      });
      this.call = null;
    }
  }

  async answerCall(withVideo) {
    if (this.call && this.call !== null) {
      //normal incoming call
      try {
        if (Platform.OS === "android") {
          let permissions = [PermissionsAndroid.PERMISSIONS.RECORD_AUDIO];
          if (withVideo) {
            permissions.push(PermissionsAndroid.PERMISSIONS.CAMERA);
          }
          const granted = await PermissionsAndroid.requestMultiple(permissions);
          const recordAudioGranted =
            granted["android.permission.RECORD_AUDIO"] === "granted";
          const cameraGranted =
            granted["android.permission.CAMERA"] === "granted";
          if (recordAudioGranted) {
            if (withVideo && !cameraGranted) {
              console.warn(
                "IncomingCallScreen: answerCall: camera permission is not granted"
              );
              return;
            }
          } else {
            console.warn(
              "IncomingCallScreen: answerCall: record audio permission is not granted"
            );
            return;
          }
        }
      } catch (e) {
        console.warn("IncomingCallScreen: asnwerCall:" + e);
        return;
      }

      // this.props.navigation.replace('Home')

      //CallScreen.getInstance()
      if (this.call.callId && this.call.callId !== null) {
        this.props.navigation.replace("Call", {
          callId: this.call.callId,
          isVideo: withVideo,
          isIncoming: true
        });
      }

    } else {
      // conference call
      console.log(
        "MainScreen: make call: " +
        this.state.callNumber +
        ", isVideo:" +
        this.isVideoCall
      );

      try {
        if (Platform.OS === "android") {
          let permissions = [PermissionsAndroid.PERMISSIONS.RECORD_AUDIO];
          if (this.isVideoCall) {
            permissions.push(PermissionsAndroid.PERMISSIONS.CAMERA);
          }
          const granted = await PermissionsAndroid.requestMultiple(permissions);
          const recordAudioGranted =
            granted["android.permission.RECORD_AUDIO"] === "granted";
          const cameraGranted =
            granted["android.permission.CAMERA"] === "granted";
          if (recordAudioGranted) {
            if (this.isVideoCall && !cameraGranted) {
              console.warn(
                "MainScreen: makeCall: camera permission is not granted"
              );
              return;
            }
          } else {
            console.warn(
              "MainScreen: makeCall: record audio permission is not granted"
            );
            return;
          }
        }

        const callSettings = {
          video: {
            sendVideo: this.isVideoCall,
            receiveVideo: this.isVideoCall
          }
        };

        if (Platform.OS === "ios" && parseInt(Platform.Version, 10) >= 10) {
          callSettings.setupCallKit = true;
          // const useCallKitString = await AsyncStorage.getItem('useCallKit');
          // callSettings.setupCallKit = JSON.parse(useCallKitString);
        }

        //  let call = await Voximplant.getInstance().call(this.state.callNumber, callSettings);

        //video call conference
        // console.log("start conference");
        let call = await Voximplant.getInstance().callConference(
          this.state.callNumber,
          callSettings
        );
        console.log("callaction", call);

        let callManager = CallManager.getInstance();
        callManager.addCall(call); // set call number

        if (callSettings.setupCallKit) {
          //ios callkit
          callManager.startOutgoingCallViaCallKit(
            this.isVideoCall,
            this.state.callNumber
          );
        }

        //android callkit
        // if(this.state.countSelectedFriends > 1){
        //     console.log("open conference screen")
        //     this.props.navigation.navigate('Call', {
        //         callId: call.callId,
        //         isVideo: this.isVideoCall,
        //         isIncoming: false,
        //     });
        // }else{
        this.props.navigation.navigate("Call", {
          callId: call.callId,
          isVideo: this.isVideoCall,
          isIncoming: false
        });
        //  }
      } catch (e) {
        console.warn("MainScreen: makeCall failed: " + e);
      }
    }
  }

  declineCall() {
    console.log("rejectpress", this.call)

    if (this.call) {
      //normal incoming call
      this.call.decline();
    } else {
      //conference call
      const { user } = this.props;

      this.props.endRejectCall({
        userID: user.userID
      });

      // NavigationService.navigate("IncomingCall", {
      //   callId: this.call.callId,
      //   isVideo: null,
      //   from: null
      // });

      this.props.navigation.navigate("Chat");
    }
  }

  _onCallDisconnected = event => {
    console.log("calldisoconnectincoming")
    CallManager.getInstance().removeCall(event.call);
    // this.props.navigation.reset([NavigationService.navigate("Chat", {
    // })], 0);
    this.props.navigation.navigate("Chat");
  };

  _onCallEndpointAdded = event => {
    console.log(
      "IncomingCallScreen: _onCallEndpointAdded: callid: " +
      this.call.callId +
      " endpoint id: " +
      event.endpoint.id
    );
    this.setState({ displayName: event.endpoint.displayName });
  };

  render() {
    return (
      <SafeAreaView style={[styles.safearea, styles.aligncenter]}>
        <Text style={styles.incoming_call}>Incoming call from:</Text>
        <Text style={styles.incoming_call}>{this.state.displayName}</Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            height: 90
          }}
        >
          <CallButton
            icon_name="call"
            color={COLOR.ACCENT}
            buttonPressed={() => this.answerCall(false)}
          />
          <Display
            isVisible={
              this.isVideoCall
            }
          >
            <CallButton
              icon_name="videocam"
              color={COLOR.ACCENT}
              buttonPressed={() => this.answerCall(true)}
            />
          </Display>

          <CallButton
            icon_name="call-end"
            color={COLOR.RED}
            buttonPressed={() => this.declineCall()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(
  mapStateToProps,
  { endRejectCall }
)(IncomingCallScreen);
