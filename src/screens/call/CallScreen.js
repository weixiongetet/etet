"use strict";

import VIForegroundService from "@voximplant/react-native-foreground-service";
import React from "react";
import { FlatList, Modal, PermissionsAndroid, Platform, SafeAreaView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import { Voximplant } from "react-native-voximplant";
import { connect } from "react-redux";
import _ from "underscore";
import { Display } from "../../components/BoxComponent";
import CallButton from "../../components/CallButton";
import { Keypad } from "../../components/Keypad";
import CallManager from "../../manager/CallManager";
import { addConference, endRejectCall } from "../../redux/modules/chat";
import COLOR from "../../styles/Color";
import COLOR_SCHEME from "../../styles/ColorScheme";
import styles from "../../styles/Styles";

const CALL_STATES = {
  DISCONNECTED: "disconnected",
  CONNECTING: "connecting",
  CONNECTED: "connected"
};

class CallScreen extends React.Component {
  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;

    this.callId = params ? params.callId : null;
    this.isVideoCall = params ? params.isVideo : false;
    this.isIncoming = params ? params.isIncoming : false;
    this.callState = CALL_STATES.DISCONNECTED;
    this.conference = params ? params.conference : false;
    this.confCallNum = params ? params.confCallNum : false;

    this.state = {
      isAudioMuted: false,
      isVideoSent: this.isVideoCall,
      isKeypadVisible: false,
      isModalOpen: false,
      modalText: "",
      localVideoStreamId: null,
      remoteVideoStreamId: [],
      audioDeviceSelectionVisible: false,
      audioDevices: [],
      audioDeviceIcon: "hearing",
      remoteVideoEndpoint: [],
      countMember: 0
    };

    console.log("callscrennstar", this.callId);

    this.call = CallManager.getInstance().getCallById(this.callId);
    console.log("callscrennstarnnnnnnnnnnn", this.call);
  }

  componentDidMount() {
    // if (this.call) {
    //   console.log("hahahahaha", this.call);
    //   Object.keys(Voximplant.CallEvents).forEach(eventName => {
    //     const callbackName = `_onCall${eventName}`;
    //     if (typeof this[callbackName] !== "undefined") {
    //       this.call.on(eventName, this[callbackName]);
    //     }
    //   });

    //   if (this.isIncoming) {
    //     this.call.getEndpoints().forEach(endpoint => {
    //       this._setupEndpointListeners(endpoint, true);
    //     });
    //   }
    // }

    Object.keys(Voximplant.Hardware.AudioDeviceEvents).forEach(eventName => {
      const callbackName = `_onAudio${eventName}`;
      if (typeof this[callbackName] !== "undefined") {
        Voximplant.Hardware.AudioDeviceManager.getInstance().on(
          eventName,
          this[callbackName]
        );
      }
    });

    const callSettings = {
      video: {
        sendVideo: this.isVideoCall,
        receiveVideo: this.isVideoCall
      }
    };
    if (this.isIncoming) {
      if (this.call) {
        this.call.answer(callSettings);
        this.setupListeners();
      }
    } else {
      (async () => {
        this.setupListeners();
      })();
    }
    this.callState = CALL_STATES.CONNECTING;

    (async () => {
      let currentAudioDevice = await Voximplant.Hardware.AudioDeviceManager.getInstance().getActiveDevice();
      switch (currentAudioDevice) {
        case Voximplant.Hardware.AudioDevice.BLUETOOTH:
          this.setState({ audioDeviceIcon: "bluetooth-audio" });
          break;
        case Voximplant.Hardware.AudioDevice.SPEAKER:
          this.setState({ audioDeviceIcon: "volume-up" });
          break;
        case Voximplant.Hardware.AudioDevice.WIRED_HEADSET:
          this.setState({ audioDeviceIcon: "headset" });
          break;
        case Voximplant.Hardware.AudioDevice.EARPIECE:
        default:
          this.setState({ audioDeviceIcon: "hearing" });
          break;
      }
    })();
  }

  componentWillUnmount() {
    if (this.call) {
      Object.keys(Voximplant.CallEvents).forEach(eventName => {
        const callbackName = `_onCall${eventName}`;
        if (typeof this[callbackName] !== "undefined") {
          this.call.off(eventName, this[callbackName]);
        }
      });
    }
    Object.keys(Voximplant.Hardware.AudioDeviceEvents).forEach(eventName => {
      const callbackName = `_onAudio${eventName}`;
      if (typeof this[callbackName] !== "undefined") {
        Voximplant.Hardware.AudioDeviceManager.getInstance().off(
          eventName,
          this[callbackName]
        );
      }
    });
  }

  muteAudio() {
    const isMuted = this.state.isAudioMuted;
    this.call.sendAudio(isMuted);
    this.setState({ isAudioMuted: !isMuted });
  }

  async sendVideo(doSend) {
    try {
      if (doSend && Platform.OS === "android") {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA
        );
        if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
          console.warn(
            "CallScreen[" +
            this.callId +
            "] sendVideo: failed due to camera permission is not granted"
          );
          return;
        }
      }

      await this.call.sendVideo(doSend);
      this.setState({ isVideoSent: doSend });
    } catch (e) {
      console.warn(
        `Failed to sendVideo(${doSend}) due to ${e.code} ${e.message}`
      );
    }
  }

  async hold(doHold) {
    try {
      await this.call.hold(doHold);
    } catch (e) {
      console.warn(
        "Failed to hold(" + doHold + ") due to " + e.code + " " + e.message
      );
    }
  }

  async receiveVideo() {
    try {
      await this.call.receiveVideo();
    } catch (e) {
      console.warn("Failed to receiveVideo due to " + e.code + " " + e.message);
    }
  }

  setupListeners() {
    if (this.call) {
      Object.keys(Voximplant.CallEvents).forEach(eventName => {
        const callbackName = `_onCall${eventName}`;
        if (typeof this[callbackName] !== "undefined") {
          this.call.on(eventName, this[callbackName]);
        }
      });
      if (this.isIncoming) {
        this.call.getEndpoints().forEach(endpoint => {
          this._setupEndpointListeners(endpoint, true);
        });
      }
    }
  }

  endCall() {
    const { user } = this.props;
    console.log("endcall");

    this.call.getEndpoints().forEach(endpoint => {
      this._setupEndpointListeners(endpoint, false);
    });
    // this.call.hangup();
    this.setState({
      remoteVideoStreamId: [],
      localVideoStreamId: null
    });

    let selFriend = this.props.seletedFriends;
    let selectedFriends = selFriend.map(friend => {
      (async () => {
        await this.props.endRejectCall({
          userID: friend.userID
        });
      })();
    });

    let callManager = CallManager.getInstance();
    callManager.endCall();
    // this.props.navigation.replace("Chat");
  }

  switchKeypad() {
    let isVisible = this.state.isKeypadVisible;
    this.setState({ isKeypadVisible: !isVisible });
  }

  async switchAudioDevice() {
    let devices = await Voximplant.Hardware.AudioDeviceManager.getInstance().getAudioDevices();
    this.setState({ audioDevices: devices, audioDeviceSelectionVisible: true });
  }

  selectAudioDevice(device) {
    Voximplant.Hardware.AudioDeviceManager.getInstance().selectAudioDevice(
      device
    );
    this.setState({ audioDeviceSelectionVisible: false });
  }

  _keypadPressed(value) {
    this.call.sendTone(value);
  }

  _closeModal() {
    console.log("closemodal");
    this.setState({ isModalOpen: false, modalText: "" });
    this.props.navigation.goBack();
  }

  _onCallFailed = event => {
    console.log("call screen fail");
    this.callState = CALL_STATES.DISCONNECTED;
    CallManager.getInstance().removeCall(this.call);
    this.setState({
      isModalOpen: true,
      modalText: "Call failed: " + event.reason,
      remoteVideoStreamId: [],
      localVideoStreamId: null
    });
  };

  _onCallDisconnected = event => {
    console.log("_onCallDisconnected");

    this.setState({
      remoteVideoStreamId: [],
      localVideoStreamId: null
    });
    CallManager.getInstance().removeCall(this.call);
    if (
      Platform.OS === "android" &&
      Platform.Version >= 26 &&
      this.callState === CALL_STATES.CONNECTED
    ) {
      (async () => {
        await VIForegroundService.stopService();
      })();
    }
    this.callState = CALL_STATES.DISCONNECTED;

    this.props.navigation.goBack();

  };

  _onCallConnected = async event => {
    // this.call.sendMessage('Test message');
    // this.call.sendInfo('rn/info', 'test info');
    this.callState = CALL_STATES.CONNECTED;
    if (Platform.OS === "android" && Platform.Version >= 26) {
      const channelConfig = {
        id: "ForegroundServiceChannel",
        name: "In progress calls",
        description: "Notify the call is in progress",
        enableVibration: false
      };
      const notificationConfig = {
        channelId: "ForegroundServiceChannel",
        id: 3456,
        title: "Voximplant",
        text: "Call in progress",
        icon: "ic_vox_notification"
      };

      if (this.conference) {
        let temSelectData = {};
        let selFriend = this.props.seletedFriends;
        const { user } = this.props;

        let selectedFriends = selFriend.map(friend => {
          temSelectData = {
            firstName: user.firstName,
            lastName: user.lastName,
            // phone: user.phone,
            email: user.email,
            callto_userID: friend.userID,
            etetID: user.etetID,
            profilePictureURL: user.profilePictureURL,
            // fcmToken: user.fcmToken,
            conference_num: this.confCallNum,
            caller_userID: user.userID,
            join_status: false,
            callId: this.callId,
            isVideo: this.props.isVideo
          };

          (async () => {
            await this.props.addConference({
              userID: friend.userID,
              items: temSelectData
            });
          })();
        });
      }

      (async () => {
        await VIForegroundService.createNotificationChannel(channelConfig);
        await VIForegroundService.startService(notificationConfig);
      })();
    }
  };

  _onCallLocalVideoStreamAdded = event => {
    this.setState({ localVideoStreamId: event.videoStream.id });
  };

  _onCallLocalVideoStreamRemoved = event => {
    this.setState({ localVideoStreamId: null });
  };

  _onCallEndpointAdded = event => {
    this._setupEndpointListeners(event.endpoint, true);
  };

  _onEndpointRemoteVideoStreamAdded = event => {
    this.setState({
      countMember: this.state.remoteVideoStreamId.length,
      remoteVideoStreamId: [
        ...this.state.remoteVideoStreamId,
        event.videoStream.id
      ]
    });

    if (this.state.remoteVideoStreamId.length >= 3) {
      this.setState({
        remoteVideoStreamId: [
          ...this.state.remoteVideoStreamId,
          this.state.localVideoStreamId
        ]
      });
    }
  };

  _onEndpointRemoteVideoStreamRemoved = event => {
    this.setState({
      remoteVideoStreamId: _.without(
        this.state.remoteVideoStreamId,
        event.videoStream.id
      )
    });
  };

  _onEndpointRemoved = event => {
    console.log("_onEndpointRemoved", event);
    this._setupEndpointListeners(event.endpoint, false);
  };

  _onEndpointInfoUpdated = event => {
    console.log("CallScreen: _onEndpointInfoUpdated:", event);
  };

  _setupEndpointListeners = (endpoint, on) => {
    Object.keys(Voximplant.EndpointEvents).forEach(eventName => {
      const callbackName = `_onEndpoint${eventName}`;
      if (typeof this[callbackName] !== "undefined") {
        endpoint[on ? "on" : "off"](eventName, this[callbackName]);
      }
    });
  };

  _onAudioDeviceChanged = event => {
    switch (event.currentDevice) {
      case Voximplant.Hardware.AudioDevice.BLUETOOTH:
        this.setState({ audioDeviceIcon: "bluetooth-audio" });
        break;
      case Voximplant.Hardware.AudioDevice.SPEAKER:
        this.setState({ audioDeviceIcon: "volume-up" });
        break;
      case Voximplant.Hardware.AudioDevice.WIRED_HEADSET:
        this.setState({ audioDeviceIcon: "headset" });
        break;
      case Voximplant.Hardware.AudioDevice.EARPIECE:
      default:
        this.setState({ audioDeviceIcon: "hearing" });
        break;
    }
  };

  _onAudioDeviceListChanged = event => {
    (async () => {
      let device = await Voximplant.Hardware.AudioDeviceManager.getInstance().getActiveDevice();
    })();
    this.setState({ audioDevices: event.newDeviceList });
  };

  flatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#607D8B",
          marginTop: 10,
          marginBottom: 10
        }}
      />
    );
  };

  render() {
    console.log("calliddddcallscreen", this.call);
    console.log(
      "remoteVideoStreamId22222222333333",
      this.state.remoteVideoStreamId
    );

    return (
      <SafeAreaView style={styles.safearea}>
        <Display
          isVisible={
            !this.isVideoCall
          }
        >
          <Text style={styles.inprogress_call}>Call in progress</Text>

        </Display>

        <StatusBar
          barStyle={
            Platform.OS === "ios" ? COLOR_SCHEME.DARK : COLOR_SCHEME.LIGHT
          }
          backgroundColor={COLOR.PRIMARY_DARK}
        />
        <View style={styles.useragent}>
          <Display
            isVisible={
              this.state.remoteVideoStreamId &&
              this.state.remoteVideoStreamId.length > 1
            }
          >
            <View style={styles.containerCam}>
              {this.state.remoteVideoStreamId.map((prop, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.itemCam}
                    onPress={() => { }}
                  >
                    <Voximplant.VideoView
                      style={styles.memberview}
                      videoStreamId={prop}
                      scaleType={Voximplant.RenderScaleType.SCALE_FIT}
                      showOnTop={true}
                    />
                  </TouchableOpacity>
                );
              })}
            </View>
          </Display>

          <Display
            isVisible={
              this.state.remoteVideoStreamId.length === 0 ||
              this.state.remoteVideoStreamId.length == 1
            }
          >
            <View style={styles.videoPanel}>
              {this.state.remoteVideoStreamId.length == 1 &&
                this.state.remoteVideoStreamId[0] ? (
                  <Voximplant.VideoView
                    style={styles.remotevideo}
                    videoStreamId={this.state.remoteVideoStreamId[0]}
                    scaleType={Voximplant.RenderScaleType.SCALE_FIT}
                  />
                ) : null}

              {this.state.isVideoSent ? ( //my cam
                <Voximplant.VideoView
                  style={styles.selfview}
                  videoStreamId={this.state.localVideoStreamId}
                  scaleType={Voximplant.RenderScaleType.SCALE_FIT}
                  showOnTop={true}
                />
              ) : null}
            </View>
          </Display>

          <Display
            isVisible={
              this.state.remoteVideoStreamId &&
              this.state.remoteVideoStreamId.length == 2
            }
          >
            <View style={styles.containerCam2}>
              {this.state.isVideoSent ? ( //my cam
                <Voximplant.VideoView
                  style={styles.selfview2}
                  videoStreamId={this.state.localVideoStreamId}
                  scaleType={Voximplant.RenderScaleType.SCALE_FIT}
                  showOnTop={true}
                />
              ) : null}
            </View>
          </Display>



          {/*                     
                    <View style={styles.videoPanel}>
                        <Voximplant.VideoView style={styles.remotevideo} videoStreamId={this.state.remoteVideoStreamId}
                                              scaleType={Voximplant.RenderScaleType.SCALE_FIT} />
                        
                        {this.state.isVideoSent ? ( //my cam
                            <Voximplant.VideoView style={styles.selfview} videoStreamId={this.state.localVideoStreamId}
                                                  scaleType={Voximplant.RenderScaleType.SCALE_FIT} showOnTop={true}/>
                        ) : null}
                    </View> */}

          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Text style={styles.call_connecting_label}>
              {this.state.callState}
            </Text>
          </View>

          {this.state.isKeypadVisible ? (
            <Keypad keyPressed={e => this._keypadPressed(e)} />
          ) : null}

          <View style={styles.call_controls}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                backgroundColor: "transparent"
              }}
            >
              {this.state.isAudioMuted ? (
                <CallButton
                  icon_name="mic"
                  color={COLOR.ACCENT}
                  buttonPressed={() => this.muteAudio()}
                />
              ) : (
                  <CallButton
                    icon_name="mic-off"
                    color={COLOR.ACCENT}
                    buttonPressed={() => this.muteAudio()}
                  />
                )}
              {/* <CallButton
                icon_name="dialpad"
                color={COLOR.ACCENT}
                buttonPressed={() => this.switchKeypad()}
              /> */}
              <CallButton
                icon_name={this.state.audioDeviceIcon}
                color={COLOR.ACCENT}
                buttonPressed={() => this.switchAudioDevice()}
              />

              <Display
                isVisible={
                  this.isVideoCall
                }
              >

                {this.state.isVideoSent ? (
                  <CallButton
                    icon_name="videocam-off"
                    color={COLOR.ACCENT}
                    buttonPressed={() => this.sendVideo(false)}
                  />
                ) : (
                    <CallButton
                      icon_name="video-call"
                      color={COLOR.ACCENT}
                      buttonPressed={() => this.sendVideo(true)}
                    />
                  )}

              </Display>

              <CallButton
                icon_name="call-end"
                color={COLOR.RED}
                buttonPressed={() => this.endCall()}
              />
            </View>
          </View>

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.audioDeviceSelectionVisible}
            onRequestClose={() => { }}
          >
            <TouchableHighlight
              onPress={() => {
                this.setState({ audioDeviceSelectionVisible: false });
              }}
              style={styles.container}
            >
              <View style={[styles.container, styles.modalBackground]}>
                <View
                  style={[
                    styles.innerContainer,
                    styles.innerContainerTransparent
                  ]}
                >
                  <FlatList
                    data={this.state.audioDevices}
                    keyExtractor={(item, index) => item}
                    ItemSeparatorComponent={this.flatListItemSeparator}
                    renderItem={({ item }) => (
                      <Text
                        onPress={() => {
                          this.selectAudioDevice(item);
                        }}
                      >
                        {" "}
                        {item}{" "}
                      </Text>
                    )}
                  />
                </View>
              </View>
            </TouchableHighlight>
          </Modal>

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isModalOpen}
            onRequestClose={() => { }}
          >
            <TouchableHighlight
              onPress={e => this._closeModal()}
              style={styles.container}
            >
              <View style={[styles.container, styles.modalBackground]}>
                <View
                  style={[
                    styles.innerContainer,
                    styles.innerContainerTransparent
                  ]}
                >
                  <Text>{this.state.modalText}</Text>
                </View>
              </View>
            </TouchableHighlight>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  seletedFriends: state.chat.seletedFriends,
  isVideo: state.chat.isVideo
});

export default connect(
  mapStateToProps,
  { endRejectCall, addConference }
)(CallScreen);
