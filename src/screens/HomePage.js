import { Button, Container, Icon, Left, List, ListItem, Right, Text } from "native-base";
import React, { Component } from "react";
import { Dimensions, Image, Platform, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import firebase from "react-native-firebase";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import IconFontAwesome from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
import SendBird from "sendbird";
import LevelModal from "../components/LevelModal";
import Namecards from "../components/namecards";
import { NavBar, NavButton } from "../components/NavComponent";
import { getDB_Friends, updFriendsRequestArr } from "../redux/modules/contacts";
// import { addNotification } from "../redux/modules/notify";
import { setCardsShareFlag, updScanWebFlag } from "../redux/modules/setting";

// import type { Notification } from 'react-native-firebase';
const assetPath = "../assets";
const WINDOW_WIDTH = Dimensions.get("window").width;

class HomePage extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Home",
    headerLeft: null,
    headerRight: (
      <NavBar>
        <NavButton
          onPress={() => navigation.navigate("CameraScreen")}
          type="AntDesign"
          name="camerao"
          iconName="camerao"
          style={{
            flex: 1,
            resizeMode: "contain"
            // overlayColor : "red"
          }}
        />
        <NavButton
          onPress={() => navigation.navigate("AiChatBotScreen")}
          type="materialCommunityIcons"
          name="comment-question-outline"
          style={{
            flex: 1,
            marginTop: 5,
            resizeMode: "contain"
            // overlayColor : "red"
          }}
        />
      </NavBar>
    )
  });

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      modal: false,
      data: [],
      cards: [],
      page: 1,
      seed: 1,
      error: null,
      channels: [],
      firstLoadChat: true,
      firstLoad: false
    };

    this.arrayholder = [];
  }

  async componentDidMount() {
    this.props.navigation.setParams({ actQRscanner: this.qrScanAction });
  }

  getFriendsRequest = async () => {
    if (this.props.user) {
      const pendingFriendsArr = [];

      const docRef = firebase
        .firestore()
        .collection("friends")
        .doc(this.props.user.userID);

      await docRef.onSnapshot(docSnapshot => {
        if (docSnapshot.data() !== undefined) {
          const keys = Object.values(docSnapshot.data());

          for (const key of keys) {
            if (key.status == "pending") {
              pendingFriendsArr.push(docSnapshot.data()[key.userID]);
            }
          }
        }
      });

      //get company request
      await firebase
        .firestore()
        .collection("users")
        .doc(this.props.user.userID)
        .collection("companies")
        .where("status", "==", false)
        .onSnapshot(querySnapshot => {
          querySnapshot.forEach(doc => {
            console.log("ssssssss");

            const tmp = doc.data();
            pendingFriendsArr.push({
              // userID: doc.comid,
              ...tmp
            });
          });
        });

      this.props.updFriendsRequestArr({
        items: pendingFriendsArr
      });
    }
  };

  async componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      selectMode: this.state.selectMode,
      showArchive: this.state.showArchive,
      leavePrompt: this.leavePrompt,
      clearSelection: this.clearSelection,
      toggleArchive: this.toggleArchive,
      search: this.state.search,
      toggleSearch: this.toggleSearch
    });

    this.props.getDB_Friends();

    // let sb = SendBird.getInstance();
    // console.log("Sendbird Instance1111", sb);
    // if (!sb || (sb && sb.connecting === false)) {
    //   console.log("sign in sendbird now1111");
    //   await this.props.sendbirdLogin(this.props.user);
    //   sb = SendBird.getInstance();
    // }

    // await this.props.loadChannels();

    // var ChannelHandler = new sb.ChannelHandler();
    // // for sendbird notification in foreground and exactly in chat tab
    // ChannelHandler.onMessageReceived = (channel, message) => {
    //   this.props.loadChannels();
    //   this.props.addNotification(-99);

    //   if (this.props.channel && this.props.channel.url === channel.url) {
    //     return;
    //   }
    // };

    // sb.addChannelHandler(`CHANNEL_LISTENER`, ChannelHandler);
  }

  componentWillUnmount() {
    const sb = SendBird.getInstance();
    if (sb !== null) {
      sb.removeChannelHandler(`CHANNEL_LISTENER`);
    }
  }

  qrScanAction = () => {
    this.props.updScanWebFlag(false);
    this.props.navigation.navigate("QRScan");
  };

  actionPressModal = val => {
    this.props.setCardsShareFlag(false);

    if (val == "scannerweb") {
      this.props.updScanWebFlag(true);
      this.props.navigation.navigate("QRScan");
    } else if (val == "qrcode") {
      this.props.navigation.navigate("ShowQRCode");
    } else if (val == "scannerapp") {
      this.props.navigation.navigate("QRScan");
    } else {
      this.props.updScanWebFlag(false);
    }
  };

  render() {
    const user = this.props.user;
    const cardShareFlag = this.props.cardShareFlag;

    const nameCards = [
      {
        user: user,
        url2:
          "https://firebasestorage.googleapis.com/v0/b/etetsg-e7e52.appspot.com/o/newcard2.jpg?alt=media&token=a3f866c3-c894-46ff-add8-36f0133e8f4c",
        text: "CARD SHARE HERE",
        subText: "Card 1",
        whichCard: "112358"
      }
    ];

    const recentEvents = [
      {
        id: 1,
        title: "Clearing Sell",
        eventcom: "H&M",
        eveDate: "1 December 2019",
        eveTime: "9am",
        imgName: require(`../assets/shopping160.jpg`)
      },
      {
        id: 2,
        title: "Birthday",
        eventcom: "Outback Steakhouse",
        eveDate: "17 February 2020",
        eveTime: "9pm",
        imgName: require(`../assets/birthday160.jpg`)
      },
      {
        id: 3,
        title: "Wine Tasting",
        eventcom: "Winestone",
        eveDate: "12 January 2020",
        eveTime: "8pm",
        imgName: require(`../assets/winetasting160.jpg`)
      },
      {
        id: 4,
        title: "Party 2020",
        eventcom: "Zouk Club",
        eveDate: "12 January 2020",
        eveTime: "9pm",
        imgName: require(`../assets/concert160.jpg`)
      },
      {
        id: 5,
        title: "Wedding Fair",
        eventcom: "Love99",
        eveDate: "12 January 2020",
        eveTime: "8am",
        imgName: require(`../assets/wedding160.jpg`)
      },
      {
        id: 6,
        title: "Dinner Year End 2019",
        eventcom: "Burlamacco",
        eveDate: "31 December 2019",
        eveTime: "8:30pm",
        imgName: require(`../assets/dinner160.jpg`)
      },
      {
        id: 7,
        title: "Interview Session",
        eventcom: "Etet Pte Ltd",
        eveDate: "12 January 2020",
        eveTime: "10am",
        imgName: require(`../assets/interview160.jpg`)
      },
      {
        id: 8,
        title: "Photoshoot",
        eventcom: "I-Cam",
        eveDate: "12 January 2020",
        eveTime: "20:00pm",
        imgName: require(`../assets/photoshoot160.jpg`)
      },
      {
        id: 9,
        title: "Workshop",
        eventcom: "Work Hard",
        eveDate: "12 January 2020",
        eveTime: "20:00pm",
        imgName: require(`../assets/workshop160.jpg`)
      },
      {
        id: 10,
        title: "Meeting",
        eventcom: "The Imperio",
        eveDate: "12 January 2020",
        eveTime: "8pm",
        imgName: require(`../assets/meeting160.jpg`)
      }
    ];

    //count unread msg badge
    // let allUnreadMsg = 0;
    // let chnalll = _.filter(this.props.channels, channel => {
    //   if (channel.members.length === 1) return null;

    //   if (channel.name === "oto") {
    //     allUnreadMsg += channel.unreadMessageCount;
    //   }
    // });

    // if (this.state.firstLoadChat) {
    //   this.props.addNotification(allUnreadMsg);
    //   this.setState({ firstLoadChat: false });
    // }

    return (
      <Container>
        <Text style={styles.myCards}>My Card</Text>
        <Namecards nameCards={nameCards} />

        <Text style={styles.recentChatsText}>Recent Events</Text>
        <ScrollView contentContainerStyle={styles.contentContainer}>
          {recentEvents.map((item, index) => (
            <View key={index}>
              <TouchableOpacity
                // onLongPress={() =>
                //   alert(JSON.parse(JSON.stringify(props.text)))
                // }
                activeOpacity={1}
                style={[styles.eventscard]}
              // onPress={() => this["card" + props.index].flip()}
              >
                <View
                  style={{
                    flex: 1,
                    // top: 60,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "grey",
                    borderRadius: 10
                  }}
                >
                  <Image
                    source={item.imgName}
                    style={{
                      resizeMode: "cover",
                      width: "100%",
                      height: "100%",
                      borderRadius: 10
                    }}
                  />
                </View>
              </TouchableOpacity>
              <View style={[styles.eventDetails]}>
                <Text style={styles.myeveTitle}>{item.title}</Text>
                <Text style={styles.myeveDate}>{item.eventcom}</Text>
                <Text style={styles.myeveDate}>{item.eveDate}</Text>
                <Text style={styles.myeveDate}>{item.eveTime}</Text>
              </View>
            </View>
          ))}
        </ScrollView>

        <LevelModal
          visible={cardShareFlag}
          onRequestClose={() => {
            this.actionPressModal("close");
          }}
        >
          <IconAntDesign
            style={{
              position: "absolute",
              right: 10,
              top: 10,
              zIndex: 99999
            }}
            name="closecircle"
            onPress={() => {
              this.actionPressModal("close");
            }}
            size={20}
          />

          <List>
            <ListItem
              style={{ borderBottomColor: "#747576" }}
              onPress={() => {
                this.actionPressModal("qrcode");
              }}
            >
              <Left>
                <Button
                  style={{
                    backgroundColor: "#DCDCDC",
                    width: 30,
                    height: 30,
                    justifyContent: "center"
                  }}
                >
                  <IconFontAwesome
                    color="#000"
                    name="qrcode"
                    style={styles.iconStyle}
                  />
                </Button>
                <Text style={{ paddingLeft: 20 }}>Show My QR</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem
              IconFontAwesome
              style={{ borderBottomColor: "#747576" }}
              onPress={() => {
                this.actionPressModal("scannerapp");
              }}
            >
              <Left>
                <Button
                  style={{
                    backgroundColor: "#00FFFF",
                    width: 30,
                    height: 30,
                    justifyContent: "center"
                  }}
                >
                  <Ionicons style={styles.iconStyle} name="ios-qr-scanner" />
                </Button>
                <Text style={{ paddingLeft: 20 }}>App QR Scanner</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem
              IconFontAwesome
              onPress={() => {
                this.actionPressModal("scannerweb");
              }}
            >
              <Left>
                <Button
                  style={{
                    backgroundColor: "#007AFF",
                    width: 30,
                    height: 30,
                    justifyContent: "center"
                  }}
                >
                  <IconAntDesign
                    color="#fff"
                    name="scan1"
                    style={styles.iconStyle2}
                  />
                </Button>
                <Text style={{ paddingLeft: 20 }}>Web QR Scanner</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </LevelModal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  cardShareFlag: state.setting.cardShareFlag
});

export default connect(mapStateToProps, {
  setCardsShareFlag,
  updScanWebFlag,
  updFriendsRequestArr,
  getDB_Friends
  // addNotification
})(HomePage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  content: {
    flexDirection: "row"
  },
  myCards: {
    paddingLeft: 16,
    fontSize: 17,
    fontWeight: "bold"
  },
  recentChatsText: {
    paddingBottom: 15,
    paddingLeft: 16,
    fontSize: 17,
    fontWeight: "bold"
  },
  recentChats: {
    marginTop: 10,
    paddingLeft: 20,
    fontSize: 17
  },
  iconStyle: {
    fontSize: Platform.OS === "ios" ? 21 : 25
  },
  iconStyle2: {
    fontSize: Platform.OS === "ios" ? 20 : 25
  },
  eventscard: {
    paddingVertical: 12,
    width: WINDOW_WIDTH * 0.5,
    maxWidth: 200,
    maxHeight: 200,
    height: 150,
    borderRadius: 20,
    paddingLeft: 10,
    paddingRight: 10
  },
  contentContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "space-between"
  },
  eventDetails: {
    width: 150,
    maxWidth: 200,
    paddingBottom: 10
  },
  myeveTitle: {
    fontSize: 13,
    fontWeight: "bold",
    paddingLeft: 10
  },
  myeveDate: {
    fontSize: 11,
    paddingLeft: 10
  }
});
