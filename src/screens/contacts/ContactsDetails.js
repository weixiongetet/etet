import { Button, Text } from "native-base";
import React, { Component } from "react";
import { ActivityIndicator, Dimensions, Image, Platform, ScrollView, View } from "react-native";
import firebase from "react-native-firebase";
import SendSMS from "react-native-sms";
import IconAntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import { connect } from "react-redux";
import SendBird from "sendbird";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import Namecards from "../../components/namecards";
import { createIndieChannel, sendbirdLogin, updFromContactFlag } from "../../redux/modules/chat";
import { addFriends, editContactName, editContactPhone, uploadContactPrfImg } from "../../redux/modules/contacts";

const platform = Platform.OS;
const { height } = Dimensions.get("window");

const stopWatchoptions = {
  // container: {
  //   // backgroundColor: '#FF0000',
  //   padding: 5,
  //   borderRadius: 5,
  //   width: 200,
  //   alignItems: 'center',
  // },
  text: {
    fontSize: 25,
    color: "#000",
    marginLeft: 7
  }
};

const DetailText = ({ label, value, isTitle }) => {
  return (
    <View
      style={{
        flexDirection: "row",
        flex: 1,
        alignSelf: "center",
        marginBottom: 5
      }}
    >
      <Text
        style={{
          fontFamily: "Roboto",
          fontWeight: "bold",
          fontSize: isTitle ? 20 : 14
          // textAlign: "left",
        }}
      >{`${value}`}</Text>
    </View>
  );
};

class ContactsDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      nameModal: false,
      phoneModal: false,
      tokenStr: "",
      a2p: false,
      a2a: false,
      callingUser: null,
      volume: 0,

      isTimerStart: false,
      isStopwatchStart: true,
      timerDuration: 90000,
      resetTimer: false,
      resetStopwatch: false,
      screenHeight: 0
    };

    this.startStopTimer = this.startStopTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.startStopStopWatch = this.startStopStopWatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

  async componentDidMount() {
    let sb = SendBird.getInstance();
    if (!sb || (sb && sb.connecting === false)) {
      await this.props.sendbirdLogin(this.props.user);
      sb = SendBird.getInstance();
    }

    await this.props.updFromContactFlag(true);

    const docRef = firebase
      .firestore()
      .collection("users")
      .doc(this.props.user.userID);

    docRef.onSnapshot(doc => {
      const { firstName, lastName } = doc.data();
      this.setState({ firstName, lastName });
    });
  }

  /* call back for  device Did Start Listening*/
  deviceDidStartListening() {
    console.log("deviceDidStartListening 11111");
  }

  inviteSMS = val => {
    // const recipientPhone = val.replace(/\s/g, '');

    SendSMS.send(
      {
        //Message body
        body: "Please download from etet.app.",
        //Recipients Number
        recipients: [val],
        //An array of types that would trigger a "completed" response when using android
        successTypes: ["sent", "queued"]
      },
      (completed, cancelled, error) => {
        if (completed) {
          console.log("SMS Sent Completed");
        } else if (cancelled) {
          console.log("SMS Sent Cancelled");
        } else if (error) {
          console.log("Some error occured");
        }
      }
    );
  };

  createChannelOneToOne = async user => {
    await this.props.createIndieChannel({
      userA: this.props.me,
      userB: {
        userId: user.userID,
        nickname: user.firstName,
        profileUrl: user.profilePictureURL
      }
    });
  };

  makeCall = val => { };

  disconnect = () => { };
  acceptCall = () => { };

  // reject an incoming call (Android only, in iOS CallKit provides the UI for this)
  rejectCall = () => { };

  // ignore an incoming call (Android only)
  ignoreCall = () => { };

  startStopTimer() {
    this.setState({
      isTimerStart: !this.state.isTimerStart,
      resetTimer: false
    });
  }
  resetTimer() {
    this.setState({ isTimerStart: false, resetTimer: true });
  }
  startStopStopWatch() {
    this.setState({
      isStopwatchStart: !this.state.isStopwatchStart,
      resetStopwatch: false
    });
  }
  resetStopwatch() {
    this.setState({ isStopwatchStart: false, resetStopwatch: true });
  }
  getFormattedTime(time) {
    this.currentTime = time;
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    // Save the content height in state
    this.setState({ screenHeight: contentHeight });
  };

  render() {
    let name = this.props.selectedContacts.name;
    // const phone = this.props.selectedContacts.phone;
    const phone = this.props.selectedContacts.phone
      ? this.props.selectedContacts.phone
      : this.props.selectedUserDetails.phone;
    let email = this.props.selectedContacts.email
      ? this.props.selectedContacts.email
      : "";
    const selectedUserStatusFlag = this.props.selectedUserStatusFlag;
    const selectedUserDetails = this.props.selectedUserDetails;
    const user = this.props.user;
    let carduser = {};
    const callingFlag = this.props.callingFlag;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    console.log("selectedUserDetails1111", selectedUserDetails);

    if (Object.keys(selectedUserDetails).length) {
      carduser = selectedUserDetails;
      name = selectedUserDetails.firstName;
      email = selectedUserDetails.email;
    } else {
      carduser = {
        firstName: name,
        phone: phone,
        profilePictureURL:
          "https://png.pngtree.com/svg/20170920/ico_avatar_1189275.png"
      };
    }

    const nameCards = [
      {
        user: carduser,
        text: "CARD SHARE HERE",
        whichCard: "112358",
        isFriends: true
      }
    ];

    // if (this.state.loading) {
    //   return null; // or render a loading icon
    // }
    const { profilePictureURL } = selectedUserDetails;
    const scrollEnabled = this.state.screenHeight > height;

    return (
      <View>
        {selectedUserStatusFlag !== "" ? (
          <ScrollView>
            <View>
              <View style={{ alignSelf: "center" }}>
                {/* <TouchableOpacity onPress={this.selectPhotoTapped}> */}
                <View>
                  <Image
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      width: 120,
                      height: 120,
                      borderRadius: 60
                    }}
                    source={{
                      uri: profilePictureURL
                        ? profilePictureURL
                        : "https://png.pngtree.com/svg/20170920/ico_avatar_1189275.png"
                    }}
                  />
                </View>
                {/* </TouchableOpacity> */}
              </View>

              {name ? (
                <DetailText isTitle value={`${name}`} />
              ) : (
                  <DetailText isTitle value={"No Name"} />
                )}

              {phone ? (
                <DetailText value={phone} />
              ) : (
                  <DetailText value={"No Phone"} />
                )}

              {email ? (
                <DetailText value={email} />
              ) : (
                  <DetailText value={"No Email"} />
                )}

              {selectedUserStatusFlag == "not" ? ( //not etet user
                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "center",
                    paddingTop: 30
                  }}
                >
                  <Button
                    info
                    style={{
                      alignSelf: "center",
                      justifyContent: "center",
                      padding: 13
                    }}
                    onPress={() => {
                      this.inviteSMS(phone);
                    }}
                  >
                    <SimpleLineIcons
                      name="envelope-letter"
                      size={20}
                      style={{ color: "#FFF" }}
                    />
                    <Text>Invite</Text>
                  </Button>

                  {phone ? (
                    <Button
                      disabled={false}
                      style={{
                        marginLeft: 10,
                        padding: 13,
                        alignSelf: "center"
                      }}
                      onPress={() => { }}
                    >
                      <IconAntDesign
                        name="phone"
                        size={20}
                        style={{ color: "#FFF" }}
                      />
                      <Text>Call</Text>
                    </Button>
                  ) : null}
                </View>
              ) : null}

              {selectedUserStatusFlag !== "not" &&
                selectedUserStatusFlag !== true &&
                selectedUserStatusFlag !== "pending" ? ( //etet user
                  <Button
                    info
                    style={{
                      width: 180,
                      alignSelf: "center",
                      justifyContent: "center"
                    }}
                    onPress={() => {
                      this.props.addFriends({
                        items: selectedUserDetails,
                        user: user,
                        status: "requested"
                      });
                    }}
                  >
                    <Text>Add</Text>
                  </Button>
                ) : null}

              {selectedUserStatusFlag == "pending" ? ( //request sended
                <Button
                  disabled
                  style={{
                    width: 180,
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text>Added</Text>
                </Button>
              ) : null}

              <View
                style={{
                  flexDirection: "row",
                  alignSelf: "center",
                  paddingTop: 30
                }}
              >
                {selectedUserStatusFlag == true ? ( //is friends
                  <Button
                    info
                    style={{ marginRight: 10, padding: 12 }}
                    onPress={async () => {
                      await this.createChannelOneToOne(selectedUserDetails);

                      this.props.navigation.navigate("Message", {
                        userID: selectedUserDetails.userID,
                        name: selectedUserDetails.firstName,
                        profileUrl: selectedUserDetails.profilePictureURL
                      });
                    }}
                  >
                    <MaterialIcons
                      name="chat"
                      size={23}
                      style={{ color: "#FFF" }}
                    />
                    <Text>Chat</Text>
                  </Button>
                ) : null}

                {selectedUserStatusFlag == true && phone ? ( //is friends, call app to app
                  <Button
                    disabled={false}
                    style={{ marginLeft: 10, padding: 13, alignSelf: "center" }}
                    onPress={() => { }}
                  >
                    <IconAntDesign
                      name="phone"
                      size={20}
                      style={{ color: "#FFF" }}
                    />
                    <Text>Call</Text>
                  </Button>
                ) : null}
              </View>

              {selectedUserStatusFlag == "not" ? null : ( //not etet user
                <View style={{ marginTop: 30 }}>
                  <Text
                    style={{
                      marginLeft: 57,
                      fontFamily:
                        platform === "ios" ? "OpenSans-Regular" : "opensans",
                      fontSize: 18
                    }}
                  >{`${name}'s Card`}</Text>
                  <Namecards nameCards={nameCards} />
                </View>
              )}

              <LevelModal
                onBackdropPress={() => console.log("Pressed")}
                visible={callingFlag}
              >
                <Text>Call to {this.state.callingUser} ...</Text>
                <View style={{ flexDirection: "row", alignSelf: "center" }}>
                  <Button
                    info
                    style={{
                      backgroundColor: "#FF0000",
                      padding: 13,
                      alignSelf: "center",
                      height: 50,
                      width: 50,
                      borderRadius: 25
                    }}
                    onPress={() => {
                      this.disconnect();
                    }}
                  >
                    <MaterialIcons
                      name="call-end"
                      size={25}
                      style={{ color: "#FFF" }}
                    />
                  </Button>
                </View>
              </LevelModal>

              <Display isVisible={incomingFlag}>
                <LevelModal
                  onBackdropPress={() => console.log("Pressed")}
                  visible={incomingFlag}
                >
                  <CallKit type={"incoming"} />
                </LevelModal>
              </Display>

              <Display isVisible={showTimerFlag}>
                <LevelModal
                  onBackdropPress={() => console.log("Pressed")}
                  visible={showTimerFlag}
                >
                  <CallKit type={"timer"} />
                </LevelModal>
              </Display>
            </View>
          </ScrollView>
        ) : (
            <ActivityIndicator size="large" color="#808080" />
          )}
      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    me: state.chat.user,
    selectedContacts: state.contacts.selectedContacts,
    selectedUserDetails: state.contacts.selectedUserDetails,
    selectedUserStatusFlag: state.contacts.selectedUserStatusFlag,
    callingFlag: state.contacts.callingFlag,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag
  }),
  {
    editContactName,
    uploadContactPrfImg,
    editContactPhone,
    addFriends,
    createIndieChannel,
    updFromContactFlag,
    sendbirdLogin
  }
)(ContactsDetails);
