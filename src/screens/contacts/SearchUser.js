import { Button, Text } from "native-base";
import React, { Component } from "react";
import { Platform, StyleSheet, TextInput, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import { searchUser } from "../../redux/modules/contacts";

const platform = Platform.OS;

class SearchUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: null,
      inputCheck: false,
      searchType: null,
      errorMsg: null,
      getText: null
    };
  }


  async componentDidMount() {

  }

  validate = (text) => {
    this.setState({ errorMsg: null })
    this.setState({ getText: text })

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (reg.test(text) === false) {
      this.setState({ searchText: null })

      const checkNum = text.replace("+", '');
      if (isNaN(checkNum)) {
        this.setState({ inputCheck: true })
      } else {
        const phoneNum = `+${checkNum}`;
        if (text !== '') {
          this.setState({ searchType: 'phone' })
          this.setState({ searchText: phoneNum })
        }
      }

      return false;
    } else {
      this.setState({ searchType: 'email' })
      this.setState({ searchText: text })
    }

  }


  userPage = () => {
    const selectedUserDetails = this.props.selectedUserDetails;

    if (Object.keys(selectedUserDetails).length) {
      this.props.navigation.navigate("ContactsDetails");
    } else {
      this.setState({ errorMsg: 'No Contact Found!' })
    }
  }


  render() {
    if (!this.props.user) return null;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    return (
      <View style={styles.container}>
        <View style={styles.SectionStyle}>

          <TextInput
            ref={input => { this.textInput = input }}
            autoFocus={true}
            style={styles.TextInputSearch}
            placeholder="Search Email/Phone"
            underlineColorAndroid="transparent"
            onChangeText={text => this.validate(text)}
          />

          {this.state.getText ? (
            <MaterialIcons
              onPress={async () => {
                this.textInput.clear();
                this.setState({ searchText: null })
                this.setState({ getText: null })
                this.setState({ errorMsg: null })
              }}
              name="cancel" size={23} style={{ color: 'red', marginRight: 10 }}
            />
          ) : (
              <MaterialIcons name="search" size={23} style={{ color: '#C0C0C0', marginRight: 10 }} />
            )}

        </View>

        {this.state.searchText ? (
          <Button style={{ backgroundColor: '#3370FF', marginLeft: 10, padding: 13, alignSelf: 'center' }}
            onPress={async () => {
              await this.props.searchUser({ items: this.state.searchText, searchType: this.state.searchType })
              this.userPage();
            }} >
            <Text style={styles.textFamily}>Search</Text>

          </Button>
        ) : (
            null
          )}

        {this.state.errorMsg ? (
          <View
            style={{ flexDirection: "row", alignSelf: "center", marginBottom: 5 }}
          >
            <Text
              style={styles.errorMsgStyle}
            >{this.state.errorMsg}</Text>
          </View>
        ) : (
            null
          )}

        <Display isVisible={incomingFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={incomingFlag} >
            <CallKit type={'incoming'} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={showTimerFlag} >
            <CallKit type={'timer'} />
          </LevelModal>
        </Display>


      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    selectedUserDetails: state.contacts.selectedUserDetails,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag,
  }),
  { searchUser }
)(SearchUser);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  textFamily: {
    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans",
  },
  TextInputSearch: {
    color: "#778899",
    flex: 1,
    textAlign: "center",
    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans",
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E8EBEC',
    // borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 20,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20
  },
  errorMsgStyle: {
    fontWeight: "bold",
    color: "red",
    fontSize: 14,
    marginTop: 10,
    textAlign: "center",
    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans",
  }
});
