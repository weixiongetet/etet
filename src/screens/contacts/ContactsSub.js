import _ from "lodash";
import { Container, Fab, Icon } from "native-base";
import React, { Component } from "react";
import {
  ActivityIndicator,
  Image,
  PermissionsAndroid,
  Platform,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from "react-native";
import AlphabetListView from "react-native-alphabetlistview";
import Contacts from "react-native-contacts";
import firebase from "react-native-firebase";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import { makePy } from "../../components/getFirstAlphabet";
import IconWithBadge from "../../components/IconWithBadge";
import LevelModal from "../../components/LevelModal";
import {
  getCardContacts,
  getCompanyRequest,
  getDB_Friends,
  setContacts,
  setSelectedContacts,
  updateAllContacts,
  updCompanyRequestArr,
  updDBacceptedCompany,
  updDBfriendsListArr,
  updFriendsRequestArr,
  updSelectedUserFlag
} from "../../redux/modules/contacts";

const assetPath = "../../assets/";
const IconComponent = IconWithBadge;

const DetailText = ({ label, value, isTitle }) => {
  return (
    <View
      style={{ flexDirection: "row", alignSelf: "center", marginBottom: 5 }}
    >
      <Text
        style={{
          fontFamily: "Roboto",
          fontWeight: "bold",
          fontSize: isTitle ? 20 : 14
        }}
      >{`${value}`}</Text>
    </View>
  );
};
class SectionHeader extends Component {
  render() {
    var textStyle = {
      marginLeft: 5,
      color: "#000000",
      fontWeight: "bold",
      fontSize: 14
    };

    return (
      <View style={{ paddingLeft: 17, color: "#FFF" }}>
        <Text style={textStyle}>{this.props.title}</Text>
      </View>
    );
  }
}

class SectionItem extends Component {
  render() {
    return (
      <Text style={{ color: "#3370FF", paddingRight: 10 }}>
        {this.props.title}
      </Text>
    );
  }
}

class Cell extends Component {
  render() {
    return (
      <TouchableHighlight onPress={() => this.props.onSelect(this.props.item)}>
        <View
          style={{
            height: 30,
            paddingLeft: 20,
            paddingTop: 5,
            paddingBottom: 5
          }}
        >
          <Text style={{ fontSize: 15 }}>{this.props.item.name}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

let countRequest = 0;

class ContactsSub extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Contacts",
    // headerLeft: (
    //   <View>
    //     <Image
    //       source={require(`${assetPath}/logo.png`)}
    //       style={{
    //         flex: 1,
    //         width: 90,
    //         height: 30,
    //         marginLeft: 15,
    //         resizeMode: "contain"
    //       }}
    //     />
    //   </View>
    // ),
    headerRight: (
      <IconComponent
        name={"notifications-none"}
        size={30}
        badgeCount={navigation.getParam("allCount")}
        onpressFunc={() => {
          navigation.getParam("goFriendRequest")();
        }}
      />
    )
  });

  constructor(props) {
    super(props);

    this.state = {
      contacts: [],
      search: false,
      mergedContacts: [],
      allContacts: [],
      contArrHolder: {},
      renderContact: false,
      searchText: null,
      noResult: null,
      loading: true,
      allCount: 0
    };
    console.log("loading_contactScreen");
  }

  async componentDidMount() {
    console.log("ssssa");
    this.setState({ renderContact: false });
    let countComRequest = 0;

    const docRef = firebase
      .firestore()
      .collection("friends")
      .doc(this.props.user.userID);

    docRef.onSnapshot(docSnapshot => {
      if (docSnapshot.data() !== undefined) {
        const keys = Object.values(docSnapshot.data());
        const pendingFriendsArr = [];
        const acceptedDBfriend = [];

        for (const key of keys) {
          if (key.status == "pending") {
            pendingFriendsArr.push(docSnapshot.data()[key.userID]);
            if (this.props.selectedUserDetails.userID == key.userID) {
              this.props.updSelectedUserFlag("pending");
            }
          } else if (key.status == "requested") {
            if (this.props.selectedUserDetails.userID == key.userID) {
              this.props.updSelectedUserFlag("pending");
            }
          } else if (key.status == "accepted") {
            acceptedDBfriend.push(docSnapshot.data()[key.userID]);

            if (this.props.selectedUserDetails.userID == key.userID) {
              this.props.updSelectedUserFlag(true);
            }
          } else {
            if (this.props.selectedUserDetails.userID == key.userID) {
              this.props.updSelectedUserFlag(false);
            }
          }
        }
        // this.props.updSelectedUserFlag(updSelectedUserStatusFlag);

        this.props.updFriendsRequestArr({
          items: pendingFriendsArr
        });

        this.props.updDBfriendsListArr({
          items: acceptedDBfriend
        });

        if (Platform.OS === "android") {
          this.contactsPermission();
        } else {
          this.loadContacts();
        }
      }

      // const countFriendsRequest =
      //   this.props.friendsRequestArr.length !== 0
      //     ? this.props.friendsRequestArr.length
      //     : "";
      // this.props.navigation.setParams({
      //   countFriendsRequest: { countFriendsRequest }
      // });

      this.setComRequest();
    });

    //get accepted company request
    firebase
      .firestore()
      .collection("users")
      .doc(this.props.user.userID)
      .collection("companies")
      .where("status", "==", true)
      .onSnapshot(async querySnapshot => {
        const acceptedDBComfriends = [];
        console.log("acceptedCompany", querySnapshot.docs.length);

        querySnapshot.forEach(function(doc) {
          const tmp = doc.data();

          acceptedDBComfriends.push({
            id: tmp.comid,
            firstName: tmp.companyName,
            email: tmp.companyEmail,
            ...tmp
          });
        });

        this.props.updDBacceptedCompany({
          items: acceptedDBComfriends
        });

        if (Platform.OS === "android") {
          this.contactsPermission();
        } else {
          this.loadContacts();
        }

        this.setComRequest();
      });

    //get pending company request
    firebase
      .firestore()
      .collection("users")
      .doc(this.props.user.userID)
      .collection("companies")
      .where("status", "==", false)
      .onSnapshot(async pendQuerySnapshot => {
        const pendingCompanyArr = [];
        console.log("pendingCompany", pendQuerySnapshot.docs.length);
        countComRequest = pendQuerySnapshot.docs.length;

        pendQuerySnapshot.forEach(function(doc) {
          const tmp = doc.data();

          pendingCompanyArr.push({
            ...tmp
          });
        });

        this.props.updCompanyRequestArr({
          items: pendingCompanyArr
        });
        console.log("friendsRequestArr", this.props.friendsRequestArr);

        if (Platform.OS === "android") {
          this.contactsPermission();
        } else {
          this.loadContacts();
        }

        this.setComRequest();
      });

    console.log("222222", this.props.companyRequestArr.length);
    this.setComRequest();
  }

  componentWillMount() {
    this.refreshContact();

    const { navigation } = this.props;
    navigation.setParams({
      goFriendRequest: this.goFriendRequest
    });
  }

  setComRequest = async () => {
    console.log("comreq", this.props.companyRequestArr.length);

    const { navigation } = this.props;
    await navigation.setParams({
      allCount:
        +this.props.friendsRequestArr.length +
        this.props.companyRequestArr.length
    });
  };

  // _requestCount = async () => {
  //     await this.setState({ allCount: + this.props.friendsRequestArr.length + this.props.companyRequestArr.length });
  // };

  goFriendRequest = () => {
    this.props.navigation.navigate("FriendsRequestList", {
      onGoBack: this.refreshContact
    });
  };

  refreshContact = async () => {
    console.log("refresh contact");
    await this.props.getCardContacts();
    await this.props.getDB_Friends();
    await this.props.getCompanyRequest();

    if (Platform.OS === "android") {
      this.contactsPermission();
    } else {
      this.loadContacts();
    }
  };

  contactsPermission() {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: "Contacts",
      message: "No contact added. Sync contacts to begin."
    })
      .then(granted => {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.loadContacts();
        } else {
          console.log("Denied");
        }
      })
      .catch(err => {
        console.log("PermissionsAndroid", err);
      });
  }

  loadContacts() {
    Contacts.getAll((err, contacts) => {
      if (err === "denied") {
        console.warn("Permission to access contacts was denied");
      } else {
        this.props.setContacts(contacts);
        const tempArr = this.mergeContactlist();
        // this.props.updateAllContacts({
        //   items: tempArr
        // });
      }
    });
  }

  mergeContactlist = () => {
    var a = 97;
    var contactsObj = {};
    for (var i = 0; i < 26; i++) {
      contactsObj[String.fromCharCode(a + i).toUpperCase()] = [];
    }

    let uniqueNamesContacts = this.props.contactsList;
    let isEmptyDBfriends = !Object.keys(this.props.dbFriendsList).length;
    let isEmptyDBCompany = !Object.keys(this.props.dbCompanyList).length;
    let isEmptyCardContacts = !Object.keys(this.props.cardContactsList).length;
    // console.log("isEmptyCardContacts", isEmptyCardContacts)

    // let isEmptyDBCompany = (this.props.dbCompanyList !== undefined && this.props.dbCompanyList.length > 0) ? !Object.keys(this.props.dbCompanyList).length : {};

    if (!isEmptyDBfriends) {
      //get db friends
      var dataContacts = uniqueNamesContacts.concat(this.props.dbFriendsList); //combine two array
      uniqueNamesContacts = _.uniqBy(dataContacts, "phone"); //unique the phone number

      this.props.dbFriendsList.forEach(function(item) {
        if (item.phone == null) {
          if (uniqueNamesContacts.indexOf(item.email) == -1) {
            uniqueNamesContacts.push(item);
          }
        }
      });
    }

    if (!isEmptyDBCompany) {
      //get company
      var dataAllContacts = uniqueNamesContacts.concat(
        this.props.dbCompanyList
      ); //combine two array
      uniqueNamesContacts = _.uniqBy(dataAllContacts, "phone"); //unique the phone number

      this.props.dbCompanyList.forEach(function(item) {
        if (item.phone == null) {
          if (uniqueNamesContacts.indexOf(item.email) == -1) {
            uniqueNamesContacts.push(item);
          }
        }
      });
    }

    uniqueNamesContacts.map(function(item) {
      var tempName = item.firstName ? item.firstName : "";
      var tempLastName = item.lastName ? item.lastName : "";
      var tempName2 = item.name ? item.name : tempName + " " + tempLastName;
      var firstStr = makePy(tempName2.toUpperCase());
      var email = item.email ? item.email : null;

      if (contactsObj[firstStr]) {
        contactsObj[firstStr].push({
          name: tempName2,
          phone: item.phone,
          email: email
        });
      }
    });

    Object.keys(contactsObj).forEach(
      key => contactsObj[key].length == 0 && delete contactsObj[key]
    );

    this.setState({ contArrHolder: contactsObj });
    this.setState({ renderContact: true });

    this.props.updateAllContacts({
      items: contactsObj
    });

    return contactsObj;
  };

  SearchFilterFunction(text) {
    this.setState({ searchText: text });

    const contactMerged = Object.values(this.state.contArrHolder);
    var matchData = [];
    var matchContacts = [];

    for (const key of contactMerged) {
      const val = Object.values(key);

      for (const key2 of val) {
        const itemData = key2.name.toUpperCase();
        const textData = text.toUpperCase();

        if (textData !== "") {
          if (itemData.indexOf(textData) > -1) {
            matchData.push(key2);
          }
        }
      }
    }

    if (matchData.length !== 0) {
      matchContacts = matchData;
    } else {
      if (text !== "") {
        this.setState({ noResult: "No results found!" });
      } else {
        this.setState({ noResult: null });
        matchContacts = this.state.contArrHolder;
      }
    }

    this.props.updateAllContacts({
      items: matchContacts
    });
  }

  getContactDetails() {
    this.props.navigation.navigate("ContactsDetails");
  }

  phoneSelect(value) {
    this.props.getDB_Friends();
    this.props.setSelectedContacts({
      items: value
    });
    this.getContactDetails();
  }

  searchUser() {
    console.log("searchuser");
    this.props.navigation.navigate("SearchUser", {
      onGoBack: this.refreshContact
    });
  }

  mergePhoneContact = () => {
    var a = 97;
    var contactsObj = {};
    for (var i = 0; i < 26; i++) {
      contactsObj[String.fromCharCode(a + i).toUpperCase()] = [];
    }

    let uniqueNamesContacts = this.props.contactsList;
    let isEmptyCardContacts = !Object.keys(this.props.cardContactsList).length;

    uniqueNamesContacts.map(function(item) {
      var tempName = item.firstName ? item.firstName : "";
      var tempLastName = item.lastName ? item.lastName : "";
      var tempName2 = item.name ? item.name : tempName + " " + tempLastName;
      var firstStr = makePy(tempName2.toUpperCase());
      var email = item.email ? item.email : null;

      if (contactsObj[firstStr]) {
        contactsObj[firstStr].push({
          name: tempName2,
          phone: item.phone,
          email: email
        });
      }
    });

    Object.keys(contactsObj).forEach(
      key => contactsObj[key].length == 0 && delete contactsObj[key]
    );

    this.setState({ contArrHolder: contactsObj });
    this.setState({ renderContact: true });

    this.props.updateAllContacts({
      items: contactsObj
    });

    return contactsObj;
  };

  mergeDBfriendsContact = () => {
    var a = 97;
    var contactsObj = {};
    for (var i = 0; i < 26; i++) {
      contactsObj[String.fromCharCode(a + i).toUpperCase()] = [];
    }

    let uniqueNamesContacts = {};
    let isEmptyDBfriends = !Object.keys(this.props.dbFriendsList).length;
    let isEmptyDBCompany = !Object.keys(this.props.dbCompanyList).length;

    if (!isEmptyDBfriends) {
      //get db friends
      var dataContacts = uniqueNamesContacts.concat(this.props.dbFriendsList); //combine two array
      uniqueNamesContacts = _.uniqBy(dataContacts, "phone"); //unique the phone number

      this.props.dbFriendsList.forEach(function(item) {
        if (item.phone == null) {
          if (uniqueNamesContacts.indexOf(item.email) == -1) {
            uniqueNamesContacts.push(item);
          }
        }
      });
    }

    if (!isEmptyDBCompany) {
      //get company
      var dataAllContacts = uniqueNamesContacts.concat(
        this.props.dbCompanyList
      ); //combine two array
      uniqueNamesContacts = _.uniqBy(dataAllContacts, "phone"); //unique the phone number

      this.props.dbCompanyList.forEach(function(item) {
        if (item.phone == null) {
          if (uniqueNamesContacts.indexOf(item.email) == -1) {
            uniqueNamesContacts.push(item);
          }
        }
      });
    }

    uniqueNamesContacts.map(function(item) {
      var tempName = item.firstName ? item.firstName : "";
      var tempLastName = item.lastName ? item.lastName : "";
      var tempName2 = item.name ? item.name : tempName + " " + tempLastName;
      var firstStr = makePy(tempName2.toUpperCase());
      var email = item.email ? item.email : null;

      if (contactsObj[firstStr]) {
        contactsObj[firstStr].push({
          name: tempName2,
          phone: item.phone,
          email: email
        });
      }
    });

    Object.keys(contactsObj).forEach(
      key => contactsObj[key].length == 0 && delete contactsObj[key]
    );

    this.setState({ contArrHolder: contactsObj });
    this.setState({ renderContact: true });

    this.props.updateAllContacts({
      items: contactsObj
    });

    return contactsObj;
  };

  render() {
    if (!this.props.user) return null;

    let allContactList = this.props.allContactList;
    var isEmptyContact = !Object.keys(allContactList).length;
    const renderContact = this.state.renderContact;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    return (
      <Container>
        <View style={styles.container}>
          <View style={styles.SectionStyle}>
            <TextInput
              ref={input => {
                this.textInput = input;
              }}
              // autoFocus={true}
              style={styles.TextInputSearch}
              placeholder="Search"
              placeholderTextColor="#778899"
              underlineColorAndroid="transparent"
              onChangeText={text => this.SearchFilterFunction(text)}
            />

            {this.state.searchText ? (
              <MaterialIcons
                onPress={async () => {
                  this.textInput.clear();
                  this.setState({ searchText: null });
                  this.setState({ noResult: null });
                  this.props.updateAllContacts({
                    items: this.state.contArrHolder
                  });
                }}
                name="cancel"
                size={23}
                style={{ color: "red", marginRight: 10 }}
              />
            ) : (
              <MaterialIcons
                name="search"
                size={23}
                style={{ color: "#C0C0C0", marginRight: 10 }}
              />
            )}
          </View>

          {this.state.noResult ? (
            <Text style={{ color: "red", textAlign: "center" }}>
              {this.state.noResult}
            </Text>
          ) : null}

          <Display
            isVisible={
              _.isEmpty(allContactList) &&
              renderContact &&
              !this.state.searchText
            }
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Image
                source={require(`${assetPath}/contacts.png`)}
                style={{ width: 70, height: 80, resizeMode: "contain" }}
              />
              <DetailText value={"No Contacts Yet?"} />
              <DetailText
                value={"Start Syncing Your Phone Contacts From Settings."}
              />
            </View>
          </Display>

          {renderContact ? (
            <AlphabetListView
              data={allContactList}
              cell={Cell}
              cellHeight={30}
              sectionListItem={SectionItem}
              sectionHeader={SectionHeader}
              sectionHeaderHeight={22.5}
              onCellSelect={value => this.phoneSelect(value)}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.isFetching}
                  onRefresh={this.refreshContact}
                />
              }
            />
          ) : (
            <ActivityIndicator size="large" color="#808080" />
          )}

          <Fab
            containerStyle={{ marginRight: 12 }}
            position="bottomRight"
            style={styles.iconBack}
            onPress={() => this.searchUser()}
          >
            <Icon
              type="Ionicons"
              style={styles.addFriend}
              name="md-person-add"
            />
          </Fab>
        </View>

        <Display isVisible={incomingFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={incomingFlag}
          >
            <CallKit type={"incoming"} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal
            onBackdropPress={() => console.log("Pressed")}
            visible={showTimerFlag}
          >
            <CallKit type={"timer"} />
          </LevelModal>
        </Display>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.user,
    contactsList: state.contacts.contactsList,
    mergedContactsList: state.contacts.mergedContactsList,
    dbFriendsFlag: state.contacts.dbFriendsFlag,
    dbFriendsList: state.contacts.dbFriendsList,
    allContactList: state.contacts.allContactList,
    contactFlag: state.contacts.contactFlag,
    friendsRequestArr: state.contacts.friendsRequestArr,
    selectedUserDetails: state.contacts.selectedUserDetails,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag,
    isFetching: state.chat.isFetching,
    dbCompanyList: state.contacts.dbCompanyList,
    companyRequestArr: state.contacts.companyRequestArr,
    cardContactsList: state.contacts.cardContactsList
  }),
  {
    setContacts,
    setSelectedContacts,
    getDB_Friends,
    updateAllContacts,
    updFriendsRequestArr,
    updDBfriendsListArr,
    updSelectedUserFlag,
    getCompanyRequest,
    updDBacceptedCompany,
    updCompanyRequestArr,
    getCardContacts
  }
)(ContactsSub);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: "bold",
    backgroundColor: "rgba(247,247,247,1.0)"
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44
  },

  TextInputSearch: {
    color: "#000",
    flex: 1,
    textAlign: "center",
    fontFamily: Platform.OS === "ios" ? "OpenSans-Regular" : "opensans"
  },

  SectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#E8EBEC",
    borderColor: "#000",
    height: 40,
    borderRadius: 20,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    marginTop: Platform.OS === "ios" ? 5 : 0
  },

  mainConatinerStyle: {
    flexDirection: "column",
    flex: 1
  },
  floatingMenuButtonStyle: {
    alignSelf: "flex-end",
    position: "absolute",
    bottom: 35
  },
  addFriend: { color: "#3370FF", marginRight: 3, fontSize: 31 },
  iconBack: { backgroundColor: "#fff" },
  icon: { color: "#616161" }
});
