

import { Body, Button, Container, List, ListItem, Separator, Content} from 'native-base';
import React, { Component } from 'react';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';
import { connect } from "react-redux";
import { Display } from "../../components/BoxComponent";
import CallKit from "../../components/CallKit";
import LevelModal from "../../components/LevelModal";
import { getFriendsRequest, submitBtn, getCompanyRequest, comRequestBtn } from "../../redux/modules/contacts";
import { stat } from 'react-native-fs';

const assetPath = "../../assets/";
const platform = Platform.OS;

class FriendsRequestList extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };

  }

  async componentWillMount() {
    await this.props.getFriendsRequest();
    await this.props.getCompanyRequest();
  }

  render() {
    const friendsRequestArr = this.props.friendsRequestArr;
    const companyRequestArr = this.props.companyRequestArr;
    const acceptedDeletedFlag = this.props.acceptedDeletedFlag;
    const user = this.props.user;
    const incomingFlag = this.props.incomingFlag;
    const showTimerFlag = this.props.showTimerFlag;

    return (
      <Container>
        <Content>

        {/* {acceptedDeletedFlag? (
            <Button info style = {{padding: '10%', alignSelf: 'center'}} onPress={() => {this.inviteSMS(phone)}}>
                <Text>Invite</Text>
            </Button>
        ) :null} */}

          {friendsRequestArr.length == 0 && companyRequestArr.length == 0? (
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 17
            }}>
              <Image
                source={require(`${assetPath}/friends.png`)}
                style={{ backgroundColor: "#FFF", width: 120, height: 120 }} />
              <Text style={styles.textPlaceholder}>No pending friends request</Text>
            </View>

          ) : (
              <View>
                <Display isVisible={friendsRequestArr.length!==0}>
                    <Separator bordered >
                      <Text style={styles.titleSeperator} >Friends request</Text>
                    </Separator>
                  <List dataArray={friendsRequestArr} renderRow={(item) =>
                    <ListItem >
                      <Body style={{ paddingLeft: '1%' }}>
                        <Text style={{ color: '#000' }}>{item.firstName} {item.lastName}</Text>
                      </Body>
                      <View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
                        <Button small info style={{ padding: '5%', marginRight: 10, alignSelf: 'center' }} onPress={() => this.props.submitBtn({ items: item, user: user, status: 'accepted' })}>
                          <Text style={{ color: 'white' }}>Accept</Text>
                        </Button>
                        <Button small style={{ borderWidth: 1, backgroundColor: '#fff', padding: '5%', alignSelf: 'center' }} onPress={() => this.props.submitBtn({ items: item, user: user, status: 'deleted' })}>
                          <Text style={{ color: '#000' }}>Delete</Text>
                        </Button>
                      </View>
                    </ListItem>
                  }>
                  </List>
                </Display>

                <Display isVisible={companyRequestArr.length!==0}>
                    <Separator bordered >
                      <Text style={styles.titleSeperator} >Company request</Text>
                    </Separator>

                    <List dataArray={companyRequestArr} renderRow={(item) =>
                      <ListItem >
                        <Body style={{ paddingLeft: '1%' }}>
                          <Text style={{ color: '#000' }}>{item.companyName}</Text>
                        </Body>
                        <View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
                          <Button small info style={{ padding: '5%', marginRight: 10, alignSelf: 'center' }} onPress={() => this.props.comRequestBtn({ items: item, status: true })}>
                            <Text style={{ color: 'white' }}>Accept</Text>
                          </Button>
                          <Button small style={{ borderWidth: 1, backgroundColor: '#fff', padding: '5%', alignSelf: 'center' }} onPress={() => this.props.comRequestBtn({ items: item, status: "delete" })}>
                            <Text style={{ color: '#000' }}>Delete</Text>
                          </Button>
                        </View>
                      </ListItem>
                    }>
                    </List>
                </Display>
              </View>
          )}

        <Display isVisible={incomingFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={incomingFlag} >
            <CallKit type={'incoming'} />
          </LevelModal>
        </Display>

        <Display isVisible={showTimerFlag}>
          <LevelModal onBackdropPress={() => console.log("Pressed")} visible={showTimerFlag} >
            <CallKit type={'timer'} />
          </LevelModal>
        </Display>
        </Content>

      </Container>
    );
  }

}


export default connect(
  state => ({
    user: state.auth.user,
    friendsRequestArr: state.contacts.friendsRequestArr,
    acceptedDeletedFlag: state.contacts.acceptedDeletedFlag,
    incomingFlag: state.contacts.incomingFlag,
    showTimerFlag: state.contacts.showTimerFlag,
    companyRequestArr: state.contacts.companyRequestArr
  }),
  { getFriendsRequest, submitBtn, getCompanyRequest, comRequestBtn}
)(FriendsRequestList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

  TextInputStyleClass: {
    textAlign: 'center',
    height: 40,
    borderWidth: 1,
    borderColor: '#009688',
    borderRadius: 7,
    backgroundColor: "#FFFFFF"
  },
  textPlaceholder: {
    color: '#000',
    fontFamily: platform === "ios" ? "OpenSans-Regular" : "opensans",
    padding: 30
  },
  titleSeperator: {
    paddingBottom: platform === "ios" ? 17 : 0,
  }
})



