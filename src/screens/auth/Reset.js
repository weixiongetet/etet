import React, { Component } from "react";
import firebase from "react-native-firebase";
import ResetReduxForm from "../../containers/form/ResetReduxForm";
import { Container } from "../../components/Container";

class Reset extends Component {
  constructor(props) {
    super(props);
  }

  handleReset = async ({ email }) => {
    return await firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(user => {
        setTimeout(() => {
          this.props.navigation.goBack(null);
        }, 2000);
        return true;
      })
      .catch(function(err) {
        alert(err);
      });
  };

  render() {
    return (
      <Container>
        <ResetReduxForm onSubmit={this.handleReset} />
      </Container>
    );
  }
}

export default Reset;
