import AsyncStorage from "@react-native-community/async-storage";
import { Button, Content, Text } from "native-base";
import React, { Component } from "react";
import {
  Image,
  KeyboardAvoidingView,
  Linking,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import firebase from "react-native-firebase";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import SendBird from "sendbird";
import SignUpReduxForm from "../../containers/form/SignUpReduxForm";
import { sendbirdLogin } from "../../redux/modules/chat";
import { Voximplant } from "react-native-voximplant";
import LoginManager from "../../manager/LoginManager";

const assetPath = "../../assets/";

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      loading: false,
      voximplant_accID: "3251266",
      voximplant_Api: "9f4a32b4-d8ec-46b7-9925-c8332d1b12d5",
      voxiNormal_AppID: "4309087", //normal call
      voxiConf_AppID: "4311407", //call conference
      client: null
    };

    this.signupSubmit = this.signupSubmit.bind(this);
  }

  async componentDidMount() {
    await this.setState({
      client: Voximplant.getInstance()
    });
  }

  async componentWillMount() {
    // this.setState({ loading: true });
  }

  async componentWillUnmount() {
    // this.unsubsribe();
  }

  sendBirdSubscribe = async user => {
    const deviceToken = await AsyncStorage.getItem("fcmToken");

    return new Promise((resolve, reject) => {
      let sb = SendBird.getInstance();
      if (!sb || (sb && sb.connecting === false)) {
        this.props.sendbirdLogin(user).then(() => {
          sb = SendBird.getInstance();
          if (Platform.OS === "ios") {
            // WARNING! FCM token doesn't work in request to APNs.
            // Use APNs token here instead.
            firebase
              .messaging()
              .ios.getAPNSToken()
              .then(token => {
                if (token) {
                  sb.registerAPNSPushTokenForCurrentUser(
                    token,
                    (response, error) => {
                      if (error) reject(error);
                      console.log(response);
                      sb.disconnect(() => {
                        resolve(
                          `APNS success register sendbird FCM with ${token}`
                        );
                      });
                    }
                  );
                } else {
                  resolve();
                }
              })
              .catch(error => {
                reject(error);
              });
          } else {
            sb.registerGCMPushTokenForCurrentUser(
              deviceToken,
              (response, error) => {
                if (error) reject(error);
                console.log(response);
                sb.disconnect(() => {
                  resolve(
                    `Android success register sendbird FCM with ${deviceToken}`
                  );
                });
              }
            );
          }
        });
      }
    });
  };

  signupSubmit = async ({ profile, email, password }) => {
    var names = profile.split(" ");

    await this.setState({
      loading: true
    });
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password);
    } catch (err) {
      alert(err);
      this.setState({
        loading: false
      });
      return;
    }
    try {
      const {
        voximplant_accID,
        voximplant_Api,
        voxiNormal_AppID,
        voxiConf_AppID
      } = this.state;
      const { _user } = firebase.auth().currentUser;
      const voximName = "12345" + _user.uid;
      const voximDisplayName = names[0];
      const voximPassword = _user.uid; //use user id as Voximplant login password

      const responseCof = await fetch(
        "https://api.voximplant.com/platform_api/AddUser/?account_id=" +
          voximplant_accID +
          "&api_key=" +
          voximplant_Api +
          "&application_id=" +
          voxiConf_AppID +
          "&user_name=" +
          voximName +
          "&user_display_name=" +
          voximDisplayName +
          "&user_password=" +
          voximPassword
      );

      const jsonConference = await responseCof.json();
      console.log("voximplam Conference add user: ", jsonConference);

      await firebase.auth().signInWithEmailAndPassword(email, password);
      await AsyncStorage.setItem("tiePhone", "yes");
      await firebase
        .firestore()
        .collection("users")
        .doc(_user.uid)
        .set({
          userID: _user.uid,
          etetID: _user.email.split("@")[0],
          fcmToken: await AsyncStorage.getItem("fcmToken"),
          email: _user.email,
          profilePictureURL:
            "https://png.pngtree.com/svg/20170920/ico_avatar_1189275.png",
          plan: "free",
          firstName: names[0],
          lastName: names.length > 1 ? names[1] : "",
          groups: {}
        });
      const ref = firebase
        .firestore()
        .collection("users")
        .doc(_user.uid);
      const user = await ref.get();
      const sendBirdNotify = await this.sendBirdSubscribe(user.data());

      //voximplant call
      await LoginManager.getInstance().loginWithPassword(
        "12345" + user.data().userID + "@etetconf.etetsg.voximplant.com",
        user.data().userID
      );
      //await this.requestToken();
      // normal call
      // await this.loginWithPassword(user.data().userID + '@etet.etetsg.voximplant.com', user.data().userID);
      console.log(sendBirdNotify);
    } catch (err) {
      alert(err);
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  render = () => {
    return (
      <KeyboardAvoidingView style={styles.container} enabled>
        <ScrollView>
          {/* Logo */}
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              resizeMode="cover"
              source={require(`${assetPath}/logo.png`)}
            />
          </View>
          {/* Title */}
          <View style={styles.welcomeContainer}>
            <Content padder>
              <Text style={styles.welcomeText}>Welcome,</Text>
              <Text style={styles.welcomeText2}>Create an account</Text>
            </Content>
          </View>
          {/* Form */}
          <View style={styles.signupForm}>
            <SignUpReduxForm
              onSubmit={this.signupSubmit}
              loading={this.state.loading}
            />

            <View style={styles.disclaimerText}>
              <Content>
                <Text style={styles.textCenter}>
                  By tapping "Sign up", you agree to the
                </Text>
              </Content>

              <View style={styles.tncContainer}>
                <TouchableOpacity
                  transparent
                  onPress={() => {
                    Linking.openURL("https://www.etet.app/terms");
                  }}
                >
                  <Text style={[styles.textCenter, styles.termsText]}>
                    Terms & Conditions
                  </Text>
                </TouchableOpacity>

                <Text>{` & `}</Text>

                <TouchableOpacity
                  transparent
                  onPress={() => {
                    Linking.openURL("https://www.etet.app/privacy-policy");
                  }}
                >
                  <Text style={[styles.textCenter, styles.termsText]}>
                    Privacy Policies
                  </Text>
                </TouchableOpacity>
              </View>

              <Button
                transparent
                full
                style={styles.loginButton}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "Login"
                  })
                }
              >
                <Text style={styles.loginText}>Have an account? Login</Text>
              </Button>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  };
}

export default connect(
  state => {
    return {
      verifyId: state.auth.verifyId,
      verifyFail: state.auth.verifyFail
    };
  },
  { sendbirdLogin }
)(Signup);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row"
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20
  },
  tncContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 180,
    height: 60
  },
  welcomeContainer: {
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  welcomeText: {
    marginTop: 10,
    fontFamily: "OpenSans-Regular",
    fontSize: 20,
    fontWeight: "500"
  },
  welcomeText2: {
    marginTop: 5,
    fontFamily: "OpenSans-Regular",
    fontSize: 16,
    fontWeight: "300",
    color: "gray"
  },
  signupForm: {
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  termsContainer: {
    flex: 1,
    marginTop: 15,
    flexDirection: "column"
  },
  termsText: {
    color: "gray",
    fontSize: 15
  },
  disclaimerText: {
    marginTop: 15
  },
  textCenter: {
    textAlign: "center"
  },
  backtoLoginButton: {
    alignSelf: "center",
    marginBottom: 20
  },
  loginButton: {
    marginTop: 10
  },
  loginText: { color: "black" }
});
