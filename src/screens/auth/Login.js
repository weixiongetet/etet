import AsyncStorage from "@react-native-community/async-storage";
import { Button, Content, Text } from "native-base";
import React, { Component } from "react";
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  View
} from "react-native";
import firebase from "react-native-firebase";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import SendBird from "sendbird";
import request from "superagent";
import { Display } from "../../components/BoxComponent";
import LoginReduxForm from "../../containers/form/LoginReduxForm";
import { sendbirdLogin, sendbirdLogout } from "../../redux/modules/chat";
import { checkRemainingStorage } from "../../redux/modules/setting";
import { Voximplant } from "react-native-voximplant";
import LoginManager from "../../manager/LoginManager";

const assetPath = "../../assets/";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      formInit: false
    };
  }

  async componentDidMount() {
    setTimeout(() => {
      this.setState({
        formInit: true
      });
    }, 500);

    LoginManager.getInstance().on("onConnectionFailed", reason =>
      this.onConnectionFailed(reason)
    );
    LoginManager.getInstance().on("onLoggedIn", displayName =>
      this.onLoggedIn(displayName)
    );
    LoginManager.getInstance().on("onLoginFailed", errorCode =>
      this.onLoginFailed(errorCode)
    );
  }

  onLoggedIn(displayName) {
    (async () => {
      // await AsyncStorage.setItem("usernameValue", this.state.username);
    })();
  }

  onConnectionFailed(reason) {
    (async () => {
      // await AsyncStorage.setItem("usernameValue", this.state.username);
    })();
  }

  onLoginFailed(errorCode) {
    (async () => {
      // await AsyncStorage.setItem("usernameValue", this.state.username);
    })();
  }
  _connectionClosed = () => {
    // this.props.navigation.navigate('Login');
  };

  loginSubmit = async values => {
    await this.setState({
      loading: true
    });
    try {
      await firebase
        .auth()
        .signInWithEmailAndPassword(values.email, values.password);
      const { _user } = firebase.auth().currentUser;
      const ref = firebase
        .firestore()
        .collection("users")
        .doc(_user.uid);

      ref.update({
        fcmToken: await AsyncStorage.getItem("fcmToken")
      });
      const user = await ref.get();

      this.props.checkRemainingStorage();
      const sendBirdNotify = await this.sendBirdSubscribe(user.data());

      //voximplant call
      let loginVoxAcc = "12345" + user.data().userID;
      console.log("startloginvoxim");
      await LoginManager.getInstance().loginWithPassword(
        loginVoxAcc + "@etetconf.etetsg.voximplant.com",
        user.data().userID
      );
    } catch (err) {
      alert(err);
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  sendBirdSubscribe = async user => {
    const deviceToken = await AsyncStorage.getItem("fcmToken");

    return new Promise((resolve, reject) => {
      let sb = SendBird.getInstance();
      if (!sb || (sb && sb.connecting === false)) {
        this.props.sendbirdLogin(user).then(() => {
          sb = SendBird.getInstance();

          if (Platform.OS === "ios") {
            // WARNING! FCM token doesn't work in request to APNs.
            // Use APNs token here instead.
            firebase
              .messaging()
              .ios.getAPNSToken()
              .then(token => {
                if (token) {
                  sb.registerAPNSPushTokenForCurrentUser(
                    token,
                    (response, error) => {
                      if (error) reject(error);
                      console.log(response);
                      sb.disconnect(() => {
                        resolve(
                          `APNS success register sendbird FCM with ${token}`
                        );
                      });
                    }
                  );
                } else {
                  resolve();
                }
              })
              .catch(error => {
                reject(error);
              });
          } else {
            sb.registerGCMPushTokenForCurrentUser(
              deviceToken,
              (response, error) => {
                if (error) reject(error);
                console.log(response);
                sb.disconnect(() => {
                  resolve(
                    `Android success register sendbird FCM with ${deviceToken}`
                  );
                });
              }
            );
          }
        });
      }
    });
  };

  render = () => {
    return (
      <KeyboardAvoidingView style={styles.container} enabled>
        <ScrollView>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              resizeMode="cover"
              source={require(`${assetPath}/logo.png`)}
            />
          </View>

          <Display isVisible={!this.state.formInit}>
            <Content padder>
              <ActivityIndicator size="large" color="#3370FF" />
            </Content>
          </Display>

          <Display isVisible={this.state.formInit}>
            <View style={styles.loginForm}>
              <Content padder>
                <Text style={styles.title}>Welcome,</Text>
                <Text style={styles.description}>Sign in to continue</Text>
              </Content>

              <LoginReduxForm
                onSubmit={this.loginSubmit.bind(this)}
                isFetching={this.state.loading}
              />

              <Button
                transparent
                style={styles.resetButton}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "Reset"
                  })
                }
              >
                <Text style={{ color: "gray", fontSize: 15 }}>
                  Forgot Password?
                </Text>
              </Button>

              <Button
                transparent
                full
                style={styles.signupButton}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "Signup"
                  })
                }
              >
                <Text style={styles.signupText}>
                  Don't have an account? Sign Up
                </Text>
              </Button>
            </View>
          </Display>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  };
}

export default connect(
  state => ({
    user: state.auth.user
  }),
  { sendbirdLogin, sendbirdLogout, checkRemainingStorage }
)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row"
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 180,
    height: 60
  },
  welcomeContainer: {
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  welcomeText: {
    marginTop: 10,
    // fontFamily: "OpenSans-Regular",
    fontSize: 20,
    fontWeight: "500"
  },
  welcomeText2: {
    marginTop: 5,
    // fontFamily: "OpenSans-Regular",
    fontSize: 16,
    fontWeight: "300",
    color: "gray"
  },
  loginForm: {
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  resetButton: {
    alignSelf: "flex-end"
  },
  signupButton: {
    marginTop: 10
  },
  title: {
    marginTop: 10,
    // fontFamily: "OpenSans-Regular",
    fontSize: 20,
    fontWeight: "500"
  },
  description: {
    marginTop: 5,
    // fontFamily: "OpenSans-Regular",
    fontSize: 16,
    fontWeight: "300",
    color: "gray"
  },
  signupText: { color: "black" }
});
