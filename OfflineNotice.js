import React, { PureComponent } from "react";
import { Dimensions, NetInfo, Platform, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import { updConnnectInfo } from "./src/redux/modules/setting";

const { width } = Dimensions.get("window");

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>Oops, no internet connection</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {
  state = {
    connectionInfo: true
  };

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      this.props.updConnnectInfo(isConnected);
      setTimeout(() => {
        this.props.updConnnectInfo("default");
      }, 1000);
    });
  }

  render() {
    if (this.props.connectInfo == "default") {
      return null;
    } else if (!this.props.connectInfo) {
      return (
        <View style={styles.offlineContainer}>
          <Text style={styles.offlineText}>Oops, no internet connection</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.onlineContainer}>
          <Text style={styles.offlineText}>Internet connected</Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: "#af4448",
    height: 20,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    width,
    position: "absolute",
    bottom: 0,
    paddingTop: Platform.OS === "ios" ? 3 : 0,
  },
  onlineContainer: {
    backgroundColor: "#5cb85c",
    height: 20,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    width,
    position: "absolute",
    bottom: 0,
    paddingTop: Platform.OS === "ios" ? 3 : 0,
  },

  offlineText: { color: "#fafafa" }
});


const mapStateToProps = state => ({
  connectInfo: state.setting.connectInfo
});

export default connect(
  mapStateToProps,
  { updConnnectInfo }
)(OfflineNotice);
