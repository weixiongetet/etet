import React from "react";
import { View } from "react-native";

const styles = {
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
};

export const Center = props => {
  return <View style={styles.center}>{props.children}</View>;
};
