import React from "react";
import { storiesOf } from "@storybook/react-native";
import IconWithBadge from "../../src/components/IconWithBadge";

storiesOf("Badge Icon", module)
  .add("Without count", () => (
    <IconWithBadge name={"md-refresh"} color="orange" size={40} />
  ))
  .add("With count", () => (
    <IconWithBadge
      name={"md-refresh"}
      size={40}
      color="orange"
      badgeCount={5}
    />
  ));
