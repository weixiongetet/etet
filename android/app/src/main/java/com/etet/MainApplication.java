package com.etet;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.wix.reactnativenotifications.RNNotificationsPackage;
import com.voximplant.reactnative.VoxImplantReactPackage;
import com.wenkesj.voice.VoicePackage;
import com.voximplant.foregroundservice.VIForegroundServicePackage;
import de.innfactory.apiai.RNApiAiPackage;
import com.rnfs.RNFSPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import com.tavernari.volumecontroller.ReactNativeVolumeControllerPackage;
import com.rtmalone.volumecontrol.RNVolumeControlPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.horcrux.svg.SvgPackage;
import cl.json.RNSharePackage;
import com.artirigo.fileprovider.RNFileProviderPackage;
import com.tkporter.sendsms.SendSMSPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.RNFetchBlob.RNFetchBlobPackage;
import net.wowmaking.RNImageToolsPackage;
import com.filepicker.FilePickerPackage;
import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.hoxfon.react.RNTwilioVoice.TwilioVoicePackage;

import com.imagepicker.ImagePickerPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.lewin.qrcode.QRScanReaderPackage;
import org.reactnative.camera.RNCameraPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage; // <-- Add this line
import io.invertase.firebase.database.RNFirebaseDatabasePackage; // <-- Add this line
import io.invertase.firebase.firestore.RNFirebaseFirestorePackage; // <-- Add this line
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; // <-- Add this line
import io.invertase.firebase.storage.RNFirebaseStoragePackage; // <-- Add this line
import io.invertase.firebase.instanceid.RNFirebaseInstanceIdPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage; // <-- Add this line
import io.invertase.firebase.functions.RNFirebaseFunctionsPackage; // <-- Add this line
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage; // <-- Add this line

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNNotificationsPackage(),
            new VoxImplantReactPackage(),
            new VoicePackage(),
            new VIForegroundServicePackage(),
            new RNApiAiPackage(),
            new RNFSPackage(),
            new RNFileViewerPackage(),
            new ReactNativeVolumeControllerPackage(),
            new RNVolumeControlPackage(),
            new LinearGradientPackage(),
            new ReactSliderPackage(),
            new AsyncStoragePackage(),
            new SvgPackage(),
            new RNSharePackage(),
            new RNFileProviderPackage(),
            SendSMSPackage.getInstance(),
            new ReactNativeContacts(),
            new RNFetchBlobPackage(),
            new RNImageToolsPackage(),
            new FilePickerPackage(),
            new ReactNativeDocumentPicker(),
            new TwilioVoicePackage(),
            new ImagePickerPackage(),
            new FastImageViewPackage(),
            new QRScanReaderPackage(),
            new RNCameraPackage(),
            new VectorIconsPackage(),
            new RNGestureHandlerPackage(),
            new RNFirebasePackage(),
            new RNFirebaseAuthPackage(),
            new RNFirebaseDatabasePackage(), // <-- Add this line
            new RNFirebaseFirestorePackage(), // <-- Add this line
            new RNFirebaseMessagingPackage(), // <-- Add this line
            new RNFirebaseStoragePackage(), // <-- Add this line
            new RNFirebaseInstanceIdPackage(), // <-- Add this line
            new RNFirebaseNotificationsPackage(), // <-- Add this line
            new RNFirebaseFunctionsPackage(),// <-- Add this line
            new RNFirebaseCrashlyticsPackage() // <-- Add this line
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    
  }
}
