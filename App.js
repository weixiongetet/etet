import AsyncStorage from "@react-native-community/async-storage";
//For Nativebase
import { StyleProvider } from "native-base";
import React, { Component } from "react";
import { AcitivityIndicator, AppRegistry, NetInfo, Platform, View } from "react-native";
import firebase from "react-native-firebase";
import { connect, Provider } from "react-redux";
//For Redux
import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { persistReducer, persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import promise from "redux-promise-middleware";
import thunk from "redux-thunk";
import SendBird from "sendbird";
import { name as appName } from "./app.json";
import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/variables";
import OfflineNotice from "./OfflineNotice";
import bgMessaging from "./src/bgmessaging";
import { AppWithNavigationState, middleware } from "./src/containers/AppNavigator";
import LoginManager from "./src/manager/LoginManager";
import reducers from "./src/redux/configureStore";
import { newNotification, setNotification } from "./src/redux/modules/notify";
import { updConnnectInfo } from "./src/redux/modules/setting";
import NavigationService from "./src/routes/NavigationService";

const persistConfig = {
  key: "abc",
  storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(
  persistedReducer,
  applyMiddleware(middleware, thunk, promise, logger)
);

const persistor = persistStore(store);
console.disableYellowBox = true;

class NotificationListener extends Component {
  constructor(props) {
    super(props);
  }

  renderLoading() {
    <View>
      <AcitivityIndicator size="large" />
    </View>;
  }

  async componentWillMount() {
    console.log("componentwilmoutnaopjs")

    // await AsyncStorage.setItem("tiePhone", "no");
    let walkthrough = await AsyncStorage.getItem("walkthrough");

    if (walkthrough == null) {
      await AsyncStorage.setItem("walkthrough", "yes");
    } else {
      await AsyncStorage.setItem("walkthrough", "no");
    }
    // let checkSync = await AsyncStorage.getItem("checkSync");
    // if (!checkSync) {
    //   await AsyncStorage.setItem("checkSync", "yes");
    // } else {
    //   await AsyncStorage.setItem("checkSync", "no");
    // }

    const permission = await firebase.messaging().hasPermission();

    const existingFCM = await AsyncStorage.getItem("fcmToken");
    if (!existingFCM) {
      let newToken = await firebase.messaging().getToken();
      AsyncStorage.setItem("fcmToken", newToken);
    }

    if (!permission) {
      await firebase.messaging().requestPermission();
      return;
    }

    if (permission) {

      if (Platform.OS === "ios") {
        firebase
          .messaging()
          .ios.registerForRemoteNotifications()
          .then(async () => {
            firebase
              .messaging()
              .getToken()
              .then(gcm => {
                firebase
                  .messaging()
                  .ios.getAPNSToken()
                  .then(async token => {
                    AsyncStorage.setItem("fcmToken", token);
                    var user = firebase.auth().currentUser;
                    if (user) {
                      console.log("userrrtoken", token)
                      console.log("iosusergcm", gcm)

                      firebase
                        .firestore()
                        .collection("users")
                        .doc(user.uid)
                        .update({
                          fcmToken: token,
                          gcmToken: gcm
                        });
                    }

                    // if (!sbSend || (sbSend && sbSend.connecting === false)) {
                    //   this.requestSendBirdloginWIthIOSSendbirdRegister();
                    // } else {
                    //   await this.RegisterIosSendBirdlogin(token);
                    // }
                  });
              });
          });
      } else { //android
        this.fcmListener = firebase.messaging().onTokenRefresh(async () => {
          let newToken = await firebase.messaging().getToken();
          AsyncStorage.setItem("fcmToken", newToken);

          // check if login, auto update
          var user = firebase.auth().currentUser;
          if (user) {
            firebase
              .firestore()
              .collection("users")
              .doc(user.uid)
              .update({
                fcmToken: newToken
              });
          }
        });

      }
    }

    const sb = SendBird.getInstance();
    if (sb && sb.connecting === true) {

      console.log("apppjs")


      sb.disconnect(function () {
        console.log("sbdisconnnect")

        // disconnect to ensure getting notification from sendbird server, login user wont get notification
      });
    } else {
      console.log("diconnectsbbbb")

      // console.log("sign in sendbird now1111");
      // await this.props.sendbirdLogin(this.props.user);
      // sb = SendBird.getInstance();
    }

    this.createNotificationListeners(); //add this line

    //add notification badge
    // await this.props.loadChannels();

    // var ChannelHandler = new sb.ChannelHandler();
    // // for sendbird notification in foreground and exactly in chat tab
    // ChannelHandler.onMessageReceived = (channel, message) => {
    //   this.props.loadChannels();
    //   this.props.addNotification(-99);

    //   if (this.props.channel && this.props.channel.url === channel.url) {
    //     return;
    //   }
    // };

    // sb.addChannelHandler(`CHANNEL_LISTENER`, ChannelHandler);
  }

  componentWillUnmount() {
    if (this.notificationListener) this.notificationListener();
    if (this.notificationOpenedListener) this.notificationOpenedListener();
    if (this.messageListener) this.messageListener();

    if (this.fcmListener) this.fcmListener();
  }

  componentDidMount() {

    console.log("componentdidmountappjs")
    // this.createNotificationListeners();

    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = () => {
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log("checkonfofffline", isConnected)
      if (!isConnected) {//offline

      } else { //online
        const { _user } = firebase.auth().currentUser;
        if (_user.uid) {
          this.loginVoximCall(_user);
        }
      }

      this.props.updConnnectInfo(isConnected);
    });

    setTimeout(() => {
      this.props.updConnnectInfo("default");
    }, 1000);

    // NetInfo.isConnected.removeEventListener(
    //   'connectionChange',
    //   this.handleFirstConnectivityChange
    // );
  };

  async loginVoximCall(user) {
    if (user.uid) {
      //login voximplant
      console.log("handleappstateVoximCall", user);

      const username = await AsyncStorage.getItem("usernameValue");
      const accessToken = await AsyncStorage.getItem("accessToken");

      if (username !== null && accessToken !== null) {
        //login voximplant with token
        LoginManager.getInstance().loginWithToken();
      } else {
        //login voximplant with password
        LoginManager.getInstance().loginWithPassword(
          "12345" + user.uid + "@etetconf.etetsg.voximplant.com",
          user.uid
        );
      }
    }
  }



  async createNotificationListeners() {
    console.log("apppjssssssssssssss")

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        try {
          console.log("notifacaitionioopen", notificationOpen)

          if (notificationOpen.notification.data.sendbird) {
            let paramSender = notificationOpen.notification.data.sendbird.sender;
            let channelUrl = notificationOpen.notification.data.sendbird.channel.channel_url;

            NavigationService.navigate("Chat", {
              channelUrl: channelUrl,
              userID: paramSender.id,
              name: paramSender.name,
              profileUrl: paramSender.profile_url
            });

          } else if (notificationOpen.notification.data.module && notificationOpen.notification.data.module == 'vault') {
            NavigationService.navigate("Vault");
          } else if (notificationOpen.notification.data.module && notificationOpen.notification.data.module == 'Request') {
            NavigationService.navigate("FriendsRequestList");
          }

        } catch (ex) {
          console.log("eror from linking1", ex);
        }
      });


    /*
// * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
// * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification()
      .then(async (notificationOpen: NotificationOpen) => {
        try {
          if (notificationOpen) {
            // await this.requestToken();

            if (Platform.OS === "ios") {
              firebase
                .messaging()
                .ios.registerForRemoteNotifications()
                .then(async () => {
                  firebase
                    .messaging()
                    .getToken()
                    .then(gcm => {
                      firebase
                        .messaging()
                        .ios.getAPNSToken()
                        .then(async token => {
                          AsyncStorage.setItem("fcmToken", token);
                          var user = firebase.auth().currentUser;
                          if (user) {
                            firebase
                              .firestore()
                              .collection("users")
                              .doc(user.uid)
                              .update({
                                fcmToken: token,
                                gcmToken: gcm
                              });
                          }


                        });
                    });
                });
            }

            // try {
            if (notificationOpen.notification.data.sendbird) {
              console.log("opendsenbrid", notificationOpen.notification.data.sendbird)
              let paramSender = notificationOpen.notification.data.sendbird.sender;

              NavigationService.navigate("Chat", {
                userID: paramSender.id,
                name: paramSender.name,
                profileUrl: paramSender.profile_url
              });

            } else if (notificationOpen.notification.data.module && notificationOpen.notification.data.module == 'Request') {
              NavigationService.navigate("FriendsRequestList", {});
            } else if (notificationOpen.notification.data.module && notificationOpen.notification.data.module == 'vault') {
              NavigationService.navigate("Vault", {});
            }
            //   } else {
            //     let url = "";
            //     let uid = "";

            //     if (Platform.OS === "ios") {
            //       uid =
            //         notificationOpen.notification._data.sendbird.channel
            //           .channel_url;
            //     } else {
            //       uid = notificationOpen.notification._data.channel.channel_url;
            //     }

            //     let displayingChannels = _.filter(
            //       this.props.channels,
            //       channel => {
            //         return channel.lastMessage.channelUrl === uid;
            //       }
            //     );

            //     let profileUrl = displayingChannels[0].coverUrl;
            //     let name = displayingChannels[0].name;

            //     if (displayingChannels[0].name === "oto") {
            //       profileUrl = this.displayOppImage(displayingChannels[0].data);
            //       name = this.displayOppName(displayingChannels[0].data);
            //     }

            //     this.props.selectChannel(displayingChannels[0]);

            //     NavigationService.navigate("Message", {
            //       name,
            //       profileUrl
            //     });
            //   }

          }
        } catch (ex) {
          console.log("eror from linking", ex);
        }
      });






    this.messageListener = firebase.messaging().onMessage(message => {
      console.log("messaging received", message);

      const { title, text, module } = message.data;

      if (!message.data.sendbird) {
        const localNotification = new firebase.notifications.Notification({
          show_in_foreground: true
        }).android
          .setChannelId("notify-channel")
          .android.setPriority(firebase.notifications.Android.Priority.High)
          .setNotificationId(message.messageId)
          .setTitle(title)
          .setBody(text)
          .setData(message.data);
        this.props.newNotification(module);
        firebase.notifications().displayNotification(localNotification);
      } else {
        console.log("sendbriddddlistener")
        const payload = JSON.parse(message.data.sendbird);
        const localNotification = new firebase.notifications.Notification({
          show_in_foreground: true
        }).android
          .setChannelId("notify-channel")
          .android.setPriority(firebase.notifications.Android.Priority.High)
          .android.setVibrate([300])
          .setNotificationId(message.messageId)
          .setTitle(
            `${payload.sender.name} ${
            payload.channel.name === "oto" ? "" : `@ ${payload.channel.name}`
            }`
          )
          .setBody(payload.message)
          .setData(payload);

        this.props.setNotification("chat", payload.unread_message_count);
        firebase.notifications().displayNotification(localNotification);
      }
    });

    // Build a channel
    const channel = new firebase.notifications.Android.Channel(
      "notify-channel",
      "Notification Channel",
      firebase.notifications.Android.Importance.Max
    ).setDescription("Etet app notify channel");

    // Create the channel
    firebase.notifications().android.createChannel(channel);
  }

  render() {
    return <AppWithNavigationState />;
  }
}

const mapStateToProps = state => ({
  connectInfo: state.setting.connectInfo
});

NotificationListener = connect(mapStateToProps, {
  newNotification,
  setNotification,
  updConnnectInfo
})(NotificationListener);

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StyleProvider style={getTheme(variables)}>
            <NotificationListener />
          </StyleProvider>
        </PersistGate>
        <OfflineNotice />
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => App);

// for sendbird notification in background or closed
AppRegistry.registerHeadlessTask(
  "RNFirebaseBackgroundMessage",
  () => bgMessaging
);
